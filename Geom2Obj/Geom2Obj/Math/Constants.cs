﻿#region Using directives



#endregion

namespace UnexpectedBytes.Math
{
    public static class Constants
    {
        #region Constants
        /// <summary>
        /// The value of PI.
        /// </summary>
        public const double PI = System.Math.PI;
        /// <summary>
        /// The value of (2 * PI).
        /// </summary>
        public const double TwoPI = 2 * System.Math.PI;
        /// <summary>
        /// The value of (PI*PI).
        /// </summary>
        public const double SquaredPI = System.Math.PI * System.Math.PI;
        /// <summary>
        /// The value of PI/2.
        /// </summary>
        public const double HalfPI = System.Math.PI / 2.0;
        /// <summary>
        /// Epsilon, a fairly small value for a single precision floating point
        /// </summary>
        public const float EpsilonF = 4.76837158203125E-7f;
        /// <summary>
        /// Epsilon, a fairly small value for a double precision floating point
        /// </summary>
        public const double EpsilonD = 8.8817841970012523233891E-16;
        #endregion
    }
}
