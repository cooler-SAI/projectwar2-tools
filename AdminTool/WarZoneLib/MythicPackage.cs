﻿// Decompiled with JetBrains decompiler
// Type: WarZoneLib.MythicPackage
// Assembly: WarZoneLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BBB51761-5057-4739-BCA0-7C3872AE637A
// Assembly location: D:\Downloads\WarZoneLib.dll

namespace WarZoneLib
{
  public enum MythicPackage
  {
    ART = 1,
    ART2 = 2,
    ART3 = 3,
    AUDIO = 4,
    DATA = 5,
    INTERFACE = 6,
    MFT = 7,
    PATCH = 8,
    VO_ENGLISH = 9,
    VO_FRENCH = 10, // 0x0000000A
    WARTEST = 11, // 0x0000000B
    WORLD = 12, // 0x0000000C
  }
}
