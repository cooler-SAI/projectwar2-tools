﻿// Decompiled with JetBrains decompiler
// Type: WarZoneLib.FixtureInfo
// Assembly: WarZoneLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BBB51761-5057-4739-BCA0-7C3872AE637A
// Assembly location: D:\Downloads\WarZoneLib.dll

using System.Collections.Generic;

namespace WarZoneLib
{
  public class FixtureInfo
  {
    public List<uint> Triangles = new List<uint>();
    public bool Hidden;
  }
}
