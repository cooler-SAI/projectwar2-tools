﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.AbilityType
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

namespace MYPLib
{
  public enum AbilityType
  {
    FIRST,
    DEFAULT,
    MORALE,
    TACTIC,
    GRANTED,
    PASSIVE,
    PET,
    GUILD,
  }
}
