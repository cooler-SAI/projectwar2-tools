﻿// Decompiled with JetBrains decompiler
// Type: MYPLib.ExtData
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System.IO;

namespace MYPLib
{
  public class ExtData
  {
    public int Val1 { get; set; }

    public int Val2 { get; set; }

    public int Val3 { get; set; }

    public int Val4 { get; set; }

    public int Val5 { get; set; }

    public int Val6 { get; set; }

    public int Val7 { get; set; }

    public int Val8 { get; set; }

    public byte Val9 { get; set; }

    public void Save(Stream stream)
    {
      PacketUtil.WriteUInt32R(stream, (uint) this.Val1);
      PacketUtil.WriteUInt32R(stream, (uint) this.Val2);
      PacketUtil.WriteUInt32R(stream, (uint) this.Val3);
      PacketUtil.WriteUInt32R(stream, (uint) this.Val4);
      PacketUtil.WriteUInt32R(stream, (uint) this.Val5);
      PacketUtil.WriteUInt32R(stream, (uint) this.Val6);
      PacketUtil.WriteUInt32R(stream, (uint) this.Val7);
      PacketUtil.WriteUInt32R(stream, (uint) this.Val8);
      PacketUtil.WriteByte(stream, this.Val9);
    }

    public ExtData()
    {
    }

    public bool isEmpty()
    {
      return this.Val1 == 0 && this.Val2 == 0 && (this.Val3 == 0 && this.Val4 == 0) && (this.Val5 == 0 && this.Val6 == 0 && (this.Val7 == 0 && this.Val8 == 0)) && this.Val9 == (byte) 0;
    }

    public ExtData(ExtData loadFrom)
    {
      this.Val1 = loadFrom.Val1;
      this.Val2 = loadFrom.Val2;
      this.Val3 = loadFrom.Val3;
      this.Val4 = loadFrom.Val4;
      this.Val5 = loadFrom.Val5;
      this.Val6 = loadFrom.Val6;
      this.Val7 = loadFrom.Val7;
      this.Val8 = loadFrom.Val8;
      this.Val9 = loadFrom.Val9;
    }

    public override string ToString()
    {
      return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} {8}", (object) this.Val1.ToString().PadLeft(4, ' '), (object) this.Val2.ToString().PadLeft(4, ' '), (object) this.Val3.ToString().PadLeft(4, ' '), (object) this.Val4.ToString().PadLeft(4, ' '), (object) this.Val5.ToString().PadLeft(4, ' '), (object) this.Val6.ToString().PadLeft(6, ' '), (object) this.Val7.ToString().PadLeft(4, ' '), (object) this.Val8.ToString().PadLeft(4, ' '), (object) this.Val9.ToString().PadLeft(3, ' '));
    }
  }
}
