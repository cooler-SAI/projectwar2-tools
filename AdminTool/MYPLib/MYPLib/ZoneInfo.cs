﻿// Decompiled with JetBrains decompiler
// Type: MypLib.ZoneInfo
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace MypLib
{
  public class ZoneInfo
  {
    private Dictionary<int, ZoneNif> _nifs = new Dictionary<int, ZoneNif>();
    private Dictionary<int, Fixture> _fixtures = new Dictionary<int, Fixture>();
    private TerrainInfo _terrain;
    public int OffsetX;
    public int OffsetY;
    public int ID;

    public TerrainInfo Terrain
    {
      get
      {
        return this._terrain;
      }
      set
      {
        this._terrain = value;
      }
    }

    public Dictionary<int, ZoneNif> Nifs
    {
      get
      {
        return this._nifs;
      }
      set
      {
        this._nifs = value;
      }
    }

    public Dictionary<int, Fixture> Fixtures
    {
      get
      {
        return this._fixtures;
      }
      set
      {
        this._fixtures = value;
      }
    }

    public ZoneInfo()
    {
    }

    public ZoneInfo(
      Stream nifs,
      Stream fixtures,
      Stream sectorData,
      Stream terrainFile,
      Stream offsetFile,
      Stream lightsFile)
    {
      if (terrainFile != null)
        this._terrain = new TerrainInfo(sectorData, terrainFile, offsetFile);
      if (nifs != null)
      {
        foreach (List<string> stringList in new CSV(new StreamReader(nifs).ReadToEnd(), (string) null, false, -1).Lines.Skip<List<string>>(2))
        {
          ZoneNif zoneNif = new ZoneNif(stringList.ToArray());
          this._nifs[zoneNif.ID] = zoneNif;
        }
      }
      if (fixtures == null)
        return;
      foreach (List<string> stringList in new CSV(new StreamReader(fixtures).ReadToEnd(), (string) null, false, -1).Lines.Skip<List<string>>(2))
      {
        Fixture fixture = new Fixture(stringList.ToArray());
        if (this._nifs.ContainsKey(fixture.NifID))
        {
          fixture.Nif = this._nifs[fixture.NifID];
          fixture.FileName = fixture.Nif.FileName;
          this._fixtures[fixture.UniqueID] = fixture;
        }
      }
    }

    private void LoadLights(XmlTextReader reader)
    {
    }

    private void SaveTerrain(BinaryWriter writer)
    {
    }

    private void LoadLights(BinaryReader reader)
    {
    }

    private void SaveLights(BinaryWriter writer)
    {
    }
  }
}
