﻿// Decompiled with JetBrains decompiler
// Type: MypLib.Language
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

namespace MypLib
{
  public enum Language
  {
    english = 1,
    french = 2,
    german = 3,
    italian = 4,
    spanish = 5,
    korean = 6,
    s_chinese = 7,
    t_chinese = 8,
    japanese = 9,
    russian = 10, // 0x0000000A
  }
}
