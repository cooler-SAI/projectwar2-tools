﻿// Decompiled with JetBrains decompiler
// Type: MypLib.WarZone
// Assembly: MYPLib, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: BFCB9AD7-B8ED-4AED-92AA-9C9175D10CF6
// Assembly location: D:\Downloads\MYPLib.dll

using System.Runtime.InteropServices;
using System.Security;

namespace MypLib
{
  public class WarZone
  {
    [SuppressUnmanagedCodeSecurity]
    [DllImport("C:\\Users\\Administrator\\Documents\\Visual Studio 2012\\Projects\\Praag\\WarZone\\Release\\WarZone.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern void InitZones(string path);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("C:\\Users\\Administrator\\Documents\\Visual Studio 2012\\Projects\\Praag\\WarZone\\Release\\WarZone.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern void LoadZone(int zoneID, int triCount);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("C:\\Users\\Administrator\\Documents\\Visual Studio 2012\\Projects\\Praag\\WarZone\\Release\\WarZone.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern int SegmentIntersect(
      int zoneIDA,
      int zoneIDB,
      float originX,
      float originY,
      float originZ,
      float targetX,
      float targetY,
      float targetZ,
      bool terrain,
      bool normalTest,
      int triCount,
      ref OcclusionInfo result);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("C:\\Users\\Administrator\\Documents\\Visual Studio 2012\\Projects\\Praag\\WarZone\\Release\\WarZone.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern bool TerrainIntersect(
      int zoneIDA,
      int zoneIDB,
      float originX,
      float originY,
      float originZ,
      float targetX,
      float targetY,
      float targetZ,
      int triCount,
      ref OcclusionInfo result);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("C:\\Users\\Administrator\\Documents\\Visual Studio 2012\\Projects\\Praag\\WarZone\\Release\\WarZone.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern int GetFixtureCount(int zoneID);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("C:\\Users\\Administrator\\Documents\\Visual Studio 2012\\Projects\\Praag\\WarZone\\Release\\WarZone.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern bool GetFixtureInfo(int zoneID, int index, ref FixtureInfo info);

    [SuppressUnmanagedCodeSecurity]
    [DllImport("C:\\Users\\Administrator\\Documents\\Visual Studio 2012\\Projects\\Praag\\WarZone\\Release\\WarZone.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
    public static extern bool SetFixtureVisible(
      int zoneID,
      uint uniqueID,
      byte instanceID,
      bool visible);
  }
}
