﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.MYPTest
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using MypLib;
using System;
using System.IO;

namespace RorAdmin
{
  public class MYPTest
  {
    public static void VerifySeqData(byte[] data, int size)
    {
      if (data.Length != size)
        throw new Exception(string.Format("Wrong seq data size {0} != {1}", (object) data.Length, (object) size));
      for (int index = 1; index < data.Length; ++index)
      {
        if ((int) data[index] != (int) (byte) index)
          throw new Exception(string.Format("Expected Seq data {0} != {1}", (object) index, (object) data[index]));
      }
      if ((int) data[0] != (int) data[data.Length - 1])
        throw new Exception(string.Format("Expected Seq data first {0} != {1}", (object) data[0], (object) data[data.Length - 1]));
    }

    public static byte[] GenerateSeqData(int size)
    {
      byte[] numArray = new byte[size];
      for (int index = 0; index < size; ++index)
      {
        numArray[index] = (byte) index;
        if (index == size - 1)
          numArray[0] = (byte) index;
      }
      return numArray;
    }

    public static void TestWithReopen(bool compress, string testName, int count)
    {
      MYP myp1 = new MYP(Program.Log, MythicPackage.DEV, "dev.myp", true);
      int count1 = myp1.Assets.Count;
      byte[] data = new byte[5]
      {
        (byte) 4,
        (byte) 3,
        (byte) 2,
        (byte) 1,
        (byte) 0
      };
      for (int index = 0; index < count; ++index)
        myp1.UpdateAsset(testName + index.ToString(), data, compress, false);
      myp1.Save();
      myp1.Dispose();
      MYP myp2 = new MYP(Program.Log, MythicPackage.DEV, "dev.myp", compress);
      for (int index = 0; index < count; ++index)
        myp2.UpdateAsset(testName + index.ToString(), MYPTest.GenerateSeqData((index + 10) * 3), compress, false);
      myp2.Save();
      myp2.Dispose();
      MYP myp3 = new MYP(Program.Log, MythicPackage.DEV, "dev.myp", compress);
      for (int index = 0; index < count; ++index)
      {
        if (index % 2 == 0 && !myp3.Delete(testName + index.ToString()))
          throw new Exception("Unable to delete data");
      }
      myp3.Save();
      myp3.Dispose();
      MYP myp4 = new MYP(Program.Log, MythicPackage.DEV, "dev.myp", compress);
      for (int index = 0; index < count; ++index)
      {
        byte[] assetData = myp4.GetAssetData(testName + index.ToString());
        if ((uint) (index % 2) > 0U)
          MYPTest.VerifySeqData(assetData, (index + 10) * 3);
        else if (assetData != null)
          throw new Exception("Data was not deleted");
      }
    }

    public static void Test(bool compress, string testName, int count)
    {
      MYP myp1 = new MYP(Program.Log, MythicPackage.DEV, "dev.myp", true);
      int count1 = myp1.Assets.Count;
      byte[] data = new byte[5]
      {
        (byte) 4,
        (byte) 3,
        (byte) 2,
        (byte) 1,
        (byte) 0
      };
      for (int index = 0; index < count; ++index)
        myp1.UpdateAsset(testName + index.ToString(), data, compress, false);
      for (int index = 0; index < count; ++index)
        myp1.UpdateAsset(testName + index.ToString(), MYPTest.GenerateSeqData((index + 10) * 3), compress, false);
      myp1.Save();
      myp1.Dispose();
      MYP myp2 = new MYP(Program.Log, MythicPackage.DEV, "dev.myp", compress);
      for (int index = 0; index < count; ++index)
      {
        if (index % 2 == 0 && !myp2.Delete(testName + index.ToString()))
          throw new Exception("Unable to delete data");
      }
      myp2.Save();
      myp2.Dispose();
      MYP myp3 = new MYP(Program.Log, MythicPackage.DEV, "dev.myp", compress);
      for (int index = 0; index < count; ++index)
      {
        byte[] assetData = myp3.GetAssetData(testName + index.ToString());
        if ((uint) (index % 2) > 0U)
          MYPTest.VerifySeqData(assetData, (index + 10) * 3);
        else if (assetData != null)
          throw new Exception("Data was not deleted");
      }
    }

    public static void Test()
    {
      if (File.Exists("dev.myp"))
        File.Delete("dev.myp");
      int num1 = 25;
      for (int index = 1; index < 3; ++index)
      {
        MYPTest.TestWithReopen(true, "compress2001", index * num1);
        MYPTest.TestWithReopen(false, "uncompress2001", index * num1);
        MYPTest.TestWithReopen(false, "1compress2001", index * num1);
      }
      int num2 = 25;
      for (int index = 1; index < 3; ++index)
      {
        MYPTest.Test(true, "compress2001", index * num2);
        MYPTest.Test(false, "uncompress2001", index * num2);
        MYPTest.Test(false, "1compress2001", index * num2);
      }
    }
  }
}
