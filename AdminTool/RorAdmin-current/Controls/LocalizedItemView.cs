﻿// Decompiled with JetBrains decompiler
// Type: RorAdmin.Controls.LocalizedItemView
// Assembly: RorAdmin, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C254147-EF93-452A-8247-C8B7E56B3161
// Assembly location: D:\Current Projects\Londo\RorAdmin-current.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace RorAdmin.Controls
{
  public class LocalizedItemView : UserControl
  {
    private IContainer components = (IContainer) null;
    private Button btnEdit;
    private TextBox txtValue;
    private Label lblDisplayName;

    public event LocalizedItemView.ButtonDelegate OnEdit;

    public string LabelValue
    {
      get
      {
        return this.lblDisplayName.Text;
      }
      set
      {
        this.lblDisplayName.Text = value;
      }
    }

    public string EditValue
    {
      get
      {
        return this.txtValue.Text;
      }
      set
      {
        this.txtValue.Text = value;
      }
    }

    public LocalizedItemView()
    {
      this.InitializeComponent();
    }

    private void btnEdit_Click(object sender, EventArgs e)
    {
      if (this.OnEdit == null)
        return;
      this.OnEdit(this);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.btnEdit = new Button();
      this.txtValue = new TextBox();
      this.lblDisplayName = new Label();
      this.SuspendLayout();
      this.btnEdit.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.btnEdit.Location = new Point(623, 1);
      this.btnEdit.Name = "btnEdit";
      this.btnEdit.Size = new Size(38, 23);
      this.btnEdit.TabIndex = 7;
      this.btnEdit.Text = "Edit";
      this.btnEdit.UseVisualStyleBackColor = true;
      this.btnEdit.Click += new EventHandler(this.btnEdit_Click);
      this.txtValue.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.txtValue.Location = new Point(99, 3);
      this.txtValue.Name = "txtValue";
      this.txtValue.ReadOnly = true;
      this.txtValue.Size = new Size(518, 20);
      this.txtValue.TabIndex = 6;
      this.lblDisplayName.Location = new Point(2, 5);
      this.lblDisplayName.Name = "lblDisplayName";
      this.lblDisplayName.Size = new Size(91, 17);
      this.lblDisplayName.TabIndex = 5;
      this.lblDisplayName.Text = "Name";
      this.lblDisplayName.TextAlign = ContentAlignment.MiddleRight;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.btnEdit);
      this.Controls.Add((Control) this.txtValue);
      this.Controls.Add((Control) this.lblDisplayName);
      this.Name = nameof (LocalizedItemView);
      this.Size = new Size(661, 27);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public delegate void ButtonDelegate(LocalizedItemView sender);
  }
}
