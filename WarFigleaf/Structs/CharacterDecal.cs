﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WarFigleaf.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    public struct CharacterDecal
    {
        public uint A01_Type;
        public uint A02_MaskID;

        public byte A03_AlphaBits;
        public byte A03_TintType;
        public byte A03_Dye1;
        public byte A03_Dye2;

        public int A04_MaskIndex;
        public uint A05;
        public uint A06;
        public uint A07;
        public int A08_DiffuseNormalGlowIndex;
        public uint A09;
        public uint A10;
        public uint A11;
        public int A12_TintSpecularIndex;
        public uint A13;
        public uint A14;
        public uint A15;
    }

    
    public class CharacterDecalView
    {
        public int Index { get; set; }
        public CharacterDecal CharacterDecal;
        public CharacterDecalView(CharacterDecal decal)
        {
            CharacterDecal = decal;
        }

        public uint A01_Type { get => CharacterDecal.A01_Type; set => CharacterDecal.A01_Type = value; }
        public uint A02_MaskID { get => CharacterDecal.A02_MaskID; set => CharacterDecal.A02_MaskID = value; }

        public byte A03_AlphaBits { get => CharacterDecal.A03_AlphaBits; set => CharacterDecal.A03_AlphaBits = value; }
        public byte A03_TintType { get => CharacterDecal.A03_TintType; set => CharacterDecal.A03_TintType = value; }
        public byte A03_Dye1 { get => CharacterDecal.A03_Dye1; set => CharacterDecal.A03_Dye1 = value; }
        public byte A03_Dye2 { get => CharacterDecal.A03_Dye2; set => CharacterDecal.A03_Dye2 = value; }

        public int A04_MaskIndex { get => CharacterDecal.A04_MaskIndex; set => CharacterDecal.A04_MaskIndex = value; }
        public string MaskName { get; set; }
        public uint A05 { get => CharacterDecal.A05; set => CharacterDecal.A05 = value; }
        public uint A06 { get => CharacterDecal.A06; set => CharacterDecal.A06 = value; }
        public uint A07 { get => CharacterDecal.A07; set => CharacterDecal.A07 = value; }
        public int A08_DiffuseNormalGlowIndex { get => CharacterDecal.A08_DiffuseNormalGlowIndex; set => CharacterDecal.A08_DiffuseNormalGlowIndex = value; }
        public string DiffuseNormalGlowName { get; set; }
        public uint A09 { get => CharacterDecal.A09; set => CharacterDecal.A09 = value; }
        public uint A10 { get => CharacterDecal.A01_Type; set => CharacterDecal.A01_Type = value; }
        public uint A11 { get => CharacterDecal.A10; set => CharacterDecal.A10 = value; }
        public int A12_TintSpecularIndex { get => CharacterDecal.A12_TintSpecularIndex; set => CharacterDecal.A12_TintSpecularIndex = value; }
        public string TintSpecularName { get; set; }
        public uint A13 { get => CharacterDecal.A13; set => CharacterDecal.A13 = value; }
        public uint A14 { get => CharacterDecal.A14; set => CharacterDecal.A14 = value; }
        public uint A15 { get => CharacterDecal.A15; set => CharacterDecal.A15 = value; }

    }
}
