﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class FigStringTable2 : FigTable
    {
        public uint Unk1 { get; set; }
        public override List<object> Records => Strings.Select(e => (object)e).ToList();
        public List<FigString2> Strings { get; private set; } = new List<FigString2>();

        protected override void ReadToc()
        {
            TocOffset = (uint)_reader.BaseStream.Position;
            Unk1 = _reader.ReadUInt32();
            EntryCount = _reader.ReadUInt32();
            Offset = _reader.ReadUInt32();
            DataSize = _reader.ReadUInt32();
        }

        public override int RecordSize
        {
            get
            {
                return 0;
            }
        }

        public override void SaveToc(BinaryWriter writer)
        {
            writer.BaseStream.Position = TocOffset;
            writer.Write((uint)Unk1);
            writer.Write((uint)0); //entrycount
            writer.Write((uint)0); //offset
            writer.Write((uint)0); //size
        }

        public override void SaveData(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;


            foreach (var entry in Strings)
            {
                if (!String.IsNullOrEmpty(entry.Value))
                    writer.Write(Encoding.GetEncoding(437).GetBytes(entry.Value), 0, entry.Value.Length);
                writer.Write((byte)0);

            }
            var endPos = writer.BaseStream.Position;
            writer.BaseStream.Position = TocOffset + 4;
            writer.Write((uint)(Strings.Count));
            writer.Write((uint)(pos));
            writer.Write((uint)(endPos - pos));
            writer.BaseStream.Position = endPos;
         
        }

        public FigStringTable2(FigleafDB db, byte[] data, BinaryReader reader) : base(TableType.Strings2, db,data,  reader)
        {
        }

        protected override int LoadInternal()
        {
            _reader.BaseStream.Position = Offset;
            long total = 0;
          
            for (int i = 0; i < EntryCount; i++)
            {
                var offset = _reader.BaseStream.Position;

                var figData = _db.FigData;

                while (figData[offset] != 0)
                {
                    offset++;
                }

                int size = (int)(offset - _reader.BaseStream.Position);
                var str = new FigString2()
                {
                    Index = i,
                    Value = Encoding.GetEncoding(437).GetString(figData, (int)_reader.BaseStream.Position, size),
                };

                Strings.Add(str);
                total += size + 1;
                _reader.BaseStream.Position = offset + 1;
              
            }

            return (int)total;
        }
    }

}
