﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class FigurePartTable : FigTable
    {
        public List<FigurePartsView > Figures;
           public override int RecordSize =>Marshal.SizeOf(typeof(Figure));

        public override List<object> Records => Figures.Select(e => (object)e).ToList();
        public FigurePartTable(FigleafDB db, int recordSize, byte[] data, BinaryReader reader) : base(TableType.FigureParts, db, data, reader)
        {
            if (Marshal.SizeOf(typeof(Figure)) != recordSize)
                throw new Exception("Invalid size for " + nameof(Figure));
        }

        public override void SaveData(BinaryWriter writer)
        {
            base.SaveData(writer);
        }
        //public override void SaveData(BinaryWriter writer)
        //{
        //    var pos = writer.BaseStream.Position;
        //    var recSize = Marshal.SizeOf(typeof(Figure));
        //    var extSize = Marshal.SizeOf(typeof(FigurePart));
        //    var size = Marshal.SizeOf(typeof(Figure)) * (int)Figures.Length;

        //    writer.BaseStream.Position += size;
        //    int last = 0;
        //    int writeCount = 0;

        //    for (int i = 0; i < Figures.Length; i++)
        //    {
        //        int skip = Figures[i].DataStart - last;
        //        int recOffset = (i * recSize);
        //        int newOffset = (int)(size + (writeCount * extSize) ) - ((i) * (recSize)) ;
        //        Figures[i].DataStart = newOffset;


        //        if (i == 0 && !Parts.ContainsKey(i))
        //        {
        //            Figures[i].DataStart = 0;
        //            continue;
        //        }


        //        Figures[i].DataStart = newOffset;
        //        last = newOffset;
        //        if (Parts.ContainsKey(i))
        //        {
        //            Figures[i].Count = (uint)Parts[i].Length;

        //            Figures[i].DataEnd = (int)(writer.BaseStream.Position - pos) + (i * recSize) + (extSize * Parts[i].Length);
        //            for (int c = 0; c < Parts[i].Length; c++)
        //            {
        //                var data = WriteStructs(Parts[i][c]);
        //                writer.Write(data, 0, data.Length);
        //                writeCount++;
        //            }
        //        }
        //    }
        //    var endPos = writer.BaseStream.Position;

        //    //write out primary data
        //    writer.BaseStream.Position = pos;
        //    for (int i = 0; i < Figures.Length; i++)
        //    {
        //        writer.Write(WriteStructs(Figures[i]), 0, recSize);
        //        _db.CompareStreams(new MemoryStream(_db.FigData), writer.BaseStream, (int)pos, (int)(writer.BaseStream.Position - pos));
        //    }

        //    writer.BaseStream.Position = TocOffset;
        //    writer.Write((uint)Figures.Length);
        //    writer.Write((uint)pos);
        //    writer.Write((uint)(endPos - pos));

        //    writer.BaseStream.Position = endPos;

        //    _db.CompareStreams(new MemoryStream(_db.FigData), writer.BaseStream, (int)pos, (int)(endPos - pos));
        //}

        protected override int LoadInternal()
        {
            int recSize = Marshal.SizeOf(typeof(Figure));

            Figures = ReadStructs<Figure>(_fileData, Offset, (int)EntryCount).Select(e => new FigurePartsView(e)).ToList();

            int i = 0;
            Figures.ForEach(e =>
            {
                e.Index = i;
                if (e.SourceIndex < _db.TableStrings1.Strings.Count)
                    e.Name = _db.TableStrings1.Strings[(int)e.SourceIndex].Value;
                else
                    e.Name = "!INVALID_INDEX!";

                i++;
            });

            return RecordSize * (int)EntryCount;
        }



        //protected override bool LoadExtData()
        //{
        //    var recSize = Marshal.SizeOf(typeof(Figure));
        //    var extSize = Marshal.SizeOf(typeof(FigurePart));
        //    var geoSize = Marshal.SizeOf(typeof(GeometryPart));
        //    int priSize = Marshal.SizeOf(typeof(Figure)) * (int)EntryCount;

        //    int total = 0;
        //    for (int i = 0; i < Figures.Length; i++)
        //    {
        //        var figure = Figures[i];

        //        if (figure.Count > 0)
        //        {

        //            total += (int)(extSize*figure.Count);
        //            var offset = Offset + figure.DataStart + (i * recSize);
        //            Parts[i] = ReadStructs<FigurePart>(_tableData, (uint)offset, (int)figure.Count);
        //        }
        //    }

        //    return true;
        //}
    }

   
}
