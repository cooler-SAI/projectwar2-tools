﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace WarFigleaf.Tables
{
    public abstract class FigTable
    {
        public TableType Type { get; protected set; }
        public virtual uint EntryCount { get; protected set; }
        public uint Offset { get; protected set; }
        public uint DataSize { get; protected set; }
        public uint TocOffset { get; protected set; }

        protected FigleafDB _db;
        protected BinaryReader _reader;

        protected byte[] _fileData;
        protected byte[] _tableData;
        public byte[] _extData;

        public abstract int RecordSize { get; }
        public abstract List<Object> Records { get; }

        protected virtual void ReadToc()
        {
            TocOffset = (uint)_reader.BaseStream.Position;
            EntryCount = _reader.ReadUInt32();
            Offset = _reader.ReadUInt32();
            DataSize = _reader.ReadUInt32();

            if (Offset == 0 && EntryCount > 0)
                throw new Exception("Offset is invalid");
        }

        public virtual void SaveToc(BinaryWriter writer)
        {
            writer.BaseStream.Position = TocOffset;
            writer.Write((uint)EntryCount);
            writer.Write((uint)0);
            writer.Write((uint)DataSize);
        }

        public virtual void SaveData(BinaryWriter writer)
        {
            Offset = (uint)writer.BaseStream.Position;
            if (EntryCount > 0)
            {
                writer.Write(_tableData, (int)0, (int)DataSize);
                var endPos = writer.BaseStream.Position;

                writer.BaseStream.Position = TocOffset;
                writer.Write((uint)EntryCount);
                writer.Write((uint)Offset);
                writer.Write((uint)DataSize);

                writer.BaseStream.Position = endPos;
            }
        }

        public FigTable(TableType type, FigleafDB db, byte[] tableData, BinaryReader reader)
        {
            Type = type;
            _db = db;
            _reader = reader;
            _fileData = tableData;
            db.AddTable(this);

            ReadToc();
        }
        protected abstract int LoadInternal();


        public static byte[] WriteStructs(object data)
        {
            int size = Marshal.SizeOf(data);
            byte[] arr = new byte[size];

            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(data, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

        public static T[] ReadStructs<T>(byte[] tableData, uint offset, int count, long maxOffset = 0)
        {
            T[] results = new T[count];

            int size = Marshal.SizeOf(typeof(T)) * (int)count;
            if (size > tableData.Length)
                throw new Exception("Invalid record count");

            if (maxOffset != 0 && offset + size > maxOffset)
                throw new Exception("Read too much");

            var pinHandle = GCHandle.Alloc(results, GCHandleType.Pinned);
            Marshal.Copy(tableData, (int)offset, pinHandle.AddrOfPinnedObject(), size);
            pinHandle.Free();

            return results;
        }

        public void Load()
        {
            if (DataSize > 0)
            {
                _tableData = new byte[DataSize];
                Buffer.BlockCopy(_fileData, (int)Offset, _tableData, 0, (int)DataSize);
            }

            int size = LoadInternal();

            if (size != DataSize)
            {
                if (!LoadExtData())
                    Console.WriteLine("EXT DATA");
            }

        }

        protected virtual bool LoadExtData()
        {
            _reader.BaseStream.Position = Offset + (RecordSize * (int)EntryCount);
            int extSize = (int)(DataSize - (_reader.BaseStream.Position - Offset));
            _extData = new byte[extSize];
            _reader.BaseStream.Read(_extData, 0, extSize);
            return true;

        }
    }
}
