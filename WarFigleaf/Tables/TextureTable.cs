﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class TextureTable : FigTable
    {
        public List<TextureView> Textures;

        public override List<object> Records => Textures.Select(e => (object)e).ToList();
        public override int RecordSize =>  Marshal.SizeOf(typeof(Texture));

        public TextureTable(FigleafDB db, int recordSize, byte[] data, BinaryReader reader) : base(TableType.Textures, db, data, reader)
        {
            if (Marshal.SizeOf(typeof(Texture)) != recordSize)
                throw new Exception("Invalid size for " + nameof(Texture));
        }

        protected override int LoadInternal()
        {
            Textures = ReadStructs<Texture>(_fileData, Offset, (int)EntryCount).Select(e => new TextureView(e)).ToList();

            Textures.ForEach(e =>
            {
                if (e.SourceIndex < _db.TableStrings1.Strings.Count)
                    e.Name = _db.TableStrings1.Strings[(int)e.SourceIndex].Value;
                else
                    e.Name = "!INVALID_INDEX!";
            }
            );

            return RecordSize * (int)EntryCount;
        }


    }
}

