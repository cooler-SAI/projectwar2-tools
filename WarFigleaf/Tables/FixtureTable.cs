﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using WarFigleaf.Structs;

namespace WarFigleaf.Tables
{
    public class FixtureTable : FigTable
    {
        public List<FixtureView> Fixtures;
        public override int RecordSize => Marshal.SizeOf(typeof(Fixture));
        public override List<object> Records => Fixtures.Select(e => (object)e).ToList();

        public FixtureTable(FigleafDB db, int recordSize, byte[] data, BinaryReader reader) : base(TableType.Fixtures, db, data, reader)
        {
            if (Marshal.SizeOf(typeof(Fixture)) != recordSize)
                throw new Exception("Invalid size for " + nameof(Fixture));
        }

        protected override int LoadInternal()
        {
            Fixtures = ReadStructs<Fixture>(_fileData, Offset, (int)EntryCount).Select(e => new FixtureView(e)).ToList();
            int i = 0;
            Fixtures.ForEach(e =>
            {
                e.Index = i;
                e.Name = _db.TableStrings1.Strings[(int)e.SourceIndex].Value;
                if (e.CollisionSourceIndex < _db.TableStrings1.Strings.Count)
                    e.NameCollision = _db.TableStrings1.Strings[(int)e.CollisionSourceIndex].Value;
                else
                    e.NameCollision = "!INVALID_INDEX!";

                i++;
            }
            );
            return RecordSize * (int)EntryCount;
        }
    }
}

