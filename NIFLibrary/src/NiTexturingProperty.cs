/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

namespace NIFLibrary
{
    using System;
    using System.IO;
#if OpenTK
	using OpenTK;
	using OpenTK.Graphics;
	using Matrix = OpenTK.Matrix4;
	using Color3 = OpenTK.Graphics.Color4;
#elif SharpDX
	using SharpDX;
#elif MonoGame
	using Microsoft.Xna.Framework;
	using Color3 = Microsoft.Xna.Framework.Color;
	using Color4 = Microsoft.Xna.Framework.Color;
#else
    using System.Numerics;
    using Matrix = System.Numerics.Matrix4x4;
    using Color3 = System.Numerics.Vector3;
    using Color4 = System.Numerics.Vector4;
#endif

    /// <summary>
    /// Class NiTexturingProperty.
    /// </summary>
    public class NiTexturingProperty : NiProperty
    {
        /// <summary>
        /// The flags
        /// </summary>
        public UInt16 Flags;

        /// <summary>
        /// The apply mode
        /// </summary>
        public UInt32 ApplyMode;

        /// <summary>
        /// The texture count
        /// </summary>
        public UInt32 TextureCount;

        /// <summary>
        /// The base texture
        /// </summary>
        public TexDesc BaseTexture;

        /// <summary>
        /// The dark texture
        /// </summary>
        public TexDesc DarkTexture;

        /// <summary>
        /// The detail texture
        /// </summary>
        public TexDesc DetailTexture;

        /// <summary>
        /// The gloss texture
        /// </summary>
        public TexDesc GlossTexture;

        /// <summary>
        /// The glow texture
        /// </summary>
        public TexDesc GlowTexture;

        /// <summary>
        /// The bump map texture
        /// </summary>
        public TexDesc BumpMapTexture;

        /// <summary>
        /// The decal0 texture
        /// </summary>
        public TexDesc Decal0Texture;

        /// <summary>
        /// The decal1 texture
        /// </summary>
        public TexDesc Decal1Texture;

        /// <summary>
        /// The decal2 texture
        /// </summary>
        public TexDesc Decal2Texture;

        /// <summary>
        /// The decal3 texture
        /// </summary>
        public TexDesc Decal3Texture;

        /// <summary>
        /// The unkown1
        /// </summary>
        public UInt32 Unkown1;

        /// <summary>
        /// The bump map luma scale
        /// </summary>
        public Single BumpMapLumaScale;

        /// <summary>
        /// The bump map luma offset
        /// </summary>
        public Single BumpMapLumaOffset;

        /// <summary>
        /// The bump map matrix
        /// </summary>
        public Vector3 BumpMapMatrix;

        /// <summary>
        /// The number shader textures
        /// </summary>
        public UInt32 NumShaderTextures;

        /// <summary>
        /// Initializes a new instance of the <see cref="NiTexturingProperty"/> class.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="reader">The reader.</param>
        public NiTexturingProperty(NiFile file, BinaryReader reader) : base(file, reader)
        {
            if (base.Version <= eNifVersion.v10_0_1_2 || base.Version >= eNifVersion.v20_1_0_3)
            {
                this.Flags = reader.ReadUInt16();
            }
            if (base.Version <= eNifVersion.v20_0_0_5)
            {
                this.ApplyMode = reader.ReadUInt32();
            }
            this.TextureCount = reader.ReadUInt32();
            if (reader.ReadBoolean(Version))
            {
                this.BaseTexture = new TexDesc(file, reader);
            }
            if (reader.ReadBoolean(Version))
            {
                this.DarkTexture = new TexDesc(file, reader);
            }
            if (reader.ReadBoolean(Version))
            {
                this.DetailTexture = new TexDesc(file, reader);
            }
            if (reader.ReadBoolean(Version))
            {
                this.GlossTexture = new TexDesc(file, reader);
            }
            if (reader.ReadBoolean(Version))
            {
                this.GlowTexture = new TexDesc(file, reader);
            }
            if (reader.ReadBoolean(Version))
            {
                this.BumpMapTexture = new TexDesc(file, reader);
                this.BumpMapLumaScale = reader.ReadSingle();
                this.BumpMapLumaOffset = reader.ReadSingle();
                this.BumpMapMatrix = new Vector3(reader.ReadSingle(), reader.ReadSingle(), reader.ReadSingle());
                reader.ReadSingle();
            }
            if (reader.ReadBoolean(Version))
            {
                this.Decal0Texture = new TexDesc(file, reader);
            }
            if (base.Version >= eNifVersion.v10_0_1_0)
            {
                this.NumShaderTextures = reader.ReadUInt32();
                var num = 0;
                while ((Int64)num < (Int64)(UInt64)this.NumShaderTextures)
                {
                    if (reader.ReadBoolean(Version))
                    {
                        new TexDesc(file, reader);
                        reader.ReadUInt32();
                    }
                    num++;
                }
            }
        }
    }
}
