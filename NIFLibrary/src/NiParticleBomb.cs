/*
 * DAWN OF LIGHT - The first free open source DAoC server emulator
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *
 */

namespace NIFLibrary
{
    using System;
    using System.IO;
#if OpenTK
	using OpenTK;
	using OpenTK.Graphics;
	using Matrix = OpenTK.Matrix4;
	using Color3 = OpenTK.Graphics.Color4;
#elif SharpDX
	using SharpDX;
#elif MonoGame
	using Microsoft.Xna.Framework;
	using Color3 = Microsoft.Xna.Framework.Color;
	using Color4 = Microsoft.Xna.Framework.Color;
#else
    using System.Numerics;
    using Matrix = System.Numerics.Matrix4x4;
    using Color3 = System.Numerics.Vector3;
    using Color4 = System.Numerics.Vector4;
#endif

    /// <summary>
    /// Class NiParticleBomb.
    /// </summary>
    public class NiParticleBomb : NiParticleModifier
    {
        /// <summary>
        /// The decay
        /// </summary>
        public Single Decay;

        /// <summary>
        /// The duration
        /// </summary>
        public Single Duration;

        /// <summary>
        /// The delta v
        /// </summary>
        public Single DeltaV;

        /// <summary>
        /// The start
        /// </summary>
        public Single Start;

        /// <summary>
        /// The decay type
        /// </summary>
        public eDecayType DecayType;

        /// <summary>
        /// The symmetry type
        /// </summary>
        public eSymmetryType SymmetryType;

        /// <summary>
        /// The position
        /// </summary>
        public Vector3 Position;

        /// <summary>
        /// The direction
        /// </summary>
        public Vector3 Direction;

        /// <summary>
        /// Initializes a new instance of the <see cref="NiParticleBomb" /> class.
        /// </summary>
        /// <param name="file">The file.</param>
        /// <param name="reader">The reader.</param>
        public NiParticleBomb(NiFile file, BinaryReader reader) : base(file, reader)
        {
            this.Decay = reader.ReadSingle();
            this.Duration = reader.ReadSingle();
            this.DeltaV = reader.ReadSingle();
            this.Start = reader.ReadSingle();
            this.DecayType = (eDecayType)reader.ReadUInt32();
            if (base.Version >= eNifVersion.v4_1_0_12)
            {
                this.SymmetryType = (eSymmetryType)reader.ReadUInt32();
            }
            this.Position = reader.ReadVector3();
            this.Direction = reader.ReadVector3();
        }
    }
}
