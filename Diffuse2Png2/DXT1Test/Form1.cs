﻿using System;
using System.IO;
using System.Windows.Forms;

namespace DXT1Test
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        private void Form1_Load(Object sender, EventArgs e)
        {
            //var mask = new MythicTexture(File.OpenRead(@"D:\Games\Warhammer Online - Age of Reckoning\myps\art2\assetdb\decals\0.fg.0.0.boss_dwm_thorgrim_body.@.@.mask"));
            //var diffuse = new MythicTexture(File.OpenRead(@"D:\Games\Warhammer Online - Age of Reckoning\myps\art2\assetdb\decals\0.fg.0.0.boss_dwm_thorgrim_body.fg.0.0.boss_dwm_thorgrim_body.@.diffuse"));

            var mask = new MythicTexture(File.OpenRead(@"D:\Games\Warhammer Online - Age of Reckoning\myps\art2\assetdb\decals\0.fg.0.0.cmf_armor_ma_04_lbody.@.@.mask"));
            var diffuse = new MythicTexture(File.OpenRead(@"D:\Games\Warhammer Online - Age of Reckoning\myps\art2\assetdb\decals\0.fg.0.0.cmf_armor_ma_04_lbody.fg.0.0.cmf_armor_ma_04_lbody.@.diffuse"));

            //var mask = new MythicTexture(File.OpenRead(@"D:\Games\Warhammer Online - Age of Reckoning\myps\art2\assetdb\decals\0.fg.0.0.cmf_base_hair_h.@.@.mask"));
            //var diffuse = new MythicTexture(File.OpenRead(@"D:\Games\Warhammer Online - Age of Reckoning\myps\art2\assetdb\decals\0.fg.0.0.cmf_base_hair_h.fg.0.0.cmf_base_hair_h.@.diffuse"));

            Byte[] result = MythicTexture.CreateDiffuseDXT1(mask, diffuse, 9, 1024);

            pictureBox1.Image = MythicTexture.DecompressDXT1(result, 512, 512); 
        }


    }

}
