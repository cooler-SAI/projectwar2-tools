﻿using System.Collections.Generic;

namespace UnexpectedBytes.War
{
    using System;
    using System.Numerics;
    using Terrain;

    public class Zone
    {
        public Vertex[] Vertices;
        public Triangle[] Triangles;

        public Sector Sector;

        public PCX Terrain;
        public PCX Offset;

        public PCX Shadow;
        public PCX Hole;

        public List<Patch> Patches;

        public Int32 ScaleFactor => Sector.ScaleFactor;

        public Int32 OffsetFactor => Sector.OffsetFactor;

        public Int32 Width => Terrain.Width;

        public Int32 Height => Terrain.Height;

        public Int32 VertexCount => Terrain.Width * Terrain.Height;

        public Int32 TriangleCount => 2 * (Terrain.Width - 2) * (Terrain.Height - 2);

        public Int32 IndexCount => 3 * TriangleCount;

        public void LoadZone(String zones_path, Int32 zone_index)
        {
            Sector = new Sector();
            Sector.Load(zones_path, zone_index);


            Terrain = new PCX();
            Terrain.Load(zones_path, zone_index, "terrain");
            Offset = new PCX();
            Offset.Load(zones_path, zone_index, "offset");
            Shadow = new PCX();
            Shadow.Load(zones_path, zone_index, "shadow");
            Hole = new PCX();
            Hole.Load(zones_path, zone_index, "holemap");


            BuildVertices();

            BuildIndices();

            BuildNormals();


            Patches = new List<Patch>();
        }

        private void BuildVertices()
        {
            System.Console.WriteLine($"[!] Rasterizing {VertexCount} vertices.");
            Vertices = new Vertex[VertexCount];

            for (var y = 0; y < Height; ++y)
            {
                for (var x = 0; x < Width; ++x)
                {
                    Vertices[(y * Width) + x] = new Vertex
                    {
                        P = GetVertexPosition(x, y),
                        N = new Vector3(0, 0, 0)
                    };
                }
            }
        }

        private void BuildIndices()
        {
            System.Console.WriteLine($"[!] Rasterizing {TriangleCount} triangles.");
            Triangles = new Triangle[TriangleCount];

            var i = 0;
            for (var y = 1; y < Height - 1; ++y)
            {
                for (var x = 1; x < Width - 1; ++x)
                {
                    var b = y * Width; // base vertex index for this row

                    Triangles[i] = new Triangle();
                    Triangles[i].A = b + x;
                    Triangles[i].B = b + x + Width;
                    Triangles[i].C = b + x + 1;
                    ++i;

                    Triangles[i] = new Triangle();
                    Triangles[i].A = b + x + 1;
                    Triangles[i].B = b + x + Width;
                    Triangles[i].C = b + x + Width + 1;
                    ++i;
                }
            }
        }

        private void BuildNormals()
        {
            System.Console.WriteLine($"[!] Calculating {Vertices.Length} vertex normals.");
            System.Console.WriteLine($"[!] Calculating {Triangles.Length} face normals.");

            for (var i = 0; i < Vertices.Length; ++i)
                Vertices[i].N = Vector3.Zero;

            for (var i = 0; i < Triangles.Length; ++i)
            {
                // Get triangle indices
                var i0 = Triangles[i].A;
                var i1 = Triangles[i].B;
                var i2 = Triangles[i].C;

                // Get vertex positions of triangle
                Vector3 v0 = Vertices[i0].P;
                Vector3 v1 = Vertices[i1].P;
                Vector3 v2 = Vertices[i2].P;

                // Compute face normal of triangle
                Vector3 n = GetFaceNormal(v0, v1, v2);

                // Add normal to all vertices referenced by this triangle
                Vertices[i0].N += n;
                Vertices[i1].N += n;
                Vertices[i2].N += n;

                // FIXME: use angle weighting
            }

            for (var i = 0; i < Vertices.Length; i++)
                Vertices[i].N = Vector3.Normalize(Vertices[i].N);
        }

        private Vector3 GetFaceNormal(Vector3 v0, Vector3 v1, Vector3 v2)
        {
            Vector3 e1 = v0 - v1;
            Vector3 e2 = v2 - v1;
            var no = Vector3.Cross(e1, e2);

            return no;
        }

        public Vector3 GetVertexPosition(Int32 x, Int32 y)
        {
            var xw = Terrain.Width - x - 1;
            var yh = Terrain.Height - y - 1;

            var fx = 65535.0f * x / (Terrain.Width - 1);
            var fy = 65535.0f * y / (Terrain.Height - 1);
            Single fz = GetValue(xw, y);

            return new Vector3(fx, fy, fz);
        }

        public Int32 GetValue(Int32 x, Int32 y) => (Sector.ScaleFactor * Terrain.GetValue(x, y)) + (Sector.OffsetFactor * Offset.GetValue(x, y));

    }
}
