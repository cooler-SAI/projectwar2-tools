﻿using System;
using System.Collections.Generic;
using System.IO;

namespace UnexpectedBytes.War.Terrain
{
    public class Patch
    {
        public UInt16 ZoneID;
        public Byte PatchX;
        public Byte PatchY;
        private UInt16 LayerCount;

        public List<UV> UVs = new List<UV>();
        public Heightmap HeightmapData = new Heightmap();
        public List<Object> UnkData = new List<Object>();

        public Vertex[] Vertices;
        public Int32[] Indices;


        public Single Unk0 { get; set; }
        public UInt16 Unk1 { get; set; }
        public UInt16 Unk2 { get; set; }
        public Single Unk3 { get; set; }
        public Single Unk4 { get; set; }
        public Single Unk5 { get; set; }
        public UInt16 Unk6 { get; set; }
        public UInt16 Unk7 { get; set; }
        public Single Unk8 { get; set; }
        public UInt16 Unk9 { get; set; }

        public void Load(String patches_path, Int32 zone_index, Int32 column, Int32 row)
        {
            var patch = Path.Combine(patches_path, $"0.{zone_index}.{column}.{row}.patch");
            if (File.Exists(patch))
            {
                Load(File.ReadAllBytes(patch));
            }
        }

        public void Load(Byte[] data)
        {
            var ms = new MemoryStream(data);
            var br = new BinaryReader(ms);

            var size = br.ReadUInt32();
            ZoneID = br.ReadUInt16();
            PatchX = br.ReadByte();
            PatchY = br.ReadByte();

            Unk0 = br.ReadSingle();
            Unk1 = br.ReadUInt16();
            Unk2 = br.ReadUInt16();
            Unk3 = br.ReadSingle();
            Unk4 = br.ReadSingle();
            Unk5 = br.ReadSingle();
            Unk6 = br.ReadUInt16();
            Unk7 = br.ReadUInt16();
            Unk8 = br.ReadSingle();
            Unk9 = br.ReadUInt16();

            var count = 0;

            while (ms.Position < 0x55C8)
            {
                var patch = new UV()
                {
                    Index = count
                };
                patch.Read(br);
                UVs.Add(patch);
                count++;
            }

            ms.Position += 2;

            HeightmapData.Read(br);

            LayerCount = br.ReadUInt16();

            //long pos = br.BaseStream.Position;
            //br.BaseStream.Position = pos;

            while (br.BaseStream.Position < 0xDCD4)
            {
                var a0 = br.ReadSingle();
                if (a0 < 0.00000001 || a0 > 2.36395336E+05)
                {
                    br.BaseStream.Position -= 4;
                    var s = br.ReadUInt16();
                    UnkData.Add("USHORT:" + s);
                }
                else
                {
                    UnkData.Add("FLOAT:" + a0);
                }
            }

            br.Dispose();
        }
    }
}
