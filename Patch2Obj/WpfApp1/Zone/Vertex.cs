﻿using System.Drawing;
using System.IO;
using System.Numerics;

namespace UnexpectedBytes.War.Terrain
{
    public class Vertex
    {
        public Vector3 Position { get; set; }
        public Vector3 P { get => Position; set => Position = value; }

        public Vector3 Normal { get; set; }
        public Vector3 N { get => Normal; set => Normal = value; }

        public Vector3 TextureCoordinates { get; set; }
        public Vector3 UVW { get => TextureCoordinates; set => TextureCoordinates = value; }

        public Vector4 Color { get; set; }
        public Vector4 C { get => Color; set => Color = value; }

        public Vertex()
        {
            P = Vector3.Zero;
            N = Vector3.Zero;
            UVW = Vector3.Zero;
            C = Vector4.Zero;
        }
    }
}
