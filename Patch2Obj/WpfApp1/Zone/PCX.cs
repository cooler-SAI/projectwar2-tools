﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;

namespace UnexpectedBytes.War.Terrain
{
    public class PCX
    {
        public struct Header
        {
            public byte Identifier { get; set; }                /* ZSoft = 0x0A */
            public byte Version { get; set; }                   /* Software Version (0x00, 0x02, 0x03, 0x04, 0x05) */
            public byte Encoding { get; set; }                  /* RLE = 0x01*/
            public byte BitsPerPixel { get; set; }              /* 0x01, 0x02, 0x04, 0x08 */
            public ushort XStart { get; set; }                  /* Left viewable image offset */
            public ushort YStart { get; set; }                  /* Top viewable image offset */
            public ushort XEnd { get; set; }                    /* Right viewable image offset */
            public ushort YEnd { get; set; }                    /* Bottom  viewable image offset */
            public ushort HorizontalResolution { get; set; }    /* Horizontal DPI */
            public ushort VerticalResolution { get; set; }      /* Vertical DPI */
            public byte[/*48*/] Palette { get; set; }           /* Color Palette for 16bit */
            public byte Reserved1 { get; set; }                 /* Reserved (Always 0x00) */
            public byte NumBitPlanes { get; set; }              /* Number of Bit Planes */
            public ushort BytesPerLine { get; set; }            /* Scan line length in bytes */
            public ushort PaletteType { get; set; }             /* Color = 0x01, Greyscale = 0x02 */
            public ushort HorzontalScreenSize { get; set; }     /* Horizontal Screen Size */
            public ushort VerticalScreenSize { get; set; }      /* Vertical Screen Size */
            public byte[/*54*/] Reserved2 { get; set; }         /* Reserved (Always 0x00) */
        }

        public Header HeaderData { get; set; }                  /*  */
        public byte[] PixelData { get; set; }

        public int Width => (HeaderData.XEnd + 1 - HeaderData.XStart);
        public int Height => (HeaderData.YEnd + 1 - HeaderData.YStart);
        public int MaxColors => (int)(1L << HeaderData.BitsPerPixel * HeaderData.NumBitPlanes);
        public int ScanLineLength => (HeaderData.BytesPerLine * HeaderData.NumBitPlanes);
        public int TotalLength => (Width * Height * HeaderData.NumBitPlanes);
        public string Name { get; set; }

        public PCX() { }

        public PCX(string zones_path, int zone_id, string filename) => Load(zones_path, zone_id, filename);

        public void Load(string zones_path, int zone_id, string filename)
        {
            Name = $"zone{zone_id:D3}/{filename}.pcx";
            string pcx = Path.Combine(zones_path, $"zone{zone_id:D3}", $"{filename}.pcx");

            if (File.Exists(pcx))
            {
                Load(new MemoryStream(File.ReadAllBytes(pcx)));
            }
        }

        public PCX(Stream bs) => Load(bs);

        public void Load(Stream bs)
        {
            BinaryReader br = new BinaryReader(bs);
            Load(ref br);
        }

        public PCX(ref BinaryReader br) => Load(ref br);

        private void Load(ref BinaryReader br)
        {
            HeaderData = new Header
            {
                Identifier = br.ReadByte(),
                Version = br.ReadByte(),
                Encoding = br.ReadByte(),
                BitsPerPixel = br.ReadByte(),

                XStart = br.ReadUInt16(),
                YStart = br.ReadUInt16(),
                XEnd = br.ReadUInt16(),
                YEnd = br.ReadUInt16(),
                HorizontalResolution = br.ReadUInt16(),
                VerticalResolution = br.ReadUInt16(),

                Palette = br.ReadBytes(48),

                Reserved1 = br.ReadByte(),
                NumBitPlanes = br.ReadByte(),

                BytesPerLine = br.ReadUInt16(),
                PaletteType = br.ReadUInt16(),
                HorzontalScreenSize = br.ReadUInt16(),
                VerticalScreenSize = br.ReadUInt16(),

                Reserved2 = br.ReadBytes(54),
            };

            DecodeImage(ref br);
        }

        public void DecodeImage(ref BinaryReader br)
        {
            PixelData = new byte[TotalLength];
            byte[] RowData = new byte[Width];

            for (int i = 0; i < Height; ++i)
            {
                long caret = br.BaseStream.Position;
                DecodeRow(ref RowData, ref br);
                int RowOffset = (i * HeaderData.NumBitPlanes) * Width;
                RowData.CopyTo(PixelData, RowOffset);
                //br.BaseStream.Seek(Marshal.SizeOf(HeaderData) + (i * HeaderData.BytesPerLine), SeekOrigin.Begin);
            }
        }

        public void DecodeRow(ref byte[] RowData, ref BinaryReader br)
        {
            byte value = 0;
            int pos = 0;
            int runLength = 0;

            while (pos < HeaderData.BytesPerLine)
            {
                if (runLength > 0)
                {
                    RowData[pos++] = value;
                    //PixelDataRow.Insert(pos++, value);
                    runLength--;
                }
                else
                {
                    value = br.ReadByte();
                    if ((value & 0xC0) == 0xC0)
                    {
                        runLength = value & 0x3F;
                        value = br.ReadByte();
                    }
                    else
                    {
                        runLength = 0;
                        RowData[pos++] = value;
                        //PixelDataRow.Insert(pos++, value);
                    }
                }
            }
        }

        public byte GetValue(int x, int y)
        {
            return GetValue(x, y, 0);
        }

        public byte GetValue(int x, int y, int plane)
        {
            if (plane >= HeaderData.NumBitPlanes)
            {
                throw new NotImplementedException();
            }

            int rowOffset = (y * (HeaderData.NumBitPlanes + plane)) * Width;
            return PixelData[rowOffset + (x * (HeaderData.NumBitPlanes + plane))];
        }
    }
}
