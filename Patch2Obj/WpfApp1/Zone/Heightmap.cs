﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;

namespace UnexpectedBytes.War.Terrain
{
    public class Heightmap
    {
        public List<ushort> data;
        public int Width = 131;
        public int Height = 131;

        public void Read(BinaryReader reader)
        {
            for (int y = 0; y < 131; y++)
            {
                for (int x = 0; x < 131; x++)
                {
                    data.Add(reader.ReadUInt16());
                }
            }
        }
    }
}
