﻿/*
using System.IO;

namespace UnexpectedBytes.War
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            Zone zone = new Zone();
            zone.LoadZone(@"D:\Games\Warhammer Online - Age of Reckoning\myps\world.myp\zones", 205);

            StreamWriter sw = new StreamWriter(new FileStream("205.obj", FileMode.Create, FileAccess.Write));

            sw.WriteLine($"o Patch 205");
            sw.WriteLine($"");

            sw.WriteLine($"g 205");
            foreach (var vertex in zone.Vertices)
            {
                sw.WriteLine($"v {vertex.P.X} {vertex.P.Z} {vertex.P.Y}");
            }
            sw.WriteLine($"# {zone.VertexCount} positions");
            sw.WriteLine($"");

            sw.WriteLine($"g 205");
            foreach (var vertex in zone.Vertices)
            {
                sw.WriteLine($"n {vertex.P.X} {vertex.P.Y} {vertex.P.Z}");
            }
            sw.WriteLine($"# {zone.VertexCount} normals");
            sw.WriteLine($"");

            sw.WriteLine($"g 205");
            foreach (var triangle in zone.Triangles)
            {
                sw.WriteLine($"f {triangle.A}/{triangle.A} {triangle.B}/{triangle.B} {triangle.C}/{triangle.C}");
            }
            sw.WriteLine($"# {zone.TriangleCount} triangles");
            sw.WriteLine($"");

            sw.Flush();
            sw.Close();
        }

    }
}
*/
