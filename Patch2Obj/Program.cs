﻿using NIFLibrary;
using System.Collections.Generic;
using System.IO;

namespace UnexpectedBytes.War
{
    public static class Program
    {
        public static System.Int32 Main(System.String[] args)
        {
            System.Console.WriteLine("[!] Warhammer Online ZoneXXX to Wavefront Obj conversion tool v0.0.1");
            System.Console.WriteLine("[?] Written by Debuggin <andi.ireland@gmail.com> and Zaru <dan.andraschko@gmail.com>");
            System.Console.WriteLine("[.]");

            //args = new string[3];
            //args[0] = "-ZONE2OBJ";
            //args[1] = @"D:\Games\Warhammer Online -Age of Reckoning\myps\world.myp\zones";
            //args[2] = @"D:\Games";
            //args[3] = @"205";

            //args = new string[3];
            //args[0] = "-nif";
            //args[1] = @"D:\Develop\war-apocalypse\myps\art.myp\art\nifs\effects";
            //args[2] = @"D:\Develop\war-apocalypse\nif\";

            if (args.Length <= 0 || args.Length > 4)
            {
                System.Console.WriteLine("[!] Error: Invalid number of parameters.");
                System.Console.WriteLine("[.]");
            }

            if (args.Length <= 0 || args.Length > 4)
            {
                if (args[0] == "/?" || args[0] == "/h" || args[0] == "/help" || args[0] == "-h" || args[0] == "-help" || args[0] == "--h" || args[0] == "--help" || args[0] == "help")
                {
                    System.Console.WriteLine("[!] -nif: Converts Warhammer Online nif files to .xyz");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] -nif [source] [destination]");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] source           Specifies the folder of nif files to convert.");
                    System.Console.WriteLine("[!] destination      Specifies the name of the ouput file.");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] -zone: Converts Warhammer Online ZoneXXX Folders to Wavefront .obj");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] -zone source [destination] [index]");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[!] source           Specifies the zones folder.");
                    System.Console.WriteLine("[!] destination      Specifies the name of the ouput file.");
                    System.Console.WriteLine("[!] index            Specifies the number of the zone to convert.");
                    System.Console.WriteLine("[ ]");
                    System.Console.WriteLine("[?] Written by Debuggin <andi.ireland@gmail.com>");
                    System.Console.WriteLine("[.]");
                }
                return 0;
            }

            var exported = false;
            var ctrlFlag = args[0].Replace("-", "").Replace("--", "").Replace("/", "").Trim().ToLower();

            if (System.String.Equals(ctrlFlag, "nif"))
            {
                exported = NifConvert(args);
            }
            else if (System.String.Equals(ctrlFlag, "zone"))
            {
                exported = Zone2Obj(args);
            }

            if (!exported)
            {
                System.Console.WriteLine("[!] Error: Nothing to export.");
                System.Console.WriteLine("[.]");
                return 1;
            }

            System.Console.WriteLine($"[!] Export complete.");
            System.Console.WriteLine($"[.]");

            return 0;
        }

        private static System.Boolean Zone2Obj(System.String[] args)
        {
            var destination = args[1];

            var index = 0;
            var start = 0;
            var end = 1000;
            var potential = 0;

            if (args.Length == 3 && !System.Int32.TryParse(args[2], out start))
                destination = args[2];

            if (args.Length == 4 && !System.Int32.TryParse(args[3], out start))
                destination = args[3];

            if (destination.Length < 5)
                destination = args[1];

            if (start > 0)
                end = start;


            if (end > start)
            {
                System.Console.WriteLine("[!] Error: Invalid zone number.");
                System.Console.WriteLine("[.]");
                return false;
            }

            var done = false;
            var attempt = 0;
            while (!Directory.Exists(Path.GetPathRoot(destination)) && !done)
            {
                Directory.CreateDirectory(Path.GetPathRoot(destination));
                if (attempt++ > 8)
                    done = true;
            }

            if (!Directory.Exists(Path.GetPathRoot(destination)))
            {
                System.Console.WriteLine("[*] Error: could not create the output folder.");
                System.Console.WriteLine("[.]");
                return false;
            }

            var source = args[1] + $"\\zone{start:D3}";
            potential = end - start;
            for (index = start; index <= end; ++index)
            {
                source = Path.GetFullPath(args[1] + $"\\zone{index:D3}");
                if (Directory.Exists(source))
                {
                    source = args[1] + $"\\zone{start:D3}";
                    System.Console.WriteLine($"[!] Scanning folder {source}\\*.*");
                    var zone = new Zone();
                    zone.LoadZone(args[1], index);

                    var sw = new StreamWriter(new FileStream(Path.Combine(destination, $"zone{index:D3}.obj"), FileMode.Create, FileAccess.Write));

                    System.Console.WriteLine($"[!] Saving to {destination}\\zone{index:D3}.obj");

                    sw.WriteLine($"o Patch {index}");
                    sw.WriteLine($"");

                    sw.WriteLine($"g {index}");
                    foreach (Terrain.Vertex vertex in zone.Vertices)
                    {
                        sw.WriteLine($"v {vertex.P.X} {vertex.P.Z} {vertex.P.Y}");
                    }
                    sw.WriteLine($"# {zone.VertexCount} positions");
                    sw.WriteLine($"");

                    sw.WriteLine($"g {index}");
                    foreach (Terrain.Vertex vertex in zone.Vertices)
                    {
                        sw.WriteLine($"n {vertex.P.X} {vertex.P.Y} {vertex.P.Z}");
                    }
                    sw.WriteLine($"# {zone.VertexCount} normals");
                    sw.WriteLine($"");

                    sw.WriteLine($"g {index}");
                    foreach (Terrain.Triangle triangle in zone.Triangles)
                    {
                        sw.WriteLine($"f {triangle.A}/{triangle.A} {triangle.B}/{triangle.B} {triangle.C}/{triangle.C}");
                    }
                    sw.WriteLine($"# {zone.TriangleCount} triangles");
                    sw.WriteLine($"");

                    sw.Flush();
                    sw.Close();

                    if (File.Exists(Path.Combine(destination, $"zone{index:D3}.obj")))
                        return true;
                }
            }

            return false;
        }

        private static System.Boolean NifConvert(System.String[] args)
        {
            var destination = args[2];
            var source = args[1];
            var files = new List<System.String>();

            foreach (var file in Directory.GetFiles(source))
            {
                if (!file.ToLower().Contains(".nif"))
                    continue;
                files.Add(file);
            }

            if (files.Count <= 0)
                return false;

            #region test area

            using (var fs = new FileStream(files[0], FileMode.Open))
            {
                using (var binReader = new BinaryReader(fs))
                {
                    var nifFile = new NiFile(binReader);

                }
            }

            #endregion test area

            return false;
        }
    }
}

