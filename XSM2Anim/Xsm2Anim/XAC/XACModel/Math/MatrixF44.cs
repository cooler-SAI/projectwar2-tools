﻿using System.IO;

namespace UnexpectedBytes
{
    public class MatrixF44
    {
        public Vector4F mCol1;
        public Vector4F mCol2;
        public Vector4F mCol3;
        public Vector4F mCol4;

        public MatrixF44()
        {
            mCol1 = new Vector4F(1.0f, 0.0f, 0.0f, 0.0f);
            mCol2 = new Vector4F(0.0f, 1.0f, 0.0f, 0.0f);
            mCol3 = new Vector4F(0.0f, 0.0f, 1.0f, 0.0f);
            mCol4 = new Vector4F(0.0f, 0.0f, 0.0f, 1.0f);
        }

        public void ReadIn(BinaryReader iStream)
        {
            mCol1.ReadIn(iStream);
            mCol2.ReadIn(iStream);
            mCol3.ReadIn(iStream);
            mCol4.ReadIn(iStream);
        }

        public void WriteOut(BinaryWriter iStream)
        {
            mCol1.WriteOut(iStream);
            mCol2.WriteOut(iStream);
            mCol3.WriteOut(iStream);
            mCol4.WriteOut(iStream);
        }

        public long GetSize()
        {
            long vSize = 0;
            vSize += mCol1.GetSize();
            vSize += mCol2.GetSize();
            vSize += mCol3.GetSize();
            vSize += mCol4.GetSize();
            return vSize;
        }

        public override string ToString()
        {
            return $"{mCol1}\r\n                  {mCol2}\r\n                  {mCol3}\r\n                  {mCol4}";
        }
    }
}
