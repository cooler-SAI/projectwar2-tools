﻿using System.IO;
using System.Runtime.InteropServices;

namespace UnexpectedBytes
{
    public class StandardMaterial
    {
        public Color4F mAmbient;
        public Color4F mDiffuse;
        public Color4F mSpecular;
        public Color4F mEmissive;
        public float mShine;
        public float mShineStrength;
        public float mOpacity;
        public float mIndexOfRefraction;
        public byte mDoubleSided;
        public byte mWireFrame;
        public byte mTransparencyType; // ASCII F=filter, S=substractive, A=additive, U=unknown
        public byte mLayerCount;
        public string mName;

        public StandardMaterial()
        {
            mAmbient = new Color4F();
            mDiffuse = new Color4F();
            mSpecular = new Color4F();
            mEmissive = new Color4F();
            mShine = 0.0f;
            mShineStrength = 0.0f;
            mOpacity = 0.0f;
            mIndexOfRefraction = 0.0f;
            mDoubleSided = 0;
            mWireFrame = 0;
            mTransparencyType = 0;
            mLayerCount = 0;
            mName = string.Empty;
        }

        public void ReadIn(BinaryReader iStream, Chunk iChunk)
        {
            mAmbient.ReadIn(iStream);
            mDiffuse.ReadIn(iStream);
            mSpecular.ReadIn(iStream);
            mEmissive.ReadIn(iStream);
            mShine = iStream.ReadSingle();
            mShineStrength = iStream.ReadSingle();
            mOpacity = iStream.ReadSingle();
            mIndexOfRefraction = iStream.ReadSingle();
            mDoubleSided = iStream.ReadByte();
            mWireFrame = iStream.ReadByte();
            mTransparencyType = iStream.ReadByte();
            mLayerCount = iStream.ReadByte();
            mName = Common.ReadString(iStream);
        }

        public void WriteOut(BinaryWriter iStream, Chunk iChunk)
        {
            mAmbient.WriteOut(iStream);
            mDiffuse.WriteOut(iStream);
            mSpecular.WriteOut(iStream);
            mEmissive.WriteOut(iStream);
            iStream.Write(mShine);
            iStream.Write(mShineStrength);
            iStream.Write(mOpacity);
            iStream.Write(mIndexOfRefraction);
            iStream.Write(mDoubleSided);
            iStream.Write(mWireFrame);
            iStream.Write(mTransparencyType);
            iStream.Write(mLayerCount);
            Common.WriteString(iStream, mName);
        }

        public long GetSize()
        {
            long vSize = 0;
            vSize += mAmbient.GetSize();
            vSize += mDiffuse.GetSize();
            vSize += mSpecular.GetSize();
            vSize += mEmissive.GetSize();
            vSize += Marshal.SizeOf(mShine);
            vSize += Marshal.SizeOf(mShineStrength);
            vSize += Marshal.SizeOf(mOpacity);
            vSize += Marshal.SizeOf(mIndexOfRefraction);
            vSize += Marshal.SizeOf(mDoubleSided);
            vSize += Marshal.SizeOf(mWireFrame);
            vSize += Marshal.SizeOf(mTransparencyType);
            vSize += Marshal.SizeOf(mLayerCount);
            vSize += sizeof(uint) + mName.Length;
            return vSize;
        }
    }
}
