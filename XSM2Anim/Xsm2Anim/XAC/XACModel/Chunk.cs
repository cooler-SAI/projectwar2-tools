﻿using System.IO;
using System.Runtime.InteropServices;

namespace UnexpectedBytes
{
    public enum ChunkType : uint
    {
        Node = 0,
        Mesh = 1,
        SkinningInfo = 2,
        StandardMaterial = 3,
        StandardMaterialLayer = 4,
        FXMaterial = 5,
        Limit = 6,
        Info = 7,
        MeshLODLevels = 8,
        StandardProgramMorphTarget = 9,
        NodeGroups = 10,
        Nodes = 11,
        StandardMorphTargets = 12,
        MaterialInfo = 13,
        NodeMotionSources = 14,
        AttachmentNodes = 15,
        MaterialAttributeSet = 16,
        GenericMaterial = 17,
        Count = 18
    };

    public class Chunk
    {
        public ChunkType mType;
        public uint mSize;
        public uint mVersion;

        public Chunk()
        {
            mType = 0;
            mSize = 0;
            mVersion = 0;
        }

        public void ReadIn(BinaryReader iStream)
        {
            mType = (ChunkType)iStream.ReadUInt32();
            mSize = iStream.ReadUInt32();
            mVersion = iStream.ReadUInt32();
        }

        public void WriteOut(BinaryWriter iStream)
        {
            iStream.Write((uint)mType);
            iStream.Write(mSize);
            iStream.Write(mVersion);
        }

        public long GetSize()
        {
            long vSize = 0;
            vSize += Marshal.SizeOf(mType);
            vSize += Marshal.SizeOf(mSize);
            vSize += Marshal.SizeOf(mVersion);
            return vSize;
        }
    }
}
