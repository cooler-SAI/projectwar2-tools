﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace WarClient.Services
{
    public abstract class ServerRPC
    {
        private readonly String _address;
        private readonly Int32 _port;

        public ServerRPC(String address, Int32 port)
        {
        }

        protected async Task<TResult> RequestAsync<TResult>(String endpoint, params (String, Object)[] parameters)
        {
            var path = $@"http://{_address}:{_port}/{endpoint}?";
            foreach ((String, Object) p in parameters)
            {
                path += p.Item1 + "=" + Uri.EscapeUriString(p.Item1.ToString()) + "&";
            }
            WebRequest request = HttpWebRequest.Create(path);

            request.Proxy = null;


            request.ContentType = "application/json";
            request.Method = "POST";

            WebResponse response = await request.GetResponseAsync();

            using (Stream stream = response.GetResponseStream())
            {
                var reader = new StreamReader(stream);
                return Newtonsoft.Json.JsonConvert.DeserializeObject<TResult>(reader.ReadToEnd());
            }
        }

    }
}
