﻿namespace WarClient.Figleaf
{
    public enum TableType
    {
        Strings1 = 0,
        Strings2 = 1,
        CharacterArt = 2,
        FigureParts = 3,
        ArtSwitches = 4,
        CharacterDecals = 5,
        CharacterMeshes = 6,
        Textures = 7,
        Unk8 = 8,
        Fixtures = 9,
        Lightmaps = 10,
        Patches = 11,
        Regions = 12,
        VAR_RECS1 = 13,
        VAR_RECS2 = 14,
        VAR_RECS3 = 15,
        VAR_RECS4 = 16,
        VAR_RECS5 = 17
    }
}
