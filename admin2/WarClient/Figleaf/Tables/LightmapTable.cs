﻿using System;
using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class LightmapTable : FigTable<Lightmap>
    {
        private const Int32 HeaderPosition = 0x84;
        private const Int32 RecordSize = 0x28;

        public LightmapTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (var i = 0; i < EntryCount; i++)
            {
                var lightmap = new Lightmap(_db, i);

                lightmap.B01 = reader.ReadUInt32();
                lightmap.B02 = reader.ReadUInt32();
                lightmap.B03 = reader.ReadUInt32();
                lightmap.B04 = reader.ReadUInt32();
                lightmap.B05_Pages = reader.ReadUInt32();
                lightmap.SourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                lightmap.B07 = reader.ReadUInt32();
                lightmap.B08 = reader.ReadUInt32();
                lightmap.B09a = reader.ReadUInt32();
                lightmap.B10a = reader.ReadUInt32();

                Records.Add(lightmap);
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for (var i = 0; i < Records.Count; i++)
            {
                Lightmap lightmap = Records[i];
                writer.Write(lightmap.B01);
                writer.Write(lightmap.B02);
                writer.Write(lightmap.B03);
                writer.Write(lightmap.B04);
                writer.Write(lightmap.B05_Pages);
                writer.Write((UInt32)lightmap.SourceIndex);
                writer.Write(lightmap.B07);
                writer.Write(lightmap.B08);
                writer.Write(lightmap.B09a);
                writer.Write(lightmap.B10a);
            }

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((UInt32)Records.Count);
            writer.Write((UInt32)(Records.Count > 0 ? pos : 0));
            var size = (UInt32)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }

    public class Lightmap : FigRecord
    {
        public Int32 Index { get; }
        public UInt32 B01 { get; set; }
        public UInt32 B02 { get; set; }
        public UInt32 B03 { get; set; }
        public UInt32 B04 { get; set; }
        public UInt32 B05_Pages { get; set; }
        public FigStringRef SourceIndex { get; set; }
        public UInt32 B07 { get; set; }
        public UInt32 B08 { get; set; }
        public UInt32 B09a { get; set; }
        public UInt32 B10a { get; set; }
        public Lightmap(FigleafDB db, Int32 index) : base(db) => Index = index;
    }
}

