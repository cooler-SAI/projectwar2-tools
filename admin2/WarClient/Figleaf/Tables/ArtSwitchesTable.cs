﻿using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class ArtSwitchTable : FigTable<ArtSwitch>
    {
        private const System.Int32 HeaderPosition = 0x3c;
        private const System.Int32 RecordSize = 0xA4;

        public ArtSwitchTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (var i = 0; i < EntryCount; i++)
            {
                var art = new ArtSwitch(_db, i);
                art.SwitchType = (SwitchType)reader.ReadUInt32();
                art.SourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                art.Unk2 = reader.ReadUInt32();
                art.Unk3 = reader.ReadUInt32();
                art.Unk4 = reader.ReadUInt32();
                art.Other = new FigStringRef(_db, reader.ReadUInt32());
                art.Male = new FigStringRef(_db, reader.ReadUInt32());
                art.Female = new FigStringRef(_db, reader.ReadUInt32());
                art.DwarfOther = new FigStringRef(_db, reader.ReadUInt32());
                art.DwarfMale = new FigStringRef(_db, reader.ReadUInt32());
                art.DwarfFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.HumanOther = new FigStringRef(_db, reader.ReadUInt32());
                art.HumanMale = new FigStringRef(_db, reader.ReadUInt32());
                art.HumanFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.ChaosHumanOther = new FigStringRef(_db, reader.ReadUInt32());
                art.ChaosHumanMale = new FigStringRef(_db, reader.ReadUInt32());
                art.ChaosHumanFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.ElfOther = new FigStringRef(_db, reader.ReadUInt32());
                art.ElfMale = new FigStringRef(_db, reader.ReadUInt32());
                art.ElfFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.DarkElfOther = new FigStringRef(_db, reader.ReadUInt32());
                art.DarkElfMale = new FigStringRef(_db, reader.ReadUInt32());
                art.DarkElfFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.OrcOther = new FigStringRef(_db, reader.ReadUInt32());
                art.OrcMale = new FigStringRef(_db, reader.ReadUInt32());
                art.OrcFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.GoblinOther = new FigStringRef(_db, reader.ReadUInt32());
                art.GoblinMale = new FigStringRef(_db, reader.ReadUInt32());
                art.GoblinFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.BeastmanOther = new FigStringRef(_db, reader.ReadUInt32());
                art.BeastmanMale = new FigStringRef(_db, reader.ReadUInt32());
                art.BeastmanFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.SkavenOther = new FigStringRef(_db, reader.ReadUInt32());
                art.SkavenMale = new FigStringRef(_db, reader.ReadUInt32());
                art.SkavenFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.OgreOther = new FigStringRef(_db, reader.ReadUInt32());
                art.OgreMale = new FigStringRef(_db, reader.ReadUInt32());
                art.OgreFemale = new FigStringRef(_db, reader.ReadUInt32());
                art.ChaosWarriorOther = new FigStringRef(_db, reader.ReadUInt32());
                art.ChaosWarriorMale = new FigStringRef(_db, reader.ReadUInt32());
                art.ChaosWarriorFemale = new FigStringRef(_db, reader.ReadUInt32());
                Records.Add(art);
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for (var i = 0; i < Records.Count; i++)
            {
                ArtSwitch art = Records[i];

                writer.Write((System.UInt32)art.SwitchType);
                writer.Write((System.UInt32)art.SourceIndex);
                writer.Write(art.Unk2);
                writer.Write(art.Unk3);
                writer.Write(art.Unk4);
                writer.Write((System.UInt32)art.Other);
                writer.Write((System.UInt32)art.Male);
                writer.Write((System.UInt32)art.Female);
                writer.Write((System.UInt32)art.DwarfOther);
                writer.Write((System.UInt32)art.DwarfMale);
                writer.Write((System.UInt32)art.DwarfFemale);
                writer.Write((System.UInt32)art.HumanOther);
                writer.Write((System.UInt32)art.HumanMale);
                writer.Write((System.UInt32)art.HumanFemale);
                writer.Write((System.UInt32)art.ChaosHumanOther);
                writer.Write((System.UInt32)art.ChaosHumanMale);
                writer.Write((System.UInt32)art.ChaosHumanFemale);
                writer.Write((System.UInt32)art.ElfOther);
                writer.Write((System.UInt32)art.ElfMale);
                writer.Write((System.UInt32)art.ElfFemale);
                writer.Write((System.UInt32)art.DarkElfOther);
                writer.Write((System.UInt32)art.DarkElfMale);
                writer.Write((System.UInt32)art.DarkElfFemale);
                writer.Write((System.UInt32)art.OrcOther);
                writer.Write((System.UInt32)art.OrcMale);
                writer.Write((System.UInt32)art.OrcFemale);
                writer.Write((System.UInt32)art.GoblinOther);
                writer.Write((System.UInt32)art.GoblinMale);
                writer.Write((System.UInt32)art.GoblinFemale);
                writer.Write((System.UInt32)art.BeastmanOther);
                writer.Write((System.UInt32)art.BeastmanMale);
                writer.Write((System.UInt32)art.BeastmanFemale);
                writer.Write((System.UInt32)art.SkavenOther);
                writer.Write((System.UInt32)art.SkavenMale);
                writer.Write((System.UInt32)art.SkavenFemale);
                writer.Write((System.UInt32)art.OgreOther);
                writer.Write((System.UInt32)art.OgreMale);
                writer.Write((System.UInt32)art.OgreFemale);
                writer.Write((System.UInt32)art.ChaosWarriorOther);
                writer.Write((System.UInt32)art.ChaosWarriorMale);
                writer.Write((System.UInt32)art.ChaosWarriorFemale);
            }
            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((System.UInt32)Records.Count);
            writer.Write((System.UInt32)(Records.Count > 0 ? pos : 0));
            var size = (System.UInt32)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }

    public enum SwitchType : System.UInt32
    {
        Mesh = 0,
        Decal = 1,
        Color = 2
    }

    public class ArtSwitch : FigRecord
    {
        public System.Int32 Index { get; }
        public SwitchType SwitchType { get; set; }

        public FigStringRef SourceIndex { get; set; }
        public System.UInt32 Unk2 { get; set; }
        public System.UInt32 Unk3 { get; set; }
        public System.UInt32 Unk4 { get; set; }
        public FigStringRef Other { get; set; }
        public FigStringRef Male { get; set; }
        public FigStringRef Female { get; set; }
        public FigStringRef DwarfOther { get; set; }
        public FigStringRef DwarfMale { get; set; }
        public FigStringRef DwarfFemale { get; set; }
        public FigStringRef HumanOther { get; set; }
        public FigStringRef HumanMale { get; set; }
        public FigStringRef HumanFemale { get; set; }
        public FigStringRef ChaosHumanOther { get; set; }
        public FigStringRef ChaosHumanMale { get; set; }
        public FigStringRef ChaosHumanFemale { get; set; }
        public FigStringRef ElfOther { get; set; }
        public FigStringRef ElfMale { get; set; }
        public FigStringRef ElfFemale { get; set; }
        public FigStringRef DarkElfOther { get; set; }
        public FigStringRef DarkElfMale { get; set; }
        public FigStringRef DarkElfFemale { get; set; }
        public FigStringRef OrcOther { get; set; }
        public FigStringRef OrcMale { get; set; }
        public FigStringRef OrcFemale { get; set; }
        public FigStringRef GoblinOther { get; set; }
        public FigStringRef GoblinMale { get; set; }
        public FigStringRef GoblinFemale { get; set; }
        public FigStringRef BeastmanOther { get; set; }
        public FigStringRef BeastmanMale { get; set; }
        public FigStringRef BeastmanFemale { get; set; }
        public FigStringRef SkavenOther { get; set; }
        public FigStringRef SkavenMale { get; set; }
        public FigStringRef SkavenFemale { get; set; }
        public FigStringRef OgreOther { get; set; }
        public FigStringRef OgreMale { get; set; }
        public FigStringRef OgreFemale { get; set; }
        public FigStringRef ChaosWarriorOther { get; set; }
        public FigStringRef ChaosWarriorMale { get; set; }
        public FigStringRef ChaosWarriorFemale { get; set; }

        public ArtSwitch(FigleafDB db, System.Int32 index) : base(db) => Index = index;
    }
}
