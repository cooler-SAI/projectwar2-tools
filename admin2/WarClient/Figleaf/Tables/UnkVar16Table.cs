﻿using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class UnkVar16Table : FigTable<UnkVarRecord>
    {
        private readonly System.Int32 HeaderPosition = 0;

        public UnkVar16Table(FigleafDB db, System.Int32 headerOffset) : base(db) => HeaderPosition = headerOffset;

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (var i = 0; i < EntryCount; i++)
            {
                var pos = reader.BaseStream.Position;
                reader.BaseStream.Position += 0x10;
                var size = reader.ReadUInt32();
                reader.BaseStream.Position = pos;
                var d = new System.Byte[size];
                reader.Read(d, 0, d.Length);
                var record = new UnkVarRecord(_db, i)
                {
                    Data = d
                };

                Records.Add(record);
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for (var i = 0; i < Records.Count; i++)
            {
                UnkVarRecord record = Records[i];
                writer.Write(record.Data);
            }

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((System.UInt32)Records.Count);
            writer.Write((System.UInt32)(Records.Count > 0 ? pos : 0));
            var size = (System.UInt32)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }

    }


    public class UnkVarRecord : FigRecord
    {
        public System.Int32 Index { get; }
        public System.Byte[] Data { get; set; }
        public UnkVarRecord(FigleafDB db, System.Int32 index) : base(db) => Index = index;
    }
}

