﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WarClient.Figleaf.Tables
{

    public class CharacterArtTable : FigTable<CharacterArt>
    {
        private const Int32 HeaderPosition = 0x24;
        private const Int32 RecordSize = 0x48;

        public CharacterArtTable(FigleafDB db) : base(db)
        {
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;

            writer.BaseStream.Position += RecordSize * Records.Count;
            var DataStart = writer.BaseStream.Position;
            var ExtStart = RecordSize * Records.Count;

            for (var i = 0; i < Records.Count; i++)
            {
                CharacterArt record = Records[i];

                // record.Part1Start = 0;
                if (record.FigureParts.Count > 0)
                {
                    record.Part1Start = (Int32)(ExtStart + (writer.BaseStream.Position - DataStart) - (i * RecordSize));

                    foreach (CharacterArtData part in record.FigureParts)
                    {
                        writer.Write(part.FigurePartIndex);
                        writer.Write(part.dword4);
                        writer.Write(part.dwordC);
                        writer.Write(part.dword10);
                        writer.Write(part.dword14);
                        writer.Write(part.byte18);
                        writer.Write(part.byte19);
                        writer.Write(part.word1A);
                        writer.Write(part.dword1E);
                        writer.Write(part.dword22);
                        writer.Write(part.dword26);
                    }
                }

                // record.Part2Start = 0;
                if (record.Data2.Count > 0)
                {
                    record.Part2Start = (Int32)(ExtStart + (writer.BaseStream.Position - DataStart) - (i * RecordSize));

                    foreach (Unk12 part in record.Data2)
                    {
                        writer.Write(part.Unk1);
                        writer.Write(part.Unk2);
                        writer.Write(part.Unk3);
                        writer.Write(part.Unk4);
                        writer.Write(part.Unk5);
                    }
                }

                // record.Part3Start = 0;
                if (record.Data3.Count > 0)
                {
                    record.Part3Start = (Int32)(ExtStart + (writer.BaseStream.Position - DataStart) - (i * RecordSize));

                    foreach (Unk14 part in record.Data3)
                    {
                        writer.Write(part.Unk1);
                        writer.Write(part.Unk2);
                        writer.Write(part.Unk3);
                    }
                }

                //record.Part4Start = 0;
                if (record.Data4.Count > 0)
                {
                    record.Part4Start = (Int32)(ExtStart + (writer.BaseStream.Position - DataStart) - (i * RecordSize));

                    foreach (Unk16 part in record.Data4)
                    {
                        writer.Write(part.Unk1);
                        writer.Write(part.Unk2);
                        writer.Write(part.Unk3);
                        writer.Write(part.Unk4);
                    }
                }
            }
            var endPos = writer.BaseStream.Position;
            writer.BaseStream.Position = pos;

            for (var i = 0; i < Records.Count; i++)
            {
                CharacterArt record = Records[i];

                writer.Write((UInt32)record.SourceIndex);
                writer.Write(record.Unk1);
                writer.Write(record.Unk2);
                writer.Write(record.Unk3a);
                writer.Write(record.Unk3b);
                writer.Write(record.Unk3c);
                writer.Write(record.Unk3d);
                writer.Write(record.Race);
                writer.Write(record.Gender);
                var skelBytes = record.Skeleton.ToCharArray().Select(e => (Byte)e).ToArray();
                if (skelBytes.Length > 8)
                    throw new Exception("Skeleton name must be less than 9 characters long");

                var bytes = new Byte[8];
                Buffer.BlockCopy(skelBytes, 0, bytes, 0, record.Skeleton.Length);
                writer.BaseStream.Write(bytes, 0, bytes.Length);

                writer.Write(record.Animations);
                writer.Write(record.Unk9);
                writer.Write(record.FigureParts.Count);
                writer.Write(record.Part1Start);
                writer.Write(record.Data2.Count);
                writer.Write(record.Part2Start);
                writer.Write(record.Data3.Count);
                writer.Write(record.Part3Start);
                writer.Write(record.Data4.Count);
                writer.Write(record.Part4Start);
            }

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((UInt32)Records.Count);
            writer.Write((UInt32)(Records.Count > 0 ? pos : 0));
            var size = (UInt32)((endPos - DataStart) + (Records.Count * RecordSize));
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (var i = 0; i < EntryCount; i++)
            {
                var art = new CharacterArt(_db, i);
                art.SourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                art.Unk1 = reader.ReadInt32();
                art.Unk2 = reader.ReadInt32();
                art.Unk3a = reader.ReadByte();
                art.Unk3b = reader.ReadByte();
                art.Unk3c = reader.ReadByte();
                art.Unk3d = reader.ReadByte();
                art.Race = reader.ReadInt32();
                art.Gender = reader.ReadInt32();
                var skel = reader.ReadBytes(8);

                art.Skeleton = System.Text.Encoding.ASCII.GetString(skel.Where(e => e != 0).ToArray());
                art.Animations = reader.ReadInt32();
                art.Unk9 = reader.ReadInt32();
                art.Part1Count = reader.ReadInt32();
                art.Part1Start = reader.ReadInt32();
                art.Part2Count = reader.ReadInt32();
                art.Part2Start = reader.ReadInt32();
                art.Part3Count = reader.ReadInt32();
                art.Part3Start = reader.ReadInt32();
                art.Part4Count = reader.ReadInt32();
                art.Part4Start = reader.ReadInt32();
                Records.Add(art);
            }

            for (var i = 0; i < EntryCount; i++)
            {
                CharacterArt record = Records[i];

                if (record.Part1Count > 0)
                {
                    reader.BaseStream.Position = Offset + record.Part1Start + (i * RecordSize);
                    for (var c = 0; c < record.Part1Count; c++)
                    {
                        var a2 = new CharacterArtData();
                        a2.FigurePartIndex = new FigureRef(_db, reader.ReadInt32());
                        a2.dword4 = (Int16)reader.ReadInt32();
                        a2.dwordC = reader.ReadInt32();
                        a2.dword10 = reader.ReadInt32();
                        a2.dword14 = reader.ReadInt32();
                        a2.byte18 = reader.ReadByte();
                        a2.byte19 = reader.ReadByte();
                        a2.word1A = reader.ReadInt16();
                        a2.dword1E = reader.ReadInt32();
                        a2.dword22 = reader.ReadInt32();
                        a2.dword26 = reader.ReadInt32();
                        record.FigureParts.Add(a2);
                    }
                }

                if (record.Part2Count > 0)
                {
                    reader.BaseStream.Position = Offset + record.Part2Start + (i * RecordSize);
                    for (var c = 0; c < record.Part2Count; c++)
                    {
                        var a16 = new Unk12();
                        a16.Unk1 = reader.ReadInt32();
                        a16.Unk2 = reader.ReadInt32();
                        a16.Unk3 = reader.ReadInt32();
                        a16.Unk4 = reader.ReadInt32();
                        a16.Unk5 = reader.ReadInt32();
                        record.Data2.Add(a16);
                    }
                }
                if (record.Part3Count > 0)
                {
                    reader.BaseStream.Position = Offset + record.Part3Start + (i * RecordSize);
                    for (var c = 0; c < record.Part3Count; c++)
                    {
                        var a16 = new Unk14();
                        a16.Unk1 = reader.ReadInt32();
                        a16.Unk2 = reader.ReadInt32();
                        a16.Unk3 = reader.ReadInt32();
                        record.Data3.Add(a16);
                    }
                }

                if (record.Part4Count > 0)
                {
                    reader.BaseStream.Position = Offset + record.Part4Start + (i * RecordSize);

                    for (var c = 0; c < record.Part4Count; c++)
                    {
                        var a16 = new Unk16();
                        a16.Unk1 = reader.ReadInt32();
                        a16.Unk2 = reader.ReadInt32();
                        a16.Unk3 = reader.ReadInt32();
                        a16.Unk4 = reader.ReadInt32();
                        record.Data4.Add(a16);
                    }
                }
            }
        }
    }

    public class CharacterArt : FigRecord
    {
        public Int32 Index { get; }
        public FigStringRef SourceIndex { get; set; }
        public Int32 Unk1 { get; set; }
        public Int32 Unk2 { get; set; }

        public Byte Unk3a { get; set; }
        public Byte Unk3b { get; set; }
        public Byte Unk3c { get; set; }
        public Byte Unk3d { get; set; }

        public Int32 Race { get; set; }
        public Int32 Gender { get; set; }
        public String Skeleton { get; set; }
        public Int32 Animations { get; set; }
        public Int32 Unk9 { get; set; }
        public Int32 Part1Count { get; set; }
        public Int32 Part1Start { get; set; }
        public Int32 Part2Count { get; set; }
        public Int32 Part2Start { get; set; }
        public Int32 Part3Count { get; set; }
        public Int32 Part3Start { get; set; }
        public Int32 Part4Count { get; set; }
        public Int32 Part4Start { get; set; }

        public List<CharacterArtData> FigureParts = new List<CharacterArtData>();
        public List<Unk12> Data2 = new List<Unk12>();
        public List<Unk14> Data3 = new List<Unk14>();
        public List<Unk16> Data4 = new List<Unk16>();
        public CharacterArt(FigleafDB db, Int32 index) : base(db) => Index = index;
    }


    public struct CharacterArtData
    {
        public FigureRef FigurePartIndex { get; set; }
        public Int32 dword4 { get; set; }
        public Int32 dwordC { get; set; }
        public Int32 dword10 { get; set; }
        public Int32 dword14 { get; set; }
        public Byte byte18 { get; set; }
        public Byte byte19 { get; set; }
        public Int16 word1A { get; set; }
        public Int32 dword1E { get; set; }
        public Int32 dword22 { get; set; }
        public Int32 dword26 { get; set; }
    }

    public struct Unk12
    {
        public Int32 Unk1 { get; set; }
        public Int32 Unk2 { get; set; }
        public Int32 Unk3 { get; set; }
        public Int32 Unk4 { get; set; }
        public Int32 Unk5 { get; set; }
    }

    public struct Unk14
    {
        public Int32 Unk1 { get; set; }
        public Int32 Unk2 { get; set; }
        public Int32 Unk3 { get; set; }
    }
    public struct Unk16
    {
        public Int32 Unk1 { get; set; }
        public Int32 Unk2 { get; set; }
        public Int32 Unk3 { get; set; }
        public Int32 Unk4 { get; set; }
    }

}

