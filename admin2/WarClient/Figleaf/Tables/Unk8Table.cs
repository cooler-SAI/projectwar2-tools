﻿using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class Unk8Table : FigTable<Unk8>
    {
        private const System.Int32 HeaderPosition = 0x6c;
        private const System.Int32 RecordSize = 0x3c;

        public Unk8Table(FigleafDB db) : base(db)
        {

        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (var i = 0; i < EntryCount; i++)
            {
                var texture = new Unk8(_db, i);
                var pos = reader.BaseStream.Position;
                var data = reader.ReadBytes(RecordSize);
                reader.BaseStream.Position = pos;
                texture.DiffuseSourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                texture.DiffuseHash = reader.ReadInt64();
                texture.DiffuseHashA = reader.ReadInt32();
                texture.SpecularSourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                texture.SpecularHash = reader.ReadInt64();
                texture.SpecularHashA = reader.ReadInt32();
                texture.TintSourceIndex = new FigStringRef(_db, reader.ReadUInt32());
                texture.TintHash = reader.ReadInt64();
                texture.TintHashA = reader.ReadInt32();
                texture.A13 = reader.ReadInt32();
                texture.A14 = reader.ReadInt32();
                texture.A15 = reader.ReadInt32();
                texture.RawHex = Util.Hex(data, 0, data.Length, null, 16, false);
                Records.Add(texture);
            }
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;
            for (var i = 0; i < Records.Count; i++)
            {
                Unk8 texture = Records[i];
                writer.Write((System.UInt32)texture.DiffuseSourceIndex);
                writer.Write(texture.DiffuseHash);
                writer.Write(texture.DiffuseHashA);
                writer.Write((System.UInt32)texture.SpecularSourceIndex);
                writer.Write(texture.SpecularHash);
                writer.Write(texture.SpecularHashA);
                writer.Write((System.UInt32)texture.TintSourceIndex);
                writer.Write(texture.TintHash);
                writer.Write(texture.TintHashA);
                writer.Write(texture.A13);
                writer.Write(texture.A14);
                writer.Write(texture.A15);
            }

            var endPos = writer.BaseStream.Position;

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((System.UInt32)Records.Count);
            writer.Write((System.UInt32)(Records.Count > 0 ? pos : 0));
            var size = (System.UInt32)(endPos - pos);
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }


    public class Unk8 : FigRecord
    {
        public System.Int32 Index { get; }
        public FigStringRef DiffuseSourceIndex { get; set; }
        public System.Int64 DiffuseHash { get; set; }
        public System.Int32 DiffuseHashA { get; set; }


        public FigStringRef SpecularSourceIndex { get; set; }
        public System.Int64 SpecularHash { get; set; }
        public System.Int32 SpecularHashA { get; set; }


        public FigStringRef TintSourceIndex { get; set; }
        public System.Int64 TintHash { get; set; }
        public System.Int32 TintHashA { get; set; }


        public System.Int32 A13 { get; set; }
        public System.Int32 A14 { get; set; }
        public System.Int32 A15 { get; set; }
        public Unk8(FigleafDB db, System.Int32 index) : base(db) => Index = index;
        public System.String RawHex { get; set; }
    }
}

