﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;

public class MythicTexture
{
    public ColorTexture ColorTextureObject;
    public MaskTexture MaskTextureObject;

    public class ColorTexture
    {
        public byte[] Magic = new byte[4];
        public uint FileVersion;
        public uint FileSize;
        public uint ID;         // 0x0C
        public ushort Width;    // 0x10
        public ushort Height;   // 0x12
        public uint ImageCount; // 0x14
        public List<MipMap> MipMaps = new List<MipMap>();
        public uint Unk1;       // 0x90
        public ushort Unk2;     // 0x94
        public ushort Unk3;     // 0x96
        public ushort Unk4;     // 0x98
        public ushort Unk5;     // 0x9A
        public uint MaskID;     // 0x9C
        public uint Unk6;       // 0xA0
        public uint Unk7;       // 0xA4

        public class MipMap
        {
            public uint FileOffset;
            public ushort Width;
            public ushort Height;
            public ushort PixelOffsetX;
            public ushort PixelOffsetY;

            public int HeaderOffset;
            public int DataOffset;
            public int DataSize;

            public int BlocksY;
            public int BlocksX;
            public int BlockCount;

            public Bitmap Image;

            public void ReadHeader(BinaryReader reader, int miplevel)
            {
                HeaderOffset = (miplevel * 0x0C) + 0x18;
                reader.BaseStream.Seek(HeaderOffset, SeekOrigin.Begin);

                FileOffset = reader.ReadUInt32();
                Width = reader.ReadUInt16();
                Height = reader.ReadUInt16();
                PixelOffsetX = reader.ReadUInt16();
                PixelOffsetY = reader.ReadUInt16();

                if (FileOffset > 0)
                {
                    DataOffset = HeaderOffset + (int)FileOffset;
                    BlocksX = Width / 4;
                    BlocksY = Height / 4;
                    BlockCount = BlocksX * BlocksY;
                    DataSize = BlockCount * 8;
                }
                else
                {
                    DataOffset = 0;
                    BlocksX = 0;
                    BlocksY = 0;
                    BlockCount = 0;
                    DataSize = 0;
                }

                Image = new Bitmap(4, 4);
                Graphics.FromImage(Image).Clear(Color.FromArgb(255, 255, 0, 255));
            }

            public void ReadData(BinaryReader reader)
            {
                if (DataOffset == 0)
                    return;

                reader.BaseStream.Seek(DataOffset, SeekOrigin.Begin);

                ushort size = Math.Max((ushort)(Width), (ushort)(Height));

                if (size == 0)
                {
                    return;
                }

                Image = new Bitmap(size, size);
                Graphics.FromImage(Image).Clear(Color.FromArgb(255, 255, 0, 255));

                int blocksy = Height / 4;
                int blocksx = Width / 4;

                for (int y = 0; y < blocksy; ++y)
                {
                    for (int x = 0; x < blocksx; ++x)
                    {
                        DecompressBlockDXT1(x, y, 0, 0, reader.ReadBytes(8));
                    }
                }
            }

            private void DecompressBlockDXT1(int x, int y, int offsetx, int offsety, byte[] blockStorage)
            {
                if (blockStorage.Length != 8)
                {
                    for (int j = 0; j < 4; ++j)
                    {
                        for (int i = 0; i < 4; ++i)
                        {
                            Image.SetPixel(offsetx + (x * 4) + i, offsety + (y * 4) + j, Color.FromArgb(0, 0, 0, 0));
                        }
                    }
                    return;
                }

                ushort color0 = (ushort)(blockStorage[0] | blockStorage[1] << 8);
                ushort color1 = (ushort)(blockStorage[2] | blockStorage[3] << 8);

                int temp;
                temp = (color0 >> 11) * 255 + 16;
                byte r0 = (byte)((temp / 32 + temp) / 32);
                temp = ((color0 & 0x07E0) >> 5) * 255 + 32;
                byte g0 = (byte)((temp / 64 + temp) / 64);
                temp = (color0 & 0x001F) * 255 + 16;
                byte b0 = (byte)((temp / 32 + temp) / 32);

                temp = (color1 >> 11) * 255 + 16;
                byte r1 = (byte)((temp / 32 + temp) / 32);
                temp = ((color1 & 0x07E0) >> 5) * 255 + 32;
                byte g1 = (byte)((temp / 64 + temp) / 64);
                temp = (color1 & 0x001F) * 255 + 16;
                byte b1 = (byte)((temp / 32 + temp) / 32);

                uint code = (uint)(blockStorage[4] | blockStorage[5] << 8 | blockStorage[6] << 16 | blockStorage[7] << 24);

                for (int j = 0; j < 4; ++j)
                {
                    for (int i = 0; i < 4; ++i)
                    {
                        Color finalColor = Color.FromArgb(0);
                        byte positionCode = (byte)((code >> 2 * (4 * j + i)) & 0x03);

                        if (color0 > color1)
                        {
                            switch (positionCode)
                            {
                                case 0:
                                    finalColor = Color.FromArgb(255, r0, g0, b0);
                                    break;
                                case 1:
                                    finalColor = Color.FromArgb(255, r1, g1, b1);
                                    break;
                                case 2:
                                    finalColor = Color.FromArgb(255, (2 * r0 + r1) / 3, (2 * g0 + g1) / 3, (2 * b0 + b1) / 3);
                                    break;
                                case 3:
                                    finalColor = Color.FromArgb(255, (r0 + 2 * r1) / 3, (g0 + 2 * g1) / 3, (b0 + 2 * b1) / 3);
                                    break;
                            }
                        }
                        else
                        {
                            switch (positionCode)
                            {
                                case 0:
                                    finalColor = Color.FromArgb(255, r0, g0, b0);
                                    break;
                                case 1:
                                    finalColor = Color.FromArgb(255, r1, g1, b1);
                                    break;
                                case 2:
                                    finalColor = Color.FromArgb(255, (r0 + r1) / 2, (g0 + g1) / 2, (b0 + b1) / 2);
                                    break;
                                case 3:
                                    finalColor = Color.FromArgb(0, 0, 0, 0);
                                    break;
                            }
                        }

                        Image.SetPixel(offsetx + (x * 4) + i, offsety + (y * 4) + j, finalColor);
                    }
                }
            }
        }

        public void ReadHeader(BinaryReader reader)
        {
            Magic = reader.ReadBytes(4);
            FileVersion = reader.ReadUInt32();
            FileSize = reader.ReadUInt32();
            ID = reader.ReadUInt32();
            Width = reader.ReadUInt16();
            Height = reader.ReadUInt16();
            ImageCount = reader.ReadUInt32();

            for (int i = 0; i <= 0x0A; ++i)
            {
                MipMap mip = new MipMap();
                MipMaps.Add(mip);

                MipMaps[i].ReadHeader(reader, i);
            }

            //for (int i = 0; i < 0x0A - 1; i++)
            //{
            //    MipMaps[i].DataSize = MipMaps[i + 1].DataOffset - MipMaps[i].DataOffset;
            //}

            //MipMaps[(int)ImageCount - 1].DataSize = (int)FileSize - MipMaps[(int)0x0A - 1].DataOffset;

            //offset = 0x90;
            //reader.BaseStream.Seek(offset, SeekOrigin.Begin);

            Unk1 = reader.ReadUInt32();
            Unk2 = reader.ReadUInt16();
            Unk3 = reader.ReadUInt16();
            Unk4 = reader.ReadUInt16();
            Unk5 = reader.ReadUInt16();
            MaskID = reader.ReadUInt32();
            Unk6 = reader.ReadUInt32();
            Unk7 = reader.ReadUInt32();
        }

        public void ReadData(BinaryReader reader)
        {
            for (int i = 0; i < ImageCount; i++)
            {
                MipMaps[i].ReadData(reader);
            }
        }
    }

    public class MaskTexture
    {
        public byte[] Magic = new byte[4];
        public uint FileVersion;
        public uint FileSize;
        public uint ID;
        public ushort Width;
        public ushort Height;
        public uint ImageCount;
        public List<MaskMipMap> MaskMipMaps = new List<MaskMipMap>();

        public class MaskMipMap
        {
            public uint FileOffset;
            public ushort Width;
            public ushort Height;
            public ushort PixelOffsetX;
            public ushort PixelOffsetY;

            public int HeaderOffset;
            public int DataOffset;
            public int DataSize;

            public int BlocksY;
            public int BlocksX;
            public int BlockCount;

            public Bitmap Image;

            public void ReadHeader(BinaryReader reader, int miplevel)
            {
                HeaderOffset = (miplevel * 0x0C) + 0x18;
                reader.BaseStream.Seek(HeaderOffset, SeekOrigin.Begin);

                FileOffset = reader.ReadUInt32();
                Width = reader.ReadUInt16();
                Height = reader.ReadUInt16();
                PixelOffsetX = reader.ReadUInt16();
                PixelOffsetY = reader.ReadUInt16();

                if (FileOffset > 0)
                {
                    DataOffset = HeaderOffset + (int)FileOffset;
                    BlocksX = Width / 4;
                    BlocksY = Height / 4;
                    BlockCount = BlocksX * BlocksY;
                    DataSize = BlockCount * 8;
                }
                else
                {
                    DataOffset = 0;
                    BlocksX = 0;
                    BlocksY = 0;
                    BlockCount = 0;
                    DataSize = 0;
                }

                Image = new Bitmap(4, 4);
                Graphics.FromImage(Image).Clear(Color.FromArgb(255, 255, 0, 255));
            }

            public void ReadData(BinaryReader reader)
            {
                reader.BaseStream.Seek(0xb0, SeekOrigin.Begin);

                //ushort size = Math.Max((ushort)(Width + PixelOffsetX), (ushort)(Height + PixelOffsetY));

                //if (size == 0)
                //{
                //    return;
                //}

                Image = new Bitmap(Width, Height);
                Graphics.FromImage(Image).Clear(Color.FromArgb(255, 255, 0, 255));

                // byte spuriousbyte = reader.ReadByte();
                //spuriousbyte = reader.ReadByte();
                //spuriousbyte = reader.ReadByte();
                //spuriousbyte = reader.ReadByte();
                // Debug.WriteLine("DataOffset=" + DataOffset.ToString() + " DataSize=" + DataSize.ToString() + " Width=" + Width.ToString() + " Height=" + Height.ToString());// + " SpuriousByte=" + spuriousbyte.ToString());


                for (int y = 0; y < Height / 4; ++y)
                {
                    for (int x = 0; x < Width / 4; ++x)
                    {
                        DecompressBlockAlphaDxt5(x, y, PixelOffsetX, PixelOffsetY, reader.ReadBytes(8));
                    }
                }
            }

            private void DecompressBlockAlphaDxt5(int x, int y, int offsetx, int offsety, byte[] blockStorage)
            {
                if (blockStorage.Length != 8)
                {
                    for (int j = 0; j < 4; ++j)
                    {
                        for (int i = 0; i < 4; ++i)
                        {
                            Image.SetPixel(offsetx + (x * 4) + i, offsety + (y * 4) + j, Color.FromArgb(255, 255, 0, 0));
                        }
                    }
                    return;
                }

                // get the two alpha values
                int alpha0 = blockStorage[0];
                int alpha1 = blockStorage[1];

                // compare the values to build the codebook
                byte[] codes = new byte[8];
                codes[0] = (byte)alpha0;
                codes[1] = (byte)alpha1;

                if (alpha0 <= alpha1)
                {   /* use 5-alpha codebook */
                    for (int i = 1; i < 5; ++i)
                    {
                        codes[1 + i] = (byte)(((5 - i) * alpha0 + i * alpha1) / 5);
                    }
                    codes[6] = 0;
                    codes[7] = 255;
                }
                else
                {   /* use 7-alpha codebook */
                    for (int i = 1; i < 7; ++i)
                    {
                        codes[1 + i] = (byte)(((7 - i) * alpha0 + i * alpha1) / 7);
                    }
                }

                byte[] indices = new byte[16];
                int src = 2;
                int dest = 0;

                for (int i = 0; i < 2; ++i) /* decode the indices */
                {
                    int value = 0;
                    for (int j = 0; j < 3; ++j) /* grab 3 bytes */
                    {
                        int byte_data = (int)blockStorage[src++];
                        value |= (byte_data << 8 * j);
                    }


                    for (int j = 0; j < 8; ++j)    /* unpack 8 3-bit values from it */
                    {
                        int index = (value >> 3 * j) & 0x7;
                        indices[dest++] = (byte)index;
                    }
                }

                for (int j = 0; j < 4; ++j) /* write out the indexed codebook values */
                {
                    for (int i = 0; i < 4; ++i)
                    {
                        byte gr = codes[indices[i * j + i]];
                        Image.SetPixel(offsetx + (x * 4) + i, offsety + (y * 4) + j, Color.FromArgb(0xff, gr, gr, gr));
                    }
                }
            }

            private void DecompressBlockDXT1(int x, int y, int offsetx, int offsety, byte[] blockStorage)
            {
                if (blockStorage.Length != 8)
                {
                    for (int j = 0; j < 4; ++j)
                    {
                        for (int i = 0; i < 4; ++i)
                        {
                            Image.SetPixel(offsetx + (x * 4) + i, offsety + (y * 4) + j, Color.FromArgb(255, 255, 0, 0));
                        }
                    }
                    return;
                }

                ushort color0 = (ushort)(blockStorage[0] | blockStorage[1] << 8);
                ushort color1 = (ushort)(blockStorage[2] | blockStorage[3] << 8);


                int temp;

                temp = (color0 >> 11) * 255 + 16;
                byte r0 = (byte)((temp / 32 + temp) / 32);
                temp = ((color0 & 0x07E0) >> 5) * 255 + 32;
                byte g0 = (byte)((temp / 64 + temp) / 64);
                temp = (color0 & 0x001F) * 255 + 16;
                byte b0 = (byte)((temp / 32 + temp) / 32);

                temp = (color1 >> 11) * 255 + 16;
                byte r1 = (byte)((temp / 32 + temp) / 32);
                temp = ((color1 & 0x07E0) >> 5) * 255 + 32;
                byte g1 = (byte)((temp / 64 + temp) / 64);
                temp = (color1 & 0x001F) * 255 + 16;
                byte b1 = (byte)((temp / 32 + temp) / 32);

                uint code = (uint)(blockStorage[4] | blockStorage[5] << 8 | blockStorage[6] << 16 | blockStorage[7] << 24);

                for (int j = 0; j < 4; ++j)
                {
                    for (int i = 0; i < 4; ++i)
                    {
                        Color finalColor = Color.FromArgb(0);
                        byte positionCode = (byte)((code >> 2 * (4 * j + i)) & 0x03);

                        if (color0 > color1)
                        {
                            switch (positionCode)
                            {
                                case 0:
                                    finalColor = Color.FromArgb(255, r0, g0, b0);
                                    break;
                                case 1:
                                    finalColor = Color.FromArgb(255, r1, g1, b1);
                                    break;
                                case 2:
                                    finalColor = Color.FromArgb(255, (2 * r0 + r1) / 3, (2 * g0 + g1) / 3, (2 * b0 + b1) / 3);
                                    break;
                                case 3:
                                    finalColor = Color.FromArgb(255, (r0 + 2 * r1) / 3, (g0 + 2 * g1) / 3, (b0 + 2 * b1) / 3);
                                    break;
                            }
                        }
                        else if (color0 == color1 && color0 == 0)
                        {
                            finalColor = Color.FromArgb(1, 0, 0, 1);
                        }
                        else
                        {
                            switch (positionCode)
                            {
                                case 0:
                                    finalColor = Color.FromArgb(255, r0, g0, b0);
                                    break;
                                case 1:
                                    finalColor = Color.FromArgb(255, r1, g1, b1);
                                    break;
                                case 2:
                                    finalColor = Color.FromArgb(255, (r0 + r1) / 2, (g0 + g1) / 2, (b0 + b1) / 2);
                                    break;
                                case 3:
                                    finalColor = Color.FromArgb(0, 0, 0, 0);
                                    break;
                            }
                        }
                        Image.SetPixel(offsetx + (x * 4) + i, offsety + (y * 4) + j, finalColor);// Color.FromArgb(0xff, r1, g1, b1));
                    }
                }
            }
        }

        public void ReadHeader(BinaryReader reader)
        {
            Magic = reader.ReadBytes(4);
            FileVersion = reader.ReadUInt32();
            FileSize = reader.ReadUInt32();
            ID = reader.ReadUInt32();
            Width = reader.ReadUInt16();
            Height = reader.ReadUInt16();
            ImageCount = reader.ReadUInt32();

            for (int i = 0; i <= 0xA; ++i)
            {
                MaskMipMap mip = new MaskMipMap();
                MaskMipMaps.Add(mip);

                MaskMipMaps[i].ReadHeader(reader, i);
            }

            for (int i = 0; i < ImageCount - 1; i++)
            {
                MaskMipMaps[i].DataSize = MaskMipMaps[i + 1].DataOffset - MaskMipMaps[i].DataOffset;
            }

            MaskMipMaps[(int)ImageCount - 1].DataSize = (int)FileSize - MaskMipMaps[(int)ImageCount - 1].DataOffset;
        }

        public void ReadData(BinaryReader reader)
        {
            for (int i = 0; i < ImageCount; i++)
            {
                MaskMipMaps[i].ReadData(reader);
            }
        }
    }

    public void Load(Stream colortexturestream)
    {
        LoadColorTexture(colortexturestream);
    }

    public void Load(Stream colortexturestream, Stream masktexturestream)
    {
        LoadMaskTexture(masktexturestream);
        LoadColorTexture(colortexturestream);
    }

    public void Load(string colortexturefilename)
    {
        FileStream colortexturestream = null;

        if (colortexturefilename != "")
        {
            colortexturestream = File.OpenRead(colortexturefilename);
        }

        if (colortexturestream != null)
        {
            LoadColorTexture(colortexturestream);
        }

        if (colortexturestream != null)
        {
            colortexturestream.Close();
        }
    }

    public void Load(string colortexturefilename, string masktexturefilename)
    {
        FileStream masktexturestream = null;

        if (masktexturefilename != "")
        {
            masktexturestream = File.OpenRead(masktexturefilename);
        }

        if (masktexturestream != null)
        {
            LoadMaskTexture(masktexturestream);
        }

        if (masktexturestream != null)
        {
            masktexturestream.Close();
        }

        FileStream colortexturestream = null;

        if (colortexturefilename != "")
        {
            colortexturestream = File.OpenRead(colortexturefilename);
        }

        if (colortexturestream != null)
        {
            LoadColorTexture(colortexturestream);
        }

        if (colortexturestream != null)
        {
            colortexturestream.Close();
        }
    }

    public void LoadMaskTexture(Stream stream)
    {
        BinaryReader reader = new BinaryReader(stream);
        MaskTextureObject = new MaskTexture();
        MaskTextureObject.ReadHeader(reader);
        MaskTextureObject.ReadData(reader);
    }

    public void LoadColorTexture(Stream stream)
    {
        BinaryReader reader = new BinaryReader(stream);
        ColorTextureObject = new ColorTexture();
        ColorTextureObject.ReadHeader(reader);
        ColorTextureObject.ReadData(reader);
    }
}
