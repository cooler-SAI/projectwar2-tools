﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            textBox1.Text = @"C:\Users\r34va\Desktop\test.diffuse";
            textBox2.Text = @"D:\Games\Warhammer Online - Age of Reckoning\myps\art2.myp\assetdb\decals\0.fg.0.0.all_bit_bloodletter_trophy_01.@.@.mask";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Diffuse files (*.diffuse)|*.diffuse|Specular files (*.specular)|*.specular|All files (*.*)|*.*";
            openFileDialog1.ShowDialog();
            textBox1.Text = openFileDialog1.FileName;
            Debug.WriteLine(openFileDialog1.FileName);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Mask files (*.mask)|*.mask|All files (*.*)|*.*";
            openFileDialog1.ShowDialog();
            textBox2.Text = openFileDialog1.FileName;
            Debug.WriteLine(openFileDialog1.FileName);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("");
            Debug.WriteLine("");


            MythicTexture texture = new MythicTexture();
            texture.Load(textBox1.Text, textBox2.Text);


            //FileStream stream3 = File.OpenWrite(openFileDialog1.FileName + ".png");
            //texture.MipMaps[(int)texture.ImageCount - 1].Image.Save(stream2, System.Drawing.Imaging.ImageFormat.Png);
            //stream2.Close();

            int mip = (int)texture.ColorTextureObject.ImageCount - 1;

           // pictureBox1.Width = texture.ColorTextureObject.MipMaps[mip].Image.Width;
            //pictureBox1.Height = texture.ColorTextureObject.MipMaps[mip].Image.Height;
            pictureBox1.Image = texture.ColorTextureObject.MipMaps[mip].Image;

            mip--;

            //pictureBox2.Width = texture.MaskTextureObject.MaskMipMaps[mip].Image.Width;
            //pictureBox2.Height = texture.MaskTextureObject.MaskMipMaps[mip].Image.Height;
            pictureBox2.Image = texture.MaskTextureObject.MaskMipMaps[mip].Image;

            mip--;

            //pictureBox3.Width = texture.ColorTextureObject.MipMaps[mip].Image.Width;
            //pictureBox3.Height = texture.ColorTextureObject.MipMaps[mip].Image.Height;
            pictureBox3.Image = texture.ColorTextureObject.MipMaps[mip].Image;

            Debug.WriteLine("");
            Debug.WriteLine("");
        }

    }
}
