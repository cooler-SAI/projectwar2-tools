#pragma once
#pragma pack(push, 1)

#include "Core.h"

#include <map>
#include <vector>
#include <string>
#include "resource.h"

namespace  WarhammerOnline::DataMining
{
    enum class BlockType : signed int
    {
        Unknown = 0,
        Header = 1,
        FileTable = 2,
        Asset = 3,
        Free = 4
    };

    class Myp
    {
    public:

        struct Header
        {
            uint32_t Magic;
            uint32_t Unk1;
            uint32_t Unk2;
            int64_t FileTableOffset;
            uint32_t EntriesPerFile;
            uint32_t FileCount;
            uint32_t Unk5;
            uint32_t Unk6;
            unsigned char Padding[0x1DC];
        };

        struct FileTableHeader
        {
            uint32_t FileCount;
            int64_t NextFileOffset;
        };

        struct FileTableEntry
        {
            int64_t Offset;
            uint32_t HeaderSize;
            uint32_t CompressedSize;
            uint32_t DecompressedSize;
            int64_t Hash;
            uint32_t CRC32;
            unsigned char IsCompressed;
            unsigned char Unk1;
        };

        struct FileTableEntryModification
        {
            uint8_t Compressed;
            int64_t Hash;
            std::string Name;
            uint32_t DataLength;
            byte* HeaderData;
            uint32_t HeaderDataLength;
            FileTableEntry* Entry;
            int64_t FileOffset;
            uint32_t CRC32;
        };

        struct FileBlock
        {
            BlockType Type;
            void* Record;
            int64_t Offset;
            int64_t Length;
        };

    private:

        static HMODULE m_InstanceHandle;

        sfs::path m_Filename;
        int64_t m_FileSize;

        std::ifstream m_InputStream;
        std::ofstream m_OutputStream;
        
        PackageState m_State;

        bool m_FreeBlocksDirty;
        bool m_UsedBlocksDirty;

        Header m_Header;
        uint32_t m_EntryCount;
        uint32_t m_FileCount;

        std::vector<FileBlock> m_UsedBlocks;
        std::vector<FileBlock> m_FreeBlocks;

        std::vector<FileTableHeader> m_FileTableHeaders;
        std::vector<int64_t> m_FileTableHeaderTOC;

        std::vector<FileTableEntry> m_FileTableEntries;
        std::vector<int64_t> m_FileTableEntriesTOC;

        std::map<int64_t, std::string> m_HashDictionary;

    public:

        Myp();
        Myp(const std::string &filename);

        ~Myp();

        bool Create(const std::string &filename = "");
        bool Open(const std::string &filename = "");
        bool Load();
        bool Read();
        bool Save();
        bool Close();
        bool Destroy();

        bool ValidateState(PackageState required);

        bool GetAssetMetaData(const FileTableEntry* entry, uint8_t* dstBuffer, int destOffset = 0);
        bool GetAssetData(const FileTableEntry* entry, uint8_t* dstBuffer, int destOffset = 0, bool decompress = true);

        inline size_t GetTotalFileTableEntries() { return m_FileTableEntries.size(); }
        const std::vector<FileTableEntry> &GetFileTableEntries();

        inline size_t GetTotalTOCFileTableEntries() { return m_FileTableEntries.size(); }
        inline const std::vector<int64_t> &GetTOCFileTableEntries();

        FileTableEntry* GetAsset(const std::string &name);
        FileTableEntry* GetAsset(const int64_t &hash);

        bool HasAsset(const std::string &name);
        bool HasAsset(const int64_t &hash);

        bool UpdateAsset(const std::string &name, const uint8_t* data, uint32_t dataLength, bool compressed, uint8_t* headerData = 0, uint32_t headerDataLength = 0, uint32_t overrideCRC32 = 0);
        bool UpdateAsset(const int64_t &hash, const uint8_t* data, uint32_t dataLength, bool compressed, uint8_t* headerData = 0, uint32_t headerDataLength = 0, uint32_t overrideCRC32 = 0);

        bool AddAsset(const std::string &name, const uint8_t* data, uint32_t dataLength, bool compressed, uint8_t* headerData = 0, uint32_t headerDataLength = 0, uint32_t overrideCRC32 = 0);
        bool AddAsset(const int64_t &hash, const uint8_t* data, uint32_t dataLength, bool compressed, uint8_t* headerData = 0, uint32_t headerDataLength = 0, uint32_t overrideCRC32 = 0);

        inline int64_t GetAssetFileSize(const std::string &name) { return GetAssetFileSize(GetAssetHashFromFilename(name)); }
        inline int64_t GetAssetFileSize(const int64_t hash) { return GetAssetFileSize(hash); }
        int64_t GetAssetFileSize(const FileTableEntry *entry);

        std::string GetAssetDataString(const std::string &name);
        std::string GetAssetDataString(const int64_t &hash);
        std::string GetAssetDataString(const FileTableEntry *entry);

        void DeleteAsset(const std::string &name);
        void DeleteAsset(const int64_t hash);
        void DeleteAsset(const FileTableEntry *entry);

        inline int64_t GetFileCount() const { return m_FileCount; }
        inline uint32_t GetEntryCount() const { return m_Header.FileCount; }
        inline std::string GetFilename() const { return m_Filename.string(); }
        inline int64_t GetFileSize() const { return m_FileSize; }

        inline PackageState &SetState(PackageState state) { return m_State &= state; }
        inline PackageState &ClearState(PackageState state) { return m_State &= ~state; }

        inline bool IsStateSet(PackageState state) const { return static_cast<bool>(state & m_State); }
        inline bool IsStateClear(PackageState state) const { return !static_cast<bool>(state & m_State); }
        
        inline bool IsCreated() const { return IsStateSet(PackageState::Created); }
        inline bool IsOpened() const { return IsStateSet(PackageState::Opened); }
        inline bool IsLoaded() const { return IsStateSet(PackageState::Loaded); }
        inline bool IsRead() const { return IsStateSet(PackageState::Read); }
        inline bool IsSaved() const { return IsStateSet(PackageState::Saved); }
        inline bool IsClosed() const { return IsStateSet(PackageState::Closed); }
        inline bool IsDeleted() const { return IsStateSet(PackageState::Deleted); }

        int64_t GetAssetHashFromFilename(const std::string &name);
        int64_t CreateAssetHashFromFilename(const std::string &name);
        std::string GetFilenameFromAssetHash(const int64_t &hash);

        bool UpdateStates(PackageState aStateToCheckIsSet, PackageState aStateSetOnSuccess = PackageState::Inactive, PackageState aStateSetOnFailure = PackageState::Inactive, std::string aSuccessgMessage = "", std::string aFailureLogMessage = "");
        void Reset(std::string aLogMessage);

        bool LoadHashDictionary(const std::string &name);

        static void SetInstanceHandle(HMODULE handle) { m_InstanceHandle = handle; }

    private:

        bool WriteHeader();

        void SortFreeBlocks();
        void SortUsedBlocks();

        void MarkBlockUsed(int64_t offset, int64_t length, BlockType type, void* recordPtr);
        void MarkBlockUsed(FileBlock&& block);

        void MarkBlockFree(int64_t offset, int64_t length);
        void MarkBlockFree(FileBlock&& block);

        void CalculateFreeBlocks();

        bool GetFileTypeCompressable(const uint8_t* data, uint32_t dataLength, bool defaultValue = true);
        bool GetFileTypeCompressable(std::filesystem::path filename, bool defaultValue = true);

        FileTableEntry* FindOrCreateFileEntry();
        FileBlock GetFreeBlock(int64_t size);
    };
};

#pragma pack(pop)