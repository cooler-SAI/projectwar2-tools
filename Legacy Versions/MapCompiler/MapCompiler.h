
#pragma once

#include "NIF.h"
#include "Fixture.h"
#include "Zone.h"
#include "Region.h"
#include "Terrain.h"
#include "Debug.h"
#include "Writer.h"
#include "Config.h"
#include "Serializer.h"
#include "BSPCompiler.h"

#include <string>

#include "niflib.h"
#include "obj/NiObject.h"
#include "obj/NiNode.h"
#include "obj/NiAVObject.h"
#include "obj/NiTriShape.h"
#include "obj/NiTriShapeData.h"
#include "obj/NiTextureProperty.h"
#include <crtdbg.h>

using namespace System;
using namespace System::Collections::Generic;
using namespace WarZoneLib;
using namespace System::Runtime::InteropServices;
public ref class MapCompiler
{
public:

	MapCompiler()
		: _errorCount(0), _lineNumber(0)
	{
		_config = gcnew Config();
		_chunkSizes = gcnew array<long>((int)ChunkType::Count);
		_nifs = 0;
	}

	int Compile(String ^configPath, String ^outputPath, int region)
	{
		// Write to a temp path to avoid corrupting existing file
		String ^tempFilePath = Utility::GetTempOutputPath(outputPath);

		FileStream ^outStream = nullptr;

		try
		{
			_config->Region = region.ToString();

			//_config->Region = "11,109,103";
			LoadConfig(configPath);
			CmdRegion(_config->Region);
			_errorCount = 0;

			if (_region != nullptr && _region->Zones != nullptr)
			{
				Console::WriteLine("Compiling " + outputPath);
				if (_config->MypsPath == "")
					CompilerError("MypsPath must be present in " + configPath);

				if (_errorCount != 0)
				{
					BuildFailure();
					return -1;
				}

				if (Directory::Exists(_config->MypsPath) && MYPManager::Instance == nullptr)
				{
					MYPManager::Instance = gcnew MYPManager(_config->MypsPath);
				}



				LoadFixtureFilters();

				outStream = File::Open(tempFilePath, FileMode::CreateNew, FileAccess::Write, FileShare::Write);
				Writer ^writer = gcnew Writer(outStream);
				WriteHeader(writer);


				try
				{
					CompileRegion(_region);
				}
				catch (Exception ^e)
				{
					CompilerError("Failed to compile region '" + _region->ID + "': " + e->Message);
					_region->ErrorCount++;
				}

				try
				{

					WriteRegion(writer, _region);
				}
				catch (Exception ^e)
				{
					CompilerError("Failed to write region '" + _region->ID + ": " + e->Message);
					_region->ErrorCount++;
				}
			}

		}
		finally
		{
			if (outStream != nullptr)
				outStream->Close();
		}

		if (_errorCount == 0 || !_config->StrictMode)
		{
			BuildSuccess();
			if (File::Exists(outputPath))
				File::Delete(outputPath);
			if (File::Exists(tempFilePath))
				File::Move(tempFilePath, outputPath);
			if (File::Exists(tempFilePath))
				File::Delete(tempFilePath);
		}
		else
		{
			BuildFailure();
			File::Delete(tempFilePath);
		}

		return 0;
	}

private:
	void CompileRegion(Region ^region)
	{
		List<Zone^>^ toRemove = gcnew List<Zone^>();

		for each(Zone^ zone in region->Zones->Values)
		{
			if (!CompileZone(zone))
				toRemove->Add(zone);
		}

		for each(Zone^ zone in toRemove)
		{
			region->Zones->Remove(zone->ID);
		}
	}

	bool CompileZone(Zone ^zone)
	{
		if (_nifs != 0)
			delete _nifs;

		_nifs = new std::map<int, Niflib::NiObjectRef>();

		zone->Collision = gcnew Collision();

		String ^directory = "zones/zone" + zone->ID.ToString()->PadLeft(3, '0');

		Console::WriteLine("Compiling zone " + zone->ID + " [" + zone->Name + "] ... ");

		auto nifspath = directory + "/nifs.csv";
		auto fixturesspath = directory + "/fixtures.csv";

		auto nifscsv = MYPManager::Instance->GetAssetStream(nifspath);
		if (nifscsv == nullptr)
		{
			CompilerError(nifspath + " not found in MYP archives");
			return false;
		}

		auto fixturescsv = MYPManager::Instance->GetAssetStream(fixturesspath);
		if (nifscsv == nullptr)
		{
			CompilerError(fixturesspath + " not found in MYP archives");
			return false;
		}

		LoadZoneNIFS(zone, nifspath, nifscsv);
		LoadZoneFixtures(zone, fixturesspath, fixturescsv);

		BuildZoneCollision(zone);

		zone->Terrain = gcnew Terrain();
		if (zone->Region->Config->ExportTerrain)
			zone->Terrain->Load(directory);




		Console::WriteLine("Building zone " + zone->ID + " BSP ... [BSPDepthCutoff=" + zone->Region->Config->BSPDepthCutoff + ", BSPTrianglesCutoff=" + zone->Region->Config->BSPTrianglesCutoff + "]");
		BSPCompiler ^bspCompiler = gcnew BSPCompiler(zone->Region->Config, zone->Collision);
		zone->Collision->BSP = bspCompiler->Compile();


		return true;
	}

	void LoadZoneNIFS(Zone ^zone, String^ path, Stream ^stream)
	{
		StreamReader ^reader = gcnew StreamReader(stream);
		String ^line;

		// Discard the first 2 lines, these are header info
		reader->ReadLine();
		reader->ReadLine();

		List<NIF ^> ^nifs = gcnew List<NIF ^>();

		while ((line = reader->ReadLine()) != nullptr)
		{
			line = line->Trim();
			array<String ^> ^tokens = line->Split(',');

			try
			{
				NIF ^nif = gcnew NIF();
				nif->ID = int::Parse(tokens[0]);
				nif->Name = tokens[1];
				nif->Filename = tokens[2];
				nif->MinAngle = float::Parse(tokens[9]);
				nif->MaxAngle = float::Parse(tokens[10]);
				nifs->Add(nif);
			}
			catch (Exception ^)
			{
				Console::WriteLine(path + ": error parsing line [tokens->Length=" + tokens->Length.ToString() + "]: " + line);
				// FIXME: make this an actual error
			}
		}

		zone->NIFS = nifs->ToArray();
	}

	void LoadFixtureFilters()
	{
		if (_config->FixtureFilter != nullptr && _config->FixtureFilter != "")
		{
			array<String ^> ^tokens = _config->FixtureFilter->Split(' ');
			for each(String^ zoneFilters in tokens)
			{
				array<String ^> ^fixtures = zoneFilters->Split(',');
				int zoneID = int::Parse(fixtures[0]);
				if (_region->Zones->ContainsKey(zoneID))
				{
					_region->Zones[zoneID]->FixtureFilter = gcnew List<int>();

					for (int i = 1; i<fixtures->Length; i++)
					{
						_region->Zones[zoneID]->FixtureFilter->Add(int::Parse(fixtures[i]));
					}
				}
			}
		}
	}

	void LoadZoneFixtures(Zone ^zone, String^ path, Stream ^stream)
	{
		StreamReader ^reader = gcnew StreamReader(stream);
		String ^line;

		// Discard the first 2 lines, these are header info
		reader->ReadLine();
		reader->ReadLine();

		List<Fixture ^> ^fixtures = gcnew List<Fixture ^>();

		Console::WriteLine("Loading fixtures for " + path + " ... ");

		while ((line = reader->ReadLine()) != nullptr)
		{
			line = line->Trim();
			array<String ^> ^tokens = line->Split(',');

			Fixture ^fixture = gcnew Fixture();
			fixture->Zone = zone;
			fixture->ID = int::Parse(tokens[0]);
			fixture->NIF = int::Parse(tokens[1]);
			fixture->Name = tokens[2];
			fixture->X = double::Parse(tokens[3]);
			fixture->Y = double::Parse(tokens[4]);
			fixture->Z = double::Parse(tokens[5]);
			fixture->A = double::Parse(tokens[6]);
			fixture->Scale = double::Parse(tokens[7]);
			fixture->Collide = int::Parse(tokens[8]);
			fixture->Radius = double::Parse(tokens[9]);

			fixture->UniqueID = int::Parse(tokens[14]);
			fixture->Angle3D = float::Parse(tokens[15]);
			fixture->Axis3D.X = float::Parse(tokens[16]);
			fixture->Axis3D.Y = float::Parse(tokens[17]);
			fixture->Axis3D.Z = float::Parse(tokens[18]);

			// Clamp A to NIF [MinAngle,MaxAngle] range
			NIF ^nif = zone->GetNIFByID(fixture->NIF);
			//fixture->A = Utility::Clamp(fixture->A, nif->MinAngle, nif->MaxAngle);
			//fixture->Angle3D = Utility::Clamp(fixture->Angle3D, Utility::ToRadians(nif->MinAngle), Utility::ToRadians(nif->MaxAngle));

			// Normalize Axis3D
			//if (fixture->Axis3D.LengthSquared > 0)
			//    fixture->Axis3D = fixture->Axis3D.Normalize();

			if (fixture->Collide != 2048 && fixture->Collide != 0)
				CompilerWarning("Fixture " + fixture->ID + " [" + fixture->Name + "] Collide=" + fixture->Collide);

			if (fixture->Name->ToLower()->Contains("vfx"))
				continue;

			if (zone->FixtureFilter != nullptr && !zone->FixtureFilter->Contains(fixture->UniqueID))
				continue;

			fixtures->Add(fixture);
		}

		zone->Fixtures = fixtures->ToArray();
	}

	void BuildZoneCollision(Zone ^zone)
	{
		Console::WriteLine("Computing collision triangles ... ");

		for (int i = 0; i < zone->Fixtures->Length; i++)
		{
			Fixture ^fixture = zone->Fixtures[i];

			if (fixture != nullptr && fixture->Collide != 0)
			{
				try
				{
					BuildFixtureCollision(fixture, zone);
				}
				catch (Exception ^e)
				{
					CompilerError("Failed to build collision for fixture " + fixture->Name + ": " + e->Message);
					zone->ErrorCount++;
				}
			}
		}


	}

	void BuildFixtureCollision(Fixture ^fixture, Zone ^zone)
	{
		_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);

		NIF ^nif = zone->GetNIFByID(fixture->NIF);

		if (nif == nullptr && zone->Region->Config->StrictMode)
		{
			CompilerError("Could not find NIF with ID=" + fixture->NIF);
			return;
		}

		// var nifPath = "assetdb/fixtures/fi.0.0." + file;

		String ^file = nif->Filename;
		String ^nifPath = "assetdb/fixtures/fi.0.0." + nif->Filename->ToLower();
		auto nifData = MYPManager::Instance->GetAsset(nifPath);

		if (nifData == nullptr)
		{
			nifPath = "assetdb/fixtures/it.0.0." + nif->Filename->ToLower();;
			nifData = MYPManager::Instance->GetAsset(nifPath);
		}

		if (nifData == nullptr && zone->Region->Config->StrictMode)
		{
			CompilerError("File '" + nifPath + "' does not exist");
			return;
		}

		Niflib::NiObjectRef root;
		Niflib::NiAVObjectRef object;
		//auto nif = GetNif
		int id = fixture->NIF;
		auto it = _nifs->find(id);
		if (it != _nifs->end())
		{
			root = it->second;
		}
		else
		{
			try
			{
				array<Byte>^ b = gcnew array<Byte>(nifData->Length);

				Buffer::BlockCopy(nifData, 0, b, 0, nifData->Length);

				pin_ptr<System::Byte> p = &b[0];
				unsigned char* pby = p;
				char* pch = reinterpret_cast<char*>(pby);


				std::stringstream ss;
				ss.write(pch, nifData->Length);
				ss.seekg(0);

				root = Niflib::ReadNifTree(ss);
				(*_nifs)[id] = root;
			}
			catch (...)
			{
			}
		}
		if (root && (object = Niflib::DynamicCast<Niflib::NiAVObject>(root)))
		{
			GatherCollisionTriangles(zone, object, fixture, fixture->TransformationMatrix, false, nifPath, "", 7);
		}
		else
		{
			String ^msg = "Failed to load NIF from '" + nifPath + "'";
			if (zone->Region->Config->StrictMode)
				CompilerError(msg);
			else
				CompilerWarning(msg);
		}
		Console::WriteLine(file + " " + fixture->ID.ToString());
	}

	void GatherCollisionTriangles(Zone ^zone, Niflib::NiAVObjectRef root, Fixture^ fixture, Matrix baseMatrix, bool inCollision, String ^nifPath, String ^parentNodePath, int instanceID)
	{
		String ^nodePath = parentNodePath + ":" + Utility::MarshalString(root->GetName());
		//	inCollision = true;
		if (zone->Region->Config->ExcludeList->Contains(Utility::MarshalString(root->GetName())->ToLower()))
		{
			CompilerInfo("Ignoring collision at " + nodePath + " [" + nifPath + "]");
			return;
		}

		if (!inCollision)
		{
			// We're in the collision subtree if this node is called 'collidee'
			inCollision = Utility::EqualsIgnoreCase(root->GetName(), "collidee");
		}
		else
		{
			//Nodes called 'nopick' can be present under a collidee node.
			// These nodes signify that everything below should not occlude LOS.
			inCollision = !Utility::EqualsIgnoreCase(root->GetName(), "nopick");
		}

		if (Utility::EqualsIgnoreCase(root->GetName(), "door001"))
		{
			instanceID = 0;
			inCollision = true;
		}
		else if (Utility::EqualsIgnoreCase(root->GetName(), "door002"))
		{
			instanceID = 1;
			inCollision = true;
		}
		else if (Utility::EqualsIgnoreCase(root->GetName(), "door003"))
		{
			instanceID = 2;
			inCollision = true;
		}
		else if (Utility::EqualsIgnoreCase(root->GetName(), "door004"))
		{
			instanceID = 3;
			inCollision = true;
		}
		else if (Utility::EqualsIgnoreCase(root->GetName(), "door005"))
		{
			instanceID = 4;
			inCollision = true;
		}
		else if (Utility::EqualsIgnoreCase(root->GetName(), "door006"))
		{
			instanceID = 5;
			inCollision = true;
		}


		Matrix localMatrix = GetLocalMatrix(root), worldMatrix = localMatrix * baseMatrix;

		if (inCollision)
		{
			Niflib::NiTriShapeRef triShape = Niflib::DynamicCast<Niflib::NiTriShape>(root);

			if (triShape)
			{
				Niflib::NiTriShapeDataRef triShapeData = Niflib::DynamicCast<Niflib::NiTriShapeData>(triShape->GetData());

				if (triShapeData)
				{
					unsigned int baseVertex = zone->Collision->GetVertexCount();

					std::vector<Niflib::Vector3> vertices(triShapeData->GetVertices());
					for (unsigned int i = 0; i < vertices.size(); i++)
					{
						Niflib::Vector3 p(vertices[i]);
						Vector3 q = worldMatrix.TransformPoint(Vector3(p.x, p.y, p.z));
						zone->Collision->AddVertex(q);
					}
					vertices.clear();

					std::vector<Niflib::Triangle> triangles(triShapeData->GetTriangles());

					UInt16 uniqueID = fixture->UniqueID;
					UInt16 zoneID = zone->ID;
					Byte instance = instanceID;

					UInt32 result = (UInt32)
						(
						(UInt32)((uniqueID & 0xC000) << 16) |
							(UInt32)((zoneID & 0x3FF) << 20) |
							(UInt32)((uniqueID & 0x3FFF) << 6) |
							(UInt32)(0x28 + instance)
							);

					for (unsigned int i = 0; i < triangles.size(); i++)
					{
						const Niflib::Triangle &t(triangles[i]);
						int index = zone->Collision->AddTriangle(Triangle(baseVertex + t.v1, baseVertex + t.v2, baseVertex + t.v3, result));
					}
					triangles.clear();
				}
			}
		}

		Niflib::NiNodeRef node = Niflib::DynamicCast<Niflib::NiNode>(root);

		if (node)
		{
			std::vector<Niflib::NiAVObjectRef> children(node->GetChildren());
			for (int i = 0; i < children.size(); i++)
				GatherCollisionTriangles(zone, children[i], fixture, worldMatrix, inCollision, nifPath, nodePath, instanceID);
		}
	}

	static Matrix MapCompiler::GetLocalMatrix(const Niflib::NiAVObject *object)
	{
		float s(object->GetLocalScale());
		Niflib::Matrix33 r(object->GetLocalRotation());
		Niflib::Vector3 t(object->GetLocalTranslation());

		Matrix mt(Matrix::Translation(t.x, t.y, t.z));
		Matrix ms(Matrix::Scaling(s, s, s));
		Matrix mr(Matrix::Identity);

		mr.M00 = r[0][0];
		mr.M01 = r[0][1];
		mr.M02 = r[0][2];
		mr.M10 = r[1][0];
		mr.M11 = r[1][1];
		mr.M12 = r[1][2];
		mr.M20 = r[2][0];
		mr.M21 = r[2][1];
		mr.M22 = r[2][2];

		return ms * mr * mt;
	}

	void WriteHeader(Writer ^writer)
	{
		const Byte FileMagic[] = { 'R', 'O', 'R' };
		const Byte FileVersion = 2;
		const UInt32 HeaderSize = 16;

		UInt32 fileSize = 0;
		UInt32 fileChecksum = 0;

		writer->Write(FileMagic[0]);
		writer->Write(FileMagic[1]);
		writer->Write(FileMagic[2]);
		writer->Write(FileVersion);
		writer->Write(HeaderSize);
		writer->Write(fileSize);
		writer->Write(fileChecksum);
	}

	void WriteChunk(Writer ^writer, ChunkType chunkType, MemoryStream ^stream)
	{
		long pos = (long)writer->BaseStream->Position;
		writer->Write((UInt32)chunkType);
		writer->Write((UInt32)stream->Length);
		writer->Write(stream->GetBuffer(), 0, (int)stream->Length);
		long numBytes = (long)(writer->BaseStream->Position - pos);
		_chunkSizes[(int)chunkType] += numBytes;
	}

	void WriteRegion(Writer ^writer, Region ^region)
	{
		Serializer ^serializer = gcnew Serializer();
		serializer->Config = region->Config;

		auto stream = gcnew MemoryStream();
		auto writer2 = gcnew Writer(stream);


		writer2->Write((UInt32)region->ID);
		writer2->Write((UInt32)region->Zones->Count);


		for each(Zone^ zone in region->Zones->Values)
		{
			writer2->Write((UInt32)zone->ID);
			writer2->Write((UInt32)zone->OffsetX);
			writer2->Write((UInt32)zone->OffsetY);
			if (zone->NIFS != nullptr)
			{
				writer2->Write((UInt32)zone->NIFS->Length);
				writer2->Write((UInt32)zone->Fixtures->Length);

				/*
				if (region->Config->ExportNIFS)
				{
				Console::WriteLine("Writing " + zone->NIFS->Length + " NIFS ... ");

				for (int j = 0; j < zone->NIFS->Length; j++)
				{
				MemoryStream ^stream;
				try
				{
				stream = serializer->Serialize(zone->NIFS[j]);
				}
				catch (Exception ^e)
				{
				String ^msg = "Failed to serialize NIF " + zone->NIFS[j]->Name + ": " + e->Message;

				if (region->Config->StrictMode)
				CompilerError(msg);
				else
				CompilerWarning(msg);

				stream = serializer->SerializeEmptyNIF(zone->NIFS[j]);
				}

				WriteChunk(writer, ChunkType::NIF, stream);
				}
				}

				if (region->Config->ExportFixtures)
				{
				Console::WriteLine("Writing " + zone->Fixtures->Length + " fixtures ... ");

				for (int k = 0; k < zone->Fixtures->Length; k++)
				{
				MemoryStream ^stream = serializer->Serialize(zone->Fixtures[k]);
				WriteChunk(writer, ChunkType::Fixture, stream);
				}
				}*/
			}
			else

			{
				writer2->Write((UInt32)0);
				writer2->Write((UInt32)0);
			}
		}


		WriteChunk(writer, ChunkType::Region, stream);



		for each(Zone^ zone in region->Zones->Values)
		{
			if (region->Config->ExportTerrain)
			{
				Console::WriteLine("Writing terrain ... ");
				auto stream = serializer->Serialize(zone, zone->Terrain);
				WriteChunk(writer, ChunkType::Terrain, stream);
			}

			Console::WriteLine("Writing collision geometry ...");
			WriteChunk(writer, ChunkType::Collision, serializer->Serialize(zone->ID, zone->Collision));

			Console::WriteLine("Writing BSP ...");
			WriteChunk(writer, ChunkType::BSP, serializer->Serialize(zone->ID, zone->Collision->BSP));
		}

	}

	void LoadConfig(String ^configPath)
	{
		StreamReader ^reader = gcnew StreamReader(configPath);

		_configPath = configPath;
		_lineNumber = 0;

		String ^line;
		while ((line = reader->ReadLine()) != nullptr)
		{
			++_lineNumber;

			line = line->Trim();

			if (line->Length > 0)
			{
				// Strip away any comment that might be on the line
				int i = 0;
				while (i < line->Length && line[i] != '#')
					i++;
				line = line->Substring(0, i);

				// Process the line
				if (line->Length > 0)
				{
					String ^cmd = Utility::GetToken(line);

					if (cmd != nullptr)
					{
						cmd = cmd->ToLower();

						if (cmd == "option")
						{
							String ^option = Utility::GetToken(line);
							CmdOption(option, line);
						}
						else if (cmd == "exclude")
						{
							String ^nodeName = nullptr;
							while ((nodeName = Utility::GetToken(line)) != nullptr && nodeName->Length > 0)
								CmdExclude(nodeName);
						}
						else
						{
							CompilerError("Unrecognized directive '" + cmd + "'");
						}
					}
				}
			}
		}
	}

	void CmdRegion(String ^regionInfo)
	{
		array<String ^> ^regionTokens = regionInfo->Split(',');



		Region ^region = gcnew Region();
		region->ID = int::Parse(regionTokens[0]);
		region->Config = gcnew Config(_config); // each region gets its own copy of the current config
		region->Zones = gcnew Dictionary<int, Zone^>();
		List<int>^ includeZoneIds = gcnew List<int>();

		if (regionTokens->Length > 1)
			for (int i = 1; i<regionTokens->Length; i++)
			{
				includeZoneIds->Add(int::Parse(regionTokens[i]));
			}

		if (!File::Exists(_config->ZoneDataPath))
		{
			region = region;
		}
		auto reader = gcnew StreamReader(_config->ZoneDataPath);

		while (!reader->EndOfStream)
		{
			String^ line = reader->ReadLine();
			if (line->Trim()->StartsWith("#"))
				continue;

			array<String ^> ^tokens = line->Split(',');
			int regionID = int::Parse(tokens[0]);
			int zoneID = int::Parse(tokens[1]);
			int x = int::Parse(tokens[2]);
			int y = int::Parse(tokens[3]);

			if (regionID == region->ID)
			{
				if ((includeZoneIds->Count == 0) || includeZoneIds->Contains(zoneID))
				{
					if (region->Zones == nullptr)
						region->Zones = gcnew Dictionary<int, Zone^>();

					Zone^ zone = gcnew Zone();
					zone->ID = zoneID;
					zone->Region = region;
					zone->OffsetX = x;
					zone->OffsetY = y;

					region->Zones[zoneID] = zone;
				}
			}
		}

		_region = region;
	}

	void CmdOption(String ^option, String ^value)
	{
		if (option == nullptr)
			throw gcnew ArgumentException();
		if (value == nullptr)
			throw gcnew ArgumentException();

		option = option->Trim();
		if (option->Length == 0)
			throw gcnew ArgumentException();

		if (String::Compare(option, "Strict", false) == 0)
		{
			ParseBoolean(_config->StrictMode, value);
		}
		else if (String::Compare(option, "MypsPath", true) == 0)
		{
			_config->MypsPath = value;
		}
		else if (String::Compare(option, "Region", true) == 0)
		{
			_config->Region = value;
		}
		else if (String::Compare(option, "ZoneDataPath", true) == 0)
		{
			_config->ZoneDataPath = value;
		}
		else if (String::Compare(option, "fixture_filter", true) == 0)
		{
			_config->FixtureFilter = value;
		}
		else if (String::Compare(option, "ExportNIFS", true) == 0)
		{
			ParseBoolean(_config->ExportNIFS, value);
		}
		else if (String::Compare(option, "ExportTerrain", true) == 0)
		{
			ParseBoolean(_config->ExportTerrain, value);
		}
		else if (String::Compare(option, "ExportFixtures", true) == 0)
		{
			ParseBoolean(_config->ExportFixtures, value);
		}
		else if (String::Compare(option, "BSPDepthCutoff", true) == 0)
		{
			ParseInteger(_config->BSPDepthCutoff, value);
		}
		else if (String::Compare(option, "BSPTrianglesCutoff", true) == 0)
		{
			ParseInteger(_config->BSPTrianglesCutoff, value);
		}
		else
		{
			CompilerError("Unrecognized option '" + option + "'");
		}
	}

	void ParseBoolean(bool %value, String ^str)
	{
		if (str == "true" || str == "on" || str == "1" || str == "yes")
		{
			value = true;
		}
		else if (str == "false" || str == "off" || str == "0" || str == "no")
		{
			value = false;
		}
		else
		{
			CompilerError("Parse error: expected boolean value, got '" + str + "'");
		}
	}

	void ParseInteger(int %value, String ^str)
	{
		if (!int::TryParse(str, value))
			CompilerError("Parse error: expected integer value, got '" + str + "'");
	}

	void CmdExclude(String ^nodeName)
	{
		if (nodeName != nullptr)
		{
			nodeName = nodeName->Trim();

			if (nodeName->Length > 0)
			{
				if (nodeName->ToLower() == "none")
				{
					_config->ExcludeList->Clear();
				}
				else
				{
					_config->ExcludeList->Add(nodeName->ToLower());
				}
			}
		}
	}

	void CompilerError(String ^message)
	{
		Console::WriteLine("[ERROR] " + _configPath + ":" + _lineNumber + ": " + message);
		_errorCount++;
	}

	void CompilerWarning(String ^message)
	{
		Console::WriteLine("[WARN] " + _configPath + ":" + _lineNumber + ": " + message);
	}

	void CompilerInfo(String ^message)
	{
		Console::WriteLine("[INFO] " + _configPath + ":" + _lineNumber + ": " + message);
	}

	void BuildSuccess()
	{
		Console::WriteLine("BUILD SUCCESSFUL");

		for (int i = 0; i < (int)ChunkType::Count; i++)
			if (_chunkSizes[i] != 0)
				Console::WriteLine(((ChunkType)i).ToString() + ": " + Utility::FormatSize(_chunkSizes[i]));
	}

	void BuildFailure()
	{
		Console::WriteLine("BUILD FAILED: " + _errorCount + " errors");
	}

private:

	String ^_configPath;
	Config ^_config;
	/* array<Zone ^> ^_zones;*/
	Region ^_region;
	int _errorCount;
	array<long> ^_chunkSizes;
	int _lineNumber;
internal:
	std::map<int, Niflib::NiObjectRef>* _nifs;
};