
#include "Writer.h"

using namespace System;
using namespace System::IO;

Writer::Writer(Stream ^stream)
	: BinaryWriter(stream)
{
}

//void Writer::Write(SlimDX::Vector3 v)
//{
//    Write(v.X);
//    Write(v.Y);
//    Write(v.Z);
//}
//
//void Writer::Write(SlimDX::Matrix m)
//{
//    Write(m.M11);
//    Write(m.M12);
//    Write(m.M13);
//    Write(m.M14);
//
//    Write(m.M21);
//    Write(m.M22);
//    Write(m.M23);
//    Write(m.M24);
//
//    Write(m.M31);
//    Write(m.M32);
//    Write(m.M33);
//    Write(m.M34);
//
//    Write(m.M41);
//    Write(m.M42);
//    Write(m.M43);
//    Write(m.M44);
//}

void Writer::Write(const Niflib::Vector3 &v)
{
	Write((float)v.x);
	Write((float)v.y);
	Write((float)v.z);
}

void Writer::Write(const Niflib::Matrix33 &m)
{
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			Write((float)m[i][j]);
}

void Writer::Write(System::String ^str)
{
	Write((UInt32)str->Length);
	for (int i = 0; i < str->Length; i++)
		Write((Byte)str[i]);
}

void Writer::WriteStdString(const std::string &str)
{
	Write((UInt32)str.size());
	for (size_t i = 0; i < str.size(); i++)
		Write((Byte)str[i]);
}
