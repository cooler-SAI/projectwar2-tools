
#pragma once

#include "PCX.h"

ref class Terrain
{
private:

	value struct Vertex
	{
		Vector3 P, N;
	};

	PCX ^_terrain;
	PCX ^_offset;

	array<Vertex> ^_vertices;
	array<UInt32> ^_indices;

public:
	int _scaleFactor, _offsetFactor;
	property int Width
	{
		int get()
		{
			return _terrain->Width;
		}
	}

	property int Height
	{
		int get()
		{
			return _terrain->Height;
		}
	}

	property int NumVertices
	{
		int get()
		{
			return Width * Height;
		}
	}

	property int NumTriangles
	{
		int get()
		{
			return 2 * (Width - 1) * (Height - 1);
		}
	}

	Terrain()
	{
		_scaleFactor = 1;
		_offsetFactor = 1;
	}

	void Load(String ^zoneDir)
	{
		LoadHeightmap(zoneDir);
		LoadFactors(zoneDir);
		BuildGeometry();
	}

	int GetValue(int x, int y)
	{
		return _scaleFactor * _terrain->GetValue(x, y) + _offsetFactor * _offset->GetValue(x, y);
	}

	MemoryStream ^GetVertexData()
	{
		MemoryStream ^stream = gcnew MemoryStream(NumVertices * 6 * sizeof(float));
		BinaryWriter ^writer = gcnew BinaryWriter(stream);

		for each (Vertex v in _vertices)
		{
			writer->Write((float)v.P.X);
			writer->Write((float)v.P.Y);
			writer->Write((float)v.P.Z);

			writer->Write((float)v.N.X);
			writer->Write((float)v.N.Y);
			writer->Write((float)v.N.Z);
		}

		return stream;
	}

	MemoryStream ^GetIndexData()
	{
		int size = 3 * sizeof(UInt32) * NumTriangles;
		MemoryStream ^stream = gcnew MemoryStream(size);
		BinaryWriter ^writer = gcnew BinaryWriter(stream);
		for each (UInt32 i in _indices)
			writer->Write(i);
		return stream;
	}

private:

	void BuildVertices()
	{
		_vertices = gcnew array<Vertex>(NumVertices);

		for (int y = 0; y < Height; y++)
		{
			for (int x = 0; x < Width; x++)
			{
				Vertex v;
				v.P = GetVertexPosition(x, y);
				v.N = Vector3(0, 0, 0);
				_vertices[y * Width + x] = v;
			}
		}
	}

	void BuildIndices()
	{
		// FIXME: switch divide based on height delta

		_indices = gcnew array<UInt32>(3 * NumTriangles);
		int i = 0;
		for (int y = 0; y < Height - 1; y++)
		{
			for (int x = 0; x < Width - 1; x++)
			{
				UInt32 b = y * Width; // base vertex index for this row

				_indices[i++] = b + x;
				_indices[i++] = b + x + Width;
				_indices[i++] = b + x + 1;

				_indices[i++] = b + x + 1;
				_indices[i++] = b + x + Width;
				_indices[i++] = b + x + Width + 1;
			}
		}
	}

	void BuildNormals()
	{
		for (int i = 0; i < _indices->Length; i += 3)
		{
			// Get triangle indices
			int i0 = _indices[i], i1 = _indices[i + 1], i2 = _indices[i + 2];

			// Get vertex positions of triangle
			Vector3 v0 = _vertices[i0].P, v1 = _vertices[i1].P, v2 = _vertices[i2].P;

			// Compute face normal of triangle
			Vector3 n = GetFaceNormal(v0, v1, v2);

			// Add normal to all vertices referenced by this triangle
			_vertices[i0].N += n;
			_vertices[i1].N += n;
			_vertices[i2].N += n;

			// FIXME: use angle weighting
		}

		for (int i = 0; i < _vertices->Length; i++)
			_vertices[i].N = _vertices[i].N.Normalize();
	}

	void BuildGeometry()
	{
		BuildVertices();
		BuildIndices();
		BuildNormals();
	}

	void LoadHeightmap(String ^zoneDir)
	{
		_terrain = gcnew PCX();
		_terrain->Load(MYPManager::Instance->GetAssetStream(zoneDir + "/terrain.pcx"));

		_offset = gcnew PCX();
		_offset->Load(MYPManager::Instance->GetAssetStream(zoneDir + "/offset.pcx"));

		// FIXME: sanity check that dimensions match
	}

	void LoadFactors(String ^zoneDir)
	{
		Stream ^stream = MYPManager::Instance->GetAssetStream(zoneDir + "/sector.dat");
		StreamReader ^reader = gcnew StreamReader(stream);

		String ^line, ^section;
		while ((line = reader->ReadLine()) != nullptr)
		{
			line = line->Trim();

			if (line->StartsWith("//"))
			{
			}
			else if (line->StartsWith("["))
			{
				if (line->EndsWith("]"))
				{
					section = line->Substring(1, line->Length - 2);
				}
				else
				{
					section = ""; // Malformed
				}
			}
			else
			{
				if (section == "terrain")
				{
					if (line->ToLower()->StartsWith("scalefactor="))
						_scaleFactor = int::Parse(line->Substring(12, line->Length - 12));
					else if (line->ToLower()->StartsWith("offsetfactor="))
						_offsetFactor = int::Parse(line->Substring(13, line->Length - 13));
				}
			}
		}
	}

	Vector3 GetVertexPosition(int x, int y)
	{
		int x0 = Width - x - 1;
		int y0 = Height - y - 1;

		float fx = 65535.0f * x / (float)(Width - 1);
		float fy = 65535.0f * y / (float)(Height - 1);
		float fz = (float)GetValue(x0, y);

		//return Vector3(fx, fy, 0);
		return Vector3(fx, fy, fz);
	}

	Vector3 GetFaceNormal(Vector3 v0, Vector3 v1, Vector3 v2)
	{
		Vector3 p = v1 - v0, q = v2 - v0;
		Vector3 r = Vector3::Cross(p, q);
		r = r.Normalize();
		if (r.Z < 0)
			r = -1 * r;
		return r;
	}

	void WriteVertex(BinaryWriter ^writer, Vector3 p, Vector3 n)
	{
		writer->Write(p.X);
		writer->Write(p.Y);
		writer->Write(p.Z);

		writer->Write(n.X);
		writer->Write(n.Y);
		writer->Write(n.Z);
	}

};