
#pragma once

value struct Triangle
{
	Triangle(unsigned int j0, unsigned int j1, unsigned int j2, unsigned int f)
	{
		i0 = j0;
		i1 = j1;
		i2 = j2;
		fixture = f;
	}

	bool IsDegenerate()
	{
		return i0 == i1 || i1 == i2 || i0 == i2;
	}

	unsigned int i0, i1, i2, fixture;
};