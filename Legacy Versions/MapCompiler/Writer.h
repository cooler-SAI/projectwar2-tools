
#pragma once

#include "niflib.h"

ref class Writer : public System::IO::BinaryWriter
{
public:
	Writer(System::IO::Stream ^stream);
	void Write(const Niflib::Vector3 &v);
	void Write(const Niflib::Matrix33 &m);
	virtual void Write(System::String ^) override;

	// This does not use function overloading because there are
	// too many implicit conversions to std::string that exist
	// and will be selected by the compiler when not appropriate
	void WriteStdString(const std::string &);
};