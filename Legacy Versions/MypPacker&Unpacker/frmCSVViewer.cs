﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarClientTool
{
    public partial class FrmCSVViewer : Form
    {
        private MYP _myp;
        private String _path;
        public Boolean Changed => csvGrid1.Changed;
        public FrmCSVViewer()
        {

            InitializeComponent();


        }
        public void LoadAsset(MYP myp, String path, Byte[] data)
        {
            csvGrid1.LoadCSV(data);
            _myp = myp;
            _path = path;
        }

        private void SaveToolStripMenuItem_Click(Object sender, EventArgs e) => _myp.UpdateAsset(_path, System.Text.ASCIIEncoding.ASCII.GetBytes(csvGrid1.CSV.ToText()), true);
    }
}
