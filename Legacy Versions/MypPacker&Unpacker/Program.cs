﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Text;


namespace WarClientTool
{
    public static class Program
    {
        private static AssemblyFileVersionAttribute version;
        private static AssemblyTitleAttribute title;
        private static String exe;

        public static Dictionary<Int64, String> AssetHash2Path = new Dictionary<Int64, String>();
        public static Dictionary<String, Int64> AssetPath2Hash = new Dictionary<String, Int64>();

        static void Main(String[] args)
        {
            version = Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyFileVersionAttribute>();
            title = Assembly.GetCallingAssembly().GetCustomAttribute<AssemblyTitleAttribute>();
            exe = System.AppDomain.CurrentDomain.FriendlyName;

            //args = new string[] { @"-e", @"D:\Games\Warhammer Online - Age of Reckoning\data.myp", @"D:\Games\Warhammer Online - Age of Reckoning\myps\data" };

            Console.WriteLine($"{title.Title} {version.Version}");

            String cmd = (args == null || args.Length == 0 || args[0] == null) ? "" : args[0].ToLower();

            LoadDehashes();

            if(File.Exists("asset_hash_path.tab"))
            {
                Byte[] hashes_slim = File.ReadAllBytes("asset_hash_path.tab");
                Byte[] hashes_slim_dat = Compress(hashes_slim);
                File.WriteAllBytes("hashes_slim.dat", hashes_slim_dat);

                Console.WriteLine("Compressed asset_hash_path.tab into hashes_slim.dat");
            }

            switch(cmd)
            {

                case "-ch":
                    if(File.Exists("hashes_slim.tab"))
                    {
                        Byte[] hashes_slim = File.ReadAllBytes("hashes_slim.tab");
                        Byte[] hashes_slim_dat = Compress(hashes_slim);
                        File.WriteAllBytes("hashes_slim.dat", hashes_slim_dat);

                        Console.WriteLine("Compressed hashes_slim.tab into hashes_slim.dat");
                    }
                    break;
                case "-e":
                    goto case "extract";
                case "extract":
                    Extract(args);
                    break;
                case "-pd":
                    goto case "pack-directory";
                case "packfolder":
                    goto case "pack-directory";
                case "pack-directory":
                    PackFolder(args);
                    break;
                case "packfile":
                    goto case "pack-file";
                case "-pf":
                    goto case "pack-file";
                case "pack-file":
                    PackFile(args);
                    break;
                case "edit":
                    goto case "edit-csv";
                case "-ecsv":
                    goto case "edit-csv";
                case "edit-csv":
                    EditCSVFile(args);
                    break;
                case "-h":
                    goto case "help";
                case "help":
                    Help();
                    break;
                default:
                    Help();
                    break;
            }

            Console.WriteLine("");
        }

        static void EditCSVFile(String[] args)
        {
            if(args.Length < 3)
            {
                Help();
                return;
            }

            if(!File.Exists(args[1]))
            {
                Console.WriteLine("Myp file not found");
                return;
            }

            var p = (MythicPackage)Enum.Parse(typeof(MythicPackage), Path.GetFileNameWithoutExtension(args[1].ToUpper()));

            using(var myp = new MYP(p, args[1], true))
            {
                String assetName = args[2].ToLower().Replace("\\", "/").Replace(".stx.dds", ".stx");
                Byte[] asset = myp.GetAssetData(MYP.HashWAR(assetName));
                if(asset == null)
                {
                    if(Int64.TryParse(assetName, out Int64 hash))
                        asset = myp.GetAssetData(hash);
                    if(asset == null)
                    {
                        Console.WriteLine("Asset name or hash not found");
                    }
                    return;
                }
                var viewer = new FrmCSVViewer();
                viewer.LoadAsset(myp, assetName, asset);
                viewer.ShowDialog();
                if(viewer.Changed)
                    myp.Save();
            }
        }

        static void Extract(String[] args)
        {
            if(args.Length < 3)
            {
                Help();
                return;
            }

            if(Directory.Exists(args[1]))
            {
                Console.WriteLine($"Extracting to {args[1]}");
                foreach(String mypName in Enum.GetNames(typeof(MythicPackage)))
                {
                    String name = Path.Combine(args[1], mypName + ".myp");
                    var p = (MythicPackage)Enum.Parse(typeof(MythicPackage), mypName);
                    ExtractMyp(p, name, args[2]);
                }
            } else if(File.Exists(args[1]))
            {
                String name = args[1].ToLower();
                var p = (MythicPackage)Enum.Parse(typeof(MythicPackage), Path.GetFileNameWithoutExtension(name).ToUpper());
                ExtractMyp(p, name, args[2]);
            }
        }

        static void PackFolder(String[] args)
        {
            if(args.Length < 2)
            {
                Help();// Console.WriteLine("Usage: PACKFOLDER [myps folder] [dest myp file]");
                return;
            }

            if(!Directory.Exists(args[1]))
            {
                Console.WriteLine($"Myps folder not found {args[1].ToLower()}");
                return;
            }

            String mypName = Path.GetFileName(args[2]).ToUpper();

            MythicPackage p = MythicPackage.NONE;
            if(Enum.TryParse<MythicPackage>(mypName, out p))
            {
                Console.WriteLine($"Destination myp name is limited to existing names");
                return;
            }

            if(!File.Exists(args[2]))
            {
                Console.WriteLine($"Creating {mypName.ToLower()}.");
                using(var myp = new MYP(p, args[2], true))
                { }
            } else
            {
                Console.WriteLine($"Processing {mypName.ToLower()}.");
            }

            var watch = System.Diagnostics.Stopwatch.StartNew();
            using(var myp = new MYP(p, args[2], true))
            {
                Boolean verbose = args.Length > 3 ? (args[3] == "-verbose") : false;
                // Parallel.ForEach(Directory.GetFiles(args[1]), (file) =>
                //   {
                foreach(String file in Directory.GetFiles(args[1]))
                {
                    String assetName = file.Replace(args[1], "").Replace("\\", "/").Substring(1).ToLower();
                    if(verbose)
                        Console.WriteLine("Packing " + Path.GetFileName(file));
                    if(!assetName.Contains("unknown_hashes"))
                    {
                        myp.UpdateAsset(assetName, File.ReadAllBytes(file), CanCompress(file));
                    } else
                    {
                        Int64 hash = Convert.ToInt64(Path.GetFileNameWithoutExtension(file).ToLower(), 16); //(long)ulong.Parse(Path.GetFileNameWithoutExtension(file));
                        myp.UpdateAsset(hash, File.ReadAllBytes(file), CanCompress(file));
                    }

                    //  myp.UpdateAsset(assetName, File.ReadAllBytes(file), CanCompress(file));
                }//);

                // Parallel.ForEach(Directory.GetFiles(args[1]), (folder) =>
                // {
                foreach(String folder in Directory.GetDirectories(args[1]))
                {
                    PackFolder(args[1], folder, myp, verbose);
                }//);
                //Console.WriteLine("Writing " + args[2]);
                myp.Save();
            }
            watch.Stop();

            Double elapsed_seconds = Math.Round(watch.ElapsedMilliseconds / 1000.0f, 2);
            Console.WriteLine($"Compression completed in {elapsed_seconds} seconds.");


        }

        static void PackFile(String[] args)
        {
            if(args.Length < 3)
            {
                Help();
                return;
            }

            if(!File.Exists(args[1]))
            {
                Console.WriteLine($"Asset to pack not found {args[1]}");
                return;
            }

            String mypName = Path.GetFileName(args[2]).ToUpper();

            MythicPackage p = MythicPackage.NONE;
            if(Enum.TryParse<MythicPackage>(mypName, out p))
            {
                Console.WriteLine($"Destination .myp file name is limited to known .myp archive names.");
                return;
            }

            if(!File.Exists(args[2]))
            {
                Console.WriteLine($"Creating myp ({mypName})");
                using(var myp = new MYP(p, args[2], true))
                { }
            }

            using(var myp = new MYP(p, args[3], true))
            {
                String assetName = args[3].Replace("\\", "/").ToLower();

                Console.WriteLine("Packing [" + assetName + "]");
                myp.UpdateAsset(assetName, File.ReadAllBytes(args[1]), CanCompress(args[1]));

                Console.WriteLine("Writing " + args[3]);
                myp.Save();

                Console.WriteLine("Done");
            }
        }

        private static void PackFolder(String root, String currentFolder, MYP myp, Boolean verbose)
        {
            foreach(String folder in Directory.GetDirectories(currentFolder))
            {
                PackFolder(root, folder, myp, verbose);
            }

            // Parallel.ForEach(Directory.GetFiles(currentFolder), (file) =>
            // {
            foreach(String file in Directory.GetFiles(currentFolder))
            {
                if(verbose)
                    Console.WriteLine("Packing " + Path.GetFileName(file));
                String assetName = file.ToLower().Replace(root, "").Replace("\\", "/").Substring(1).Replace(".stx.dds", ".stx");

                if(assetName.Contains("no_dehash"))
                {
                    // long hash = (long)ulong.Parse(Path.GetFileNameWithoutExtension(file));
                    Int64 hash = Convert.ToInt64(Path.GetFileNameWithoutExtension(file).ToLower(), 16);
                    myp.UpdateAsset(hash, File.ReadAllBytes(file), CanCompress(file));
                } else
                {
                    myp.UpdateAsset(assetName, File.ReadAllBytes(file), CanCompress(file));
                }
            }//);
        }

        static Boolean CanCompress(String filename, Boolean defaultValue = true)
        {
            FileAttributes attributes = File.GetAttributes(filename);

            //Console.WriteLine($"{filename} C[{attributes.HasFlag(FileAttributes.Archive)}]");

            return attributes.HasFlag(FileAttributes.Archive);
        }

        static void ExtractMyp(MythicPackage p, String name, String dest)
        {
            if(File.Exists(name))
            {
                try
                {
                    using(var myp = new MYP(p, name, false))
                    {
                        Console.WriteLine($"Processing {name} containing {myp.Assets.Count} assets.");

                        Int32 count = 0;

                        Task.Run(() =>
                        {
                            while(true)
                            {
                                unchecked
                                {
                                    Single delta_progress = 100.0f / myp.Assets.Count;
                                    Int32 percent = (Int32)(delta_progress * count);

                                    Console.Title = $"WAaaAGHhhH TOolz WOT iZ 4 MeKAniks =>< {count}/{myp.Assets.Count} assets {percent}% extracted from {name.ToLower()}";
                                }

                                if(count == myp.Assets.Count)
                                    break;

                                Thread.Yield();
                                Thread.Sleep(1000);
                            }
                        });

                        var watch = System.Diagnostics.Stopwatch.StartNew();
                        Parallel.ForEach(myp.Assets, (asset) =>
                        {
                            Int64 hash = asset.Key;
                            Byte[] data = myp.GetAssetData(hash);
                            String path = String.Empty;
                            if(AssetHash2Path.ContainsKey(hash))
                            {
                                path = Path.Combine(dest, AssetHash2Path[hash].ToLower().Replace("/", "\\").Replace(".stx", ".stx.dds"));
                                String folder = Path.GetDirectoryName(path);
                                if(!Directory.Exists(folder))
                                    Directory.CreateDirectory(folder);
                            } else
                            {
                                String type = MYP.GetExtension(data);

                                String folder = Path.Combine(dest, "unknown_hashes", type);
                                if(!Directory.Exists(folder))
                                    Directory.CreateDirectory(folder);

                                UInt64 uhash = (UInt64)hash;
                                uhash = (uhash >> 32) | (uhash << 32); // swap adjacent 32-bit blocks
                                uhash = ((uhash & 0xFFFF0000FFFF0000) >> 16) | ((uhash & 0x0000FFFF0000FFFF) << 16); // swap adjacent 16-bit blocks
                                uhash = ((uhash & 0xFF00FF00FF00FF00) >> 8) | ((uhash & 0x00FF00FF00FF00FF) << 8); // swap adjacent 8-bit blocks

                                Byte[] ubytes = BitConverter.GetBytes(uhash);
                                var ustringBuilder = new StringBuilder(ubytes.Length);

                                foreach(Byte byteValue in ubytes)
                                    ustringBuilder.Append(byteValue.ToString("x2"));

                                path = Path.Combine(folder, ustringBuilder.ToString() + "." + type.ToLower());
                            }
                            File.WriteAllBytes(path, data);

                            FileAttributes attributes = File.GetAttributes(path);

                            // Console.WriteLine($"{path} C[{asset.Value.Compressed}]");

                            if(asset.Value.Compressed == 1)
                                attributes |= FileAttributes.Archive;
                            else
                                attributes &= ~FileAttributes.Archive;

                            File.SetAttributes(path, attributes);

                            ++count;

                        });
                        watch.Stop();

                        Double elapsed_seconds = Math.Round(watch.ElapsedMilliseconds / 1000.0f, 2);
                        Console.WriteLine($"Extraction completed in {elapsed_seconds} seconds.");
                    }
                } catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        public static Byte[] Compress(Byte[] data)
        {
            var outB = new MemoryStream();

            using(var archive = new DeflateStream(outB, CompressionLevel.Optimal, true))
            {
                archive.Write(data, 0, (Int32)data.Length);
            }
            outB.Position = 0;
            return outB.ToArray();
        }

        public static Byte[] DeCompress(Byte[] data, Boolean gzip = false)
        {
            var outB = new MemoryStream(data);
            if(gzip)
                outB.Position += 2;

            using(var archive = new DeflateStream(outB, CompressionMode.Decompress, true))
            {
                var dest = new MemoryStream();
                archive.CopyTo(dest);
                return dest.ToArray();
            }
        }

        static void LoadDehashes()
        {
            using(Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("WarClientTool.hashes_slim.dat"))
            {
                Byte[] data = new Byte[stream.Length];
                stream.Read(data, 0, data.Length);
                data = DeCompress(data);
                LoadDehashes(data);

                Int32 hash_count = AssetHash2Path.Count;
                Int32 new_hash_count = 0;
                Console.WriteLine($"Using {hash_count} built in asset path(s) and hashe(s).");

                if(File.Exists($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\hashes_slim.tab"))
                {
                    LoadDehashes(File.ReadAllBytes($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\hashes_slim.tab"));
                    new_hash_count = AssetHash2Path.Count - hash_count;
                    Console.WriteLine($"Loaded {new_hash_count} extra asset path(s) and hashe(s) from hashes_slim.tab");
                }
                hash_count = AssetHash2Path.Count;

                if(File.Exists($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\asset_hash_path.tab"))
                {
                    LoadDehashes(File.ReadAllBytes($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\asset_hash_path.tab"));
                    new_hash_count = AssetHash2Path.Count - hash_count;
                    Console.WriteLine($"Loaded {new_hash_count} extra asset path(s) and hashe(s) from asset_hash_path.tab");
                }
                hash_count = AssetHash2Path.Count;

                if(File.Exists($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\asset_path.tab"))
                {
                    LoadAndHashPaths(File.ReadAllBytes($"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\\asset_path.tab"));
                    new_hash_count = AssetHash2Path.Count - hash_count;
                    Console.WriteLine($"Loaded {new_hash_count} extra asset path(s) from asset_path.tab");
                }
            }
        }

        static void LoadAndHashPaths(Byte[] data)
        {
            try
            {
                using(var reader = new StreamReader(new MemoryStream(data)))
                {
                    while(!reader.EndOfStream)
                    {
                        String line = reader.ReadLine();
                        Int64 hash = MYP.HashWAR(line);
                        if(!AssetHash2Path.ContainsKey(hash))
                        {
                            AssetHash2Path[hash] = line;
                            AssetPath2Hash[line] = hash;
                        }
                    }
                }
            } catch
            {
                Console.WriteLine("Error loading asset hashes");
            }
        }

        static void LoadDehashes(Byte[] data)
        {
            try
            {
                using(var reader = new StreamReader(new MemoryStream(data)))
                {
                    while(!reader.EndOfStream)
                    {
                        String[] items = reader.ReadLine().Split(new Char[] { '\t' });
                        Int64 hash = Int64.Parse(items[0]);
                        if(!AssetHash2Path.ContainsKey(hash))
                        {
                            AssetHash2Path[hash] = items[1];
                            AssetPath2Hash[items[1]] = hash;
                        }
                    }
                }
            } catch
            {
                Console.WriteLine("Error loading asset hashes");
            }
        }

        static void Help()
        {
            Console.WriteLine($"{title.Title} {version.Version}");
            Console.WriteLine("  ");
            Console.WriteLine($" Usage: {exe.ToLower()} <command> <source> <destination> <asset string or hash> (optional) -verbose");
            Console.WriteLine("  ");
            Console.WriteLine("  <commands>");
            Console.WriteLine("  -e    : Extracts either all .myp files in the <source> directory or extract the .myp <source> file to the <destination> directory.");
            Console.WriteLine("  -pd   : Recursively packs all files below the <source> directory in to the <destination> .myp file creating or modifying as requried.");
            Console.WriteLine("  -pf   : Packs a single <source> file in to the <destination> .myp file optionally providing an <asset string or hash(override)>");
            Console.WriteLine("  -csv  : Unpacks a csv file from the <source> .myp file and a <asset string or hash> to be edited and re-saved.");
            Console.WriteLine("  -h    : Displays this message.");

            //Console.WriteLine("  DELETE [dest myp file] [asset name]");
        }
    }
}
