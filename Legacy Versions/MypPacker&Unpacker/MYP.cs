﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace WarClientTool
{
    public enum MythicPackage : Int32
    {
        NONE = 0,
        MFT = 1,
        ART = 2,
        ART2 = 3,
        ART3 = 4,
        AUDIO = 5,
        DATA = 6,
        WORLD = 7,
        INTERFACE = 8,
        VIDEO = 9,
        BLOODHUNT = 10,
        PATCH = 11,
        VO_ENGLISH = 12,
        VO_FRENCH = 13,
        VIDEO_FRENCH = 14,
        VO_GERMAN = 15,
        VIDEO_GERMAN = 16,
        VO_ITALIAN = 17,
        VIDEO_ITALIAN = 18,
        VO_SPANISH = 19,
        VIDEO_SPANISH = 20,
        VO_KOREAN = 21,
        VIDEO_KOREAN = 22,
        VO_CHINESE = 23,
        VIDEO_CHINESE = 24,
        VO_JAPANESE = 25,
        VIDEO_JAPANESE = 26,
        VO_RUSSIAN = 27,
        VIDEO_RUSSIAN = 28,
        WARTEST = 29,
        NDA = 30,
        DEV = 31,
    }

    public interface ILog
    {
        void Info(String msg, String context = "");
        void Error(String msg, String context = "");
        void Exception(String msg, Exception e, String context = "");
        void Warning(String msg, String context = "");
    }
    public class AssetInfo
    {
        public MythicPackage Package { get; set; }
        public Int64 Hash { get; set; }
        public UInt32 CompressedSize { get; set; }
        public UInt32 UncompressedSize { get; set; }
        public Boolean Compressed { get; set; }
        public String Name { get; set; }
        public UInt32 CRC { get; set; }
        public Int64 ID { get; set; }
        public Boolean Changed { get; set; }
        public Byte[] Data { get; set; }
    }


    public class MYP : IDisposable
    {

        public Dictionary<Int64, PatcherAsset> Assets { private set; get; } = new Dictionary<Int64, PatcherAsset>();
        //  private static ILog Log;
        private IntPtr _mypptr = IntPtr.Zero;
        public MythicPackage Package { get; private set; }
        public MYPManager Manager { get; set; }
        public Boolean Changed { get; set; } = false;

        #region IMPORTS
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate void LogDelegate(Int32 type, Int32 level, String msg);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern IntPtr LoadMyp(IntPtr log, String path, Int32 archive);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern IntPtr CreateMyp(IntPtr log, String path);


        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern void UnloadMyp(IntPtr ptr);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern UInt32 GetAssetSize(IntPtr ptr, String assetPath);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern UInt32 GetAssetSizeByHash(IntPtr ptr, Int64 hash);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern UInt32 GetAssetSizeCompressedByHash(IntPtr ptr, Int64 hash);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Int32 GetAssetCount(IntPtr ptr);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Int32 GetAssetData(IntPtr ptr, String assetPath, [In, Out]Byte[] data);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Int32 GetAssetDataByHash(IntPtr ptr, Int64 hash, [In, Out]Byte[] data);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Int32 GetAssetDataByHashRaw(IntPtr ptr, Int64 hash, [In, Out]Byte[] data);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Int32 UpdateAssetByHash(IntPtr ptr, Int64 hash, [In, Out]Byte[] data, UInt32 size, Boolean compressed);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Boolean Save(IntPtr ptr);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Boolean DeleteAssetByHash(IntPtr ptr, Int64 hash);

        [SuppressUnmanagedCodeSecurity]
#if WIN64
        [DllImport(@"c:\wardoc\lib\MYPFast64Release.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#else
        [DllImport(@"myp32.dll", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
#endif
        private static extern Int32 GetAssetsInfo(IntPtr ptr, [In, Out]Int64[] hashes, [In, Out]UInt32[] CompressedSize,
            [In, Out]UInt32[] UnCompressedSize, [In, Out]UInt32[] compressed, [In, Out]UInt32[] crc);

        #endregion

        public static void MypLog(Int32 type, Int32 level, String msg)
        {
            //if (Log != null)
            //{
            //    if (level == 8)
            //        Log.Error(msg);
            //    else
            //        Log.Info(msg);
            //}
        }

        public void Dispose()
        {
            if(_mypptr != IntPtr.Zero)
            {
                UnloadMyp(_mypptr);
                _mypptr = IntPtr.Zero;
            }
        }

        //   private IntPtr _logDelegate;
        public String Filename { private set; get; }

        //    private bool _loaded = false;
        //   private bool _closed = true;
        private readonly LogDelegate _callback = new LogDelegate(MypLog);
        private readonly IntPtr _callbackhandle;

        public MYP(MythicPackage package)
        {
            try
            {
                _callbackhandle = Marshal.GetFunctionPointerForDelegate(_callback);
                GC.KeepAlive(_callback);
                Package = package;
            } catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public MYP(MythicPackage package, String path, Boolean create)
        {
            try
            {
                _callbackhandle = Marshal.GetFunctionPointerForDelegate(_callback);
                GC.KeepAlive(_callback);
                Package = package;
                LoadFromFile(path, create);
            } catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void LoadFromFile(String filename, Boolean create = true)
        {
            lock(Assets)
            {
                Filename = filename;
                if(create && !File.Exists(filename))
                    CreateMyp(IntPtr.Zero, filename);


                _mypptr = LoadMyp(_callbackhandle, filename, (Int32)Package);

                if(_mypptr == IntPtr.Zero)
                    throw new Exception($"Error loading '{filename}'");


                Int32 count = GetAssetCount(_mypptr);
                Int64[] hashes = new Int64[(Int32)count];
                UInt32[] csize = new UInt32[(Int32)count];
                UInt32[] uscise = new UInt32[(Int32)count];
                UInt32[] crc = new UInt32[(Int32)count];
                UInt32[] c = new UInt32[(Int32)count];

                GetAssetsInfo(_mypptr, hashes, csize, uscise, c, crc);
                for(Int32 i = 0; i < count; i++)
                {
                    var asset = new PatcherAsset()
                    {
                        Package = Package,
                        Hash = hashes[i],
                        CompressedSize = csize[i],
                        Size = uscise[i],
                        Compressed = c[i],
                        CRC32 = crc[i],
                    };
                    Assets[asset.Hash] = asset;
                }
                //_loaded = true;
            }
        }


        public void UpdateAsset(PatcherAsset asset)
        {
            lock(Assets)
            {
                Changed = true;
                Assets[asset.Hash] = asset;
                UpdateAssetByHash(_mypptr, asset.Hash, asset.Data, (UInt32)asset.Data.Length, asset.Compressed == 1);
            }
        }

        public void UpdateAsset(Int64 hash, Byte[] data, Boolean compress)
        {
            lock(Assets)
            {
                Changed = true;
                UpdateAssetByHash(_mypptr, hash, data, (UInt32)data.Length, compress);
            }
        }

        public void UpdateAsset(String path, Byte[] data, Boolean compress)
        {
            lock(Assets)
            {
                Int64 hash = HashWAR(path);
                Changed = true;
                UpdateAssetByHash(_mypptr, hash, data, (UInt32)data.Length, compress);
            }
        }

        public Boolean Save()
        {
            // _dataChanges.Clear();

            lock(Assets)
            {
                if(Save(_mypptr))
                {

                    Int32 count = GetAssetCount(_mypptr);
                    Int64[] hashes = new Int64[(Int32)count];
                    UInt32[] csize = new UInt32[(Int32)count];
                    UInt32[] uscise = new UInt32[(Int32)count];
                    UInt32[] crc = new UInt32[(Int32)count];
                    UInt32[] c = new UInt32[(Int32)count];

                    GetAssetsInfo(_mypptr, hashes, csize, uscise, c, crc);
                    for(Int32 i = 0; i < count; i++)
                    {
                        if(Assets.ContainsKey(hashes[i]))
                        {
                            Assets[hashes[i]].CompressedSize = csize[i];
                            Assets[hashes[i]].CRC32 = crc[i];
                            Assets[hashes[i]].Compressed = c[i];
                        }
                    }

                    Changed = false;
                    return true;
                }
            }
            return false;
        }

        public Boolean Delete(PatcherAsset asset)
        {
            lock(Assets)
            {
                if(Assets.ContainsKey(asset.Hash) && DeleteAssetByHash(_mypptr, asset.Hash))
                {
                    Assets.Remove(asset.Hash);
                    Changed = true;
                    return true;
                }
            }
            return false;
        }

        public Byte[] GetAssetData(String name) => GetAssetData(HashWAR(name));

        public Byte[] GetAssetData(Int64 hash)
        {
            //if (_dataChanges.ContainsKey(hash))
            //    return _dataChanges[hash].Item2;
            lock(Assets)
            {
                UInt32 assetSize = GetAssetSizeByHash(_mypptr, hash);
                if(assetSize > 0)
                {
                    Byte[] data = new Byte[(Int32)assetSize];
                    GetAssetDataByHash(_mypptr, hash, data);
                    return data;
                }
                return null;
            }
        }

        public Byte[] GetAssetDataRaw(Int64 hash)
        {
            lock(Assets)
            {
                UInt32 assetSize = GetAssetSizeCompressedByHash(_mypptr, hash);
                if(assetSize > 0)
                {
                    Byte[] data = new Byte[(Int32)assetSize];
                    GetAssetDataByHashRaw(_mypptr, hash, data);
                    return data;
                }
            }
            return null;
        }

        public static Int64 HashWAR(String s)
        {
            UInt32 edx, eax, esi, ebx, edi, ecx;

            eax = ecx = edx = ebx = 0;
            ebx = edi = esi = (UInt32)s.Length + 0xDEADBEEF;

            Int32 i = 0;

            for(i = 0; i + 12 < s.Length; i += 12)
            {
                edi = (UInt32)((s[i + 7] << 24) | (s[i + 6] << 16) | (s[i + 5] << 8) | s[i + 4]) + edi;
                esi = (UInt32)((s[i + 11] << 24) | (s[i + 10] << 16) | (s[i + 9] << 8) | s[i + 8]) + esi;
                edx = (UInt32)((s[i + 3] << 24) | (s[i + 2] << 16) | (s[i + 1] << 8) | s[i]) - esi;

                edx = (edx + ebx) ^ (esi >> 28) ^ (esi << 4);
                esi += edi;
                edi = (edi - edx) ^ (edx >> 26) ^ (edx << 6);
                edx += esi;
                esi = (esi - edi) ^ (edi >> 24) ^ (edi << 8);
                edi += edx;
                ebx = (edx - esi) ^ (esi >> 16) ^ (esi << 16);
                esi += edi;
                edi = (edi - ebx) ^ (ebx >> 13) ^ (ebx << 19);
                ebx += esi;
                esi = (esi - edi) ^ (edi >> 28) ^ (edi << 4);
                edi += ebx;
            }

            if(s.Length - i > 0)
            {
                switch(s.Length - i)
                {
                    case 12:
                        esi += (UInt32)s[i + 11] << 24;
                        goto case 11;
                    case 11:
                        esi += (UInt32)s[i + 10] << 16;
                        goto case 10;
                    case 10:
                        esi += (UInt32)s[i + 9] << 8;
                        goto case 9;
                    case 9:
                        esi += s[i + 8];
                        goto case 8;
                    case 8:
                        edi += (UInt32)s[i + 7] << 24;
                        goto case 7;
                    case 7:
                        edi += (UInt32)s[i + 6] << 16;
                        goto case 6;
                    case 6:
                        edi += (UInt32)s[i + 5] << 8;
                        goto case 5;
                    case 5:
                        edi += s[i + 4];
                        goto case 4;
                    case 4:
                        ebx += (UInt32)s[i + 3] << 24;
                        goto case 3;
                    case 3:
                        ebx += (UInt32)s[i + 2] << 16;
                        goto case 2;
                    case 2:
                        ebx += (UInt32)s[i + 1] << 8;
                        goto case 1;
                    case 1:
                        ebx += s[i];
                        break;
                }

                esi = (esi ^ edi) - ((edi >> 18) ^ (edi << 14));
                ecx = (esi ^ ebx) - ((esi >> 21) ^ (esi << 11));
                edi = (edi ^ ecx) - ((ecx >> 7) ^ (ecx << 25));
                esi = (esi ^ edi) - ((edi >> 16) ^ (edi << 16));
                edx = (esi ^ ecx) - ((esi >> 28) ^ (esi << 4));
                edi = (edi ^ edx) - ((edx >> 18) ^ (edx << 14));
                eax = (esi ^ edi) - ((edi >> 8) ^ (edi << 24));

                return ((Int64)edi << 32) + eax;
            }
            return ((Int64)esi << 32) + eax;
        }

        public static String GetExtension(Byte[] buffer)
        {
            String header = System.Text.Encoding.ASCII.GetString(buffer, 0, 4);

            if(buffer[0] == 0xFF && buffer[1] == 0xFE)
            {
                return "txt";
            }
            if(buffer[0] == 0xFE && buffer[1] == 0xFF)
            {
                return "txt";
            }
            if(buffer[0] == 0xEF && buffer[1] == 0xBB && buffer[2] == 0xBF)
            {
                return "txt";
            }
            if(buffer[0] == 0xEF && buffer[1] == 0xBB && buffer[2] == 0xBF)
            {
                return "txt";
            } else if(buffer[0] == 0x00 && buffer[1] == 0x00 && buffer[2] == 0xFE && buffer[3] == 0xFF)
            {
                return "txt";
            } else if(buffer[0] == 0xFF && buffer[1] == 0xFE && buffer[2] == 0x00 && buffer[3] == 0x00)
            {
                return "txt";
            } else if(buffer[0] == 0x00 && buffer[1] == 0x01 && buffer[2] == 0x00)
            {
                return "ttf";
            } else if(buffer[0] == 0x0a && buffer[1] == 0x05 && buffer[2] == 0x01 && buffer[3] == 0x08)
            {
                return "pcx";
            } else if(buffer[0] == 0x42 && buffer[1] == 0x49 && buffer[2] == 0x4b)
            {
                return "bik";
            } else if(header.IndexOf("PK") >= 0)
            {
                return "zip";
            } else if(header.IndexOf("SCPT") >= 0)
            {
                return "scpt";
            } else if(header[0] == '<')
            {
                return "xml";
            } else if(header.IndexOf("DDS") >= 0)
            {
                return "dds";
            } else if(header.IndexOf("XSM") >= 0)
            {
                return "xsm";
            } else if(header.IndexOf("XAC") >= 0)
            {
                return "xac";
            } else if(header.IndexOf("8BPS") >= 0)
            {
                return "8bps";
            } else if(header.IndexOf("bdLF") >= 0)
            {
                return "db";
            } else if(header.IndexOf("gsLF") >= 0)
            {
                return "geom";
            } else if(header.IndexOf("ID3") >= 0)
            {
                return "mp3";
            } else if(header.IndexOf("idLF") >= 0)
            {
                return "diffuse";
            } else if(header.IndexOf("psLF") >= 0)
            {
                return "specular";
            } else if(header.IndexOf("amLF") >= 0)
            {
                return "mask";
            } else if(header.IndexOf("ntLF") >= 0)
            {
                return "tint";
            } else if(header.IndexOf("lgLF") >= 0)
            {
                return "glow";
            } else if(header.IndexOf("RÍ") >= 0)
            {
                return "zmft";
            } else if(header.IndexOf("RIFF") >= 0)
            {
                String data = System.Text.Encoding.ASCII.GetString(buffer, 8, 4);
                return data.IndexOf("WAVE") >= 0 ? "wav" : "riff";
            } else if(header.IndexOf("; Zo") >= 0)
            {
                return "zone.txt";
            } else if(header.IndexOf("\0\0\0\0") >= 0)
            {
                return "zero.txt";
            } else if(header.IndexOf("PNG") >= 0)
            {
                return "png";
            } else if(header.IndexOf("AMX") >= 0)
            {
                return "amx";
            } else if(header.IndexOf("SIDS") >= 0)
            {
                return "sids";
            } else if(header.IndexOf("MZ") >= 0)
            {
                return "exe";
            }

            if(buffer.Length > 0x1E84800)
                return "";

            Int32 size = buffer.Length;
            if(size > 2048)
                size = 2048;

            String file = System.Text.Encoding.ASCII.GetString(buffer, 0, size);

            if(file.IndexOf("lua") >= 0 && file.IndexOf("lua") < 50)
            {
                return "lua";
            } else if(file.IndexOf("Gamebry") >= 0)
            {
                return "nif";
            } else if(file.IndexOf("WMPHOTO") >= 0)
            {
                return "lmp";
            }

            String csv2 = file.Replace(",", "");

            Int32 commaNum = file.Length - csv2.Length;
            Int32 commas = Math.Max(size / 22, 16);

            if(commaNum >= commas)
            {
                return "csv";
            } else if(file.IndexOf("vs_3_") >= 0)
            {
                return "vsh";
            } else if(file.IndexOf("vs_2_") >= 0)
            {
                return "vsh";
            } else if(file.IndexOf("vs_1_") >= 0)
            {
                return "vsh";
            } else if(file.IndexOf("ps_3_") >= 0)
            {
                return "psh";
            } else if(file.IndexOf("ps_2_") >= 0)
            {
                return "psh";
            } else if(file.IndexOf("ps_1_") >= 0)
            {
                return "psh";
            }

            return "txt";
        }
    }
}
