// Mythic.Geom.Fbx.Interop.cpp : Defines the exported functions for the DLL.
//

#include "pch.h"
#include <fbxsdk.h>
#include "framework0.h"
#include "Mythic.Geom.Fbx.Interop.h"


// This is an example of an exported variable
MYTHICGEOMFBXINTEROP_API int nMythicGeomFbxInterop=0;

// This is an example of an exported function.
MYTHICGEOMFBXINTEROP_API int fnMythicGeomFbxInterop(void)
{
    return 0;
}








// This is the constructor of a class that has been exported.
CMythicGeomFbxInterop::CMythicGeomFbxInterop()
{
    return;
}
