CREATE DATABASE  IF NOT EXISTS `War` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `War`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: War
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `PlayerItemTalisman`
--

DROP TABLE IF EXISTS `PlayerItemTalisman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `PlayerItemTalisman` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PlayerItemID` bigint(20) DEFAULT NULL,
  `TalismanSlotIndex` int(11) DEFAULT NULL,
  `Time` bigint(20) DEFAULT NULL,
  `ItemID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `IX_PlayerItemTalisman_PlayerItemID` (`PlayerItemID`),
  KEY `IX_PlayerItemTalisman_ItemID` (`ItemID`),
  CONSTRAINT `FK_PlayerItemTalisman_Item_ItemID` FOREIGN KEY (`ItemID`) REFERENCES `item` (`id`),
  CONSTRAINT `FK_PlayerItemTalisman_PlayerItem_PlayerItemID` FOREIGN KEY (`PlayerItemID`) REFERENCES `playeritem` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PlayerItemTalisman`
--

LOCK TABLES `PlayerItemTalisman` WRITE;
/*!40000 ALTER TABLE `PlayerItemTalisman` DISABLE KEYS */;
INSERT INTO `PlayerItemTalisman` VALUES (35,439,0,0,821104);
/*!40000 ALTER TABLE `PlayerItemTalisman` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-26 18:40:11
