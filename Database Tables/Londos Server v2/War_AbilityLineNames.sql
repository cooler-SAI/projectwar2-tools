CREATE DATABASE  IF NOT EXISTS `War` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */;
USE `War`;
-- MySQL dump 10.13  Distrib 8.0.13, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: War
-- ------------------------------------------------------
-- Server version	8.0.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AbilityLineNames`
--

DROP TABLE IF EXISTS `AbilityLineNames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `AbilityLineNames` (
  `ID` bigint(20) NOT NULL,
  `Name` longtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AbilityLineNames`
--

LOCK TABLES `AbilityLineNames` WRITE;
/*!40000 ALTER TABLE `AbilityLineNames` DISABLE KEYS */;
INSERT INTO `AbilityLineNames` VALUES (23,'Snare'),(27,'Snare'),(48,'Unstoppable'),(456,'Stealthed'),(1001,'Hex'),(1002,'Curse'),(1003,'Cripple'),(1004,'Ailment'),(1005,'Bolster'),(1007,'Blessing'),(1008,'Enchantment'),(1011,'Disarm'),(1015,'Knockdown'),(1016,'Root'),(1017,'Exit Wound'),(1018,'Silence'),(1019,'Snare'),(1027,'Healing Abilities'),(9183,'DarkMagic 1 to 10'),(9187,'DarkMagic 91 to 100'),(9603,'Curse,Hex,Ailment'),(9622,'MeleeWeaponEquipped2'),(9625,'MeleeWeaponEquipped1');
/*!40000 ALTER TABLE `AbilityLineNames` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-26 18:40:11
