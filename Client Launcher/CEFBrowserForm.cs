﻿using CefSharp;
using CefSharp.Handler;
using CefSharp.Internals;
using CefSharp.WinForms;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace Launcher
{
    [Docking(DockingBehavior.AutoDock),
     ToolboxBitmap(@"Content\toolbox.ico"),
     Description("Chromium Embedded Framework Browser WinForms Component"),
     Designer(typeof(ChromiumWebBrowserDesigner))]
    public partial class CEFBrowserForm : Form, IWebBrowserInternal, IWinFormsWebBrowser
    {
        void IWebBrowser.Load(string address)
        {

        }

        #region Private fields
        private bool drag = false; // determine if we should be moving the form
        private Point startPoint = new Point(0, 0); // also for the moving
        private ManagedCefBrowserAdapter managedCefBrowserAdapter;
        private ParentFormMessageInterceptor parentFormMessageInterceptor;
        private IBrowser browser;
        private IRequestContext requestContext;
        #endregion

        #region Public properties
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IBrowserProcessHandler BrowserProcessHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IResourceHandlerFactory DefaultResourceHandlerFactory { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IRequestHandler RequestEventHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(false)]
        public bool IsParentBrowserCreated { get; private set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(false)]
        public bool IsFieldsAndCefInitialized { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(false)]
        public bool IsDesignMode { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(false)]
        public bool IsActivating { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public BrowserSettings BrowserSettings { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IRequestContext RequestContext
        {
            get => requestContext;
            set
            {
                if (IsParentBrowserCreated)
                {
                    throw new Exception("Browser has already been created. RequestContext must be" +
                                        "set before the underlying CEF browser is created.");
                }
                if (value != null && value.GetType() != typeof(RequestContext))
                {
                    throw new Exception(string.Format("RequestContxt can only be of type {0} or null", typeof(RequestContext)));
                }
                requestContext = value;
            }
        }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(false)]
        public bool IsLoading { get; private set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public string TooltipText { get; private set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public string Address { get; private set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IDialogHandler DialogHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IJsDialogHandler JsDialogHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IKeyboardHandler KeyboardHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IRequestHandler RequestHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IDownloadHandler DownloadHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public ILoadHandler LoadHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public ILifeSpanHandler LifeSpanHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IDisplayHandler DisplayHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IContextMenuHandler MenuHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IRenderProcessMessageHandler RenderProcessMessageHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IFindHandler FindHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IFocusHandler FocusHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IDragHandler DragHandler { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(null)]
        public IResourceHandlerFactory ResourceHandlerFactory { get; set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(false)]
        public bool CanGoForward { get; private set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(false)]
        public bool CanGoBack { get; private set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(false)]
        public bool IsBrowserInitialized { get; private set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(false)]
        public bool CanExecuteJavascriptInMainFrame { get; private set; }
        [Browsable(false), EditorBrowsable(EditorBrowsableState.Never), DefaultValue(false)]
        public bool UseParentFormMessageInterceptor { get; set; } = true;
        #endregion

        #region Public events
        public event EventHandler<LoadErrorEventArgs> LoadError;
        public event EventHandler<FrameLoadStartEventArgs> FrameLoadStart;
        public event EventHandler<FrameLoadEndEventArgs> FrameLoadEnd;
        public event EventHandler<LoadingStateChangedEventArgs> LoadingStateChanged;
        public event EventHandler<ConsoleMessageEventArgs> ConsoleMessage;
        public event EventHandler<StatusMessageEventArgs> StatusMessage;
        public event EventHandler<AddressChangedEventArgs> AddressChanged;
        public event EventHandler<TitleChangedEventArgs> TitleChanged;
        public event EventHandler<IsBrowserInitializedChangedEventArgs> IsBrowserInitializedChanged;
        public event EventHandler<RedirectEventArgs> OnClientRedirect;
        public event EventHandler<RedirectEventArgs> OnFormSubmit;
        public event EventHandler<SourceReadyEventArgs> OnSourceReady;
        #endregion

        #region Constructors
        public CEFBrowserForm() : base()
        {
            InitializeComponent();
        }

        public CEFBrowserForm(string address, IRequestContext requestContext = null) : base()
        {
            //SetStyle(System.Windows.Forms.ControlStyles.Opaque, true);
            //UpdateStyles();

            AllowTransparency = true;
            TransparencyKey = Color.FromArgb(0xff, 0xff, 0x0, 0xff);
            BackColor = Color.FromArgb(0xff, 0xff, 0x0, 0xff);

            RequestContext = requestContext;

            InitializeFieldsAndCefIfRequired();

            InitializeComponent();

            Browse(address);
        }
        #endregion

        #region Public Methods
        [MethodImpl(MethodImplOptions.NoInlining)]
        private void InitializeFieldsAndCefIfRequired()
        {
            if (!IsFieldsAndCefInitialized && !IsDesignMode)
            {
                if (!Cef.IsInitialized)
                {
                    Program.InitializeCef();

                    Initialize();

                    if (FocusHandler == null)
                    {
                        FocusHandler = new DefaultFocusHandler();
                    }

                    if (ResourceHandlerFactory == null)
                    {
                        ResourceHandlerFactory = new DefaultResourceHandlerFactory();
                    }

                    if (BrowserSettings == null)
                    {
                        BrowserSettings = new BrowserSettings
                        {
                            Javascript = CefState.Enabled,
                            JavascriptCloseWindows = CefState.Disabled,
                            JavascriptAccessClipboard = CefState.Disabled,
                            JavascriptDomPaste = CefState.Enabled,
                            RemoteFonts = CefState.Enabled,
                            Plugins = CefState.Enabled,
                            UniversalAccessFromFileUrls = CefState.Enabled,
                            FileAccessFromFileUrls = CefState.Enabled,
                            WindowlessFrameRate = 0x18,
                            DefaultFontSize = 0x0d,
                            DefaultEncoding = "utf-8",
                            MinimumFontSize = 0x02,
                            BackgroundColor = Cef.ColorSetARGB(0x32, 0x64, 0x95, 0xed),
                            MinimumLogicalFontSize = 0x04,
                            WebGl = CefState.Enabled,
                            Databases = CefState.Enabled,
                            ApplicationCache = CefState.Enabled,
                            TabToLinks = CefState.Disabled,
                            TextAreaResize = CefState.Enabled,
                            ImageShrinkStandaloneToFit = CefState.Enabled,
                            ImageLoading = CefState.Enabled,
                            WebSecurity = CefState.Enabled,
                            LocalStorage = CefState.Enabled,
                        };
                    }

                    if (RequestEventHandler == null)
                    {
                        RequestEventHandler = new RequestEventHandler();
                    }

                    if (RenderProcessMessageHandler == null)
                    {
                        RenderProcessMessageHandler = new Launcher.RenderProcessMessageHandler();
                    }

                    if (DownloadHandler == null)
                    {
                        DownloadHandler = new DownloadHandler();
                    }

                    if (managedCefBrowserAdapter == null)
                    {
                        managedCefBrowserAdapter = new ManagedCefBrowserAdapter(this, false);
                    }

                    if (JsDialogHandler == null)
                    {
                        JsDialogHandler = new JsDialogHandler(this, false);
                    }

                    IsFieldsAndCefInitialized = true;

                    Cef.AddDisposable(this);
                }
            }
        }
        protected override void Dispose(bool disposing)
        {
            IsBrowserInitialized = false;

            if (disposing && !IsDesignMode)
            {
                Cef.RemoveDisposable(this);
                FreeUnmanagedResources();

                browser.Dispose();
                browser = null;
            }

            // Don't maintain a reference to event listeners
            LoadError = null;
            FrameLoadStart = null;
            FrameLoadEnd = null;
            LoadingStateChanged = null;
            OnClientRedirect = null;
            OnFormSubmit = null;
            ConsoleMessage = null;
            StatusMessage = null;
            AddressChanged = null;
            TitleChanged = null;
            IsBrowserInitializedChanged = null;

            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }
        [MethodImpl(MethodImplOptions.NoInlining)]
        private void FreeUnmanagedResources()
        {
            if (parentFormMessageInterceptor != null)
            {
                parentFormMessageInterceptor.Dispose();
                parentFormMessageInterceptor = null;
            }

            if (BrowserSettings != null)
            {
                BrowserSettings.Dispose();
                BrowserSettings = null;
            }

            if (managedCefBrowserAdapter != null)
            {
                managedCefBrowserAdapter.Dispose();
                managedCefBrowserAdapter = null;
            }

            IsBrowserInitialized = false;
            IsParentBrowserCreated = false;
        }
        public void Browse(string url)
        {
            if (!string.IsNullOrEmpty(url))
            {
                if (IsBrowserInitialized)
                {
                    this.GetMainFrame().LoadUrl(url);
                }
                else
                {
                    Address = url;
                }
            }
        }
        public void RegisterJsObject(string name, object objectToBind, BindingOptions options = null)
        {
            if (!CefSharpSettings.LegacyJavascriptBindingEnabled)
            {
                throw new Exception(@"CefSharpSettings.LegacyJavascriptBindingEnabled is currently false,
                                    for legacy binding you must set CefSharpSettings.LegacyJavascriptBindingEnabled = true
                                    before registering your first object see https://github.com/cefsharp/CefSharp/issues/2246
                                    for details on the new binding options. If you perform cross-site navigations bound objects will
                                    no longer be registered and you will have to migrate to the new method.");
            }

            if (IsBrowserInitialized)
            {
                throw new Exception("Browser is already initialized. RegisterJsObject must be" +
                                    "called before the underlying CEF browser is created.");
            }

            InitializeFieldsAndCefIfRequired();

            JavascriptObjectRepository objectRepository = managedCefBrowserAdapter.JavascriptObjectRepository;

            if (objectRepository == null)
            {
                throw new Exception("Object Repository Null, Browser has likely been Disposed.");
            }

            objectRepository.Register(name, objectToBind, false, options);
        }
        public void RegisterAsyncJsObject(string name, object objectToBind, BindingOptions options = null)
        {
            if (!CefSharpSettings.LegacyJavascriptBindingEnabled)
            {
                throw new Exception(@"CefSharpSettings.LegacyJavascriptBindingEnabled is currently false,
                                    for legacy binding you must set CefSharpSettings.LegacyJavascriptBindingEnabled = true
                                    before registering your first object see https://github.com/cefsharp/CefSharp/issues/2246
                                    for details on the new binding options. If you perform cross-site navigations bound objects will
                                    no longer be registered and you will have to migrate to the new method.");
            }

            if (IsBrowserInitialized)
            {
                throw new Exception("Browser is already initialized. RegisterJsObject must be" +
                                    "called before the underlying CEF browser is created.");
            }

            InitializeFieldsAndCefIfRequired();

            JavascriptObjectRepository objectRepository = managedCefBrowserAdapter.JavascriptObjectRepository;

            if (objectRepository == null)
            {
                throw new Exception("Object Repository Null, Browser has likely been Disposed.");
            }

            objectRepository.Register(name, objectToBind, true, options);
        }
        public IJavascriptObjectRepository JavascriptObjectRepository => managedCefBrowserAdapter?.JavascriptObjectRepository;
        protected override void OnHandleCreated(EventArgs e)
        {
            IsDesignMode = DesignMode;
            if (!IsDesignMode)
            {
                InitializeFieldsAndCefIfRequired();

                // NOTE: Had to move the code out of this function otherwise the designer would crash
                CreateBrowser();

                ResizeBrowser();
            }

            base.OnHandleCreated(e);
        }
        [MethodImpl(MethodImplOptions.NoInlining)]
        private void CreateBrowser()
        {
            if (((IWebBrowserInternal)this).HasParent == false)
            {
                if (IsBrowserInitialized == false || browser == null)
                {
                    //TODO: Revert temp workaround for default url not loading
                    managedCefBrowserAdapter.CreateBrowser(BrowserSettings, (RequestContext)RequestContext, Handle, null);
                }
                else
                {
                    //If the browser already exists we'll reparent it to the new Handle
                    IntPtr browserHandle = browser.GetHost().GetWindowHandle();
                    NativeMethodWrapper.SetWindowParent(browserHandle, Handle);
                }
                IsParentBrowserCreated = true;
            }
        }
        public virtual void Initialize()
        {
            //SuspendLayout();

            IsBrowserInitializedChanged += BrowserInitializedEvent;
            TitleChanged += TitleChangedEvent;
            AddressChanged += AddressChangedEvent;
            OnClientRedirect += OnClientRedirectEvent;
            OnFormSubmit += OnFormSubmitEvent;
            LoadingStateChanged += LoadingStateChangedEvent;
            ConsoleMessage += ConsoleMessageEvent;
            StatusMessage += StatusMessageEvent;
            FrameLoadStart += FrameLoadStartEvent;
            FrameLoadEnd += FrameLoadEndEvent;
            LoadError += LoadErrorEvent;

            if (null != RequestEventHandler)
            {
                ((Launcher.RequestEventHandler)RequestEventHandler).OnBeforeResourceLoadEvent += OnBeforeResourceLoadEvent;
                ((Launcher.RequestEventHandler)RequestEventHandler).OnProtocolExecutionEvent += OnProtocolExecutionEvent;
                ((Launcher.RequestEventHandler)RequestEventHandler).OnResourceRedirectEvent += OnResourceRedirectEvent;
                ((Launcher.RequestEventHandler)RequestEventHandler).OnResourceResponseEvent += OnResourceResponseEvent;
                ((Launcher.RequestEventHandler)RequestEventHandler).GetResourceResponseFilterEvent += GetResourceResponseFilterEvent;
                ((Launcher.RequestEventHandler)RequestEventHandler).OnResourceLoadCompleteEvent += OnResourceLoadCompleteEvent;
                ((Launcher.RequestEventHandler)RequestEventHandler).OnBeforeBrowseEvent += OnBeforeBrowseEvent;
                ((Launcher.RequestEventHandler)RequestEventHandler).OnRenderProcessTerminatedEvent += OnRenderProcessTerminatedEvent;
                ((Launcher.RequestEventHandler)RequestEventHandler).OnRenderViewReadyEvent += OnRenderViewReadyEvent;
                ((Launcher.RequestEventHandler)RequestEventHandler).OnCertificateErrorEvent += OnCertificateErrorEvent;
                ((Launcher.RequestEventHandler)RequestEventHandler).GetAuthCredentialsEvent += GetAuthCredentialsEvent;
            }
            if (null != DownloadHandler)
            {
                ((Launcher.DownloadHandler)DownloadHandler).OnBeforeDownloadFired += OnBeforeDownloadFired;
                ((Launcher.DownloadHandler)DownloadHandler).OnDownloadUpdatedFired += OnDownloadUpdatedFired;
            }

            DefaultResourceHandlerFactory = ResourceHandlerFactory as DefaultResourceHandlerFactory;

            //ResumeLayout();
        }
        #endregion

        #region WebBrowserInternal
        void IWebBrowserInternal.OnAfterBrowserCreated(IBrowser browser)
        {
            this.browser = browser;
            IsBrowserInitialized = true;

            //TODO: Revert temp workaround for default url not loading
            if (!string.IsNullOrEmpty(Address))
            {
                browser.MainFrame.LoadUrl(Address);
            }

            // By the time this callback gets called, this control
            // is most likely hooked into a browser Form of some sort. 
            // (Which is what ParentFormMessageInterceptor relies on.)
            // Ensure the ParentFormMessageInterceptor construction occurs on the WinForms UI thread:
            if (UseParentFormMessageInterceptor)
            {
                this.InvokeOnUiThreadIfRequired(() =>
                {
                    parentFormMessageInterceptor = new ParentFormMessageInterceptor(this);
                });
            }

            ResizeBrowser();

            IsBrowserInitializedChanged?.Invoke(this, new IsBrowserInitializedChangedEventArgs(IsBrowserInitialized));
        }
        void IWebBrowserInternal.SetAddress(AddressChangedEventArgs args)
        {
            Address = args.Address;
            AddressChanged?.Invoke(this, args);
        }
        void IWebBrowserInternal.SetLoadingStateChange(LoadingStateChangedEventArgs args)
        {
            CanGoBack = args.CanGoBack;
            CanGoForward = args.CanGoForward;
            IsLoading = args.IsLoading;
            LoadingStateChanged?.Invoke(this, args);
        }
        void IWebBrowserInternal.SetTitle(TitleChangedEventArgs args)
        {
            TitleChanged?.Invoke(this, args);
        }
        void IWebBrowserInternal.SetTooltipText(string tooltipText)
        {
            TooltipText = tooltipText;
        }
        void IWebBrowserInternal.OnFrameLoadStart(FrameLoadStartEventArgs args)
        {
            FrameLoadStart?.Invoke(this, args);
        }
        void IWebBrowserInternal.OnFrameLoadEnd(FrameLoadEndEventArgs args)
        {
            FrameLoadEnd?.Invoke(this, args);
        }
        void IWebBrowserInternal.OnConsoleMessage(ConsoleMessageEventArgs args)
        {
            Debug.WriteLine($"{args.Level}:{args.Source} ({args.Line}) => {args.Message}");

            ConsoleMessage?.Invoke(this, args);
        }
        void IWebBrowserInternal.OnStatusMessage(StatusMessageEventArgs args)
        {
            StatusMessage?.Invoke(this, args);
        }
        void IWebBrowserInternal.OnLoadError(LoadErrorEventArgs args)
        {
            LoadError?.Invoke(this, args);
        }
        void IWebBrowserInternal.SetCanExecuteJavascriptOnMainFrame(bool canExecute)
        {
            CanExecuteJavascriptInMainFrame = canExecute;
        }
        IBrowserAdapter IWebBrowserInternal.BrowserAdapter => managedCefBrowserAdapter;
        bool IWebBrowserInternal.HasParent { get; set; }
        #endregion

        public override bool Focused
        {
            get
            {
                if (base.Focused)
                {
                    return true;
                }

                if (!IsHandleCreated)
                {
                    return false;
                }

                return NativeMethodWrapper.IsFocused(Handle);
            }
        }
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);

            if (!IsDesignMode && IsBrowserInitialized)
            {
                ResizeBrowser();
            }
        }
        private void ResizeBrowser()
        {
            if (IsBrowserInitialized)
            {
                managedCefBrowserAdapter.Resize(Width, Height);
            }
        }
        protected override void OnGotFocus(EventArgs e)
        {
            if (IsBrowserInitialized)
            {
                browser.GetHost().SetFocus(true);
            }

            base.OnGotFocus(e);
        }
        protected override bool IsInputKey(Keys keyData)
        {
            //This code block is only called/required when CEF is running in the
            //same message loop as the WinForms UI (CefSettings.MultiThreadedMessageLoop = false)
            //Without this code, arrows and tab won't be processed
            switch (keyData)
            {
                case Keys.Right:
                case Keys.Left:
                case Keys.Up:
                case Keys.Down:
                case Keys.Tab:
                {
                    return true;
                }
                case Keys.Shift | Keys.Right:
                case Keys.Shift | Keys.Left:
                case Keys.Shift | Keys.Up:
                case Keys.Shift | Keys.Down:
                {
                    return true;
                }
            }

            return base.IsInputKey(keyData);
        }
        public IBrowser GetBrowser()
        {
            this.ThrowExceptionIfBrowserNotInitialized();

            return browser;
        }

        #region Move gripper events
        private void InvisibleButtonMouseDown(object sender, MouseEventArgs e)
        {
            startPoint = e.Location;
            drag = true;
        }

        private void InvisibleButtonMouseUp(object sender, MouseEventArgs e)
        {
            drag = false;
        }

        private void InvisibleButtonMouseMove(object sender, MouseEventArgs e)
        {
            if (drag)
            {
                SuspendLayout();
                // if we should be dragging it, we need to figure out some movement
                Point p1 = new Point(e.X, e.Y);
                Point p2 = PointToScreen(p1);
                Point p3 = new Point(p2.X - startPoint.X, p2.Y - startPoint.Y);
                Location = p3;

                ResumeLayout();
            }
        }
        #endregion
    }
}
