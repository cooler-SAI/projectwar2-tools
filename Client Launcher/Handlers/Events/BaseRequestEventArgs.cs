﻿using CefSharp;

namespace Launcher
{
    public abstract class RequestEventArgs : System.EventArgs
    {
        public IWebBrowser BrowserControl { get; private set; }

        public IBrowser Browser { get; private set; }

        protected RequestEventArgs(IWebBrowser browserControl, IBrowser browser)
        {
            BrowserControl = browserControl;
            Browser = browser;
        }

    }
}
