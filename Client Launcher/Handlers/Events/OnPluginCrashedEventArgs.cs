﻿using CefSharp;

namespace Launcher
{
    public class OnPluginCrashedEventArgs : RequestEventArgs
    {
        public OnPluginCrashedEventArgs(IWebBrowser browserControl, IBrowser browser, string pluginPath) : base(browserControl, browser)
        {
            PluginPath = pluginPath;
        }

        public string PluginPath { get; private set; }
    }
}
