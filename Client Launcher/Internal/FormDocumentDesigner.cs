﻿
using System;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Launcher
{
    internal class FormDocumentDesigner : DocumentDesigner
    {

        public FormDocumentDesigner()
        {
        }

        public override void Initialize(IComponent component)
        {
            if (!(component is Form form))
            {
                throw new NotSupportedException("FormDocumentDesigner can be initialized only with Forms");
            }

            form.TopLevel = false;
            form.Visible = true;
            base.Initialize(component);
        }

        public override bool CanParent(Control control)
        {
            if (control is Form)
            {
                return false;
            }

            return base.CanParent(control);
        }

        public const int WM_NCMOUSEMOVE = 0x00A0;
        public const int WM_NCMOUSELEAVE = 0x02A2;
        public const int WM_NCLBUTTONDOWN = 0x00A1;
        public const int WM_NCLBUTTONUP = 0x00A2;
        public const int WM_NCLBUTTONDBLCLK = 0x00A3;
        public const int WM_NCRBUTTONDOWN = 0x00A4;
        public const int WM_NCRBUTTONUP = 0x00A5;
        public const int WM_NCRBUTTONDBLCLK = 0x00A6;
        public const int WM_NCMBUTTONDOWN = 0x00A7;
        public const int WM_NCMBUTTONUP = 0x00A8;
        public const int WM_NCMBUTTONDBLCLK = 0x00A9;
        public const int WM_NCXBUTTONDOWN = 0x00AB;
        public const int WM_NCXBUTTONUP = 0x00AC;
        public const int WM_NCXBUTTONDBLCLK = 0x00AD;

        protected override void WndProc(ref Message m)
        {
            if (m.Msg >= WM_NCMOUSEMOVE && m.Msg <= WM_NCXBUTTONDBLCLK)
            {
                if (this.GetService(typeof(ISelectionService)) is ISelectionService selectionServ)
                    selectionServ.SetSelectedComponents(new object[] { this.Component
                });
            }
            else
            {
                base.WndProc(ref m);
            }
        }
    }
}