using System.Windows.Forms.Design;

namespace Launcher
{
    public class CEFBrowserFormDesigner : ControlDesigner
    {
        protected override void OnPaintAdornments(System.Windows.Forms.PaintEventArgs pe)
        {
            /*
                NOTE:
                Removed until a better image can be found
                
                Add an image as an Embedded Resource
                Update name below and uncomment

                var assembly = System.Reflection.Assembly.GetAssembly(typeof(ChromiumWebBrowserDesigner));
                using (var logo = assembly.GetManifestResourceStream("CefSharp.WinForms.CefSharpLogo.png"))
                using (var img = Image.FromStream(logo)) { pe.Graphics.DrawImage(img, 0, 0); }
            */
            using (System.Drawing.Font font = new System.Drawing.Font("Arial", 16))
            {
                using (System.Drawing.StringFormat stringFormat = new System.Drawing.StringFormat())
                {
                    stringFormat.Alignment = System.Drawing.StringAlignment.Center;
                    stringFormat.LineAlignment = System.Drawing.StringAlignment.Center;
                    pe.Graphics.DrawString(@"Chromium Embedded Framework Windows Forms Web Browser", font, System.Drawing.Brushes.Black, pe.ClipRectangle, stringFormat);
                }
            }

            base.OnPaintAdornments(pe);
        }

        protected override void PreFilterProperties(System.Collections.IDictionary properties)
        {
            properties.Remove("BackgroundImage");
            properties.Remove("BackgroundImageLayout");
            properties.Remove("Text");
            properties.Remove("Font");
            properties.Remove("ForeColor");
            properties.Remove("BackColor");
            properties.Remove("RightToLeft");

            base.PreFilterProperties(properties);

        }
    }
}
