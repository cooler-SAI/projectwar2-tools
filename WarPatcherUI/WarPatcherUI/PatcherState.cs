﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarPatcherUI
{
    public enum PatcherState
    {
        InitializeArchives,
        GetServerStatus,
        RequestManifest,
        ValidateFiles,
        ValidateArchives,
        RequestAssets,
        AssetsDownloaded,
        Ready,
        Playing,
        Error,
        ServerClosed,
    }
}
