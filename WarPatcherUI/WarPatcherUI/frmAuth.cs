﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarPatcherUI.Types;

namespace WarPatcherUI
{
    public partial class frmAuth : MythicForm 
    {
        public override string _bgName { get; set; } = "authbackground";
        public override string _windowName { get; set; } = "authwindow";
        public bool LoggedIn { get; set; } = true;
        public LayeredWindowForm ContainerForm = null;
        private HttpUtil _httpUtil = new HttpUtil();
        public string Username { get; set; }
        public LOGIN_RESPONSE Response { get; private set; }
        public frmAuth()
        {
            InitializeComponent();

            InitResource(
                "WarPatcherUI.patch.txt",
                "WarPatcherUI.9420659275846915044.xml",
                "WarPatcherUI.patcher.png",
                "WarPatcherUI.ageofreckoning.ttf");
        }



        private void frmAuth_Load(object sender, EventArgs e)
        {
            ContainerForm = alphaFormTransformer1.TransformForm2(255);
            txtName.TextBox.Focus();
            txtName.TextBox.Select();
            txtName.BackColor = Color.FromArgb(25, 25, 25);
            mTextBox1.BackColor = Color.FromArgb(25, 25, 25);

            txtName.ForeColor = Color.White;
            mTextBox1.ForeColor = Color.White;
            ActiveControl = txtName.TextBox;
            AdminUtil.UIThread = TaskScheduler.FromCurrentSynchronizationContext();

            if (Program.Config.HasValue("user"))
            {
                txtName.Text = Program.Config.GetString("user");
            }
            if (Program.Config.HasValue("password"))
            {
                mTextBox1.Text = Program.Config.GetString("password");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private async void mButton1_Click(object sender, EventArgs e)
        {
            frmNewAccount acc = new frmNewAccount();
            if (acc.ShowDialog() == DialogResult.OK)
            {
                var response = await _httpUtil.Request<LOGIN_RESPONSE, LOGIN_CREATE>(Program.PatcherServer, new LOGIN_CREATE() {
                    Username = acc.Username,
                    Email = acc.Email,
                    Password = acc.Password,
                });
                Log.Info($"LOGIN_CREATE_RESPONSE:{response.Status}");
                if (response.Status != LoginStatus.OK)
                {
                    MessageBox.Show(response.Status.ToString(), "Error");

                }
             
            }
        }

        private void frmAuth_Shown(object sender, EventArgs e)
        {
    
        }

        private void btnPlay_Click(object sender, EventArgs ee)
        {
            lblError.Text = "";
            if (txtName.Text.Trim().Length < 4)
            {
                lblError.Text = "Invalid user";
                Invalidate();
                txtName.Focus();
                return;
            }
            if (mTextBox1.Text.Trim().Length < 4)
            {
                lblError.Text = "Invalid password";
                Invalidate();
                txtName.Focus();
                return;
            }
            btnPlay.Enabled = false;
            txtName.Enabled = false;
            mTextBox1.Enabled = false;
            Username = txtName.Text;
            Task.Run(async () =>
            {
                try
                {
                    Response = await _httpUtil.Request<LOGIN_RESPONSE, LOGIN>(Program.PatcherServer, new LOGIN() { Username = txtName.Text, Password = mTextBox1.Text });
                    Log.Info($"LOGIN_RESPONSE:{Response.Status}");
                    await AdminUtil.UITask((task) =>
                    {
                        if (Response.Status == LoginStatus.OK)
                        {
                            DialogResult = DialogResult.OK;
                            LoggedIn = true;
                            if (Program.Config.Loaded)
                            {
                                Program.Config["user"] = txtName.Text;
                                Program.Config["password"] = mTextBox1.Text;
                                Program.Config.Save();
                            }
                            Close();
                        }
                        else
                        {
                            lblError.Text = Response.Status.ToString();
                            Invalidate();
                        }
                        btnPlay.Enabled = true;
                        txtName.Enabled = true;
                        mTextBox1.Enabled = true;
                    });
                }
                catch (Exception e)
                {
                    Log.Exception(e.Message, e);
                    if (MessageBox.Show($"Unabled to connect to {Program.PatcherServer} Try Again?", "Error", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        btnPlay_Click(null, null);
                    }
                    else
                    {
                        Application.Exit();
                    }
                }
            }).RunInBackground();
        }

        private void frmAuth_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnPlay_Click(null, null);
            }
        }
    }
}
