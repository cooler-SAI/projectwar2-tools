﻿using System.Collections.Generic;
using System.IO;

namespace WarPatcherUI
{
    public class Config
    {
        public string WarFolder { get; set; } = @"c:\warhammertest";
        public string PatcherServer { get; set; } = "10.10.1.60:8045"; //default value, overwritten by config file
        public string LogFolder { get; set; }
        public string User { get; set; }
        public string Password { get; set; } //TODO: move to windows secure store

        public static Config Load(string path)
        {
            string folder = Path.GetDirectoryName(path);

            if (!File.Exists(path))
            {
                var obj = new Config();
                File.WriteAllText(path, Newtonsoft.Json.JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented));
            }

            var result = Newtonsoft.Json.JsonConvert.DeserializeObject<Config>(File.ReadAllText(path));
            File.WriteAllText(path, Newtonsoft.Json.JsonConvert.SerializeObject(result, Newtonsoft.Json.Formatting.Indented));

            if (!string.IsNullOrEmpty(result.LogFolder) && !Directory.Exists(result.LogFolder))
                Directory.CreateDirectory(result.LogFolder);

            return result;
        }
    }
}
