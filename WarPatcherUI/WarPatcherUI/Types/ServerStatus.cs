﻿namespace WarPatcherUI.Types
{
    public enum ServerStatus
    {
        Online,
        Patching,
        Offline
    }
}
