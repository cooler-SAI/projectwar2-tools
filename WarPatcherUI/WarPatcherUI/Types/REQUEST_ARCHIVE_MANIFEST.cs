﻿using System.Collections.Generic;

namespace WarPatcherUI.Types
{
    public class REQUEST_ARCHIVE_MANIFEST
    {
        public List<MythicPackage> Archives { get; set; } = new List<MythicPackage>();
        public Dictionary<MythicPackage, byte[]> Manifests = new Dictionary<MythicPackage, byte[]>();
    }
}
