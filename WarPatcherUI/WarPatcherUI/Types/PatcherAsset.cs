﻿namespace WarPatcherUI.Types
{
    public class PatcherAsset
    {
        public MythicPackage Archive { get; set; }
        public long Size { get; set; }
        public long Hash { get; set; }
        public uint CRC32 { get; set; }
        public long CompressedSize { get; set; }
        public bool Compressed { get; set; }
    }
}
