﻿namespace WarPatcherUI.Types
{
    public class REQUEST_ASSET
    {
        public MythicPackage Archive { get; set; }
        public long Hash { get; set; }
    }
}
