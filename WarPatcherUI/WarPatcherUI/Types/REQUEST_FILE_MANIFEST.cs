﻿using System.Collections.Generic;
using System.IO;

namespace WarPatcherUI.Types
{
    public class ManifestItem
    {
        public string Name { get; set; }
        public long Size { get; set; }
    }

    public class ManifestFileItem: ManifestItem
    {
        public long ID { get; set; }
        public uint CRC { get; set; }
        public bool Required { get; set; }
        public bool Compressed { get; set; }
        public Stream Stream;
        public string TempFileName;
        public override string ToString()
        {
            return Name;
        }

    }

    public class ManifestAssetItem : ManifestItem
    {
        public MythicPackage Archive { get; set; }
        public long Hash { get; set; }
        public int CompressedSize { get; set; }
        public byte[] Data { get; set; }
        public uint CRC { get; set; }
        public bool Compressed { get; set; }
    }

    public class REQUEST_FILE_MANIFEST
    {
        public List<ManifestFileItem> Items { get; set; } = new List<ManifestFileItem>();
        public uint CRC32
        {
            get
            {
                uint crc = 0;
                foreach (var item in Items)
                {
                    crc = Util.Adler32(crc, item.CRC, 4);
                }
                return crc;
            }
        }
    }
}
