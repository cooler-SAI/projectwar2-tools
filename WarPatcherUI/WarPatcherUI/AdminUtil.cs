﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WarPatcherUI
{
    public static class TaskExtensions
    {
        public static void RunInBackground(this Task task)
        {
            task.ContinueWith(t => { }, TaskContinuationOptions.OnlyOnFaulted);
        }


        public static async Task<T> Timeout<T>(this Task<T> task, int ms)
        {
            var source = new System.Threading.CancellationTokenSource();
            var completed = await Task.WhenAny(task, Task.Delay(ms, source.Token));
            if (completed == task)
            {
                source.Cancel();
                return await task;
            }
            throw new TimeoutException();
        }


        public static async Task Timeout(this Task task, int ms)
        {
            var source = new System.Threading.CancellationTokenSource();
            var completed = await Task.WhenAny(task, Task.Delay(ms, source.Token));
            if (completed == task)
            {
                source.Cancel();
                await task;
            }
            else
                throw new TimeoutException();
        }

    }

    public static class AdminUtil
    {

        public static uint Adler32(uint adler, byte[] bytes)
        {
            const uint a32mod = 65521;
            uint s1 = (uint)(adler & 0xFFFF), s2 = (uint)(adler >> 16);
            for (Int64 i = 0; i < (long)bytes.Length; i++)
            {
                byte b = bytes[i];
                s1 = (s1 + b) % a32mod;
                s2 = (s2 + s1) % a32mod;
            }
            return unchecked((uint)((s2 << 16) + s1));
        }

        public static uint Adler32(uint adler, byte[] bytes, UInt64 length)
        {
            const uint a32mod = 65521;
            uint s1 = (uint)(adler & 0xFFFF), s2 = (uint)(adler >> 16);
            for (UInt64 i = 0; i < length; i++)
            {
                byte b = bytes[i];
                s1 = (s1 + b) % a32mod;
                s2 = (s2 + s1) % a32mod;
            }
            return unchecked((uint)((s2 << 16) + s1));
        }

        public static uint Adler32(uint adler, UInt32 value, UInt64 length)
        {
            const uint a32mod = 65521;
            uint s1 = (uint)(adler & 0xFFFF), s2 = (uint)(adler >> 16);


            byte b = (byte)(value & 0xFF);
            s1 = (s1 + b) % a32mod;
            s2 = (s2 + s1) % a32mod;

            b = (byte)((value >> 8) & 0xFF);
            s1 = (s1 + b) % a32mod;
            s2 = (s2 + s1) % a32mod;

            b = (byte)((value >> 16) & 0xFF);
            s1 = (s1 + b) % a32mod;
            s2 = (s2 + s1) % a32mod;

            b = (byte)((value >> 24) & 0xFF);
            s1 = (s1 + b) % a32mod;
            s2 = (s2 + s1) % a32mod;

            return unchecked((uint)((s2 << 16) + s1));
        }
        sealed class Adler
        {

            // largest prime smaller than 65536
            private const int BASE = 65521;
            // NMAX is the largest n such that 255n(n+1)/2 + (n+1)(BASE-1) <= 2^32-1
            private const int NMAX = 5552;

            internal long adler32(long adler, byte[] buf, int index, int len)
            {
                if (buf == null)
                {
                    return 1L;
                }

                long s1 = adler & 0xffff;
                long s2 = (adler >> 16) & 0xffff;
                int k;

                while (len > 0)
                {
                    k = len < NMAX ? len : NMAX;
                    len -= k;
                    while (k >= 16)
                    {
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        s1 += (buf[index++] & 0xff); s2 += s1;
                        k -= 16;
                    }
                    if (k != 0)
                    {
                        do
                        {
                            s1 += (buf[index++] & 0xff); s2 += s1;
                        }
                        while (--k != 0);
                    }
                    s1 %= BASE;
                    s2 %= BASE;
                }
                return (s2 << 16) | s1;
            }

        }

        public static uint Adler32(Stream stream, long size, int blockSize = 0xFFFFF, uint adler = 0)
        {
            var pos = stream.Position;
            long remain = size;
            long readSize = blockSize;
            byte[] block = new byte[blockSize];
            var a = new Adler();
            while (remain > 0)
            {
                if (stream.Position + blockSize > stream.Length)
                    readSize = stream.Length - stream.Position;

                stream.Read(block, 0, (int)readSize);
                adler = (uint)a.adler32(adler, block, 0, (int)readSize);
                //   adler = Adler32(adler, block, (int)readSize);
                remain -= readSize;
            }
            stream.Position = pos;
            return adler;
        }

        public static TaskScheduler UIThread { get; set; }

        public static void RunAsync(this Task task)
        {
            task.ContinueWith(t => { }, TaskContinuationOptions.OnlyOnFaulted);
        }

        public static Task UITask(Action<Task> a)
        {

            return Task.Run(() =>
            {
            }).ContinueWith(a, UIThread);

        }


        public static List<int> CSVToIntList(string csv, char[] splitTokens = null)
        {
            var list = new List<int>();
            if (splitTokens == null)
                splitTokens = new char[] { ',', ' ', '|' }; ;

            var tokens = csv.Split(splitTokens, StringSplitOptions.RemoveEmptyEntries);
            foreach (var token in tokens)
            {
                int value = 0;
                if (int.TryParse(token, out value))
                    list.Add(value);
            }
            return list;
        }


        public static List<string> CSVToStrList(string csv, char[] splitTokens = null)
        {
            var list = new List<string>();
            if (splitTokens == null)
                splitTokens = new char[] { ',', ' ', '|' }; ;

            var tokens = csv.Split(splitTokens, StringSplitOptions.RemoveEmptyEntries);
            foreach (var token in tokens)
            {
                list.Add(token);
            }
            return list;
        }




    }
}
