STR_APPWINDOW_TITLE			War Legacy Patcher
STR_PROMPT_LOGIN			Login:
STR_PROMPT_PASSWORD			Password:
STR_SAVE_LOGIN_NAME			Save Login Name
STR_FULL_FILE_CHECK			Perform Full File Check
STR_ACCEPT					I Accept
STR_DECLINE					Decline
STR_OK						OK
STR_CANCEL					Cancel
STR_PLAY					Play
STR_PATCH_FAILURE			Patch Failure
STR_SELFPATCH_FAIL			The patcher has failed to initialize the self-patch action.
STR_NO_PROD_LIST			A product list could not be located. Please contact customer support for assistance.
STR_STAT_INITIALIZE			Initializing...
STR_STAT_VALIDATE			Validating files...
STR_STAT_DEFRAGMENT			Defragmenting archives...
STR_STAT_MANIFEST			Retrieving manifest files...
STR_STAT_CALC_PATCH			Determining patch set...
STR_STAT_PATCH				Patching files...
STR_STAT_START_PROD			Starting product patch...
STR_STAT_PATCH_COMPLETE		Patch complete.
STR_STAT_RESTART_pD			Restarting in %d seconds.
STR_STAT_PATCH_INCOMP		Patch incomplete.
STR_STAT_PATCH_ERROR		Patch error!
STR_STAT_PROD_DISABLED		Game unavailable.
STR_BYTE					B
STR_KILOBYTE				KB
STR_MEGABYTE				MB
STR_GIGABYTE				GB
STR_TERABYTE				TB
STR_ENTER_LOGIN				Please login to begin the patch process.
STR_INVALID_LOGIN			Authentication failed. Please re-enter your login credentials.
STR_LOGIN_FAILED			Login Failed
STR_TOO_MANY_LOGINS			You have exceeded the maximum number of login attempts for this session. If you have forgotten your password, please contact customer support for assistance.
STR_NDAWINDOW_TITLE			Beta Test Agreement (BTA)
STR_ACCEPT_NDA				I have read and agree to the above Beta Test Agreement (BTA).
STR_REJECT_NDA				By refusing to agree to the Beta Test Agreement (BTA) you will forfeit your access to the Warhammer Age of Reckoning beta and must erase this software from your computer. Are you sure you want to do this?
STR_NEED_NDA_ACCEPT			You must check the Beta Test Agreement (BTA) approval checkbox before continuing.
STR_MUST_READ_NDA				You must read the entire Beta Test Agreement (BTA) before the acceptance checkbox and button will be enabled.
STR_ERR_CANNOT_SPAWN_THREAD		The patch client was unable to initialize the background processing thread.
STR_ERR_WRITE_FAILED			The patch client was unable to update the file [%ls] (%ls total errors encountered). Please ensure the game is not already running and there is free space available on the disk.
STR_ERR_FILE_RETRIEVE_FAILED		The patch client was unable to retrieve the file [%ls] from the patch server (%ls total errors encountered). Please try the patch operation again in a short while.
STR_ERR_FILE_CHECK_FAILED		The patch client encountered corruption in the file [%ls] on the remote server (%ls total errors encountered). Please try the patch operation again.
STR_ERR_MANIFEST_INVALID		Some file descriptors in the file [%ls] retrieved from the server were corrupted (%ls total errors encountered). Please try the patch operation again in a short while.
STR_ERR_RESTART_FAILED			The patch client needs to restart. Please select OK and then run the application again.
STR_ERR_LAUNCHPROG_MISSING		The main game executable is missing or corrupt. Please run the application again.
STR_ERR_LAUNCHPROG_CHECK_FAILED		The main game executable is missing or corrupt. Please run the application again.
STR_ERR_LAUNCHPROG_LAUNCH_FAILED	The patch program failed to launch the game executable.
STR_ERR_DUPLICATE_INSTANCE		Another instance of the patch client is currently running. If you cannot locate another instance, your system may require a reboot to continue.
STR_ERR_PATCH_TOO_BIG		Your Warhammer folder is missing a large number of files. Reinstall the game from the supplied installation media.
STR_ERR_UNKNOWN				The program was unable to complete the patch operation. Please try again later.
STR_ERR_DISK_FULL			The game disk appears to be full. Please free up some space on your system and run the application again.
STR_ERR_INVALID_CONFIG		The file [%ls] contains an invalid configuration.
STR_ERR_NDA_MISSING			The installation is missing the Beta Test Agreement (BTA) file. The patch cannot continue.
STR_ERR_INIT_TITLE			Initialization Error
STR_ERR_UI_FAILED			The User Interface module [%ls] could not be loaded. The patcher is now attempting to repair itself without a user interface.
STR_ERR_CIDER_PATCH_FAILED	The patch client was unable to initiate a Cider update. Please contact customer support for assistance.
STR_ERR_CIDER_PATCH_CORRUPT	The patch client detected corruption in the Cider update file. Please contact customer support for assistance.
STR_ERR_NETWORK_FAILURE		A network communication error has occurred. Check your network connection. Please contact customer support for assistance if this problem persists.
STR_STAT_RESTART_PATCH		New patch detected! Restarting...
STR_ACCOUNT_URL				https://accounts.eamythic.com/
STR_CREATE_ACCOUNT			Create Account
STR_ERR_ACCT_TERMINATED	This account has been terminated. Please contact customer support for further information.
STR_ERR_ACCT_SUSPENDED	This account has been temporarily suspended. Please contact customer support for further information.
STR_ERR_ACCT_EXPIRED		Your subscription has expired! You will now be directed to the Account Center to update your subscription information.
STR_ACCOUNT_CLOSED_URL	https://accounts.eamythic.com/war/%ls/reactivate
STR_MINIMIZE_TO_TRAY_WARNING	This program will now be minimized to your system tray while your trial is updated to the latest version.  You may view your progress at any time by hovering the mouse over the icon or clicking it to view this window again.
STR_STREAM_DATA			Stream Game Data During Play
STR_STREAMING_TOO_SLOW_TITLE	Streaming Performance Warning
STR_STREAMING_TOO_SLOW_TEXT		Your internet connection appears to be operating too slowly for optimal streaming performance. Would you like to disable streaming?