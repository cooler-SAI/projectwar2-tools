﻿namespace WarPatcherUI
{
    partial class frmPatcher
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPatcher));
            this.btnPlay = new WarPatcherUI.MButton();
            this.btnCancel = new WarPatcherUI.MButton();
            this.pbFile = new WarPatcherUI.MProgressBar();
            this.mTotalProgress = new WarPatcherUI.MProgressBar();
            this.lblFile = new WarPatcherUI.MLabel();
            this.lblTotal = new WarPatcherUI.MLabel();
            this.mBrowser1 = new WarPatcherUI.MBrowser();
            this.btnClose = new WarPatcherUI.MButton();
            this.btnMinimize = new WarPatcherUI.MButton();
            this.mLabel1 = new WarPatcherUI.MLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.mLabel2 = new WarPatcherUI.MLabel();
            this.mLabel3 = new WarPatcherUI.MLabel();
            this.mLabel4 = new WarPatcherUI.MLabel();
            this.mLabel5 = new WarPatcherUI.MLabel();
            this.mLabel6 = new WarPatcherUI.MLabel();
            this.mLabel7 = new WarPatcherUI.MLabel();
            this.txtLog = new System.Windows.Forms.TextBox();
            this.lblShowLog = new System.Windows.Forms.Button();
            this.alphaFormTransformer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // alphaFormTransformer1
            // 
            this.alphaFormTransformer1.BackColor = System.Drawing.Color.DimGray;
            this.alphaFormTransformer1.BackgroundImage = null;
            this.alphaFormTransformer1.Controls.Add(this.txtLog);
            this.alphaFormTransformer1.Controls.Add(this.lblShowLog);
            this.alphaFormTransformer1.Controls.Add(this.mLabel6);
            this.alphaFormTransformer1.Controls.Add(this.mLabel7);
            this.alphaFormTransformer1.Controls.Add(this.mLabel4);
            this.alphaFormTransformer1.Controls.Add(this.mLabel5);
            this.alphaFormTransformer1.Controls.Add(this.mLabel3);
            this.alphaFormTransformer1.Controls.Add(this.mLabel2);
            this.alphaFormTransformer1.Controls.Add(this.mLabel1);
            this.alphaFormTransformer1.Controls.Add(this.btnMinimize);
            this.alphaFormTransformer1.Controls.Add(this.btnClose);
            this.alphaFormTransformer1.Controls.Add(this.mBrowser1);
            this.alphaFormTransformer1.Controls.Add(this.mTotalProgress);
            this.alphaFormTransformer1.Controls.Add(this.pbFile);
            this.alphaFormTransformer1.Controls.Add(this.btnCancel);
            this.alphaFormTransformer1.Controls.Add(this.btnPlay);
            this.alphaFormTransformer1.Controls.Add(this.lblFile);
            this.alphaFormTransformer1.Controls.Add(this.lblTotal);
            this.alphaFormTransformer1.Size = new System.Drawing.Size(599, 507);
            // 
            // btnPlay
            // 
            this.btnPlay.Enabled = false;
            this.btnPlay.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPlay.LayoutName = "playbutton";
            this.btnPlay.Location = new System.Drawing.Point(416, 394);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.ResourceName = "bigbutton";
            this.btnPlay.Size = new System.Drawing.Size(104, 23);
            this.btnPlay.TabIndex = 0;
            this.btnPlay.Text = "[STR_PLAY]";
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancel.LayoutName = "cancelbutton";
            this.btnCancel.Location = new System.Drawing.Point(526, 394);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.ResourceName = "bigbutton";
            this.btnCancel.Size = new System.Drawing.Size(119, 23);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "[STR_CANCEL]";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // pbFile
            // 
            this.pbFile.LayoutName = "fileprogress";
            this.pbFile.Location = new System.Drawing.Point(104, 304);
            this.pbFile.Name = "pbFile";
            this.pbFile.ResourceName = "progressbar";
            this.pbFile.Size = new System.Drawing.Size(100, 23);
            this.pbFile.TabIndex = 3;
            this.pbFile.Value = 0F;
            // 
            // mTotalProgress
            // 
            this.mTotalProgress.LayoutName = "totalprogress";
            this.mTotalProgress.Location = new System.Drawing.Point(104, 241);
            this.mTotalProgress.Name = "mTotalProgress";
            this.mTotalProgress.ResourceName = "progressbar";
            this.mTotalProgress.Size = new System.Drawing.Size(100, 23);
            this.mTotalProgress.TabIndex = 4;
            this.mTotalProgress.Value = 0F;
            // 
            // lblFile
            // 
            this.lblFile.AutoSize = true;
            this.lblFile.ForeColor = System.Drawing.Color.Goldenrod;
            this.lblFile.LayoutName = "filetext";
            this.lblFile.Location = new System.Drawing.Point(222, 240);
            this.lblFile.Margin = new System.Windows.Forms.Padding(0);
            this.lblFile.Name = "lblFile";
            this.lblFile.PadLeft = 0;
            this.lblFile.PadTop = 0;
            this.lblFile.ResourceName = null;
            this.lblFile.Size = new System.Drawing.Size(111, 13);
            this.lblFile.TabIndex = 5;
            this.lblFile.Text = "dev.myp [3MB/11MB]";
            this.lblFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.ForeColor = System.Drawing.Color.Goldenrod;
            this.lblTotal.LayoutName = "totaltext";
            this.lblTotal.Location = new System.Drawing.Point(218, 293);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.PadLeft = 0;
            this.lblTotal.PadTop = 0;
            this.lblTotal.ResourceName = null;
            this.lblTotal.Size = new System.Drawing.Size(127, 13);
            this.lblTotal.TabIndex = 6;
            this.lblTotal.Text = "Patching files... [3/11MB]";
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mBrowser1
            // 
            this.mBrowser1.LayoutName = "browserwindow";
            this.mBrowser1.Location = new System.Drawing.Point(73, 137);
            this.mBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.mBrowser1.Name = "mBrowser1";
            this.mBrowser1.ResourceName = null;
            this.mBrowser1.Size = new System.Drawing.Size(157, 38);
            this.mBrowser1.TabIndex = 7;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.LayoutName = "closebutton";
            this.btnClose.Location = new System.Drawing.Point(582, 107);
            this.btnClose.Name = "btnClose";
            this.btnClose.ResourceName = "closebutton";
            this.btnClose.Size = new System.Drawing.Size(104, 23);
            this.btnClose.TabIndex = 8;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnMinimize
            // 
            this.btnMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.LayoutName = "minimizebutton";
            this.btnMinimize.Location = new System.Drawing.Point(472, 107);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.ResourceName = "minimizebutton";
            this.btnMinimize.Size = new System.Drawing.Size(104, 23);
            this.btnMinimize.TabIndex = 9;
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // mLabel1
            // 
            this.mLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mLabel1.ForeColor = System.Drawing.Color.White;
            this.mLabel1.LayoutName = "titletext_left";
            this.mLabel1.Location = new System.Drawing.Point(27, 32);
            this.mLabel1.Margin = new System.Windows.Forms.Padding(0);
            this.mLabel1.Name = "mLabel1";
            this.mLabel1.PadLeft = 0;
            this.mLabel1.PadTop = 0;
            this.mLabel1.ResourceName = null;
            this.mLabel1.Size = new System.Drawing.Size(221, 16);
            this.mLabel1.TabIndex = 11;
            this.mLabel1.Text = "40% Patcher";
            this.mLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // mLabel2
            // 
            this.mLabel2.AutoSize = true;
            this.mLabel2.ForeColor = System.Drawing.Color.White;
            this.mLabel2.LayoutName = "";
            this.mLabel2.Location = new System.Drawing.Point(472, 219);
            this.mLabel2.Margin = new System.Windows.Forms.Padding(0);
            this.mLabel2.Name = "mLabel2";
            this.mLabel2.PadLeft = 0;
            this.mLabel2.PadTop = 0;
            this.mLabel2.ResourceName = null;
            this.mLabel2.Size = new System.Drawing.Size(77, 13);
            this.mLabel2.TabIndex = 12;
            this.mLabel2.Text = "Server Status: ";
            this.mLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mLabel3
            // 
            this.mLabel3.AutoSize = true;
            this.mLabel3.ForeColor = System.Drawing.Color.Lime;
            this.mLabel3.LayoutName = "";
            this.mLabel3.Location = new System.Drawing.Point(548, 219);
            this.mLabel3.Margin = new System.Windows.Forms.Padding(0);
            this.mLabel3.Name = "mLabel3";
            this.mLabel3.PadLeft = 0;
            this.mLabel3.PadTop = 0;
            this.mLabel3.ResourceName = null;
            this.mLabel3.Size = new System.Drawing.Size(37, 13);
            this.mLabel3.TabIndex = 13;
            this.mLabel3.Text = "Online";
            this.mLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mLabel4
            // 
            this.mLabel4.AutoSize = true;
            this.mLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.mLabel4.LayoutName = "";
            this.mLabel4.Location = new System.Drawing.Point(548, 235);
            this.mLabel4.Margin = new System.Windows.Forms.Padding(0);
            this.mLabel4.Name = "mLabel4";
            this.mLabel4.PadLeft = 0;
            this.mLabel4.PadTop = 0;
            this.mLabel4.ResourceName = null;
            this.mLabel4.Size = new System.Drawing.Size(19, 13);
            this.mLabel4.TabIndex = 15;
            this.mLabel4.Text = "21";
            this.mLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mLabel5
            // 
            this.mLabel5.AutoSize = true;
            this.mLabel5.ForeColor = System.Drawing.Color.White;
            this.mLabel5.LayoutName = "";
            this.mLabel5.Location = new System.Drawing.Point(472, 235);
            this.mLabel5.Margin = new System.Windows.Forms.Padding(0);
            this.mLabel5.Name = "mLabel5";
            this.mLabel5.PadLeft = 0;
            this.mLabel5.PadTop = 0;
            this.mLabel5.ResourceName = null;
            this.mLabel5.Size = new System.Drawing.Size(44, 13);
            this.mLabel5.TabIndex = 14;
            this.mLabel5.Text = "Players:";
            this.mLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mLabel6
            // 
            this.mLabel6.AutoSize = true;
            this.mLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.mLabel6.LayoutName = "";
            this.mLabel6.Location = new System.Drawing.Point(548, 251);
            this.mLabel6.Margin = new System.Windows.Forms.Padding(0);
            this.mLabel6.Name = "mLabel6";
            this.mLabel6.PadLeft = 0;
            this.mLabel6.PadTop = 0;
            this.mLabel6.ResourceName = null;
            this.mLabel6.Size = new System.Drawing.Size(13, 13);
            this.mLabel6.TabIndex = 17;
            this.mLabel6.Text = "3";
            this.mLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // mLabel7
            // 
            this.mLabel7.AutoSize = true;
            this.mLabel7.ForeColor = System.Drawing.Color.White;
            this.mLabel7.LayoutName = "";
            this.mLabel7.Location = new System.Drawing.Point(472, 251);
            this.mLabel7.Margin = new System.Windows.Forms.Padding(0);
            this.mLabel7.Name = "mLabel7";
            this.mLabel7.PadLeft = 0;
            this.mLabel7.PadTop = 0;
            this.mLabel7.ResourceName = null;
            this.mLabel7.Size = new System.Drawing.Size(33, 13);
            this.mLabel7.TabIndex = 16;
            this.mLabel7.Text = "Inbox";
            this.mLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtLog
            // 
            this.txtLog.BackColor = System.Drawing.Color.Black;
            this.txtLog.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtLog.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLog.ForeColor = System.Drawing.Color.Silver;
            this.txtLog.Location = new System.Drawing.Point(53, 68);
            this.txtLog.Multiline = true;
            this.txtLog.Name = "txtLog";
            this.txtLog.ReadOnly = true;
            this.txtLog.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtLog.Size = new System.Drawing.Size(84, 63);
            this.txtLog.TabIndex = 18;
            // 
            // lblShowLog
            // 
            this.lblShowLog.BackColor = System.Drawing.Color.Black;
            this.lblShowLog.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblShowLog.Font = new System.Drawing.Font("Consolas", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblShowLog.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.lblShowLog.Location = new System.Drawing.Point(53, 141);
            this.lblShowLog.Name = "lblShowLog";
            this.lblShowLog.Size = new System.Drawing.Size(59, 18);
            this.lblShowLog.TabIndex = 19;
            this.lblShowLog.Text = "Show Log";
            this.lblShowLog.UseVisualStyleBackColor = false;
            this.lblShowLog.Click += new System.EventHandler(this.lblShowLog_Click_1);
            // 
            // frmPatcher
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(599, 507);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmPatcher";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmPatcher";
            this.Load += new System.EventHandler(this.frmPatcher_Load);
            this.alphaFormTransformer1.ResumeLayout(false);
            this.alphaFormTransformer1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MButton btnPlay;
        private MButton btnCancel;
        private MProgressBar pbFile;
        private MProgressBar mTotalProgress;
        private MLabel lblFile;
        private MLabel lblTotal;
        private MBrowser mBrowser1;
        private MButton btnClose;
        private MButton btnMinimize;
        private MLabel mLabel1;
        private System.Windows.Forms.Timer timer1;
        private MLabel mLabel2;
        private MLabel mLabel3;
        private MLabel mLabel4;
        private MLabel mLabel5;
        private MLabel mLabel6;
        private MLabel mLabel7;
        private System.Windows.Forms.TextBox txtLog;
        private System.Windows.Forms.Button lblShowLog;
    }
}