﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WarPatcherUI
{
    static class Program
    {
        [DllImport("kernel32")]
        static extern bool AllocConsole();
        // P/Invoke required:
        private const UInt32 StdOutputHandle = 0xFFFFFFF5;
        [DllImport("kernel32.dll")]
        private static extern IntPtr GetStdHandle(UInt32 nStdHandle);
        [DllImport("kernel32.dll")]
        private static extern void SetStdHandle(UInt32 nStdHandle, IntPtr handle);

        public static WarPatcherWrapper.ClientPatcher Patcher;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Patcher = new WarPatcherWrapper.ClientPatcher(@"c:\wardoc\patcher_config.txt");
            Patcher.InitPatcher(@"c:\warhammer2", @"c:\wardoc\patcher_config.txt", "127.0.0.1", 8083);
            var auth = new frmAuth();
            Application.Run(auth);
            
            if (auth.LoggedIn)
            {
                Application.Run(new frmPatcher());

            }
        }
    }
}
