﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace WarPatcherUI
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            var token = new CancellationTokenSource();
            try
            {
                var config = Config.Load("patcher_settings.json");

                if (string.IsNullOrEmpty(config.PatcherServer))
                    throw new Exception("PatcherServer address is required");

                if (string.IsNullOrEmpty(config.WarFolder))
                    config.WarFolder = Application.StartupPath;

                if (string.IsNullOrEmpty(config.LogFolder))
                    config.LogFolder = Application.StartupPath;

                Log.Instance = new FileLogger(token, Path.Combine(config.LogFolder, "patcher_log.txt"));

                var assembly = System.Reflection.Assembly.GetExecutingAssembly();
                var fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                Log.Info($"Warhammer patcher ({fvi.FileVersion}) started");

                var auth = new frmAuth(config);
                Application.Run(auth);

                if (auth.DialogResult == DialogResult.OK)
                {
                    if (auth.LoggedIn)
                    {
                        Application.Run(new frmPatcher(config, auth.Username, auth.Response.LoginToken.ToString().Replace("-", "")));
                    }
                }
                token.Cancel();
            }
            catch (Exception e)
            {
                Log.Exception(e.Message, e);
                token.Cancel();
            }
            finally
            {
                Log.Shutdown();
            }
        }
    }
}
