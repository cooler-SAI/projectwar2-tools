﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WarPatcherUI.Types;

namespace WarPatcherUI
{
    public partial class frmPatcher : MythicForm
    {
        public override string _bgName { get; set; } = "mainbackground";
        public override string _windowName { get; set; } = "mainwindow";
        private Patcher _patcher;
        public LayeredWindowForm ContainerForm = null;

        public frmPatcher()
        {
            InitializeComponent();
            Title = "Patcher";
        }

        public frmPatcher(string username, string password)
        {
            AdminUtil.UIThread = TaskScheduler.FromCurrentSynchronizationContext();

            var httpUtil = new HttpUtil();
            var clientPatcher = new WarPatcherWrapper.ClientPatcher();
            _patcher = new Patcher(Program.WarFolder, username, password, clientPatcher, new Downloader(httpUtil), httpUtil);
            _patcher.OnError += _patcher_OnError;
            _patcher.OnMOTD += _patcher_OnMOTD;
            _patcher.OnStateChanged += _patcher_OnStateChanged;
            clientPatcher.OnLog += Patcher_OnLog;

            InitializeComponent();

            InitResource(
                "WarPatcherUI.patch.txt",
                "WarPatcherUI.9420659275846915044.xml",
                "WarPatcherUI.patcher.png",
                "WarPatcherUI.ageofreckoning.ttf");

            lblTotal.Text = "";
            lblFile.Text = "";

            patchNotes.BackColor = Color.Black;
            patchNotes.ForeColor = Color.White;
            patchNotes.Multiline = true;
            patchNotes.WordWrap = false;
            patchNotes.Text = "";
            Title = "Patcher";
        }

        private void _patcher_OnStateChanged(Patcher patcher, PatcherState state)
        {
            AdminUtil.UITask(t =>
            {
                btnPlay.Enabled = state == PatcherState.Ready || state == PatcherState.Playing;
                if (state == PatcherState.Playing)
                {
                    btnMinimize_Click(null, null);
                }
            });
        }

        private void _patcher_OnMOTD(Patcher patcher, string msg)
        {
            AdminUtil.UITask(t =>
            {
                patchNotes.Text = msg;
            });
        }

        private void _patcher_OnError(Patcher patcher, string msg)
        {
            throw new NotImplementedException();
        }

        public string Title
        {
            get
            {
                return lblTitle.Text;
            }
            set
            {
                lblTitle.Text = value;
                Text = value;
            }
        }

        private void Patcher_OnLog(WarPatcherWrapper.LogType type, WarPatcherWrapper.LogLevel level, string msg)
        {
            Log.Info(msg);
        }


        private void btnPlay_Click(object sender, EventArgs e)
        {
            _patcher.Play();
        }


        private void frmPatcher_Load(object sender, EventArgs e)
        {
            ContainerForm = alphaFormTransformer1.TransformForm2(255);
            Thread d = new Thread(() =>
            {
                _patcher.UpdateState().Wait();
            });
            d.IsBackground = true;
            d.Start();
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            ContainerForm.WindowState = FormWindowState.Minimized;
        }

        private static string GetSize(long size)
        {
            if (size > (1024 * 1024 * 1024))
                return (size / (1024 * 1024 * 1024)) + " GB";
            else if (size > (1024 * 1024))
                return (size / (1024 * 1024)) + " MB";
            else if (size > (1024))
                return (size / (1024)) + " KB";
            else
                return (size) + " B";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
             pbFile.Value = 0;
            mTotalProgress.Value = 0;
            if (_patcher.State == PatcherState.GetServerStatus)
            {
                lblFile.Text = "Retreiving server status";
            }
            else if (_patcher.State == PatcherState.InitializeArchives && _patcher.TotalSize > 0)
            {
                lblFile.Text = $"Initializing {_patcher.CurrentItemName} ({_patcher.TotalCurrent}/{_patcher.TotalSize})";
                pbFile.Value = _patcher.CurrentPercent;
            }
            else if (_patcher.State == PatcherState.RequestManifest)
            {
                lblFile.Text = "Requesting Manifest";
            }
            else if (_patcher.State == PatcherState.RequestAssets)
            {
                lblFile.Text = $"{_patcher.CurrentItemName}";
                 pbFile.Value = 0;


                lblTotal.Text = $"Downloading {(float)Math.Round(_patcher.TotalPercent, 0)}% ({GetSize(_patcher.TotalSize)})";
                mTotalProgress.Value = (float)Math.Round(_patcher.TotalPercent,2);
            }
            else if (_patcher.State == PatcherState.ValidateFiles)
            {
                lblFile.Text = "Verifying";
            }
            else if (_patcher.State == PatcherState.Ready)
            {
                lblFile.Text = "Ready";
                lblTotal.Text = "";
            }
            else
            {
                if (lblFile.Text != "")
                    lblFile.Text = "";
            }

            //if (_state == PatcherState.RequestManifest)
            //{
            //    lblTotal.Text = "";
            //    lblFile.Text = $"Requesting manifest";

            //    pbFile.Value = 0;
            //    mTotalProgress.Value = 0;
            //}
            //else if (_state == PatcherState.Downloadidng && _downloader.TotalCurrent > 0)
            //{

            //    //float f = _downloader.FileCurrent * 100f / _downloader.FileTotalSize;
            //    //mTotalProgress.Value = (f);
            //    //lblTotal.Text = _downloader.CurrentFile + $" ({Math.Round(f, 0)}% {GetSize(_downloader.Rate)}/sec)";

            //    float total = _downloader.TotalCurrent * 100f / _downloader.TotalSize;
            //    pbFile.Value = (total);
            //    lblFile.Text = $"Downloaded ({Math.Round(total, 0)}% {_downloader.TotalItems})";
            //    ContainerForm.Text = lblFile.Text;
            //    Title = lblFile.Text;

            //}
            //else if (_state == PatcherState.ScanArchives)
            //{

            //    int mypIndex = Program.MypPatcher.GetProgressIndex();
            //    int mypTotal = Program.MypPatcher.GetProgressTotal();
            //    if (mypTotal > 0)
            //    {
            //        float f = mypIndex * 100f / mypTotal;
            //        mTotalProgress.Value = (f);
            //        lblTotal.Text = "Scanning " + Program.MypPatcher.GetProgressMsg();
            //    }
            //}
            //else if (_state == PatcherState.Ready)
            //{

            //    lblTotal.Text = "Completed";
            //    lblFile.Text = $"";
            //    pbFile.Value = 0;
            //    mTotalProgress.Value = 0;
            //}
            //else if (_state == PatcherState.Error)
            //{

            //    lblTotal.Text = "Error";
            //    lblFile.Text = $"";
            //    pbFile.Value = 0;
            //    mTotalProgress.Value = 0;
            //}
            mTotalProgress.Paint();
            pbFile.Paint();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        private void lblShowLog_Click(object sender, EventArgs e)
        {
        
        }
    }

}
