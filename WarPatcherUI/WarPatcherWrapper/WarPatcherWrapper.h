#pragma once
#pragma unmanaged
#include <MYPManager.h>
#pragma managed


using namespace System;
using namespace System::Runtime::InteropServices;



namespace WarPatcherWrapper 
{
	public enum class LogLevel
	{
		LogLevel_VERBOSE = 1,
		LogLevel_INFO = 2,
		LogLevel_WARNING = 4,
		LogLevel_ERROR = 8
	};

	public enum class LogType
	{
		LogType_NETWORK = 0,
		LogType_SYSTEM = 1,
		LogType_PATCHER = 2,
		LogType_MYP = 3,
		LogType_UI = 4,
		LogType_CONSOLE = 5,
		LogType_FTP = 6,
		LogType_HTTP = 7,
	};

	[UnmanagedFunctionPointer(CallingConvention::Cdecl)]
	delegate void LogD(int type, int level, const char* msg);

	[UnmanagedFunctionPointer(CallingConvention::Cdecl)]
	delegate void MypLoadProgressDelegate(const char* filename, int index, int total);

	public delegate void LogDelegate2(LogType type, LogLevel level, String^ msg);
	public delegate void MypLoadProgressDelegate2(String^ filename, int index, int total);
	public delegate void OnServerStateDelegate2(int state);
	public delegate void OnServerConnectingDelegate2(const char* addr, int port);
	public delegate void OnServerDelegate2();

	public ref class AssetInfo
	{
	public:
		UInt32 Size;
		Int64 Hash;
		UInt32 CRC;
		bool Compressed;
	};

	public ref class ClientPatcher
	{
	public:
		event LogDelegate2^ OnLog;
		event MypLoadProgressDelegate2^ OnMYPLoadProgress;
		event OnServerStateDelegate2^ On_ServerState;
	public:

		ClientPatcher()
		{
			_logDel = gcnew LogD(this, &ClientPatcher::Log);
			IntPtr stub = Marshal::GetFunctionPointerForDelegate(_logDel);
			_logPtr = static_cast<LogFunc>(stub.ToPointer());
			GC::KeepAlive(_logDel);

			_mypLoadDel = gcnew MypLoadProgressDelegate(this, &ClientPatcher::MypLoadProgress);
			stub = Marshal::GetFunctionPointerForDelegate(_mypLoadDel);
			_mypLoadPtr = static_cast<MYPLoadProgress>(stub.ToPointer());
			GC::KeepAlive(_mypLoadDel);
		}
		
		UInt32 InitManager(String^ path)
		{
			char* nameStr = (char*)Marshal::StringToHGlobalAnsi(path).ToPointer();
			UInt32 result = 0;
			if (_mypManager != nullptr)
			{
				_mypManager->SaveAll();
				_mypManager->CloseMyps();
				delete _mypManager;
			}

			bool loaded = false;
			_mypManager = new MYPManager(_logPtr);
			result = _mypManager->Load(nameStr, _mypLoadPtr);


			Marshal::FreeHGlobal(IntPtr(nameStr));

			return result;
		}

		System::Collections::Generic::Dictionary<int, UInt32>^ GetArchiveHashes()
		{
			System::Collections::Generic::Dictionary<int, UInt32>^ ret = gcnew System::Collections::Generic::Dictionary<int, UInt32>();

			for (auto& k : _mypManager->GetArchiveHashes())
			{
				ret[k.first] = k.second;
			}

			return ret;
		}

		void DeleteAsset(int archiveID, Int64 hash)
		{
			FileEntry* entry = _mypManager->GetArchive((Archive)archiveID, false)->GetAsset(hash);
			if (entry != nullptr)
			{
				_mypManager->GetArchive((Archive)archiveID, false)->Delete(entry);
			}
		}

		void CreateArchive(String^ path, int archiveID)
		{
			char* nameStr = (char*)Marshal::StringToHGlobalAnsi(path).ToPointer();
			_mypManager->GetArchive((Archive)archiveID, true);
			Marshal::FreeHGlobal(IntPtr(nameStr));
		}

		void SaveArchive(int archiveID)
		{
			_mypManager->GetArchive((Archive)archiveID, false)->Save();
		}

		void CloseManager()
		{
			_mypManager->CloseMyps();
		}

		void Close(int archiveID)
		{
			_mypManager->GetArchive((Archive)archiveID, false)->Close();
		}

		void UpdateAsset(int archiveID, Int64 hash, bool compress, cli::array<byte>^ data, UInt32 crc)
		{
			pin_ptr<System::Byte> p = &data[0];
			unsigned char* pby = p;

			_mypManager->GetArchive((Archive)archiveID, false)->UpdateAsset(hash, pby, data->Length, compress, 0, 0, crc);
		}

		cli::array<byte>^ GetAsset(String^ assetName)
		{
			char* nameStr = (char*)Marshal::StringToHGlobalAnsi(assetName).ToPointer();
			Archive arch;
			int size = 0;
			uint8_t* data = _mypManager->GetAssetData(nameStr, size, arch);

			cli::array<byte>^ ret = gcnew cli::array<byte>(size);
			Marshal::Copy(IntPtr(data), ret, 0, size);
			return ret;
		}

		System::Collections::Generic::Dictionary<Int64, AssetInfo^>^ GetAssetHashes(int archiveID)
		{
			System::Collections::Generic::Dictionary<Int64, AssetInfo^>^ ret = gcnew System::Collections::Generic::Dictionary<Int64, AssetInfo^>();

			for (auto& k : _mypManager->GetArchive((Archive)archiveID, false)->GetAssets())
			{
				int64_t hash = ((int64_t)k.second->Hash2) << 32 | ((int64_t)k.second->Hash1);

				AssetInfo^ info = gcnew AssetInfo();
				info->Compressed = k.second->Compressed > 0;
				info->Size = k.second->UnCompressedSize;
				info->CRC = k.second->CRC32;
				info->Hash = hash;
				ret->Add(hash, info);
			}

			return ret;
		}

		String^ GetProgressMsg()
		{
			return _progressMsg;
		}

		int GetProgressIndex()
		{
			return _progresIndex;
		}

		int GetProgressTotal()
		{
			return _progressTotal;
		}

	private:
		MYPManager* _mypManager;
		
		LogD^ _logDel;
		LogFunc _logPtr;
		
		MypLoadProgressDelegate^ _mypLoadDel;
		MYPLoadProgress _mypLoadPtr;
		String^ _progressMsg;
		int _progresIndex=0;
		int _progressTotal = -1;

		
		void Log(int type, int level, const char* msg)
		{
			//Console::WriteLine(gcnew String(msg));
			OnLog((LogType)type, (LogLevel)level, gcnew String(msg));
		}

		void MypLoadProgress(const char* filename, int index, int total)
		{
			_progressMsg = gcnew String(filename);
			_progresIndex = index;
			_progressTotal = total;
			OnMYPLoadProgress(_progressMsg, index, total);
		}
	};
}
