
EventLogger = {}
function EventLogger.Initialize()
	RegisterEventHandler(SystemData.Events.ADD_USER_MAP_POINT, "EventLogger.ADD_USER_MAP_POINT")
	RegisterEventHandler(SystemData.Events.ADVANCED_WAR_FORTRESS_UPDATE, "EventLogger.ADVANCED_WAR_FORTRESS_UPDATE")
	RegisterEventHandler(SystemData.Events.ADVANCED_WAR_RELIC_UPDATE, "EventLogger.ADVANCED_WAR_RELIC_UPDATE")
	RegisterEventHandler(SystemData.Events.ADVANCED_WAR_RELIC_ZONE_UPDATE, "EventLogger.ADVANCED_WAR_RELIC_ZONE_UPDATE")
	RegisterEventHandler(SystemData.Events.ALL_MODULES_INITIALIZED, "EventLogger.ALL_MODULES_INITIALIZED")
	RegisterEventHandler(SystemData.Events.ALLIANCE_UPDATED, "EventLogger.ALLIANCE_UPDATED")
	RegisterEventHandler(SystemData.Events.APPLICATION_ONE_BUTTON_DIALOG, "EventLogger.APPLICATION_ONE_BUTTON_DIALOG")
	RegisterEventHandler(SystemData.Events.APPLICATION_REMOVE_DIALOG, "EventLogger.APPLICATION_REMOVE_DIALOG")
	RegisterEventHandler(SystemData.Events.APPLICATION_TWO_BUTTON_DIALOG, "EventLogger.APPLICATION_TWO_BUTTON_DIALOG")
	RegisterEventHandler(SystemData.Events.AUCTION_BID_RESULT_RECEIVED, "EventLogger.AUCTION_BID_RESULT_RECEIVED")
	RegisterEventHandler(SystemData.Events.AUCTION_INIT_RECEIVED, "EventLogger.AUCTION_INIT_RECEIVED")
	RegisterEventHandler(SystemData.Events.AUCTION_SEARCH_RESULT_RECEIVED, "EventLogger.AUCTION_SEARCH_RESULT_RECEIVED")
	RegisterEventHandler(SystemData.Events.AUTHENTICATION_ERROR, "EventLogger.AUTHENTICATION_ERROR")
	RegisterEventHandler(SystemData.Events.AUTHENTICATION_LOGIN_START, "EventLogger.AUTHENTICATION_LOGIN_START")
	RegisterEventHandler(SystemData.Events.AUTHENTICATION_RESPONSE, "EventLogger.AUTHENTICATION_RESPONSE")
	RegisterEventHandler(SystemData.Events.AUTHENTICATION_START, "EventLogger.AUTHENTICATION_START")
	RegisterEventHandler(SystemData.Events.AUTO_LOOT, "EventLogger.AUTO_LOOT")
	RegisterEventHandler(SystemData.Events.AUTOMATED_CHARACTER_CREATE, "EventLogger.AUTOMATED_CHARACTER_CREATE")
	RegisterEventHandler(SystemData.Events.BATTLEGROUP_ACCEPT_INVITATION, "EventLogger.BATTLEGROUP_ACCEPT_INVITATION")
	RegisterEventHandler(SystemData.Events.BATTLEGROUP_DECLINE_INVITATION, "EventLogger.BATTLEGROUP_DECLINE_INVITATION")
	RegisterEventHandler(SystemData.Events.BATTLEGROUP_MEMBER_UPDATED, "EventLogger.BATTLEGROUP_MEMBER_UPDATED")
	RegisterEventHandler(SystemData.Events.BATTLEGROUP_UPDATED, "EventLogger.BATTLEGROUP_UPDATED")
	RegisterEventHandler(SystemData.Events.BEGIN_CREATE_CHARACTER, "EventLogger.BEGIN_CREATE_CHARACTER")
	RegisterEventHandler(SystemData.Events.BEGIN_CUSTOMIZE, "EventLogger.BEGIN_CUSTOMIZE")
	RegisterEventHandler(SystemData.Events.BEGIN_ENTER_CHAT, "EventLogger.BEGIN_ENTER_CHAT")
	RegisterEventHandler(SystemData.Events.BEGIN_REALM_SELECT, "EventLogger.BEGIN_REALM_SELECT")
	RegisterEventHandler(SystemData.Events.BOLSTER_ACCEPT, "EventLogger.BOLSTER_ACCEPT")
	RegisterEventHandler(SystemData.Events.BOLSTER_CANCEL, "EventLogger.BOLSTER_CANCEL")
	RegisterEventHandler(SystemData.Events.BOLSTER_DECLINE, "EventLogger.BOLSTER_DECLINE")
	RegisterEventHandler(SystemData.Events.BOLSTER_OFFER, "EventLogger.BOLSTER_OFFER")
	RegisterEventHandler(SystemData.Events.BRACKET_CHAT, "EventLogger.BRACKET_CHAT")
	RegisterEventHandler(SystemData.Events.CAMPAIGN_CITY_UPDATED, "EventLogger.CAMPAIGN_CITY_UPDATED")
	RegisterEventHandler(SystemData.Events.CAMPAIGN_PAIRING_UPDATED, "EventLogger.CAMPAIGN_PAIRING_UPDATED")
	RegisterEventHandler(SystemData.Events.CAMPAIGN_ZONE_UPDATED, "EventLogger.CAMPAIGN_ZONE_UPDATED")
	RegisterEventHandler(SystemData.Events.CANCEL_LOGIN, "EventLogger.CANCEL_LOGIN")
	RegisterEventHandler(SystemData.Events.CHANNEL_NAMES_UPDATED, "EventLogger.CHANNEL_NAMES_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_CHARACTER_SELECTION_UPDATED, "EventLogger.CHARACTER_CHARACTER_SELECTION_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_CREATE_CAREER_UPDATED, "EventLogger.CHARACTER_CREATE_CAREER_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_CREATE_FEATURES_UPDATED, "EventLogger.CHARACTER_CREATE_FEATURES_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_CREATE_GENDER_UPDATED, "EventLogger.CHARACTER_CREATE_GENDER_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_CREATE_RACE_UPDATED, "EventLogger.CHARACTER_CREATE_RACE_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_DATA_LUA_VARS_UPDATED, "EventLogger.CHARACTER_DATA_LUA_VARS_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_LIST_RESPONSE, "EventLogger.CHARACTER_LIST_RESPONSE")
	RegisterEventHandler(SystemData.Events.CHARACTER_LIST_START, "EventLogger.CHARACTER_LIST_START")
	RegisterEventHandler(SystemData.Events.CHARACTER_MOUSE_OVER_UPDATED, "EventLogger.CHARACTER_MOUSE_OVER_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_PREGAME_ANIMATION_FINISHED, "EventLogger.CHARACTER_PREGAME_ANIMATION_FINISHED")
	RegisterEventHandler(SystemData.Events.CHARACTER_PREGAME_ANIMATION_STARTED, "EventLogger.CHARACTER_PREGAME_ANIMATION_STARTED")
	RegisterEventHandler(SystemData.Events.CHARACTER_PREGAME_FORCED_RANDOM_NAME_ACCEPT, "EventLogger.CHARACTER_PREGAME_FORCED_RANDOM_NAME_ACCEPT")
	RegisterEventHandler(SystemData.Events.CHARACTER_PREGAME_FORCED_RANDOM_NAME_START, "EventLogger.CHARACTER_PREGAME_FORCED_RANDOM_NAME_START")
	RegisterEventHandler(SystemData.Events.CHARACTER_PREGAME_NAMING_CONFLICT_POP_UP_WINDOW, "EventLogger.CHARACTER_PREGAME_NAMING_CONFLICT_POP_UP_WINDOW")
	RegisterEventHandler(SystemData.Events.CHARACTER_PREGAME_NAMING_CONFLICT_RESPONSE, "EventLogger.CHARACTER_PREGAME_NAMING_CONFLICT_RESPONSE")
	RegisterEventHandler(SystemData.Events.CHARACTER_PREGAME_RANDOM_NAME_LIST_RECEIVED, "EventLogger.CHARACTER_PREGAME_RANDOM_NAME_LIST_RECEIVED")
	RegisterEventHandler(SystemData.Events.CHARACTER_PREGAME_RANDOM_NAME_REQUESTED, "EventLogger.CHARACTER_PREGAME_RANDOM_NAME_REQUESTED")
	RegisterEventHandler(SystemData.Events.CHARACTER_PREGAME_RANDOM_NAME_UPDATE_CHAR_SELECT, "EventLogger.CHARACTER_PREGAME_RANDOM_NAME_UPDATE_CHAR_SELECT")
	RegisterEventHandler(SystemData.Events.CHARACTER_PREGAME_RANDOM_NAME_UPDATE_FORCED_SELECT, "EventLogger.CHARACTER_PREGAME_RANDOM_NAME_UPDATE_FORCED_SELECT")
	RegisterEventHandler(SystemData.Events.CHARACTER_PREGAME_TRANSFER_FLAG_UPDATED, "EventLogger.CHARACTER_PREGAME_TRANSFER_FLAG_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_QUEUE_UPDATED, "EventLogger.CHARACTER_QUEUE_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_REALM_OVER_UPDATED, "EventLogger.CHARACTER_REALM_OVER_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_REALM_UPDATED, "EventLogger.CHARACTER_REALM_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_SELECT_CURRENT_PAGE_UPDATED, "EventLogger.CHARACTER_SELECT_CURRENT_PAGE_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_SELECT_LOCKOUT_TIMER_UPDATED, "EventLogger.CHARACTER_SELECT_LOCKOUT_TIMER_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_SELECT_NUM_PAID_NAME_CHANGES_UPDATED, "EventLogger.CHARACTER_SELECT_NUM_PAID_NAME_CHANGES_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_SELECT_PAGES_UPDATED, "EventLogger.CHARACTER_SELECT_PAGES_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_SELECT_PAID_NAME_CHANGE_RESPONSE, "EventLogger.CHARACTER_SELECT_PAID_NAME_CHANGE_RESPONSE")
	RegisterEventHandler(SystemData.Events.CHARACTER_SETTINGS_ON_CHARACTER_DELETED, "EventLogger.CHARACTER_SETTINGS_ON_CHARACTER_DELETED")
	RegisterEventHandler(SystemData.Events.CHARACTER_SETTINGS_UPDATED, "EventLogger.CHARACTER_SETTINGS_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_STATE_UPDATED, "EventLogger.CHARACTER_STATE_UPDATED")
	RegisterEventHandler(SystemData.Events.CHARACTER_TEMPLATES_UPDATED, "EventLogger.CHARACTER_TEMPLATES_UPDATED")
	RegisterEventHandler(SystemData.Events.CHAT_REPLY, "EventLogger.CHAT_REPLY")
	RegisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "EventLogger.CHAT_TEXT_ARRIVED")
	RegisterEventHandler(SystemData.Events.CINEMA_INTRO_ENDED, "EventLogger.CINEMA_INTRO_ENDED")
	RegisterEventHandler(SystemData.Events.CINEMA_INTRO_STARTED, "EventLogger.CINEMA_INTRO_STARTED")
	RegisterEventHandler(SystemData.Events.CITY_CAPTURE_FLEE, "EventLogger.CITY_CAPTURE_FLEE")
	RegisterEventHandler(SystemData.Events.CITY_CAPTURE_LEAVE_QUEUE, "EventLogger.CITY_CAPTURE_LEAVE_QUEUE")
	RegisterEventHandler(SystemData.Events.CITY_CAPTURE_REQUEST_INSTANCE_DATA, "EventLogger.CITY_CAPTURE_REQUEST_INSTANCE_DATA")
	RegisterEventHandler(SystemData.Events.CITY_CAPTURE_SHOW_JOIN_PROMPT, "EventLogger.CITY_CAPTURE_SHOW_JOIN_PROMPT")
	RegisterEventHandler(SystemData.Events.CITY_CAPTURE_SHOW_LOW_LEVEL_JOIN_PROMPT, "EventLogger.CITY_CAPTURE_SHOW_LOW_LEVEL_JOIN_PROMPT")
	RegisterEventHandler(SystemData.Events.CITY_RATING_UPDATED, "EventLogger.CITY_RATING_UPDATED")
	RegisterEventHandler(SystemData.Events.CITY_SCENARIO_BEGIN, "EventLogger.CITY_SCENARIO_BEGIN")
	RegisterEventHandler(SystemData.Events.CITY_SCENARIO_END, "EventLogger.CITY_SCENARIO_END")
	RegisterEventHandler(SystemData.Events.CITY_SCENARIO_INSTANCE_ID_SELECTED, "EventLogger.CITY_SCENARIO_INSTANCE_ID_SELECTED")
	RegisterEventHandler(SystemData.Events.CITY_SCENARIO_UPDATE_POINTS, "EventLogger.CITY_SCENARIO_UPDATE_POINTS")
	RegisterEventHandler(SystemData.Events.CITY_SCENARIO_UPDATE_STATUS, "EventLogger.CITY_SCENARIO_UPDATE_STATUS")
	RegisterEventHandler(SystemData.Events.CITY_SCENARIO_UPDATE_TIME, "EventLogger.CITY_SCENARIO_UPDATE_TIME")
	RegisterEventHandler(SystemData.Events.CLAIM_REWARDS, "EventLogger.CLAIM_REWARDS")
	RegisterEventHandler(SystemData.Events.CLEAR_USER_MAP_POINT, "EventLogger.CLEAR_USER_MAP_POINT")
	RegisterEventHandler(SystemData.Events.CONTESTED_INSTANCE_CANCEL, "EventLogger.CONTESTED_INSTANCE_CANCEL")
	RegisterEventHandler(SystemData.Events.CONTESTED_INSTANCE_ENTER, "EventLogger.CONTESTED_INSTANCE_ENTER")
	RegisterEventHandler(SystemData.Events.CONTESTED_SCENARIO_SELECT_INSTANCE, "EventLogger.CONTESTED_SCENARIO_SELECT_INSTANCE")
	RegisterEventHandler(SystemData.Events.CONVERSATION_TEXT_ARRIVED, "EventLogger.CONVERSATION_TEXT_ARRIVED")
	RegisterEventHandler(SystemData.Events.CRAFTING_SHOW_WINDOW, "EventLogger.CRAFTING_SHOW_WINDOW")
	RegisterEventHandler(SystemData.Events.CREATE_CHARACTER, "EventLogger.CREATE_CHARACTER")
	RegisterEventHandler(SystemData.Events.CURRENT_EVENTS_JUMP_TIMER_UPDATED, "EventLogger.CURRENT_EVENTS_JUMP_TIMER_UPDATED")
	RegisterEventHandler(SystemData.Events.CURRENT_EVENTS_LIST_UPDATED, "EventLogger.CURRENT_EVENTS_LIST_UPDATED")
	RegisterEventHandler(SystemData.Events.CUSTOM_UI_SCALE_CHANGED, "EventLogger.CUSTOM_UI_SCALE_CHANGED")
	RegisterEventHandler(SystemData.Events.DELETE_CHARACTER, "EventLogger.DELETE_CHARACTER")
	RegisterEventHandler(SystemData.Events.END_ITEM_ENHANCEMENT, "EventLogger.END_ITEM_ENHANCEMENT")
	RegisterEventHandler(SystemData.Events.ENTER_KEY_PROCESSED, "EventLogger.ENTER_KEY_PROCESSED")
	RegisterEventHandler(SystemData.Events.ENTER_WORLD, "EventLogger.ENTER_WORLD")
	RegisterEventHandler(SystemData.Events.EQUIPMENT_UPGRADE_COST, "EventLogger.EQUIPMENT_UPGRADE_COST")
	RegisterEventHandler(SystemData.Events.EQUIPMENT_UPGRADE_RESULT, "EventLogger.EQUIPMENT_UPGRADE_RESULT")
	RegisterEventHandler(SystemData.Events.ESCAPE_KEY_PROCESSED, "EventLogger.ESCAPE_KEY_PROCESSED")
	RegisterEventHandler(SystemData.Events.EXIT_GAME, "EventLogger.EXIT_GAME")
	RegisterEventHandler(SystemData.Events.GAMEPLAY_RULESET_TYPE_UPDATED, "EventLogger.GAMEPLAY_RULESET_TYPE_UPDATED")
	RegisterEventHandler(SystemData.Events.GO_BACK, "EventLogger.GO_BACK")
	RegisterEventHandler(SystemData.Events.GROUP_ACCEPT_INVITATION, "EventLogger.GROUP_ACCEPT_INVITATION")
	RegisterEventHandler(SystemData.Events.GROUP_ACCEPT_REFERRAL, "EventLogger.GROUP_ACCEPT_REFERRAL")
	RegisterEventHandler(SystemData.Events.GROUP_DECLINE_INVITATION, "EventLogger.GROUP_DECLINE_INVITATION")
	RegisterEventHandler(SystemData.Events.GROUP_DECLINE_REFERRAL, "EventLogger.GROUP_DECLINE_REFERRAL")
	RegisterEventHandler(SystemData.Events.GROUP_EFFECTS_UPDATED, "EventLogger.GROUP_EFFECTS_UPDATED")
	RegisterEventHandler(SystemData.Events.GROUP_INVITE_PLAYER, "EventLogger.GROUP_INVITE_PLAYER")
	RegisterEventHandler(SystemData.Events.GROUP_KICK_PLAYER, "EventLogger.GROUP_KICK_PLAYER")
	RegisterEventHandler(SystemData.Events.GROUP_LEAVE, "EventLogger.GROUP_LEAVE")
	RegisterEventHandler(SystemData.Events.GROUP_PLAYER_ADDED, "EventLogger.GROUP_PLAYER_ADDED")
	RegisterEventHandler(SystemData.Events.GROUP_REFER_PLAYER, "EventLogger.GROUP_REFER_PLAYER")
	RegisterEventHandler(SystemData.Events.GROUP_SET_LEADER, "EventLogger.GROUP_SET_LEADER")
	RegisterEventHandler(SystemData.Events.GROUP_SET_MAIN_ASSIST, "EventLogger.GROUP_SET_MAIN_ASSIST")
	RegisterEventHandler(SystemData.Events.GROUP_SET_MASTER_LOOT_ON, "EventLogger.GROUP_SET_MASTER_LOOT_ON")
	RegisterEventHandler(SystemData.Events.GROUP_SETTINGS_PRIVACY_UPDATED, "EventLogger.GROUP_SETTINGS_PRIVACY_UPDATED")
	RegisterEventHandler(SystemData.Events.GROUP_SETTINGS_UPDATED, "EventLogger.GROUP_SETTINGS_UPDATED")
	RegisterEventHandler(SystemData.Events.GROUP_STATUS_UPDATED, "EventLogger.GROUP_STATUS_UPDATED")
	RegisterEventHandler(SystemData.Events.GROUP_UPDATED, "EventLogger.GROUP_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_ABILITIES_AVAILABLE_UPDATED, "EventLogger.GUILD_ABILITIES_AVAILABLE_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_ABILITIES_PURCHASED_UPDATED, "EventLogger.GUILD_ABILITIES_PURCHASED_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_APPOINTMENTS_UPDATED, "EventLogger.GUILD_APPOINTMENTS_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_BANNERS_UPDATED, "EventLogger.GUILD_BANNERS_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_ALLIANCE_INVITE_ACCEPT, "EventLogger.GUILD_COMMAND_ALLIANCE_INVITE_ACCEPT")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_ALLIANCE_INVITE_DECLINE, "EventLogger.GUILD_COMMAND_ALLIANCE_INVITE_DECLINE")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_ASSIGN, "EventLogger.GUILD_COMMAND_ASSIGN")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_CLAIM_ENTITY_ACCEPT, "EventLogger.GUILD_COMMAND_CLAIM_ENTITY_ACCEPT")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_CLAIM_ENTITY_DECLINE, "EventLogger.GUILD_COMMAND_CLAIM_ENTITY_DECLINE")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_CLAIM_ENTITY_LOW_FUNDS_ACCEPT, "EventLogger.GUILD_COMMAND_CLAIM_ENTITY_LOW_FUNDS_ACCEPT")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_CREATE, "EventLogger.GUILD_COMMAND_CREATE")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_DEMOTE, "EventLogger.GUILD_COMMAND_DEMOTE")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_INVITE, "EventLogger.GUILD_COMMAND_INVITE")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_INVITE_ACCEPT, "EventLogger.GUILD_COMMAND_INVITE_ACCEPT")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_INVITE_DECLINE, "EventLogger.GUILD_COMMAND_INVITE_DECLINE")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_LEAVE, "EventLogger.GUILD_COMMAND_LEAVE")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_PROMOTE, "EventLogger.GUILD_COMMAND_PROMOTE")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_PURCHASE_TACTIC, "EventLogger.GUILD_COMMAND_PURCHASE_TACTIC")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_REMOVE, "EventLogger.GUILD_COMMAND_REMOVE")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_RENAME, "EventLogger.GUILD_COMMAND_RENAME")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_UNCLAIM_ENTITY_DECLINE, "EventLogger.GUILD_COMMAND_UNCLAIM_ENTITY_DECLINE")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_UNCLAIM_ENTITY_NOPENALTY_ACCEPT, "EventLogger.GUILD_COMMAND_UNCLAIM_ENTITY_NOPENALTY_ACCEPT")
	RegisterEventHandler(SystemData.Events.GUILD_COMMAND_UNCLAIM_ENTITY_PENALTY_ACCEPT, "EventLogger.GUILD_COMMAND_UNCLAIM_ENTITY_PENALTY_ACCEPT")
	RegisterEventHandler(SystemData.Events.GUILD_EXP_UPDATED, "EventLogger.GUILD_EXP_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_HERALDRY_UPDATED, "EventLogger.GUILD_HERALDRY_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_INFO_UPDATED, "EventLogger.GUILD_INFO_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_KEEP_UPDATED, "EventLogger.GUILD_KEEP_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_MEMBER_ADDED, "EventLogger.GUILD_MEMBER_ADDED")
	RegisterEventHandler(SystemData.Events.GUILD_MEMBER_NOTES_UPDATED, "EventLogger.GUILD_MEMBER_NOTES_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_MEMBER_REMOVED, "EventLogger.GUILD_MEMBER_REMOVED")
	RegisterEventHandler(SystemData.Events.GUILD_MEMBER_UPDATED, "EventLogger.GUILD_MEMBER_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_NEWBIE_GUILD_STATUS_UPDATED, "EventLogger.GUILD_NEWBIE_GUILD_STATUS_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_NEWS_UPDATED, "EventLogger.GUILD_NEWS_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_PERMISSIONS_UPDATED, "EventLogger.GUILD_PERMISSIONS_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_PERSONAL_STATISTICS_UPDATED, "EventLogger.GUILD_PERSONAL_STATISTICS_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_POLL_UPDATED, "EventLogger.GUILD_POLL_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_RECRUITMENT_PROFILE_UPDATED, "EventLogger.GUILD_RECRUITMENT_PROFILE_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_RECRUITMENT_SEARCH_RESULTS_UPDATED, "EventLogger.GUILD_RECRUITMENT_SEARCH_RESULTS_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_REFRESH, "EventLogger.GUILD_REFRESH")
	RegisterEventHandler(SystemData.Events.GUILD_REWARDS_UPDATED, "EventLogger.GUILD_REWARDS_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_ROSTER_INIT, "EventLogger.GUILD_ROSTER_INIT")
	RegisterEventHandler(SystemData.Events.GUILD_STATISTICS_UPDATED, "EventLogger.GUILD_STATISTICS_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_TAX_TITHE_UPDATED, "EventLogger.GUILD_TAX_TITHE_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_UNGUILDED, "EventLogger.GUILD_UNGUILDED")
	RegisterEventHandler(SystemData.Events.GUILD_VAULT_CAPACITY_UPDATED, "EventLogger.GUILD_VAULT_CAPACITY_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_VAULT_COIN_UPDATED, "EventLogger.GUILD_VAULT_COIN_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_VAULT_ITEMS_UPDATED, "EventLogger.GUILD_VAULT_ITEMS_UPDATED")
	RegisterEventHandler(SystemData.Events.GUILD_VAULT_SLOT_LOCKED, "EventLogger.GUILD_VAULT_SLOT_LOCKED")
	RegisterEventHandler(SystemData.Events.GUILD_VAULT_SLOT_UNLOCKED, "EventLogger.GUILD_VAULT_SLOT_UNLOCKED")
	RegisterEventHandler(SystemData.Events.HELP_LOG_UPDATED, "EventLogger.HELP_LOG_UPDATED")
	RegisterEventHandler(SystemData.Events.HELP_STATUS_UPDATED, "EventLogger.HELP_STATUS_UPDATED")
	RegisterEventHandler(SystemData.Events.HELP_TIP_UPDATED, "EventLogger.HELP_TIP_UPDATED")
	RegisterEventHandler(SystemData.Events.INFO_ALERT, "EventLogger.INFO_ALERT")
	RegisterEventHandler(SystemData.Events.INTERACT_ACCEPT_QUEST, "EventLogger.INTERACT_ACCEPT_QUEST")
	RegisterEventHandler(SystemData.Events.INTERACT_ALLIANCE_FORM_ACCEPT, "EventLogger.INTERACT_ALLIANCE_FORM_ACCEPT")
	RegisterEventHandler(SystemData.Events.INTERACT_ALLIANCE_FORM_DECLINE, "EventLogger.INTERACT_ALLIANCE_FORM_DECLINE")
	RegisterEventHandler(SystemData.Events.INTERACT_BARBERSHOP_FEATURE_UPDATE, "EventLogger.INTERACT_BARBERSHOP_FEATURE_UPDATE")
	RegisterEventHandler(SystemData.Events.INTERACT_BARBERSHOP_OPEN, "EventLogger.INTERACT_BARBERSHOP_OPEN")
	RegisterEventHandler(SystemData.Events.INTERACT_BARBERSHOP_RESULT, "EventLogger.INTERACT_BARBERSHOP_RESULT")
	RegisterEventHandler(SystemData.Events.INTERACT_BIND, "EventLogger.INTERACT_BIND")
	RegisterEventHandler(SystemData.Events.INTERACT_BUY_BACK_ITEM, "EventLogger.INTERACT_BUY_BACK_ITEM")
	RegisterEventHandler(SystemData.Events.INTERACT_BUY_ITEM, "EventLogger.INTERACT_BUY_ITEM")
	RegisterEventHandler(SystemData.Events.INTERACT_BUY_ITEM_WITH_ALT_CURRENCY, "EventLogger.INTERACT_BUY_ITEM_WITH_ALT_CURRENCY")
	RegisterEventHandler(SystemData.Events.INTERACT_CLOSE, "EventLogger.INTERACT_CLOSE")
	RegisterEventHandler(SystemData.Events.INTERACT_COMPLETE_QUEST, "EventLogger.INTERACT_COMPLETE_QUEST")
	RegisterEventHandler(SystemData.Events.INTERACT_DEFAULT, "EventLogger.INTERACT_DEFAULT")
	RegisterEventHandler(SystemData.Events.INTERACT_DIVINEFAVORALTAR_OPEN, "EventLogger.INTERACT_DIVINEFAVORALTAR_OPEN")
	RegisterEventHandler(SystemData.Events.INTERACT_DIVINEFAVORALTAR_UPDATE, "EventLogger.INTERACT_DIVINEFAVORALTAR_UPDATE")
	RegisterEventHandler(SystemData.Events.INTERACT_DONE, "EventLogger.INTERACT_DONE")
	RegisterEventHandler(SystemData.Events.INTERACT_DYE_MERCHANT, "EventLogger.INTERACT_DYE_MERCHANT")
	RegisterEventHandler(SystemData.Events.INTERACT_DYE_MERCHANT_DYE_ALL, "EventLogger.INTERACT_DYE_MERCHANT_DYE_ALL")
	RegisterEventHandler(SystemData.Events.INTERACT_DYE_MERCHANT_DYE_SINGLE, "EventLogger.INTERACT_DYE_MERCHANT_DYE_SINGLE")
	RegisterEventHandler(SystemData.Events.INTERACT_EQUIPMENT_UPGRADE_OPEN, "EventLogger.INTERACT_EQUIPMENT_UPGRADE_OPEN")
	RegisterEventHandler(SystemData.Events.INTERACT_FLIGHT_TRAVEL, "EventLogger.INTERACT_FLIGHT_TRAVEL")
	RegisterEventHandler(SystemData.Events.INTERACT_GROUP_JOIN_SCENARIO_QUEUE, "EventLogger.INTERACT_GROUP_JOIN_SCENARIO_QUEUE")
	RegisterEventHandler(SystemData.Events.INTERACT_GROUP_JOIN_SCENARIO_QUEUE_ALL, "EventLogger.INTERACT_GROUP_JOIN_SCENARIO_QUEUE_ALL")
	RegisterEventHandler(SystemData.Events.INTERACT_GUILD_CREATION_COMPLETE, "EventLogger.INTERACT_GUILD_CREATION_COMPLETE")
	RegisterEventHandler(SystemData.Events.INTERACT_GUILD_FORM_ACCEPT, "EventLogger.INTERACT_GUILD_FORM_ACCEPT")
	RegisterEventHandler(SystemData.Events.INTERACT_GUILD_FORM_DECLINE, "EventLogger.INTERACT_GUILD_FORM_DECLINE")
	RegisterEventHandler(SystemData.Events.INTERACT_GUILD_NPC_ERROR, "EventLogger.INTERACT_GUILD_NPC_ERROR")
	RegisterEventHandler(SystemData.Events.INTERACT_GUILD_RENAME_BEGIN, "EventLogger.INTERACT_GUILD_RENAME_BEGIN")
	RegisterEventHandler(SystemData.Events.INTERACT_GUILD_SHOW_FORM, "EventLogger.INTERACT_GUILD_SHOW_FORM")
	RegisterEventHandler(SystemData.Events.INTERACT_GUILD_VAULT_CLOSED, "EventLogger.INTERACT_GUILD_VAULT_CLOSED")
	RegisterEventHandler(SystemData.Events.INTERACT_GUILD_VAULT_OPEN, "EventLogger.INTERACT_GUILD_VAULT_OPEN")
	RegisterEventHandler(SystemData.Events.INTERACT_HEALER, "EventLogger.INTERACT_HEALER")
	RegisterEventHandler(SystemData.Events.INTERACT_JOIN_SCENARIO_QUEUE, "EventLogger.INTERACT_JOIN_SCENARIO_QUEUE")
	RegisterEventHandler(SystemData.Events.INTERACT_JOIN_SCENARIO_QUEUE_ALL, "EventLogger.INTERACT_JOIN_SCENARIO_QUEUE_ALL")
	RegisterEventHandler(SystemData.Events.INTERACT_KEEP_UPGRADE_OPEN, "EventLogger.INTERACT_KEEP_UPGRADE_OPEN")
	RegisterEventHandler(SystemData.Events.INTERACT_KEEP_UPGRADE_UPDATE, "EventLogger.INTERACT_KEEP_UPGRADE_UPDATE")
	RegisterEventHandler(SystemData.Events.INTERACT_LAST_NAME_MERCHANT, "EventLogger.INTERACT_LAST_NAME_MERCHANT")
	RegisterEventHandler(SystemData.Events.INTERACT_LAST_NAME_MERCHANT_BUY, "EventLogger.INTERACT_LAST_NAME_MERCHANT_BUY")
	RegisterEventHandler(SystemData.Events.INTERACT_LEAVE_SCENARIO_QUEUE, "EventLogger.INTERACT_LEAVE_SCENARIO_QUEUE")
	RegisterEventHandler(SystemData.Events.INTERACT_LOOT_CLOSE, "EventLogger.INTERACT_LOOT_CLOSE")
	RegisterEventHandler(SystemData.Events.INTERACT_LOOT_ROLL_FIRST_ITEM, "EventLogger.INTERACT_LOOT_ROLL_FIRST_ITEM")
	RegisterEventHandler(SystemData.Events.INTERACT_MAILBOX_CLOSED, "EventLogger.INTERACT_MAILBOX_CLOSED")
	RegisterEventHandler(SystemData.Events.INTERACT_MAILBOX_OPEN, "EventLogger.INTERACT_MAILBOX_OPEN")
	RegisterEventHandler(SystemData.Events.INTERACT_OPEN_BANK, "EventLogger.INTERACT_OPEN_BANK")
	RegisterEventHandler(SystemData.Events.INTERACT_PURCHASE_HEAL, "EventLogger.INTERACT_PURCHASE_HEAL")
	RegisterEventHandler(SystemData.Events.INTERACT_REPAIR, "EventLogger.INTERACT_REPAIR")
	RegisterEventHandler(SystemData.Events.INTERACT_SELECT_FLIGHT_POINT, "EventLogger.INTERACT_SELECT_FLIGHT_POINT")
	RegisterEventHandler(SystemData.Events.INTERACT_SELECT_FLIGHTMASTER, "EventLogger.INTERACT_SELECT_FLIGHTMASTER")
	RegisterEventHandler(SystemData.Events.INTERACT_SELECT_QUEST, "EventLogger.INTERACT_SELECT_QUEST")
	RegisterEventHandler(SystemData.Events.INTERACT_SELECT_STORE, "EventLogger.INTERACT_SELECT_STORE")
	RegisterEventHandler(SystemData.Events.INTERACT_SELECT_TRAINING, "EventLogger.INTERACT_SELECT_TRAINING")
	RegisterEventHandler(SystemData.Events.INTERACT_SELL_ITEM, "EventLogger.INTERACT_SELL_ITEM")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_ADVANCED_WAR_WINDOW, "EventLogger.INTERACT_SHOW_ADVANCED_WAR_WINDOW")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_CLAIM_WINDOW, "EventLogger.INTERACT_SHOW_CLAIM_WINDOW")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_EVENT_REWARDS, "EventLogger.INTERACT_SHOW_EVENT_REWARDS")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_FLIGHTMASTER, "EventLogger.INTERACT_SHOW_FLIGHTMASTER")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_HEALER, "EventLogger.INTERACT_SHOW_HEALER")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_INFLUENCE_REWARDS, "EventLogger.INTERACT_SHOW_INFLUENCE_REWARDS")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_ITEM_CONTAINER_LOOT, "EventLogger.INTERACT_SHOW_ITEM_CONTAINER_LOOT")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_LIBRARIAN, "EventLogger.INTERACT_SHOW_LIBRARIAN")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_LOOT, "EventLogger.INTERACT_SHOW_LOOT")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_LOOT_ROLL_DATA, "EventLogger.INTERACT_SHOW_LOOT_ROLL_DATA")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_PQ_LOOT, "EventLogger.INTERACT_SHOW_PQ_LOOT")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_QUEST, "EventLogger.INTERACT_SHOW_QUEST")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_SCENARIO_QUEUE_LIST, "EventLogger.INTERACT_SHOW_SCENARIO_QUEUE_LIST")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_SIEGE_PAD_BUILD_LIST, "EventLogger.INTERACT_SHOW_SIEGE_PAD_BUILD_LIST")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_STORE, "EventLogger.INTERACT_SHOW_STORE")
	RegisterEventHandler(SystemData.Events.INTERACT_SHOW_TRAINING, "EventLogger.INTERACT_SHOW_TRAINING")
	RegisterEventHandler(SystemData.Events.INTERACT_UPDATE_LIBRARIAN, "EventLogger.INTERACT_UPDATE_LIBRARIAN")
	RegisterEventHandler(SystemData.Events.INTERACT_UPDATED_SCENARIO_QUEUE_LIST, "EventLogger.INTERACT_UPDATED_SCENARIO_QUEUE_LIST")
	RegisterEventHandler(SystemData.Events.INTERFACE_RELOADED, "EventLogger.INTERFACE_RELOADED")
	RegisterEventHandler(SystemData.Events.ITEM_SET_DATA_ARRIVED, "EventLogger.ITEM_SET_DATA_ARRIVED")
	RegisterEventHandler(SystemData.Events.ITEM_SET_DATA_UPDATED, "EventLogger.ITEM_SET_DATA_UPDATED")
	RegisterEventHandler(SystemData.Events.KEYBINDINGS_UPDATED, "EventLogger.KEYBINDINGS_UPDATED")
	RegisterEventHandler(SystemData.Events.KILLER_NAME_UPDATED, "EventLogger.KILLER_NAME_UPDATED")
--	RegisterEventHandler(SystemData.Events.L_BUTTON_DOWN_PROCESSED, "EventLogger.L_BUTTON_DOWN_PROCESSED")
--	RegisterEventHandler(SystemData.Events.L_BUTTON_UP_PROCESSED, "EventLogger.L_BUTTON_UP_PROCESSED")
	RegisterEventHandler(SystemData.Events.LANGUAGE_TOGGLED, "EventLogger.LANGUAGE_TOGGLED")
	RegisterEventHandler(SystemData.Events.LOADING_BEGIN, "EventLogger.LOADING_BEGIN")
	RegisterEventHandler(SystemData.Events.LOADING_END, "EventLogger.LOADING_END")
	RegisterEventHandler(SystemData.Events.LOADING_PROGRESS_UPDATED, "EventLogger.LOADING_PROGRESS_UPDATED")
	RegisterEventHandler(SystemData.Events.LOCKOUTS_UPDATED, "EventLogger.LOCKOUTS_UPDATED")
	RegisterEventHandler(SystemData.Events.LOG_OUT, "EventLogger.LOG_OUT")
	RegisterEventHandler(SystemData.Events.LOGIN, "EventLogger.LOGIN")
	RegisterEventHandler(SystemData.Events.LOGIN_LOCAL, "EventLogger.LOGIN_LOCAL")
	RegisterEventHandler(SystemData.Events.LOGIN_PROGRESS_STARTING, "EventLogger.LOGIN_PROGRESS_STARTING")
	RegisterEventHandler(SystemData.Events.LOGIN_PROGRESS_UPDATED, "EventLogger.LOGIN_PROGRESS_UPDATED")
	RegisterEventHandler(SystemData.Events.LOOT_ALL, "EventLogger.LOOT_ALL")
	RegisterEventHandler(SystemData.Events.LOOT_GREED, "EventLogger.LOOT_GREED")
	RegisterEventHandler(SystemData.Events.LOOT_NEED, "EventLogger.LOOT_NEED")
	RegisterEventHandler(SystemData.Events.LOOT_PASS, "EventLogger.LOOT_PASS")
	RegisterEventHandler(SystemData.Events.M_BUTTON_DOWN_PROCESSED, "EventLogger.M_BUTTON_DOWN_PROCESSED")
	RegisterEventHandler(SystemData.Events.M_BUTTON_UP_PROCESSED, "EventLogger.M_BUTTON_UP_PROCESSED")
	RegisterEventHandler(SystemData.Events.MACRO_UPDATED, "EventLogger.MACRO_UPDATED")
	RegisterEventHandler(SystemData.Events.MACROS_LOADED, "EventLogger.MACROS_LOADED")
	RegisterEventHandler(SystemData.Events.MAILBOX_HEADER_UPDATED, "EventLogger.MAILBOX_HEADER_UPDATED")
	RegisterEventHandler(SystemData.Events.MAILBOX_HEADERS_UPDATED, "EventLogger.MAILBOX_HEADERS_UPDATED")
	RegisterEventHandler(SystemData.Events.MAILBOX_MESSAGE_DELETED, "EventLogger.MAILBOX_MESSAGE_DELETED")
	RegisterEventHandler(SystemData.Events.MAILBOX_MESSAGE_OPENED, "EventLogger.MAILBOX_MESSAGE_OPENED")
	RegisterEventHandler(SystemData.Events.MAILBOX_POSTAGE_COST_UPDATED, "EventLogger.MAILBOX_POSTAGE_COST_UPDATED")
	RegisterEventHandler(SystemData.Events.MAILBOX_RESULTS_UPDATED, "EventLogger.MAILBOX_RESULTS_UPDATED")
	RegisterEventHandler(SystemData.Events.MAILBOX_UNREAD_COUNT_CHANGED, "EventLogger.MAILBOX_UNREAD_COUNT_CHANGED")
	RegisterEventHandler(SystemData.Events.MAIN_ASSIST_TARGET_UPDATED, "EventLogger.MAIN_ASSIST_TARGET_UPDATED")
	RegisterEventHandler(SystemData.Events.MAP_SETTINGS_UPDATED, "EventLogger.MAP_SETTINGS_UPDATED")
	RegisterEventHandler(SystemData.Events.MOUSE_DOWN_ON_CHAR_SELECT_NIF, "EventLogger.MOUSE_DOWN_ON_CHAR_SELECT_NIF")
	RegisterEventHandler(SystemData.Events.MOUSE_DOWN_ON_NIF, "EventLogger.MOUSE_DOWN_ON_NIF")
	RegisterEventHandler(SystemData.Events.MOUSE_UP_ON_CHAR_SELECT_NIF, "EventLogger.MOUSE_UP_ON_CHAR_SELECT_NIF")
	RegisterEventHandler(SystemData.Events.MOUSE_UP_ON_NIF, "EventLogger.MOUSE_UP_ON_NIF")
	RegisterEventHandler(SystemData.Events.MOUSEOVER_WORLD_OBJECT, "EventLogger.MOUSEOVER_WORLD_OBJECT")
	RegisterEventHandler(SystemData.Events.MOVE_INVENTORY_OBJECT, "EventLogger.MOVE_INVENTORY_OBJECT")
	RegisterEventHandler(SystemData.Events.OBJECTIVE_AREA_EXIT, "EventLogger.OBJECTIVE_AREA_EXIT")
	RegisterEventHandler(SystemData.Events.OBJECTIVE_CONTROL_POINTS_UPDATED, "EventLogger.OBJECTIVE_CONTROL_POINTS_UPDATED")
	RegisterEventHandler(SystemData.Events.OBJECTIVE_MAP_TIMER_UPDATED, "EventLogger.OBJECTIVE_MAP_TIMER_UPDATED")
	RegisterEventHandler(SystemData.Events.OBJECTIVE_OWNER_UPDATED, "EventLogger.OBJECTIVE_OWNER_UPDATED")
	RegisterEventHandler(SystemData.Events.OPEN_LFM_WINDOW, "EventLogger.OPEN_LFM_WINDOW")
	RegisterEventHandler(SystemData.Events.PAIRING_MAP_HOTSPOT_DATA_UPDATED, "EventLogger.PAIRING_MAP_HOTSPOT_DATA_UPDATED")
	RegisterEventHandler(SystemData.Events.PAPERDOLL_SPIN_LEFT, "EventLogger.PAPERDOLL_SPIN_LEFT")
	RegisterEventHandler(SystemData.Events.PAPERDOLL_SPIN_RIGHT, "EventLogger.PAPERDOLL_SPIN_RIGHT")
	RegisterEventHandler(SystemData.Events.PET_AGGRESSIVE, "EventLogger.PET_AGGRESSIVE")
	RegisterEventHandler(SystemData.Events.PET_DEFENSIVE, "EventLogger.PET_DEFENSIVE")
	RegisterEventHandler(SystemData.Events.PET_PASSIVE, "EventLogger.PET_PASSIVE")
	RegisterEventHandler(SystemData.Events.PLAY, "EventLogger.PLAY")
	RegisterEventHandler(SystemData.Events.PLAY_AS_MONSTER_STATUS, "EventLogger.PLAY_AS_MONSTER_STATUS")
	RegisterEventHandler(SystemData.Events.PLAYER_ABILITIES_LIST_UPDATED, "EventLogger.PLAYER_ABILITIES_LIST_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_ABILITIES_QUEUED_UPDATED, "EventLogger.PLAYER_ABILITIES_QUEUED_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_ABILITY_TOGGLED, "EventLogger.PLAYER_ABILITY_TOGGLED")
	RegisterEventHandler(SystemData.Events.PLAYER_ACTIVE_TACTICS_UPDATED, "EventLogger.PLAYER_ACTIVE_TACTICS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_ACTIVE_TITLE_UPDATED, "EventLogger.PLAYER_ACTIVE_TITLE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_ADVANCE_ALERT, "EventLogger.PLAYER_ADVANCE_ALERT")
	RegisterEventHandler(SystemData.Events.PLAYER_AGRO_MODE_UPDATED, "EventLogger.PLAYER_AGRO_MODE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_AREA_CHANGED, "EventLogger.PLAYER_AREA_CHANGED")
	RegisterEventHandler(SystemData.Events.PLAYER_AREA_NAME_CHANGED, "EventLogger.PLAYER_AREA_NAME_CHANGED")
	RegisterEventHandler(SystemData.Events.PLAYER_BANK_SLOT_UPDATED, "EventLogger.PLAYER_BANK_SLOT_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_BATTLE_LEVEL_UPDATED, "EventLogger.PLAYER_BATTLE_LEVEL_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_BEGIN_CAST, "EventLogger.PLAYER_BEGIN_CAST")
	RegisterEventHandler(SystemData.Events.PLAYER_BLOCKED_ABILITIES_UPDATED, "EventLogger.PLAYER_BLOCKED_ABILITIES_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_CAREER_CATEGORY_UPDATED, "EventLogger.PLAYER_CAREER_CATEGORY_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_CAREER_LINE_UPDATED, "EventLogger.PLAYER_CAREER_LINE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_CAREER_RANK_UPDATED, "EventLogger.PLAYER_CAREER_RANK_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_CAREER_RESOURCE_UPDATED, "EventLogger.PLAYER_CAREER_RESOURCE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_CAST_TIMER_SETBACK, "EventLogger.PLAYER_CAST_TIMER_SETBACK")
	RegisterEventHandler(SystemData.Events.PLAYER_CHAPTER_UPDATED, "EventLogger.PLAYER_CHAPTER_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_COMBAT_FLAG_UPDATED, "EventLogger.PLAYER_COMBAT_FLAG_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_COOLDOWN_TIMER_SET, "EventLogger.PLAYER_COOLDOWN_TIMER_SET")
	RegisterEventHandler(SystemData.Events.PLAYER_CRAFTING_INVENTORY_UPDATED, "EventLogger.PLAYER_CRAFTING_INVENTORY_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_CRAFTING_SLOT_UPDATED, "EventLogger.PLAYER_CRAFTING_SLOT_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_CRAFTING_UPDATED, "EventLogger.PLAYER_CRAFTING_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_CULTIVATION_UPDATED, "EventLogger.PLAYER_CULTIVATION_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_CUR_ACTION_POINTS_UPDATED, "EventLogger.PLAYER_CUR_ACTION_POINTS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_CUR_HIT_POINTS_UPDATED, "EventLogger.PLAYER_CUR_HIT_POINTS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_CURRENCY_SLOT_UPDATED, "EventLogger.PLAYER_CURRENCY_SLOT_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_DATA_RESPONSE, "EventLogger.PLAYER_DATA_RESPONSE")
	RegisterEventHandler(SystemData.Events.PLAYER_DATA_START, "EventLogger.PLAYER_DATA_START")
	RegisterEventHandler(SystemData.Events.PLAYER_DEATH, "EventLogger.PLAYER_DEATH")
	RegisterEventHandler(SystemData.Events.PLAYER_DEATH_CLEARED, "EventLogger.PLAYER_DEATH_CLEARED")
	RegisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED, "EventLogger.PLAYER_EFFECTS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_END_CAST, "EventLogger.PLAYER_END_CAST")
	RegisterEventHandler(SystemData.Events.PLAYER_EQUIPMENT_SLOT_UPDATED, "EventLogger.PLAYER_EQUIPMENT_SLOT_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_EXP_TABLE_UPDATED, "EventLogger.PLAYER_EXP_TABLE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_EXP_UPDATED, "EventLogger.PLAYER_EXP_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_GROUP_LEADER_STATUS_UPDATED, "EventLogger.PLAYER_GROUP_LEADER_STATUS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_HEALTH_FADE_UPDATED, "EventLogger.PLAYER_HEALTH_FADE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_HOT_BAR_ENABLED_STATE_CHANGED, "EventLogger.PLAYER_HOT_BAR_ENABLED_STATE_CHANGED")
	RegisterEventHandler(SystemData.Events.PLAYER_HOT_BAR_PAGE_UPDATED, "EventLogger.PLAYER_HOT_BAR_PAGE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_HOT_BAR_UPDATED, "EventLogger.PLAYER_HOT_BAR_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_INFLUENCE_RANK_UPDATED, "EventLogger.PLAYER_INFLUENCE_RANK_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_INFLUENCE_REWARDS_UPDATED, "EventLogger.PLAYER_INFLUENCE_REWARDS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_INFLUENCE_UPDATED, "EventLogger.PLAYER_INFLUENCE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_INFO_CHANGED, "EventLogger.PLAYER_INFO_CHANGED")
	RegisterEventHandler(SystemData.Events.PLAYER_INTERNAL_BUFF_UPDATED, "EventLogger.PLAYER_INTERNAL_BUFF_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_INVENTORY_OVERFLOW_UPDATED, "EventLogger.PLAYER_INVENTORY_OVERFLOW_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_INVENTORY_SLOT_UPDATED, "EventLogger.PLAYER_INVENTORY_SLOT_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_IS_BEING_THROWN, "EventLogger.PLAYER_IS_BEING_THROWN")
	RegisterEventHandler(SystemData.Events.PLAYER_IS_DISARMED, "EventLogger.PLAYER_IS_DISARMED")
	RegisterEventHandler(SystemData.Events.PLAYER_IS_IMMUNE_TO_DISABLES, "EventLogger.PLAYER_IS_IMMUNE_TO_DISABLES")
	RegisterEventHandler(SystemData.Events.PLAYER_IS_IMMUNE_TO_MOVEMENT_IMPARING, "EventLogger.PLAYER_IS_IMMUNE_TO_MOVEMENT_IMPARING")
	RegisterEventHandler(SystemData.Events.PLAYER_IS_SILENCED, "EventLogger.PLAYER_IS_SILENCED")
	RegisterEventHandler(SystemData.Events.PLAYER_IS_UNABLE_TO_MOVE, "EventLogger.PLAYER_IS_UNABLE_TO_MOVE")
	RegisterEventHandler(SystemData.Events.PLAYER_KILLING_SPREE_UPDATED, "EventLogger.PLAYER_KILLING_SPREE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_LEARNED_ABOUT_UI_ELEMENT, "EventLogger.PLAYER_LEARNED_ABOUT_UI_ELEMENT")
	RegisterEventHandler(SystemData.Events.PLAYER_MAIN_ASSIST_UPDATED, "EventLogger.PLAYER_MAIN_ASSIST_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_MAX_ACTION_POINTS_UPDATED, "EventLogger.PLAYER_MAX_ACTION_POINTS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_MAX_HIT_POINTS_UPDATED, "EventLogger.PLAYER_MAX_HIT_POINTS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_MONEY_UPDATED, "EventLogger.PLAYER_MONEY_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_MORALE_BAR_UPDATED, "EventLogger.PLAYER_MORALE_BAR_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_MORALE_UPDATED, "EventLogger.PLAYER_MORALE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_NEW_ABILITY_LEARNED, "EventLogger.PLAYER_NEW_ABILITY_LEARNED")
	RegisterEventHandler(SystemData.Events.PLAYER_NEW_NUMBER_OF_BACKPACK_SLOTS, "EventLogger.PLAYER_NEW_NUMBER_OF_BACKPACK_SLOTS")
	RegisterEventHandler(SystemData.Events.PLAYER_NEW_NUMBER_OF_BANK_SLOTS, "EventLogger.PLAYER_NEW_NUMBER_OF_BANK_SLOTS")
	RegisterEventHandler(SystemData.Events.PLAYER_NEW_PET_ABILITY_LEARNED, "EventLogger.PLAYER_NEW_PET_ABILITY_LEARNED")
	RegisterEventHandler(SystemData.Events.PLAYER_NUM_TACTIC_SLOTS_UPDATED, "EventLogger.PLAYER_NUM_TACTIC_SLOTS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_OBJECTIVE_CONTRIBUTION_UPDATED, "EventLogger.PLAYER_OBJECTIVE_CONTRIBUTION_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_OBJECTIVES_UPDATED, "EventLogger.PLAYER_OBJECTIVES_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_OFFER_ITEMS_UPDATED, "EventLogger.PLAYER_OFFER_ITEMS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_PET_HEALTH_UPDATED, "EventLogger.PLAYER_PET_HEALTH_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_PET_STATE_UPDATED, "EventLogger.PLAYER_PET_STATE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_PET_TARGET_HEALTH_UPDATED, "EventLogger.PLAYER_PET_TARGET_HEALTH_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_PET_TARGET_UPDATED, "EventLogger.PLAYER_PET_TARGET_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_PET_UPDATED, "EventLogger.PLAYER_PET_UPDATED")
	--RegisterEventHandler(SystemData.Events.PLAYER_POSITION_UPDATED, "EventLogger.PLAYER_POSITION_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_QUEST_ITEM_SLOT_UPDATED, "EventLogger.PLAYER_QUEST_ITEM_SLOT_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_RACE_UPDATED, "EventLogger.PLAYER_RACE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_REALM_BONUS_UPDATED, "EventLogger.PLAYER_REALM_BONUS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_REMOVED_ITEM, "EventLogger.PLAYER_REMOVED_ITEM")
	RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_RANK_UPDATED, "EventLogger.PLAYER_RENOWN_RANK_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_TITLE_UPDATED, "EventLogger.PLAYER_RENOWN_TITLE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_RENOWN_UPDATED, "EventLogger.PLAYER_RENOWN_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_RVR_FLAG_UPDATED, "EventLogger.PLAYER_RVR_FLAG_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_RVR_STATS_UPDATED, "EventLogger.PLAYER_RVR_STATS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_RVRLAKE_CHANGED, "EventLogger.PLAYER_RVRLAKE_CHANGED")
	RegisterEventHandler(SystemData.Events.PLAYER_SET_ACTIVE_WEAPON_SET, "EventLogger.PLAYER_SET_ACTIVE_WEAPON_SET")
	RegisterEventHandler(SystemData.Events.PLAYER_SINGLE_ABILITY_UPDATED, "EventLogger.PLAYER_SINGLE_ABILITY_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_SKILLS_UPDATED, "EventLogger.PLAYER_SKILLS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_STANCE_UPDATED, "EventLogger.PLAYER_STANCE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_START_INTERACT_TIMER, "EventLogger.PLAYER_START_INTERACT_TIMER")
	RegisterEventHandler(SystemData.Events.PLAYER_START_RVR_FLAG_TIMER, "EventLogger.PLAYER_START_RVR_FLAG_TIMER")
	RegisterEventHandler(SystemData.Events.PLAYER_STATS_UPDATED, "EventLogger.PLAYER_STATS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_EFFECTS_UPDATED, "EventLogger.PLAYER_TARGET_EFFECTS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_HIT_POINTS_UPDATED, "EventLogger.PLAYER_TARGET_HIT_POINTS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_IS_IMMUNE_TO_DISABLES, "EventLogger.PLAYER_TARGET_IS_IMMUNE_TO_DISABLES")
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_IS_IMMUNE_TO_MOVEMENT_IMPARING, "EventLogger.PLAYER_TARGET_IS_IMMUNE_TO_MOVEMENT_IMPARING")
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_STATE_UPDATED, "EventLogger.PLAYER_TARGET_STATE_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "EventLogger.PLAYER_TARGET_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_TRADE_ACCEPTED, "EventLogger.PLAYER_TRADE_ACCEPTED")
	RegisterEventHandler(SystemData.Events.PLAYER_TRADE_CANCELLED, "EventLogger.PLAYER_TRADE_CANCELLED")
	RegisterEventHandler(SystemData.Events.PLAYER_TRADE_INITIATED, "EventLogger.PLAYER_TRADE_INITIATED")
	RegisterEventHandler(SystemData.Events.PLAYER_TRADE_ITEMS_UPDATED, "EventLogger.PLAYER_TRADE_ITEMS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_TROPHY_SLOT_UPDATED, "EventLogger.PLAYER_TROPHY_SLOT_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_WEAPON_SETS_UPDATED, "EventLogger.PLAYER_WEAPON_SETS_UPDATED")
	RegisterEventHandler(SystemData.Events.PLAYER_ZONE_CHANGED, "EventLogger.PLAYER_ZONE_CHANGED")
	RegisterEventHandler(SystemData.Events.PRE_MODULE_SHUTDOWNS, "EventLogger.PRE_MODULE_SHUTDOWNS")
	RegisterEventHandler(SystemData.Events.PREGAME_CLEAR_RANDOM_NAME_LIST, "EventLogger.PREGAME_CLEAR_RANDOM_NAME_LIST")
	RegisterEventHandler(SystemData.Events.PREGAME_EULA_ACCEPTED, "EventLogger.PREGAME_EULA_ACCEPTED")
	RegisterEventHandler(SystemData.Events.PREGAME_GO_TO_CHARACTER_SELECT, "EventLogger.PREGAME_GO_TO_CHARACTER_SELECT")
	RegisterEventHandler(SystemData.Events.PREGAME_LAUNCH_SERVER_SELECT, "EventLogger.PREGAME_LAUNCH_SERVER_SELECT")
	RegisterEventHandler(SystemData.Events.PREGAME_PLAY_ANIMATION, "EventLogger.PREGAME_PLAY_ANIMATION")
	RegisterEventHandler(SystemData.Events.PREGAME_ROC_ACCEPTED, "EventLogger.PREGAME_ROC_ACCEPTED")
	RegisterEventHandler(SystemData.Events.PREGAME_SET_STATE, "EventLogger.PREGAME_SET_STATE")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_ADDED, "EventLogger.PUBLIC_QUEST_ADDED")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_COMPLETED, "EventLogger.PUBLIC_QUEST_COMPLETED")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_CONDITION_UPDATED, "EventLogger.PUBLIC_QUEST_CONDITION_UPDATED")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_FAILED, "EventLogger.PUBLIC_QUEST_FAILED")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_FORCEDOUT, "EventLogger.PUBLIC_QUEST_FORCEDOUT")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_HIDE_WINOMETER, "EventLogger.PUBLIC_QUEST_HIDE_WINOMETER")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_INFO_UPDATED, "EventLogger.PUBLIC_QUEST_INFO_UPDATED")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_LIST_UPDATED, "EventLogger.PUBLIC_QUEST_LIST_UPDATED")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_OPTOUT, "EventLogger.PUBLIC_QUEST_OPTOUT")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_REMOVED, "EventLogger.PUBLIC_QUEST_REMOVED")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_RESETTING, "EventLogger.PUBLIC_QUEST_RESETTING")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_SHOW_SCOREBOARD, "EventLogger.PUBLIC_QUEST_SHOW_SCOREBOARD")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_SHOW_WINOMETER, "EventLogger.PUBLIC_QUEST_SHOW_WINOMETER")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_UPDATED, "EventLogger.PUBLIC_QUEST_UPDATED")
	RegisterEventHandler(SystemData.Events.PUBLIC_QUEST_WINOMETER_UPDATED, "EventLogger.PUBLIC_QUEST_WINOMETER_UPDATED")
	RegisterEventHandler(SystemData.Events.QUEST_CONDITION_UPDATED, "EventLogger.QUEST_CONDITION_UPDATED")
	RegisterEventHandler(SystemData.Events.QUEST_INFO_UPDATED, "EventLogger.QUEST_INFO_UPDATED")
	RegisterEventHandler(SystemData.Events.QUEST_LIST_UPDATED, "EventLogger.QUEST_LIST_UPDATED")
	RegisterEventHandler(SystemData.Events.QUIT, "EventLogger.QUIT")
	--RegisterEventHandler(SystemData.Events.R_BUTTON_DOWN_PROCESSED, "EventLogger.R_BUTTON_DOWN_PROCESSED")
	--RegisterEventHandler(SystemData.Events.R_BUTTON_UP_PROCESSED, "EventLogger.R_BUTTON_UP_PROCESSED")
	RegisterEventHandler(SystemData.Events.RALLY_CALL_INVITE, "EventLogger.RALLY_CALL_INVITE")
	RegisterEventHandler(SystemData.Events.RALLY_CALL_JOIN, "EventLogger.RALLY_CALL_JOIN")
	RegisterEventHandler(SystemData.Events.RELEASE_CORPSE, "EventLogger.RELEASE_CORPSE")
	RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "EventLogger.RELOAD_INTERFACE")
	RegisterEventHandler(SystemData.Events.REPORT_GOLD_SELLER, "EventLogger.REPORT_GOLD_SELLER")
	RegisterEventHandler(SystemData.Events.RESOLUTION_CHANGED, "EventLogger.RESOLUTION_CHANGED")
	RegisterEventHandler(SystemData.Events.RESURRECTION_ACCEPT, "EventLogger.RESURRECTION_ACCEPT")
	RegisterEventHandler(SystemData.Events.RESURRECTION_DECLINE, "EventLogger.RESURRECTION_DECLINE")
	RegisterEventHandler(SystemData.Events.RRQ_LIST_UPDATED, "EventLogger.RRQ_LIST_UPDATED")
	--RegisterEventHandler(SystemData.Events.RVR_REWARD_POOLS_UPDATED, "EventLogger.RVR_REWARD_POOLS_UPDATED")
	RegisterEventHandler(SystemData.Events.SCENARIO_ACTIVE_QUEUE_UPDATED, "EventLogger.SCENARIO_ACTIVE_QUEUE_UPDATED")
	RegisterEventHandler(SystemData.Events.SCENARIO_BEGIN, "EventLogger.SCENARIO_BEGIN")
	RegisterEventHandler(SystemData.Events.SCENARIO_END, "EventLogger.SCENARIO_END")
	RegisterEventHandler(SystemData.Events.SCENARIO_FINAL_SCOREBOARD_CLOSED, "EventLogger.SCENARIO_FINAL_SCOREBOARD_CLOSED")
	RegisterEventHandler(SystemData.Events.SCENARIO_GROUP_JOIN, "EventLogger.SCENARIO_GROUP_JOIN")
	RegisterEventHandler(SystemData.Events.SCENARIO_GROUP_LEAVE, "EventLogger.SCENARIO_GROUP_LEAVE")
	RegisterEventHandler(SystemData.Events.SCENARIO_GROUP_UPDATED, "EventLogger.SCENARIO_GROUP_UPDATED")
	RegisterEventHandler(SystemData.Events.SCENARIO_INSTANCE_CANCEL, "EventLogger.SCENARIO_INSTANCE_CANCEL")
	RegisterEventHandler(SystemData.Events.SCENARIO_INSTANCE_JOIN_NOW, "EventLogger.SCENARIO_INSTANCE_JOIN_NOW")
	RegisterEventHandler(SystemData.Events.SCENARIO_INSTANCE_JOIN_WAIT, "EventLogger.SCENARIO_INSTANCE_JOIN_WAIT")
	RegisterEventHandler(SystemData.Events.SCENARIO_INSTANCE_LEAVE, "EventLogger.SCENARIO_INSTANCE_LEAVE")
	RegisterEventHandler(SystemData.Events.SCENARIO_PLAYER_HITS_UPDATED, "EventLogger.SCENARIO_PLAYER_HITS_UPDATED")
	RegisterEventHandler(SystemData.Events.SCENARIO_PLAYERS_LIST_GROUPS_UPDATED, "EventLogger.SCENARIO_PLAYERS_LIST_GROUPS_UPDATED")
	RegisterEventHandler(SystemData.Events.SCENARIO_PLAYERS_LIST_RESERVATIONS_UPDATED, "EventLogger.SCENARIO_PLAYERS_LIST_RESERVATIONS_UPDATED")
	RegisterEventHandler(SystemData.Events.SCENARIO_PLAYERS_LIST_STATS_UPDATED, "EventLogger.SCENARIO_PLAYERS_LIST_STATS_UPDATED")
	RegisterEventHandler(SystemData.Events.SCENARIO_PLAYERS_LIST_UPDATED, "EventLogger.SCENARIO_PLAYERS_LIST_UPDATED")
	RegisterEventHandler(SystemData.Events.SCENARIO_POST_MODE, "EventLogger.SCENARIO_POST_MODE")
	RegisterEventHandler(SystemData.Events.SCENARIO_SHOW_JOIN_PROMPT, "EventLogger.SCENARIO_SHOW_JOIN_PROMPT")
	RegisterEventHandler(SystemData.Events.SCENARIO_SHOW_LEVELED_NEED_REJOIN_BRACKET, "EventLogger.SCENARIO_SHOW_LEVELED_NEED_REJOIN_BRACKET")
	RegisterEventHandler(SystemData.Events.SCENARIO_SHOW_LEVELED_OUT_OF_BRACKETS, "EventLogger.SCENARIO_SHOW_LEVELED_OUT_OF_BRACKETS")
	RegisterEventHandler(SystemData.Events.SCENARIO_START_UPDATING_PLAYERS_STATS, "EventLogger.SCENARIO_START_UPDATING_PLAYERS_STATS")
	RegisterEventHandler(SystemData.Events.SCENARIO_STARTING_SCENARIO_UPDATED, "EventLogger.SCENARIO_STARTING_SCENARIO_UPDATED")
	RegisterEventHandler(SystemData.Events.SCENARIO_STOP_UPDATING_PLAYERS_STATS, "EventLogger.SCENARIO_STOP_UPDATING_PLAYERS_STATS")
	RegisterEventHandler(SystemData.Events.SCENARIO_UPDATE_POINTS, "EventLogger.SCENARIO_UPDATE_POINTS")
	RegisterEventHandler(SystemData.Events.SELECT_CHARACTER, "EventLogger.SELECT_CHARACTER")
	RegisterEventHandler(SystemData.Events.SERVER_LIST_RESPONSE, "EventLogger.SERVER_LIST_RESPONSE")
	RegisterEventHandler(SystemData.Events.SERVER_LIST_START, "EventLogger.SERVER_LIST_START")
	RegisterEventHandler(SystemData.Events.SERVER_LOAD_STATUS, "EventLogger.SERVER_LOAD_STATUS")
	RegisterEventHandler(SystemData.Events.SETTINGS_CHAT_UPDATED, "EventLogger.SETTINGS_CHAT_UPDATED")
	RegisterEventHandler(SystemData.Events.SHOW_ADVANCED_WAR, "EventLogger.SHOW_ADVANCED_WAR")
	RegisterEventHandler(SystemData.Events.SHOW_ALERT_TEXT, "EventLogger.SHOW_ALERT_TEXT")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_CONTROL_TIMER_UPDATED, "EventLogger.SIEGE_WEAPON_CONTROL_TIMER_UPDATED")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_FIRE_RESULTS, "EventLogger.SIEGE_WEAPON_FIRE_RESULTS")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_GOLF_BEGIN, "EventLogger.SIEGE_WEAPON_GOLF_BEGIN")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_GOLF_END, "EventLogger.SIEGE_WEAPON_GOLF_END")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_GOLF_RESET, "EventLogger.SIEGE_WEAPON_GOLF_RESET")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_GOLF_START_BACK_SWING, "EventLogger.SIEGE_WEAPON_GOLF_START_BACK_SWING")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_GOLF_START_FORWARD_SWING, "EventLogger.SIEGE_WEAPON_GOLF_START_FORWARD_SWING")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_HEALTH_UPDATED, "EventLogger.SIEGE_WEAPON_HEALTH_UPDATED")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_RELEASE, "EventLogger.SIEGE_WEAPON_RELEASE")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_REUSE_TIMER_UPDATED, "EventLogger.SIEGE_WEAPON_REUSE_TIMER_UPDATED")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SCORCH_BEGIN, "EventLogger.SIEGE_WEAPON_SCORCH_BEGIN")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SCORCH_END, "EventLogger.SIEGE_WEAPON_SCORCH_END")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SCORCH_POWER_UPDATED, "EventLogger.SIEGE_WEAPON_SCORCH_POWER_UPDATED")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SCORCH_WIND_UPDATED, "EventLogger.SIEGE_WEAPON_SCORCH_WIND_UPDATED")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SNIPER_AIM_TARGET_LOS_UPDATED, "EventLogger.SIEGE_WEAPON_SNIPER_AIM_TARGET_LOS_UPDATED")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SNIPER_AIM_TARGET_UPDATED, "EventLogger.SIEGE_WEAPON_SNIPER_AIM_TARGET_UPDATED")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SNIPER_AIM_TIME_UPDATED, "EventLogger.SIEGE_WEAPON_SNIPER_AIM_TIME_UPDATED")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SNIPER_BEGIN, "EventLogger.SIEGE_WEAPON_SNIPER_BEGIN")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SNIPER_END, "EventLogger.SIEGE_WEAPON_SNIPER_END")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_STATE_UPDATED, "EventLogger.SIEGE_WEAPON_STATE_UPDATED")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SWEET_SPOT_BEGIN, "EventLogger.SIEGE_WEAPON_SWEET_SPOT_BEGIN")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SWEET_SPOT_END, "EventLogger.SIEGE_WEAPON_SWEET_SPOT_END")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SWEET_SPOT_FIRE_RESULTS, "EventLogger.SIEGE_WEAPON_SWEET_SPOT_FIRE_RESULTS")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SWEET_SPOT_RESET, "EventLogger.SIEGE_WEAPON_SWEET_SPOT_RESET")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_SWEET_SPOT_START_MOVING, "EventLogger.SIEGE_WEAPON_SWEET_SPOT_START_MOVING")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_UPDATED, "EventLogger.SIEGE_WEAPON_UPDATED")
	RegisterEventHandler(SystemData.Events.SIEGE_WEAPON_USERS_UPDATED, "EventLogger.SIEGE_WEAPON_USERS_UPDATED")
	RegisterEventHandler(SystemData.Events.SOCIAL_BRAGGING_RIGHTS_UPDATED, "EventLogger.SOCIAL_BRAGGING_RIGHTS_UPDATED")
	RegisterEventHandler(SystemData.Events.SOCIAL_FRIENDS_UPDATED, "EventLogger.SOCIAL_FRIENDS_UPDATED")
	RegisterEventHandler(SystemData.Events.SOCIAL_IGNORE_UPDATED, "EventLogger.SOCIAL_IGNORE_UPDATED")
	RegisterEventHandler(SystemData.Events.SOCIAL_INSPECTION_UPDATED, "EventLogger.SOCIAL_INSPECTION_UPDATED")
	RegisterEventHandler(SystemData.Events.SOCIAL_OPENPARTY_NOTIFY, "EventLogger.SOCIAL_OPENPARTY_NOTIFY")
	RegisterEventHandler(SystemData.Events.SOCIAL_OPENPARTY_UPDATED, "EventLogger.SOCIAL_OPENPARTY_UPDATED")
	RegisterEventHandler(SystemData.Events.SOCIAL_OPENPARTY_WORLD_UPDATED, "EventLogger.SOCIAL_OPENPARTY_WORLD_UPDATED")
	RegisterEventHandler(SystemData.Events.SOCIAL_OPENPARTYINTEREST_UPDATED, "EventLogger.SOCIAL_OPENPARTYINTEREST_UPDATED")
	RegisterEventHandler(SystemData.Events.SOCIAL_OPENPARTYWORLD_SETTINGS_UPDATED, "EventLogger.SOCIAL_OPENPARTYWORLD_SETTINGS_UPDATED")
	RegisterEventHandler(SystemData.Events.SOCIAL_OPTIONS_UPDATED, "EventLogger.SOCIAL_OPTIONS_UPDATED")
	RegisterEventHandler(SystemData.Events.SOCIAL_SEARCH_UPDATED, "EventLogger.SOCIAL_SEARCH_UPDATED")
	RegisterEventHandler(SystemData.Events.SOCIAL_YOU_HAVE_BEEN_FRIENDED, "EventLogger.SOCIAL_YOU_HAVE_BEEN_FRIENDED")
	RegisterEventHandler(SystemData.Events.SPELL_CAST_CANCEL, "EventLogger.SPELL_CAST_CANCEL")
	RegisterEventHandler(SystemData.Events.STREAMING_STATUS_UPDATED, "EventLogger.STREAMING_STATUS_UPDATED")
	RegisterEventHandler(SystemData.Events.SUMMON_ACCEPT, "EventLogger.SUMMON_ACCEPT")
	RegisterEventHandler(SystemData.Events.SUMMON_DECLINE, "EventLogger.SUMMON_DECLINE")
	RegisterEventHandler(SystemData.Events.SUMMON_SHOW_PROMPT, "EventLogger.SUMMON_SHOW_PROMPT")
	RegisterEventHandler(SystemData.Events.SURVEY_POPUP, "EventLogger.SURVEY_POPUP")
	RegisterEventHandler(SystemData.Events.SURVEY_UPDATED, "EventLogger.SURVEY_UPDATED")
	RegisterEventHandler(SystemData.Events.TARGET_GROUP_MEMBER_1, "EventLogger.TARGET_GROUP_MEMBER_1")
	RegisterEventHandler(SystemData.Events.TARGET_GROUP_MEMBER_1_PET, "EventLogger.TARGET_GROUP_MEMBER_1_PET")
	RegisterEventHandler(SystemData.Events.TARGET_GROUP_MEMBER_2, "EventLogger.TARGET_GROUP_MEMBER_2")
	RegisterEventHandler(SystemData.Events.TARGET_GROUP_MEMBER_2_PET, "EventLogger.TARGET_GROUP_MEMBER_2_PET")
	RegisterEventHandler(SystemData.Events.TARGET_GROUP_MEMBER_3, "EventLogger.TARGET_GROUP_MEMBER_3")
	RegisterEventHandler(SystemData.Events.TARGET_GROUP_MEMBER_3_PET, "EventLogger.TARGET_GROUP_MEMBER_3_PET")
	RegisterEventHandler(SystemData.Events.TARGET_GROUP_MEMBER_4, "EventLogger.TARGET_GROUP_MEMBER_4")
	RegisterEventHandler(SystemData.Events.TARGET_GROUP_MEMBER_4_PET, "EventLogger.TARGET_GROUP_MEMBER_4_PET")
	RegisterEventHandler(SystemData.Events.TARGET_GROUP_MEMBER_5, "EventLogger.TARGET_GROUP_MEMBER_5")
	RegisterEventHandler(SystemData.Events.TARGET_GROUP_MEMBER_5_PET, "EventLogger.TARGET_GROUP_MEMBER_5_PET")
	RegisterEventHandler(SystemData.Events.TARGET_GROUP_MEMBER_6, "EventLogger.TARGET_GROUP_MEMBER_6")
	RegisterEventHandler(SystemData.Events.TARGET_GROUP_MEMBER_6_PET, "EventLogger.TARGET_GROUP_MEMBER_6_PET")
	RegisterEventHandler(SystemData.Events.TARGET_PET, "EventLogger.TARGET_PET")
	RegisterEventHandler(SystemData.Events.TARGET_SELF, "EventLogger.TARGET_SELF")
	RegisterEventHandler(SystemData.Events.TITLE_SCREEN_INIT_LOADING_UI, "EventLogger.TITLE_SCREEN_INIT_LOADING_UI")
	RegisterEventHandler(SystemData.Events.TITLE_SCREEN_INIT_LOADING_UI_COMPLETE, "EventLogger.TITLE_SCREEN_INIT_LOADING_UI_COMPLETE")
	RegisterEventHandler(SystemData.Events.TOGGLE_ABILITIES_WINDOW, "EventLogger.TOGGLE_ABILITIES_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_BACKPACK_WINDOW, "EventLogger.TOGGLE_BACKPACK_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_BATTLEGROUP_WINDOW, "EventLogger.TOGGLE_BATTLEGROUP_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_CHARACTER_WINDOW, "EventLogger.TOGGLE_CHARACTER_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_CURRENT_EVENTS_WINDOW, "EventLogger.TOGGLE_CURRENT_EVENTS_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_FULLSCREEN, "EventLogger.TOGGLE_FULLSCREEN")
	RegisterEventHandler(SystemData.Events.TOGGLE_GUILD_WINDOW, "EventLogger.TOGGLE_GUILD_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_HELP_WINDOW, "EventLogger.TOGGLE_HELP_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_HOTBAR, "EventLogger.TOGGLE_HOTBAR")
	RegisterEventHandler(SystemData.Events.TOGGLE_MENU, "EventLogger.TOGGLE_MENU")
	RegisterEventHandler(SystemData.Events.TOGGLE_MENU_WINDOW, "EventLogger.TOGGLE_MENU_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_PARTY_WINDOW, "EventLogger.TOGGLE_PARTY_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_SCENARIO_SUMMARY_WINDOW, "EventLogger.TOGGLE_SCENARIO_SUMMARY_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_SOCIAL_WINDOW, "EventLogger.TOGGLE_SOCIAL_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_TOME_WINDOW, "EventLogger.TOGGLE_TOME_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_USER_SETTINGS_WINDOW, "EventLogger.TOGGLE_USER_SETTINGS_WINDOW")
	RegisterEventHandler(SystemData.Events.TOGGLE_WORLD_MAP_WINDOW, "EventLogger.TOGGLE_WORLD_MAP_WINDOW")
	RegisterEventHandler(SystemData.Events.TOME_ACHIEVEMENTS_SUBTYPE_UPDATED, "EventLogger.TOME_ACHIEVEMENTS_SUBTYPE_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_ACHIEVEMENTS_TOC_UPDATED, "EventLogger.TOME_ACHIEVEMENTS_TOC_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_ACHIEVEMENTS_TYPE_UPDATED, "EventLogger.TOME_ACHIEVEMENTS_TYPE_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_ALERT_ADDED, "EventLogger.TOME_ALERT_ADDED")
	RegisterEventHandler(SystemData.Events.TOME_ALERTS_UPDATED, "EventLogger.TOME_ALERTS_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_BESTIARY_SPECIES_KILL_COUNT_UPDATED, "EventLogger.TOME_BESTIARY_SPECIES_KILL_COUNT_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_BESTIARY_SPECIES_UPDATED, "EventLogger.TOME_BESTIARY_SPECIES_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_BESTIARY_SUBTYPE_KILL_COUNT_UPDATED, "EventLogger.TOME_BESTIARY_SUBTYPE_KILL_COUNT_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_BESTIARY_SUBTYPE_UPDATED, "EventLogger.TOME_BESTIARY_SUBTYPE_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_BESTIARY_TOC_UPDATED, "EventLogger.TOME_BESTIARY_TOC_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_BESTIARY_TOTAL_KILL_COUNT_UPDATED, "EventLogger.TOME_BESTIARY_TOTAL_KILL_COUNT_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_CARD_LIST_UPDATED, "EventLogger.TOME_CARD_LIST_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_GAME_FAQ_TOC_UPDATED, "EventLogger.TOME_GAME_FAQ_TOC_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_HISTORY_AND_LORE_ENTRY_UPDATED, "EventLogger.TOME_HISTORY_AND_LORE_ENTRY_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_HISTORY_AND_LORE_PAIRING_UPDATED, "EventLogger.TOME_HISTORY_AND_LORE_PAIRING_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_HISTORY_AND_LORE_TOC_UPDATED, "EventLogger.TOME_HISTORY_AND_LORE_TOC_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_HISTORY_AND_LORE_ZONE_UPDATED, "EventLogger.TOME_HISTORY_AND_LORE_ZONE_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_ID_UNLOCKED_FOR_PLAYER, "EventLogger.TOME_ID_UNLOCKED_FOR_PLAYER")
	RegisterEventHandler(SystemData.Events.TOME_INITIALIZED_FOR_PLAYER, "EventLogger.TOME_INITIALIZED_FOR_PLAYER")
	RegisterEventHandler(SystemData.Events.TOME_ITEM_REWARDS_LIST_UPDATED, "EventLogger.TOME_ITEM_REWARDS_LIST_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_LIVE_EVENT_ENDED, "EventLogger.TOME_LIVE_EVENT_ENDED")
	RegisterEventHandler(SystemData.Events.TOME_LIVE_EVENT_LOADED, "EventLogger.TOME_LIVE_EVENT_LOADED")
	RegisterEventHandler(SystemData.Events.TOME_LIVE_EVENT_OVERALL_COUNTER_UPDATED, "EventLogger.TOME_LIVE_EVENT_OVERALL_COUNTER_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_LIVE_EVENT_REMOVED, "EventLogger.TOME_LIVE_EVENT_REMOVED")
	RegisterEventHandler(SystemData.Events.TOME_LIVE_EVENT_TASK_COUNTER_UPDATED, "EventLogger.TOME_LIVE_EVENT_TASK_COUNTER_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_LIVE_EVENT_TASKS_UPDATED, "EventLogger.TOME_LIVE_EVENT_TASKS_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_NOTEWORTHY_PERSONS_ENTRY_UPDATED, "EventLogger.TOME_NOTEWORTHY_PERSONS_ENTRY_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_NOTEWORTHY_PERSONS_PAIRING_UPDATED, "EventLogger.TOME_NOTEWORTHY_PERSONS_PAIRING_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_NOTEWORTHY_PERSONS_TOC_UPDATED, "EventLogger.TOME_NOTEWORTHY_PERSONS_TOC_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_NOTEWORTHY_PERSONS_ZONE_UPDATED, "EventLogger.TOME_NOTEWORTHY_PERSONS_ZONE_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_OLD_WORLD_ARMORY_ARMOR_SET_UPDATED, "EventLogger.TOME_OLD_WORLD_ARMORY_ARMOR_SET_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_OLD_WORLD_ARMORY_TOC_UPDATED, "EventLogger.TOME_OLD_WORLD_ARMORY_TOC_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_PLAYER_TITLES_TOC_UPDATED, "EventLogger.TOME_PLAYER_TITLES_TOC_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_PLAYER_TITLES_TYPE_UPDATED, "EventLogger.TOME_PLAYER_TITLES_TYPE_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_SIGIL_ENTRY_UPDATED, "EventLogger.TOME_SIGIL_ENTRY_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_SIGIL_TOC_UPDATED, "EventLogger.TOME_SIGIL_TOC_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_STAT_PLAYED_TIME_UPDATED, "EventLogger.TOME_STAT_PLAYED_TIME_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_STAT_TOTAL_CARDS_UPDATED, "EventLogger.TOME_STAT_TOTAL_CARDS_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_STAT_TOTAL_ITEM_REWARDS_UPDATED, "EventLogger.TOME_STAT_TOTAL_ITEM_REWARDS_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_STAT_TOTAL_TACTIC_REWARDS_UPDATED, "EventLogger.TOME_STAT_TOTAL_TACTIC_REWARDS_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_STAT_TOTAL_TITLE_REWARDS_UPDATED, "EventLogger.TOME_STAT_TOTAL_TITLE_REWARDS_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_STAT_TOTAL_UNLOCKS_UPDATED, "EventLogger.TOME_STAT_TOTAL_UNLOCKS_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_STAT_TOTAL_XP_UPDATED, "EventLogger.TOME_STAT_TOTAL_XP_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_TACTIC_REWARDS_LIST_UPDATED, "EventLogger.TOME_TACTIC_REWARDS_LIST_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_WAR_JOURNAL_ENTRY_UPDATED, "EventLogger.TOME_WAR_JOURNAL_ENTRY_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_WAR_JOURNAL_STORYLINE_UPDATED, "EventLogger.TOME_WAR_JOURNAL_STORYLINE_UPDATED")
	RegisterEventHandler(SystemData.Events.TOME_WAR_JOURNAL_TOC_UPDATED, "EventLogger.TOME_WAR_JOURNAL_TOC_UPDATED")
	RegisterEventHandler(SystemData.Events.TRADE_SKILL_UPDATED, "EventLogger.TRADE_SKILL_UPDATED")
	RegisterEventHandler(SystemData.Events.TRANSFER_GUILD, "EventLogger.TRANSFER_GUILD")
	RegisterEventHandler(SystemData.Events.TRIAL_ALERT_POPUP, "EventLogger.TRIAL_ALERT_POPUP")
	RegisterEventHandler(SystemData.Events.UPDATE_CREATION_CHARACTER, "EventLogger.UPDATE_CREATION_CHARACTER")
	RegisterEventHandler(SystemData.Events.UPDATE_GUILDHERALDRY, "EventLogger.UPDATE_GUILDHERALDRY")
	RegisterEventHandler(SystemData.Events.UPDATE_GUILDSTANDARD, "EventLogger.UPDATE_GUILDSTANDARD")
	RegisterEventHandler(SystemData.Events.UPDATE_ITEM_ENHANCEMENT, "EventLogger.UPDATE_ITEM_ENHANCEMENT")
	--RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "EventLogger.UPDATE_PROCESSED")
	RegisterEventHandler(SystemData.Events.USER_SETTINGS_CHANGED, "EventLogger.USER_SETTINGS_CHANGED")
	RegisterEventHandler(SystemData.Events.VIDEO_PLAYER_START, "EventLogger.VIDEO_PLAYER_START")
	RegisterEventHandler(SystemData.Events.VIDEO_PLAYER_STOP, "EventLogger.VIDEO_PLAYER_STOP")
	RegisterEventHandler(SystemData.Events.VISIBLE_EQUIPMENT_UPDATED, "EventLogger.VISIBLE_EQUIPMENT_UPDATED")
	RegisterEventHandler(SystemData.Events.WORLD_EVENT_TEXT_ARRIVED, "EventLogger.WORLD_EVENT_TEXT_ARRIVED")
	RegisterEventHandler(SystemData.Events.WORLD_MAP_POINTS_LOADED, "EventLogger.WORLD_MAP_POINTS_LOADED")
	RegisterEventHandler(SystemData.Events.WORLD_OBJ_COMBAT_EVENT, "EventLogger.WORLD_OBJ_COMBAT_EVENT")
	RegisterEventHandler(SystemData.Events.WORLD_OBJ_HEALTH_CHANGED, "EventLogger.WORLD_OBJ_HEALTH_CHANGED")
	RegisterEventHandler(SystemData.Events.WORLD_OBJ_INFLUENCE_GAINED, "EventLogger.WORLD_OBJ_INFLUENCE_GAINED")
	RegisterEventHandler(SystemData.Events.WORLD_OBJ_RENOWN_GAINED, "EventLogger.WORLD_OBJ_RENOWN_GAINED")
	RegisterEventHandler(SystemData.Events.WORLD_OBJ_XP_GAINED, "EventLogger.WORLD_OBJ_XP_GAINED")
	RegisterEventHandler(SystemData.Events.ZONE_CONFIRMATION_NO, "EventLogger.ZONE_CONFIRMATION_NO")
	RegisterEventHandler(SystemData.Events.ZONE_CONFIRMATION_YES, "EventLogger.ZONE_CONFIRMATION_YES")
end

function EventLogger.ADD_USER_MAP_POINT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ADD_USER_MAP_POINT params="..pCount)
end

function EventLogger.ADVANCED_WAR_FORTRESS_UPDATE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ADVANCED_WAR_FORTRESS_UPDATE params="..pCount)
end

function EventLogger.ADVANCED_WAR_RELIC_UPDATE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ADVANCED_WAR_RELIC_UPDATE params="..pCount)
end

function EventLogger.ADVANCED_WAR_RELIC_ZONE_UPDATE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ADVANCED_WAR_RELIC_ZONE_UPDATE params="..pCount)
end

function EventLogger.ALL_MODULES_INITIALIZED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ALL_MODULES_INITIALIZED params="..pCount)
end

function EventLogger.ALLIANCE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ALLIANCE_UPDATED params="..pCount)
end

function EventLogger.APPLICATION_ONE_BUTTON_DIALOG(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"APPLICATION_ONE_BUTTON_DIALOG params="..pCount)
end

function EventLogger.APPLICATION_REMOVE_DIALOG(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"APPLICATION_REMOVE_DIALOG params="..pCount)
end

function EventLogger.APPLICATION_TWO_BUTTON_DIALOG(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"APPLICATION_TWO_BUTTON_DIALOG params="..pCount)
end

function EventLogger.AUCTION_BID_RESULT_RECEIVED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"AUCTION_BID_RESULT_RECEIVED params="..pCount)
end

function EventLogger.AUCTION_INIT_RECEIVED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"AUCTION_INIT_RECEIVED params="..pCount)
end

function EventLogger.AUCTION_SEARCH_RESULT_RECEIVED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"AUCTION_SEARCH_RESULT_RECEIVED params="..pCount)
end

function EventLogger.AUTHENTICATION_ERROR(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"AUTHENTICATION_ERROR params="..pCount)
end

function EventLogger.AUTHENTICATION_LOGIN_START(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"AUTHENTICATION_LOGIN_START params="..pCount)
end

function EventLogger.AUTHENTICATION_RESPONSE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"AUTHENTICATION_RESPONSE params="..pCount)
end

function EventLogger.AUTHENTICATION_START(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"AUTHENTICATION_START params="..pCount)
end

function EventLogger.AUTO_LOOT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"AUTO_LOOT params="..pCount)
end

function EventLogger.AUTOMATED_CHARACTER_CREATE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"AUTOMATED_CHARACTER_CREATE params="..pCount)
end

function EventLogger.BATTLEGROUP_ACCEPT_INVITATION(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BATTLEGROUP_ACCEPT_INVITATION params="..pCount)
end

function EventLogger.BATTLEGROUP_DECLINE_INVITATION(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BATTLEGROUP_DECLINE_INVITATION params="..pCount)
end

function EventLogger.BATTLEGROUP_MEMBER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BATTLEGROUP_MEMBER_UPDATED params="..pCount)
end

function EventLogger.BATTLEGROUP_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BATTLEGROUP_UPDATED params="..pCount)
end

function EventLogger.BEGIN_CREATE_CHARACTER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BEGIN_CREATE_CHARACTER params="..pCount)
end

function EventLogger.BEGIN_CUSTOMIZE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BEGIN_CUSTOMIZE params="..pCount)
end

function EventLogger.BEGIN_ENTER_CHAT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BEGIN_ENTER_CHAT params="..pCount)
end

function EventLogger.BEGIN_REALM_SELECT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BEGIN_REALM_SELECT params="..pCount)
end

function EventLogger.BOLSTER_ACCEPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BOLSTER_ACCEPT params="..pCount)
end

function EventLogger.BOLSTER_CANCEL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BOLSTER_CANCEL params="..pCount)
end

function EventLogger.BOLSTER_DECLINE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BOLSTER_DECLINE params="..pCount)
end

function EventLogger.BOLSTER_OFFER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BOLSTER_OFFER params="..pCount)
end

function EventLogger.BRACKET_CHAT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"BRACKET_CHAT params="..pCount)
end

function EventLogger.CAMPAIGN_CITY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CAMPAIGN_CITY_UPDATED params="..pCount)
end

function EventLogger.CAMPAIGN_PAIRING_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CAMPAIGN_PAIRING_UPDATED params="..pCount)
end

function EventLogger.CAMPAIGN_ZONE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CAMPAIGN_ZONE_UPDATED params="..pCount)
end

function EventLogger.CANCEL_LOGIN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CANCEL_LOGIN params="..pCount)
end

function EventLogger.CHANNEL_NAMES_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHANNEL_NAMES_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_CHARACTER_SELECTION_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_CHARACTER_SELECTION_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_CREATE_CAREER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_CREATE_CAREER_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_CREATE_FEATURES_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_CREATE_FEATURES_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_CREATE_GENDER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_CREATE_GENDER_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_CREATE_RACE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_CREATE_RACE_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_DATA_LUA_VARS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_DATA_LUA_VARS_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_LIST_RESPONSE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_LIST_RESPONSE params="..pCount)
end

function EventLogger.CHARACTER_LIST_START(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_LIST_START params="..pCount)
end

function EventLogger.CHARACTER_MOUSE_OVER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_MOUSE_OVER_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_PREGAME_ANIMATION_FINISHED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_PREGAME_ANIMATION_FINISHED params="..pCount)
end

function EventLogger.CHARACTER_PREGAME_ANIMATION_STARTED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_PREGAME_ANIMATION_STARTED params="..pCount)
end

function EventLogger.CHARACTER_PREGAME_FORCED_RANDOM_NAME_ACCEPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_PREGAME_FORCED_RANDOM_NAME_ACCEPT params="..pCount)
end

function EventLogger.CHARACTER_PREGAME_FORCED_RANDOM_NAME_START(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_PREGAME_FORCED_RANDOM_NAME_START params="..pCount)
end

function EventLogger.CHARACTER_PREGAME_NAMING_CONFLICT_POP_UP_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_PREGAME_NAMING_CONFLICT_POP_UP_WINDOW params="..pCount)
end

function EventLogger.CHARACTER_PREGAME_NAMING_CONFLICT_RESPONSE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_PREGAME_NAMING_CONFLICT_RESPONSE params="..pCount)
end

function EventLogger.CHARACTER_PREGAME_RANDOM_NAME_LIST_RECEIVED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_PREGAME_RANDOM_NAME_LIST_RECEIVED params="..pCount)
end

function EventLogger.CHARACTER_PREGAME_RANDOM_NAME_REQUESTED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_PREGAME_RANDOM_NAME_REQUESTED params="..pCount)
end

function EventLogger.CHARACTER_PREGAME_RANDOM_NAME_UPDATE_CHAR_SELECT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_PREGAME_RANDOM_NAME_UPDATE_CHAR_SELECT params="..pCount)
end

function EventLogger.CHARACTER_PREGAME_RANDOM_NAME_UPDATE_FORCED_SELECT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_PREGAME_RANDOM_NAME_UPDATE_FORCED_SELECT params="..pCount)
end

function EventLogger.CHARACTER_PREGAME_TRANSFER_FLAG_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_PREGAME_TRANSFER_FLAG_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_QUEUE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_QUEUE_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_REALM_OVER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_REALM_OVER_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_REALM_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_REALM_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_SELECT_CURRENT_PAGE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_SELECT_CURRENT_PAGE_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_SELECT_LOCKOUT_TIMER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_SELECT_LOCKOUT_TIMER_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_SELECT_NUM_PAID_NAME_CHANGES_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_SELECT_NUM_PAID_NAME_CHANGES_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_SELECT_PAGES_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_SELECT_PAGES_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_SELECT_PAID_NAME_CHANGE_RESPONSE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_SELECT_PAID_NAME_CHANGE_RESPONSE params="..pCount)
end

function EventLogger.CHARACTER_SETTINGS_ON_CHARACTER_DELETED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_SETTINGS_ON_CHARACTER_DELETED params="..pCount)
end

function EventLogger.CHARACTER_SETTINGS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_SETTINGS_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_STATE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_STATE_UPDATED params="..pCount)
end

function EventLogger.CHARACTER_TEMPLATES_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHARACTER_TEMPLATES_UPDATED params="..pCount)
end

function EventLogger.CHAT_REPLY(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHAT_REPLY params="..pCount)
end

function EventLogger.CHAT_TEXT_ARRIVED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CHAT_TEXT_ARRIVED params="..pCount)
end

function EventLogger.CINEMA_INTRO_ENDED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CINEMA_INTRO_ENDED params="..pCount)
end

function EventLogger.CINEMA_INTRO_STARTED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CINEMA_INTRO_STARTED params="..pCount)
end

function EventLogger.CITY_CAPTURE_FLEE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CITY_CAPTURE_FLEE params="..pCount)
end

function EventLogger.CITY_CAPTURE_LEAVE_QUEUE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CITY_CAPTURE_LEAVE_QUEUE params="..pCount)
end

function EventLogger.CITY_CAPTURE_REQUEST_INSTANCE_DATA(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CITY_CAPTURE_REQUEST_INSTANCE_DATA params="..pCount)
end

function EventLogger.CITY_CAPTURE_SHOW_JOIN_PROMPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CITY_CAPTURE_SHOW_JOIN_PROMPT params="..pCount)
end

function EventLogger.CITY_CAPTURE_SHOW_LOW_LEVEL_JOIN_PROMPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CITY_CAPTURE_SHOW_LOW_LEVEL_JOIN_PROMPT params="..pCount)
end

function EventLogger.CITY_RATING_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CITY_RATING_UPDATED params="..pCount)
end

function EventLogger.CITY_SCENARIO_BEGIN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CITY_SCENARIO_BEGIN params="..pCount)
end

function EventLogger.CITY_SCENARIO_END(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CITY_SCENARIO_END params="..pCount)
end

function EventLogger.CITY_SCENARIO_INSTANCE_ID_SELECTED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CITY_SCENARIO_INSTANCE_ID_SELECTED params="..pCount)
end

function EventLogger.CITY_SCENARIO_UPDATE_POINTS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CITY_SCENARIO_UPDATE_POINTS params="..pCount)
end

function EventLogger.CITY_SCENARIO_UPDATE_STATUS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CITY_SCENARIO_UPDATE_STATUS params="..pCount)
end

function EventLogger.CITY_SCENARIO_UPDATE_TIME(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CITY_SCENARIO_UPDATE_TIME params="..pCount)
end

function EventLogger.CLAIM_REWARDS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CLAIM_REWARDS params="..pCount)
end

function EventLogger.CLEAR_USER_MAP_POINT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CLEAR_USER_MAP_POINT params="..pCount)
end

function EventLogger.CONTESTED_INSTANCE_CANCEL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CONTESTED_INSTANCE_CANCEL params="..pCount)
end

function EventLogger.CONTESTED_INSTANCE_ENTER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CONTESTED_INSTANCE_ENTER params="..pCount)
end

function EventLogger.CONTESTED_SCENARIO_SELECT_INSTANCE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CONTESTED_SCENARIO_SELECT_INSTANCE params="..pCount)
end

function EventLogger.CONVERSATION_TEXT_ARRIVED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CONVERSATION_TEXT_ARRIVED params="..pCount)
end

function EventLogger.CRAFTING_SHOW_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CRAFTING_SHOW_WINDOW params="..pCount)
end

function EventLogger.CREATE_CHARACTER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CREATE_CHARACTER params="..pCount)
end

function EventLogger.CURRENT_EVENTS_JUMP_TIMER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CURRENT_EVENTS_JUMP_TIMER_UPDATED params="..pCount)
end

function EventLogger.CURRENT_EVENTS_LIST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CURRENT_EVENTS_LIST_UPDATED params="..pCount)
end

function EventLogger.CUSTOM_UI_SCALE_CHANGED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"CUSTOM_UI_SCALE_CHANGED params="..pCount)
end

function EventLogger.DELETE_CHARACTER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"DELETE_CHARACTER params="..pCount)
end

function EventLogger.END_ITEM_ENHANCEMENT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"END_ITEM_ENHANCEMENT params="..pCount)
end

function EventLogger.ENTER_KEY_PROCESSED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ENTER_KEY_PROCESSED params="..pCount)
end

function EventLogger.ENTER_WORLD(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ENTER_WORLD params="..pCount)
end

function EventLogger.EQUIPMENT_UPGRADE_COST(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"EQUIPMENT_UPGRADE_COST params="..pCount)
end

function EventLogger.EQUIPMENT_UPGRADE_RESULT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"EQUIPMENT_UPGRADE_RESULT params="..pCount)
end

function EventLogger.ESCAPE_KEY_PROCESSED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ESCAPE_KEY_PROCESSED params="..pCount)
end

function EventLogger.EXIT_GAME(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"EXIT_GAME params="..pCount)
end

function EventLogger.GAMEPLAY_RULESET_TYPE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GAMEPLAY_RULESET_TYPE_UPDATED params="..pCount)
end

function EventLogger.GO_BACK(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GO_BACK params="..pCount)
end

function EventLogger.GROUP_ACCEPT_INVITATION(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_ACCEPT_INVITATION params="..pCount)
end

function EventLogger.GROUP_ACCEPT_REFERRAL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_ACCEPT_REFERRAL params="..pCount)
end

function EventLogger.GROUP_DECLINE_INVITATION(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_DECLINE_INVITATION params="..pCount)
end

function EventLogger.GROUP_DECLINE_REFERRAL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_DECLINE_REFERRAL params="..pCount)
end

function EventLogger.GROUP_EFFECTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_EFFECTS_UPDATED params="..pCount)
end

function EventLogger.GROUP_INVITE_PLAYER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_INVITE_PLAYER params="..pCount)
end

function EventLogger.GROUP_KICK_PLAYER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_KICK_PLAYER params="..pCount)
end

function EventLogger.GROUP_LEAVE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_LEAVE params="..pCount)
end

function EventLogger.GROUP_PLAYER_ADDED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_PLAYER_ADDED params="..pCount)
end

function EventLogger.GROUP_REFER_PLAYER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_REFER_PLAYER params="..pCount)
end

function EventLogger.GROUP_SET_LEADER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_SET_LEADER params="..pCount)
end

function EventLogger.GROUP_SET_MAIN_ASSIST(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_SET_MAIN_ASSIST params="..pCount)
end

function EventLogger.GROUP_SET_MASTER_LOOT_ON(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_SET_MASTER_LOOT_ON params="..pCount)
end

function EventLogger.GROUP_SETTINGS_PRIVACY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_SETTINGS_PRIVACY_UPDATED params="..pCount)
end

function EventLogger.GROUP_SETTINGS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_SETTINGS_UPDATED params="..pCount)
end

function EventLogger.GROUP_STATUS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_STATUS_UPDATED params="..pCount)
end

function EventLogger.GROUP_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GROUP_UPDATED params="..pCount)
end

function EventLogger.GUILD_ABILITIES_AVAILABLE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_ABILITIES_AVAILABLE_UPDATED params="..pCount)
end

function EventLogger.GUILD_ABILITIES_PURCHASED_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_ABILITIES_PURCHASED_UPDATED params="..pCount)
end

function EventLogger.GUILD_APPOINTMENTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_APPOINTMENTS_UPDATED params="..pCount)
end

function EventLogger.GUILD_BANNERS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_BANNERS_UPDATED params="..pCount)
end

function EventLogger.GUILD_COMMAND_ALLIANCE_INVITE_ACCEPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_ALLIANCE_INVITE_ACCEPT params="..pCount)
end

function EventLogger.GUILD_COMMAND_ALLIANCE_INVITE_DECLINE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_ALLIANCE_INVITE_DECLINE params="..pCount)
end

function EventLogger.GUILD_COMMAND_ASSIGN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_ASSIGN params="..pCount)
end

function EventLogger.GUILD_COMMAND_CLAIM_ENTITY_ACCEPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_CLAIM_ENTITY_ACCEPT params="..pCount)
end

function EventLogger.GUILD_COMMAND_CLAIM_ENTITY_DECLINE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_CLAIM_ENTITY_DECLINE params="..pCount)
end

function EventLogger.GUILD_COMMAND_CLAIM_ENTITY_LOW_FUNDS_ACCEPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_CLAIM_ENTITY_LOW_FUNDS_ACCEPT params="..pCount)
end

function EventLogger.GUILD_COMMAND_CREATE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_CREATE params="..pCount)
end

function EventLogger.GUILD_COMMAND_DEMOTE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_DEMOTE params="..pCount)
end

function EventLogger.GUILD_COMMAND_INVITE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_INVITE params="..pCount)
end

function EventLogger.GUILD_COMMAND_INVITE_ACCEPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_INVITE_ACCEPT params="..pCount)
end

function EventLogger.GUILD_COMMAND_INVITE_DECLINE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_INVITE_DECLINE params="..pCount)
end

function EventLogger.GUILD_COMMAND_LEAVE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_LEAVE params="..pCount)
end

function EventLogger.GUILD_COMMAND_PROMOTE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_PROMOTE params="..pCount)
end

function EventLogger.GUILD_COMMAND_PURCHASE_TACTIC(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_PURCHASE_TACTIC params="..pCount)
end

function EventLogger.GUILD_COMMAND_REMOVE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_REMOVE params="..pCount)
end

function EventLogger.GUILD_COMMAND_RENAME(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_RENAME params="..pCount)
end

function EventLogger.GUILD_COMMAND_UNCLAIM_ENTITY_DECLINE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_UNCLAIM_ENTITY_DECLINE params="..pCount)
end

function EventLogger.GUILD_COMMAND_UNCLAIM_ENTITY_NOPENALTY_ACCEPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_UNCLAIM_ENTITY_NOPENALTY_ACCEPT params="..pCount)
end

function EventLogger.GUILD_COMMAND_UNCLAIM_ENTITY_PENALTY_ACCEPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_COMMAND_UNCLAIM_ENTITY_PENALTY_ACCEPT params="..pCount)
end

function EventLogger.GUILD_EXP_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_EXP_UPDATED params="..pCount)
end

function EventLogger.GUILD_HERALDRY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_HERALDRY_UPDATED params="..pCount)
end

function EventLogger.GUILD_INFO_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_INFO_UPDATED params="..pCount)
end

function EventLogger.GUILD_KEEP_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_KEEP_UPDATED params="..pCount)
end

function EventLogger.GUILD_MEMBER_ADDED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_MEMBER_ADDED params="..pCount)
end

function EventLogger.GUILD_MEMBER_NOTES_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_MEMBER_NOTES_UPDATED params="..pCount)
end

function EventLogger.GUILD_MEMBER_REMOVED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_MEMBER_REMOVED params="..pCount)
end

function EventLogger.GUILD_MEMBER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_MEMBER_UPDATED params="..pCount)
end

function EventLogger.GUILD_NEWBIE_GUILD_STATUS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_NEWBIE_GUILD_STATUS_UPDATED params="..pCount)
end

function EventLogger.GUILD_NEWS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_NEWS_UPDATED params="..pCount)
end

function EventLogger.GUILD_PERMISSIONS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_PERMISSIONS_UPDATED params="..pCount)
end

function EventLogger.GUILD_PERSONAL_STATISTICS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_PERSONAL_STATISTICS_UPDATED params="..pCount)
end

function EventLogger.GUILD_POLL_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_POLL_UPDATED params="..pCount)
end

function EventLogger.GUILD_RECRUITMENT_PROFILE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_RECRUITMENT_PROFILE_UPDATED params="..pCount)
end

function EventLogger.GUILD_RECRUITMENT_SEARCH_RESULTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_RECRUITMENT_SEARCH_RESULTS_UPDATED params="..pCount)
end

function EventLogger.GUILD_REFRESH(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_REFRESH params="..pCount)
end

function EventLogger.GUILD_REWARDS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_REWARDS_UPDATED params="..pCount)
end

function EventLogger.GUILD_ROSTER_INIT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_ROSTER_INIT params="..pCount)
end

function EventLogger.GUILD_STATISTICS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_STATISTICS_UPDATED params="..pCount)
end

function EventLogger.GUILD_TAX_TITHE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_TAX_TITHE_UPDATED params="..pCount)
end

function EventLogger.GUILD_UNGUILDED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_UNGUILDED params="..pCount)
end

function EventLogger.GUILD_VAULT_CAPACITY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_VAULT_CAPACITY_UPDATED params="..pCount)
end

function EventLogger.GUILD_VAULT_COIN_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_VAULT_COIN_UPDATED params="..pCount)
end

function EventLogger.GUILD_VAULT_ITEMS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_VAULT_ITEMS_UPDATED params="..pCount)
end

function EventLogger.GUILD_VAULT_SLOT_LOCKED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_VAULT_SLOT_LOCKED params="..pCount)
end

function EventLogger.GUILD_VAULT_SLOT_UNLOCKED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"GUILD_VAULT_SLOT_UNLOCKED params="..pCount)
end

function EventLogger.HELP_LOG_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"HELP_LOG_UPDATED params="..pCount)
end

function EventLogger.HELP_STATUS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"HELP_STATUS_UPDATED params="..pCount)
end

function EventLogger.HELP_TIP_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"HELP_TIP_UPDATED params="..pCount)
end

function EventLogger.INFO_ALERT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INFO_ALERT params="..pCount)
end

function EventLogger.INTERACT_ACCEPT_QUEST(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_ACCEPT_QUEST params="..pCount)
end

function EventLogger.INTERACT_ALLIANCE_FORM_ACCEPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_ALLIANCE_FORM_ACCEPT params="..pCount)
end

function EventLogger.INTERACT_ALLIANCE_FORM_DECLINE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_ALLIANCE_FORM_DECLINE params="..pCount)
end

function EventLogger.INTERACT_BARBERSHOP_FEATURE_UPDATE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_BARBERSHOP_FEATURE_UPDATE params="..pCount)
end

function EventLogger.INTERACT_BARBERSHOP_OPEN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_BARBERSHOP_OPEN params="..pCount)
end

function EventLogger.INTERACT_BARBERSHOP_RESULT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_BARBERSHOP_RESULT params="..pCount)
end

function EventLogger.INTERACT_BIND(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_BIND params="..pCount)
end

function EventLogger.INTERACT_BUY_BACK_ITEM(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_BUY_BACK_ITEM params="..pCount)
end

function EventLogger.INTERACT_BUY_ITEM(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_BUY_ITEM params="..pCount)
end

function EventLogger.INTERACT_BUY_ITEM_WITH_ALT_CURRENCY(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_BUY_ITEM_WITH_ALT_CURRENCY params="..pCount)
end

function EventLogger.INTERACT_CLOSE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_CLOSE params="..pCount)
end

function EventLogger.INTERACT_COMPLETE_QUEST(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_COMPLETE_QUEST params="..pCount)
end

function EventLogger.INTERACT_DEFAULT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_DEFAULT params="..pCount)
end

function EventLogger.INTERACT_DIVINEFAVORALTAR_OPEN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_DIVINEFAVORALTAR_OPEN params="..pCount)
end

function EventLogger.INTERACT_DIVINEFAVORALTAR_UPDATE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_DIVINEFAVORALTAR_UPDATE params="..pCount)
end

function EventLogger.INTERACT_DONE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_DONE params="..pCount)
end

function EventLogger.INTERACT_DYE_MERCHANT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_DYE_MERCHANT params="..pCount)
end

function EventLogger.INTERACT_DYE_MERCHANT_DYE_ALL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_DYE_MERCHANT_DYE_ALL params="..pCount)
end

function EventLogger.INTERACT_DYE_MERCHANT_DYE_SINGLE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_DYE_MERCHANT_DYE_SINGLE params="..pCount)
end

function EventLogger.INTERACT_EQUIPMENT_UPGRADE_OPEN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_EQUIPMENT_UPGRADE_OPEN params="..pCount)
end

function EventLogger.INTERACT_FLIGHT_TRAVEL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_FLIGHT_TRAVEL params="..pCount)
end

function EventLogger.INTERACT_GROUP_JOIN_SCENARIO_QUEUE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_GROUP_JOIN_SCENARIO_QUEUE params="..pCount)
end

function EventLogger.INTERACT_GROUP_JOIN_SCENARIO_QUEUE_ALL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_GROUP_JOIN_SCENARIO_QUEUE_ALL params="..pCount)
end

function EventLogger.INTERACT_GUILD_CREATION_COMPLETE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_GUILD_CREATION_COMPLETE params="..pCount)
end

function EventLogger.INTERACT_GUILD_FORM_ACCEPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_GUILD_FORM_ACCEPT params="..pCount)
end

function EventLogger.INTERACT_GUILD_FORM_DECLINE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_GUILD_FORM_DECLINE params="..pCount)
end

function EventLogger.INTERACT_GUILD_NPC_ERROR(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_GUILD_NPC_ERROR params="..pCount)
end

function EventLogger.INTERACT_GUILD_RENAME_BEGIN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_GUILD_RENAME_BEGIN params="..pCount)
end

function EventLogger.INTERACT_GUILD_SHOW_FORM(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_GUILD_SHOW_FORM params="..pCount)
end

function EventLogger.INTERACT_GUILD_VAULT_CLOSED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_GUILD_VAULT_CLOSED params="..pCount)
end

function EventLogger.INTERACT_GUILD_VAULT_OPEN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_GUILD_VAULT_OPEN params="..pCount)
end

function EventLogger.INTERACT_HEALER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_HEALER params="..pCount)
end

function EventLogger.INTERACT_JOIN_SCENARIO_QUEUE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_JOIN_SCENARIO_QUEUE params="..pCount)
end

function EventLogger.INTERACT_JOIN_SCENARIO_QUEUE_ALL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_JOIN_SCENARIO_QUEUE_ALL params="..pCount)
end

function EventLogger.INTERACT_KEEP_UPGRADE_OPEN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_KEEP_UPGRADE_OPEN params="..pCount)
end

function EventLogger.INTERACT_KEEP_UPGRADE_UPDATE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_KEEP_UPGRADE_UPDATE params="..pCount)
end

function EventLogger.INTERACT_LAST_NAME_MERCHANT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_LAST_NAME_MERCHANT params="..pCount)
end

function EventLogger.INTERACT_LAST_NAME_MERCHANT_BUY(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_LAST_NAME_MERCHANT_BUY params="..pCount)
end

function EventLogger.INTERACT_LEAVE_SCENARIO_QUEUE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_LEAVE_SCENARIO_QUEUE params="..pCount)
end

function EventLogger.INTERACT_LOOT_CLOSE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_LOOT_CLOSE params="..pCount)
end

function EventLogger.INTERACT_LOOT_ROLL_FIRST_ITEM(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_LOOT_ROLL_FIRST_ITEM params="..pCount)
end

function EventLogger.INTERACT_MAILBOX_CLOSED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_MAILBOX_CLOSED params="..pCount)
end

function EventLogger.INTERACT_MAILBOX_OPEN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_MAILBOX_OPEN params="..pCount)
end

function EventLogger.INTERACT_OPEN_BANK(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_OPEN_BANK params="..pCount)
end

function EventLogger.INTERACT_PURCHASE_HEAL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_PURCHASE_HEAL params="..pCount)
end

function EventLogger.INTERACT_REPAIR(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_REPAIR params="..pCount)
end

function EventLogger.INTERACT_SELECT_FLIGHT_POINT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SELECT_FLIGHT_POINT params="..pCount)
end

function EventLogger.INTERACT_SELECT_FLIGHTMASTER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SELECT_FLIGHTMASTER params="..pCount)
end

function EventLogger.INTERACT_SELECT_QUEST(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SELECT_QUEST params="..pCount)
end

function EventLogger.INTERACT_SELECT_STORE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SELECT_STORE params="..pCount)
end

function EventLogger.INTERACT_SELECT_TRAINING(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SELECT_TRAINING params="..pCount)
end

function EventLogger.INTERACT_SELL_ITEM(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SELL_ITEM params="..pCount)
end

function EventLogger.INTERACT_SHOW_ADVANCED_WAR_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_ADVANCED_WAR_WINDOW params="..pCount)
end

function EventLogger.INTERACT_SHOW_CLAIM_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_CLAIM_WINDOW params="..pCount)
end

function EventLogger.INTERACT_SHOW_EVENT_REWARDS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_EVENT_REWARDS params="..pCount)
end

function EventLogger.INTERACT_SHOW_FLIGHTMASTER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_FLIGHTMASTER params="..pCount)
end

function EventLogger.INTERACT_SHOW_HEALER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_HEALER params="..pCount)
end

function EventLogger.INTERACT_SHOW_INFLUENCE_REWARDS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_INFLUENCE_REWARDS params="..pCount)
end

function EventLogger.INTERACT_SHOW_ITEM_CONTAINER_LOOT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_ITEM_CONTAINER_LOOT params="..pCount)
end

function EventLogger.INTERACT_SHOW_LIBRARIAN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_LIBRARIAN params="..pCount)
end

function EventLogger.INTERACT_SHOW_LOOT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_LOOT params="..pCount)
end

function EventLogger.INTERACT_SHOW_LOOT_ROLL_DATA(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_LOOT_ROLL_DATA params="..pCount)
end

function EventLogger.INTERACT_SHOW_PQ_LOOT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_PQ_LOOT params="..pCount)
end

function EventLogger.INTERACT_SHOW_QUEST(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_QUEST params="..pCount)
end

function EventLogger.INTERACT_SHOW_SCENARIO_QUEUE_LIST(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_SCENARIO_QUEUE_LIST params="..pCount)
end

function EventLogger.INTERACT_SHOW_SIEGE_PAD_BUILD_LIST(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_SIEGE_PAD_BUILD_LIST params="..pCount)
end

function EventLogger.INTERACT_SHOW_STORE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_STORE params="..pCount)
end

function EventLogger.INTERACT_SHOW_TRAINING(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_SHOW_TRAINING params="..pCount)
end

function EventLogger.INTERACT_UPDATE_LIBRARIAN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_UPDATE_LIBRARIAN params="..pCount)
end

function EventLogger.INTERACT_UPDATED_SCENARIO_QUEUE_LIST(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERACT_UPDATED_SCENARIO_QUEUE_LIST params="..pCount)
end

function EventLogger.INTERFACE_RELOADED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"INTERFACE_RELOADED params="..pCount)
end

function EventLogger.ITEM_SET_DATA_ARRIVED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ITEM_SET_DATA_ARRIVED params="..pCount)
end

function EventLogger.ITEM_SET_DATA_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ITEM_SET_DATA_UPDATED params="..pCount)
end

function EventLogger.KEYBINDINGS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"KEYBINDINGS_UPDATED params="..pCount)
end

function EventLogger.KILLER_NAME_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"KILLER_NAME_UPDATED params="..pCount)
end

function EventLogger.L_BUTTON_DOWN_PROCESSED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"L_BUTTON_DOWN_PROCESSED params="..pCount)
end

function EventLogger.L_BUTTON_UP_PROCESSED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"L_BUTTON_UP_PROCESSED params="..pCount)
end

function EventLogger.LANGUAGE_TOGGLED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LANGUAGE_TOGGLED params="..pCount)
end

function EventLogger.LOADING_BEGIN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOADING_BEGIN params="..pCount)
end

function EventLogger.LOADING_END(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOADING_END params="..pCount)
end

function EventLogger.LOADING_PROGRESS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOADING_PROGRESS_UPDATED params="..pCount)
end

function EventLogger.LOCKOUTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOCKOUTS_UPDATED params="..pCount)
end

function EventLogger.LOG_OUT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOG_OUT params="..pCount)
end

function EventLogger.LOGIN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOGIN params="..pCount)
end

function EventLogger.LOGIN_LOCAL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOGIN_LOCAL params="..pCount)
end

function EventLogger.LOGIN_PROGRESS_STARTING(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOGIN_PROGRESS_STARTING params="..pCount)
end

function EventLogger.LOGIN_PROGRESS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOGIN_PROGRESS_UPDATED params="..pCount)
end

function EventLogger.LOOT_ALL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOOT_ALL params="..pCount)
end

function EventLogger.LOOT_GREED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOOT_GREED params="..pCount)
end

function EventLogger.LOOT_NEED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOOT_NEED params="..pCount)
end

function EventLogger.LOOT_PASS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"LOOT_PASS params="..pCount)
end

function EventLogger.M_BUTTON_DOWN_PROCESSED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"M_BUTTON_DOWN_PROCESSED params="..pCount)
end

function EventLogger.M_BUTTON_UP_PROCESSED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"M_BUTTON_UP_PROCESSED params="..pCount)
end

function EventLogger.MACRO_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MACRO_UPDATED params="..pCount)
end

function EventLogger.MACROS_LOADED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MACROS_LOADED params="..pCount)
end

function EventLogger.MAILBOX_HEADER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MAILBOX_HEADER_UPDATED params="..pCount)
end

function EventLogger.MAILBOX_HEADERS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MAILBOX_HEADERS_UPDATED params="..pCount)
end

function EventLogger.MAILBOX_MESSAGE_DELETED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MAILBOX_MESSAGE_DELETED params="..pCount)
end

function EventLogger.MAILBOX_MESSAGE_OPENED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MAILBOX_MESSAGE_OPENED params="..pCount)
end

function EventLogger.MAILBOX_POSTAGE_COST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MAILBOX_POSTAGE_COST_UPDATED params="..pCount)
end

function EventLogger.MAILBOX_RESULTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MAILBOX_RESULTS_UPDATED params="..pCount)
end

function EventLogger.MAILBOX_UNREAD_COUNT_CHANGED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MAILBOX_UNREAD_COUNT_CHANGED params="..pCount)
end

function EventLogger.MAIN_ASSIST_TARGET_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MAIN_ASSIST_TARGET_UPDATED params="..pCount)
end

function EventLogger.MAP_SETTINGS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MAP_SETTINGS_UPDATED params="..pCount)
end

function EventLogger.MOUSE_DOWN_ON_CHAR_SELECT_NIF(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MOUSE_DOWN_ON_CHAR_SELECT_NIF params="..pCount)
end

function EventLogger.MOUSE_DOWN_ON_NIF(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MOUSE_DOWN_ON_NIF params="..pCount)
end

function EventLogger.MOUSE_UP_ON_CHAR_SELECT_NIF(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MOUSE_UP_ON_CHAR_SELECT_NIF params="..pCount)
end

function EventLogger.MOUSE_UP_ON_NIF(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MOUSE_UP_ON_NIF params="..pCount)
end

function EventLogger.MOUSEOVER_WORLD_OBJECT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MOUSEOVER_WORLD_OBJECT params="..pCount)
end

function EventLogger.MOVE_INVENTORY_OBJECT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"MOVE_INVENTORY_OBJECT params="..pCount)
end

function EventLogger.OBJECTIVE_AREA_EXIT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"OBJECTIVE_AREA_EXIT params="..pCount)
end

function EventLogger.OBJECTIVE_CONTROL_POINTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"OBJECTIVE_CONTROL_POINTS_UPDATED params="..pCount)
end

function EventLogger.OBJECTIVE_MAP_TIMER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"OBJECTIVE_MAP_TIMER_UPDATED params="..pCount)
end

function EventLogger.OBJECTIVE_OWNER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"OBJECTIVE_OWNER_UPDATED params="..pCount)
end

function EventLogger.OPEN_LFM_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"OPEN_LFM_WINDOW params="..pCount)
end

function EventLogger.PAIRING_MAP_HOTSPOT_DATA_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PAIRING_MAP_HOTSPOT_DATA_UPDATED params="..pCount)
end

function EventLogger.PAPERDOLL_SPIN_LEFT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PAPERDOLL_SPIN_LEFT params="..pCount)
end

function EventLogger.PAPERDOLL_SPIN_RIGHT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PAPERDOLL_SPIN_RIGHT params="..pCount)
end

function EventLogger.PET_AGGRESSIVE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PET_AGGRESSIVE params="..pCount)
end

function EventLogger.PET_DEFENSIVE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PET_DEFENSIVE params="..pCount)
end

function EventLogger.PET_PASSIVE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PET_PASSIVE params="..pCount)
end

function EventLogger.PLAY(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAY params="..pCount)
end

function EventLogger.PLAY_AS_MONSTER_STATUS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAY_AS_MONSTER_STATUS params="..pCount)
end

function EventLogger.PLAYER_ABILITIES_LIST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_ABILITIES_LIST_UPDATED params="..pCount)
end

function EventLogger.PLAYER_ABILITIES_QUEUED_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_ABILITIES_QUEUED_UPDATED params="..pCount)
end

function EventLogger.PLAYER_ABILITY_TOGGLED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_ABILITY_TOGGLED params="..pCount)
end

function EventLogger.PLAYER_ACTIVE_TACTICS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_ACTIVE_TACTICS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_ACTIVE_TITLE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_ACTIVE_TITLE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_ADVANCE_ALERT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_ADVANCE_ALERT params="..pCount)
end

function EventLogger.PLAYER_AGRO_MODE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_AGRO_MODE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_AREA_CHANGED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_AREA_CHANGED params="..pCount)
end

function EventLogger.PLAYER_AREA_NAME_CHANGED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_AREA_NAME_CHANGED params="..pCount)
end

function EventLogger.PLAYER_BANK_SLOT_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_BANK_SLOT_UPDATED params="..pCount)
end

function EventLogger.PLAYER_BATTLE_LEVEL_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_BATTLE_LEVEL_UPDATED params="..pCount)
end

function EventLogger.PLAYER_BEGIN_CAST(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_BEGIN_CAST params="..pCount)
end

function EventLogger.PLAYER_BLOCKED_ABILITIES_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_BLOCKED_ABILITIES_UPDATED params="..pCount)
end

function EventLogger.PLAYER_CAREER_CATEGORY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CAREER_CATEGORY_UPDATED params="..pCount)
end

function EventLogger.PLAYER_CAREER_LINE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CAREER_LINE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_CAREER_RANK_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CAREER_RANK_UPDATED params="..pCount)
end

function EventLogger.PLAYER_CAREER_RESOURCE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CAREER_RESOURCE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_CAST_TIMER_SETBACK(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CAST_TIMER_SETBACK params="..pCount)
end

function EventLogger.PLAYER_CHAPTER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CHAPTER_UPDATED params="..pCount)
end

function EventLogger.PLAYER_COMBAT_FLAG_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_COMBAT_FLAG_UPDATED params="..pCount)
end

function EventLogger.PLAYER_COOLDOWN_TIMER_SET(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_COOLDOWN_TIMER_SET params="..pCount)
end

function EventLogger.PLAYER_CRAFTING_INVENTORY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CRAFTING_INVENTORY_UPDATED params="..pCount)
end

function EventLogger.PLAYER_CRAFTING_SLOT_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CRAFTING_SLOT_UPDATED params="..pCount)
end

function EventLogger.PLAYER_CRAFTING_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CRAFTING_UPDATED params="..pCount)
end

function EventLogger.PLAYER_CULTIVATION_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CULTIVATION_UPDATED params="..pCount)
end

function EventLogger.PLAYER_CUR_ACTION_POINTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CUR_ACTION_POINTS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_CUR_HIT_POINTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CUR_HIT_POINTS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_CURRENCY_SLOT_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_CURRENCY_SLOT_UPDATED params="..pCount)
end

function EventLogger.PLAYER_DATA_RESPONSE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_DATA_RESPONSE params="..pCount)
end

function EventLogger.PLAYER_DATA_START(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_DATA_START params="..pCount)
end

function EventLogger.PLAYER_DEATH(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_DEATH params="..pCount)
end

function EventLogger.PLAYER_DEATH_CLEARED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_DEATH_CLEARED params="..pCount)
end

function EventLogger.PLAYER_EFFECTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_EFFECTS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_END_CAST(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_END_CAST params="..pCount)
end

function EventLogger.PLAYER_EQUIPMENT_SLOT_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_EQUIPMENT_SLOT_UPDATED params="..pCount)
end

function EventLogger.PLAYER_EXP_TABLE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_EXP_TABLE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_EXP_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_EXP_UPDATED params="..pCount)
end

function EventLogger.PLAYER_GROUP_LEADER_STATUS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_GROUP_LEADER_STATUS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_HEALTH_FADE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_HEALTH_FADE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_HOT_BAR_ENABLED_STATE_CHANGED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_HOT_BAR_ENABLED_STATE_CHANGED params="..pCount)
end

function EventLogger.PLAYER_HOT_BAR_PAGE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_HOT_BAR_PAGE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_HOT_BAR_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_HOT_BAR_UPDATED params="..pCount)
end

function EventLogger.PLAYER_INFLUENCE_RANK_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_INFLUENCE_RANK_UPDATED params="..pCount)
end

function EventLogger.PLAYER_INFLUENCE_REWARDS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_INFLUENCE_REWARDS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_INFLUENCE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_INFLUENCE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_INFO_CHANGED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_INFO_CHANGED params="..pCount)
end

function EventLogger.PLAYER_INTERNAL_BUFF_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_INTERNAL_BUFF_UPDATED params="..pCount)
end

function EventLogger.PLAYER_INVENTORY_OVERFLOW_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_INVENTORY_OVERFLOW_UPDATED params="..pCount)
end

function EventLogger.PLAYER_INVENTORY_SLOT_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_INVENTORY_SLOT_UPDATED params="..pCount)
end

function EventLogger.PLAYER_IS_BEING_THROWN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_IS_BEING_THROWN params="..pCount)
end

function EventLogger.PLAYER_IS_DISARMED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_IS_DISARMED params="..pCount)
end

function EventLogger.PLAYER_IS_IMMUNE_TO_DISABLES(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_IS_IMMUNE_TO_DISABLES params="..pCount)
end

function EventLogger.PLAYER_IS_IMMUNE_TO_MOVEMENT_IMPARING(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_IS_IMMUNE_TO_MOVEMENT_IMPARING params="..pCount)
end

function EventLogger.PLAYER_IS_SILENCED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_IS_SILENCED params="..pCount)
end

function EventLogger.PLAYER_IS_UNABLE_TO_MOVE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_IS_UNABLE_TO_MOVE params="..pCount)
end

function EventLogger.PLAYER_KILLING_SPREE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_KILLING_SPREE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_LEARNED_ABOUT_UI_ELEMENT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_LEARNED_ABOUT_UI_ELEMENT params="..pCount)
end

function EventLogger.PLAYER_MAIN_ASSIST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_MAIN_ASSIST_UPDATED params="..pCount)
end

function EventLogger.PLAYER_MAX_ACTION_POINTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_MAX_ACTION_POINTS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_MAX_HIT_POINTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_MAX_HIT_POINTS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_MONEY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_MONEY_UPDATED params="..pCount)
end

function EventLogger.PLAYER_MORALE_BAR_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_MORALE_BAR_UPDATED params="..pCount)
end

function EventLogger.PLAYER_MORALE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_MORALE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_NEW_ABILITY_LEARNED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_NEW_ABILITY_LEARNED params="..pCount)
end

function EventLogger.PLAYER_NEW_NUMBER_OF_BACKPACK_SLOTS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_NEW_NUMBER_OF_BACKPACK_SLOTS params="..pCount)
end

function EventLogger.PLAYER_NEW_NUMBER_OF_BANK_SLOTS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_NEW_NUMBER_OF_BANK_SLOTS params="..pCount)
end

function EventLogger.PLAYER_NEW_PET_ABILITY_LEARNED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_NEW_PET_ABILITY_LEARNED params="..pCount)
end

function EventLogger.PLAYER_NUM_TACTIC_SLOTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_NUM_TACTIC_SLOTS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_OBJECTIVE_CONTRIBUTION_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_OBJECTIVE_CONTRIBUTION_UPDATED params="..pCount)
end

function EventLogger.PLAYER_OBJECTIVES_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_OBJECTIVES_UPDATED params="..pCount)
end

function EventLogger.PLAYER_OFFER_ITEMS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_OFFER_ITEMS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_PET_HEALTH_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_PET_HEALTH_UPDATED params="..pCount)
end

function EventLogger.PLAYER_PET_STATE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_PET_STATE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_PET_TARGET_HEALTH_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_PET_TARGET_HEALTH_UPDATED params="..pCount)
end

function EventLogger.PLAYER_PET_TARGET_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_PET_TARGET_UPDATED params="..pCount)
end

function EventLogger.PLAYER_PET_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_PET_UPDATED params="..pCount)
end

function EventLogger.PLAYER_POSITION_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_POSITION_UPDATED params="..pCount)
end

function EventLogger.PLAYER_QUEST_ITEM_SLOT_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_QUEST_ITEM_SLOT_UPDATED params="..pCount)
end

function EventLogger.PLAYER_RACE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_RACE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_REALM_BONUS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_REALM_BONUS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_REMOVED_ITEM(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_REMOVED_ITEM params="..pCount)
end

function EventLogger.PLAYER_RENOWN_RANK_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_RENOWN_RANK_UPDATED params="..pCount)
end

function EventLogger.PLAYER_RENOWN_TITLE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_RENOWN_TITLE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_RENOWN_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_RENOWN_UPDATED params="..pCount)
end

function EventLogger.PLAYER_RVR_FLAG_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_RVR_FLAG_UPDATED params="..pCount)
end

function EventLogger.PLAYER_RVR_STATS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_RVR_STATS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_RVRLAKE_CHANGED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_RVRLAKE_CHANGED params="..pCount)
end

function EventLogger.PLAYER_SET_ACTIVE_WEAPON_SET(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_SET_ACTIVE_WEAPON_SET params="..pCount)
end

function EventLogger.PLAYER_SINGLE_ABILITY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_SINGLE_ABILITY_UPDATED params="..pCount)
end

function EventLogger.PLAYER_SKILLS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_SKILLS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_STANCE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_STANCE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_START_INTERACT_TIMER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_START_INTERACT_TIMER params="..pCount)
end

function EventLogger.PLAYER_START_RVR_FLAG_TIMER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_START_RVR_FLAG_TIMER params="..pCount)
end

function EventLogger.PLAYER_STATS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_STATS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_TARGET_EFFECTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_TARGET_EFFECTS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_TARGET_HIT_POINTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_TARGET_HIT_POINTS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_TARGET_IS_IMMUNE_TO_DISABLES(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_TARGET_IS_IMMUNE_TO_DISABLES params="..pCount)
end

function EventLogger.PLAYER_TARGET_IS_IMMUNE_TO_MOVEMENT_IMPARING(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_TARGET_IS_IMMUNE_TO_MOVEMENT_IMPARING params="..pCount)
end

function EventLogger.PLAYER_TARGET_STATE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_TARGET_STATE_UPDATED params="..pCount)
end

function EventLogger.PLAYER_TARGET_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_TARGET_UPDATED params="..pCount)
end

function EventLogger.PLAYER_TRADE_ACCEPTED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_TRADE_ACCEPTED params="..pCount)
end

function EventLogger.PLAYER_TRADE_CANCELLED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_TRADE_CANCELLED params="..pCount)
end

function EventLogger.PLAYER_TRADE_INITIATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_TRADE_INITIATED params="..pCount)
end

function EventLogger.PLAYER_TRADE_ITEMS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_TRADE_ITEMS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_TROPHY_SLOT_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_TROPHY_SLOT_UPDATED params="..pCount)
end

function EventLogger.PLAYER_WEAPON_SETS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_WEAPON_SETS_UPDATED params="..pCount)
end

function EventLogger.PLAYER_ZONE_CHANGED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PLAYER_ZONE_CHANGED params="..pCount)
end

function EventLogger.PRE_MODULE_SHUTDOWNS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PRE_MODULE_SHUTDOWNS params="..pCount)
end

function EventLogger.PREGAME_CLEAR_RANDOM_NAME_LIST(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PREGAME_CLEAR_RANDOM_NAME_LIST params="..pCount)
end

function EventLogger.PREGAME_EULA_ACCEPTED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PREGAME_EULA_ACCEPTED params="..pCount)
end

function EventLogger.PREGAME_GO_TO_CHARACTER_SELECT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PREGAME_GO_TO_CHARACTER_SELECT params="..pCount)
end

function EventLogger.PREGAME_LAUNCH_SERVER_SELECT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PREGAME_LAUNCH_SERVER_SELECT params="..pCount)
end

function EventLogger.PREGAME_PLAY_ANIMATION(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PREGAME_PLAY_ANIMATION params="..pCount)
end

function EventLogger.PREGAME_ROC_ACCEPTED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PREGAME_ROC_ACCEPTED params="..pCount)
end

function EventLogger.PREGAME_SET_STATE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PREGAME_SET_STATE params="..pCount)
end

function EventLogger.PUBLIC_QUEST_ADDED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_ADDED params="..pCount)
end

function EventLogger.PUBLIC_QUEST_COMPLETED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_COMPLETED params="..pCount)
end

function EventLogger.PUBLIC_QUEST_CONDITION_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_CONDITION_UPDATED params="..pCount)
end

function EventLogger.PUBLIC_QUEST_FAILED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_FAILED params="..pCount)
end

function EventLogger.PUBLIC_QUEST_FORCEDOUT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_FORCEDOUT params="..pCount)
end

function EventLogger.PUBLIC_QUEST_HIDE_WINOMETER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_HIDE_WINOMETER params="..pCount)
end

function EventLogger.PUBLIC_QUEST_INFO_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_INFO_UPDATED params="..pCount)
end

function EventLogger.PUBLIC_QUEST_LIST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_LIST_UPDATED params="..pCount)
end

function EventLogger.PUBLIC_QUEST_OPTOUT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_OPTOUT params="..pCount)
end

function EventLogger.PUBLIC_QUEST_REMOVED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_REMOVED params="..pCount)
end

function EventLogger.PUBLIC_QUEST_RESETTING(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_RESETTING params="..pCount)
end

function EventLogger.PUBLIC_QUEST_SHOW_SCOREBOARD(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_SHOW_SCOREBOARD params="..pCount)
end

function EventLogger.PUBLIC_QUEST_SHOW_WINOMETER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_SHOW_WINOMETER params="..pCount)
end

function EventLogger.PUBLIC_QUEST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_UPDATED params="..pCount)
end

function EventLogger.PUBLIC_QUEST_WINOMETER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"PUBLIC_QUEST_WINOMETER_UPDATED params="..pCount)
end

function EventLogger.QUEST_CONDITION_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"QUEST_CONDITION_UPDATED params="..pCount)
end

function EventLogger.QUEST_INFO_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"QUEST_INFO_UPDATED params="..pCount)
end

function EventLogger.QUEST_LIST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"QUEST_LIST_UPDATED params="..pCount)
end

function EventLogger.QUIT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"QUIT params="..pCount)
end

function EventLogger.R_BUTTON_DOWN_PROCESSED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"R_BUTTON_DOWN_PROCESSED params="..pCount)
end

function EventLogger.R_BUTTON_UP_PROCESSED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"R_BUTTON_UP_PROCESSED params="..pCount)
end

function EventLogger.RALLY_CALL_INVITE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"RALLY_CALL_INVITE params="..pCount)
end

function EventLogger.RALLY_CALL_JOIN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"RALLY_CALL_JOIN params="..pCount)
end

function EventLogger.RELEASE_CORPSE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"RELEASE_CORPSE params="..pCount)
end

function EventLogger.RELOAD_INTERFACE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"RELOAD_INTERFACE params="..pCount)
end

function EventLogger.REPORT_GOLD_SELLER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"REPORT_GOLD_SELLER params="..pCount)
end

function EventLogger.RESOLUTION_CHANGED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"RESOLUTION_CHANGED params="..pCount)
end

function EventLogger.RESURRECTION_ACCEPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"RESURRECTION_ACCEPT params="..pCount)
end

function EventLogger.RESURRECTION_DECLINE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"RESURRECTION_DECLINE params="..pCount)
end

function EventLogger.RRQ_LIST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"RRQ_LIST_UPDATED params="..pCount)
end

function EventLogger.RVR_REWARD_POOLS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"RVR_REWARD_POOLS_UPDATED params="..pCount)
end

function EventLogger.SCENARIO_ACTIVE_QUEUE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_ACTIVE_QUEUE_UPDATED params="..pCount)
end

function EventLogger.SCENARIO_BEGIN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_BEGIN params="..pCount)
end

function EventLogger.SCENARIO_END(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_END params="..pCount)
end

function EventLogger.SCENARIO_FINAL_SCOREBOARD_CLOSED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_FINAL_SCOREBOARD_CLOSED params="..pCount)
end

function EventLogger.SCENARIO_GROUP_JOIN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_GROUP_JOIN params="..pCount)
end

function EventLogger.SCENARIO_GROUP_LEAVE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_GROUP_LEAVE params="..pCount)
end

function EventLogger.SCENARIO_GROUP_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_GROUP_UPDATED params="..pCount)
end

function EventLogger.SCENARIO_INSTANCE_CANCEL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_INSTANCE_CANCEL params="..pCount)
end

function EventLogger.SCENARIO_INSTANCE_JOIN_NOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_INSTANCE_JOIN_NOW params="..pCount)
end

function EventLogger.SCENARIO_INSTANCE_JOIN_WAIT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_INSTANCE_JOIN_WAIT params="..pCount)
end

function EventLogger.SCENARIO_INSTANCE_LEAVE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_INSTANCE_LEAVE params="..pCount)
end

function EventLogger.SCENARIO_PLAYER_HITS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_PLAYER_HITS_UPDATED params="..pCount)
end

function EventLogger.SCENARIO_PLAYERS_LIST_GROUPS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_PLAYERS_LIST_GROUPS_UPDATED params="..pCount)
end

function EventLogger.SCENARIO_PLAYERS_LIST_RESERVATIONS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_PLAYERS_LIST_RESERVATIONS_UPDATED params="..pCount)
end

function EventLogger.SCENARIO_PLAYERS_LIST_STATS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_PLAYERS_LIST_STATS_UPDATED params="..pCount)
end

function EventLogger.SCENARIO_PLAYERS_LIST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_PLAYERS_LIST_UPDATED params="..pCount)
end

function EventLogger.SCENARIO_POST_MODE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_POST_MODE params="..pCount)
end

function EventLogger.SCENARIO_SHOW_JOIN_PROMPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_SHOW_JOIN_PROMPT params="..pCount)
end

function EventLogger.SCENARIO_SHOW_LEVELED_NEED_REJOIN_BRACKET(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_SHOW_LEVELED_NEED_REJOIN_BRACKET params="..pCount)
end

function EventLogger.SCENARIO_SHOW_LEVELED_OUT_OF_BRACKETS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_SHOW_LEVELED_OUT_OF_BRACKETS params="..pCount)
end

function EventLogger.SCENARIO_START_UPDATING_PLAYERS_STATS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_START_UPDATING_PLAYERS_STATS params="..pCount)
end

function EventLogger.SCENARIO_STARTING_SCENARIO_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_STARTING_SCENARIO_UPDATED params="..pCount)
end

function EventLogger.SCENARIO_STOP_UPDATING_PLAYERS_STATS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_STOP_UPDATING_PLAYERS_STATS params="..pCount)
end

function EventLogger.SCENARIO_UPDATE_POINTS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SCENARIO_UPDATE_POINTS params="..pCount)
end

function EventLogger.SELECT_CHARACTER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SELECT_CHARACTER params="..pCount)
end

function EventLogger.SERVER_LIST_RESPONSE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SERVER_LIST_RESPONSE params="..pCount)
end

function EventLogger.SERVER_LIST_START(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SERVER_LIST_START params="..pCount)
end

function EventLogger.SERVER_LOAD_STATUS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SERVER_LOAD_STATUS params="..pCount)
end

function EventLogger.SETTINGS_CHAT_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SETTINGS_CHAT_UPDATED params="..pCount)
end

function EventLogger.SHOW_ADVANCED_WAR(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SHOW_ADVANCED_WAR params="..pCount)
end

function EventLogger.SHOW_ALERT_TEXT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SHOW_ALERT_TEXT params="..pCount)
end

function EventLogger.SIEGE_WEAPON_CONTROL_TIMER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_CONTROL_TIMER_UPDATED params="..pCount)
end

function EventLogger.SIEGE_WEAPON_FIRE_RESULTS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_FIRE_RESULTS params="..pCount)
end

function EventLogger.SIEGE_WEAPON_GOLF_BEGIN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_GOLF_BEGIN params="..pCount)
end

function EventLogger.SIEGE_WEAPON_GOLF_END(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_GOLF_END params="..pCount)
end

function EventLogger.SIEGE_WEAPON_GOLF_RESET(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_GOLF_RESET params="..pCount)
end

function EventLogger.SIEGE_WEAPON_GOLF_START_BACK_SWING(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_GOLF_START_BACK_SWING params="..pCount)
end

function EventLogger.SIEGE_WEAPON_GOLF_START_FORWARD_SWING(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_GOLF_START_FORWARD_SWING params="..pCount)
end

function EventLogger.SIEGE_WEAPON_HEALTH_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_HEALTH_UPDATED params="..pCount)
end

function EventLogger.SIEGE_WEAPON_RELEASE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_RELEASE params="..pCount)
end

function EventLogger.SIEGE_WEAPON_REUSE_TIMER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_REUSE_TIMER_UPDATED params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SCORCH_BEGIN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SCORCH_BEGIN params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SCORCH_END(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SCORCH_END params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SCORCH_POWER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SCORCH_POWER_UPDATED params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SCORCH_WIND_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SCORCH_WIND_UPDATED params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SNIPER_AIM_TARGET_LOS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SNIPER_AIM_TARGET_LOS_UPDATED params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SNIPER_AIM_TARGET_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SNIPER_AIM_TARGET_UPDATED params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SNIPER_AIM_TIME_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SNIPER_AIM_TIME_UPDATED params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SNIPER_BEGIN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SNIPER_BEGIN params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SNIPER_END(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SNIPER_END params="..pCount)
end

function EventLogger.SIEGE_WEAPON_STATE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_STATE_UPDATED params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SWEET_SPOT_BEGIN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SWEET_SPOT_BEGIN params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SWEET_SPOT_END(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SWEET_SPOT_END params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SWEET_SPOT_FIRE_RESULTS(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SWEET_SPOT_FIRE_RESULTS params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SWEET_SPOT_RESET(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SWEET_SPOT_RESET params="..pCount)
end

function EventLogger.SIEGE_WEAPON_SWEET_SPOT_START_MOVING(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_SWEET_SPOT_START_MOVING params="..pCount)
end

function EventLogger.SIEGE_WEAPON_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_UPDATED params="..pCount)
end

function EventLogger.SIEGE_WEAPON_USERS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SIEGE_WEAPON_USERS_UPDATED params="..pCount)
end

function EventLogger.SOCIAL_BRAGGING_RIGHTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SOCIAL_BRAGGING_RIGHTS_UPDATED params="..pCount)
end

function EventLogger.SOCIAL_FRIENDS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SOCIAL_FRIENDS_UPDATED params="..pCount)
end

function EventLogger.SOCIAL_IGNORE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SOCIAL_IGNORE_UPDATED params="..pCount)
end

function EventLogger.SOCIAL_INSPECTION_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SOCIAL_INSPECTION_UPDATED params="..pCount)
end

function EventLogger.SOCIAL_OPENPARTY_NOTIFY(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SOCIAL_OPENPARTY_NOTIFY params="..pCount)
end

function EventLogger.SOCIAL_OPENPARTY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SOCIAL_OPENPARTY_UPDATED params="..pCount)
end

function EventLogger.SOCIAL_OPENPARTY_WORLD_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SOCIAL_OPENPARTY_WORLD_UPDATED params="..pCount)
end

function EventLogger.SOCIAL_OPENPARTYINTEREST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SOCIAL_OPENPARTYINTEREST_UPDATED params="..pCount)
end

function EventLogger.SOCIAL_OPENPARTYWORLD_SETTINGS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SOCIAL_OPENPARTYWORLD_SETTINGS_UPDATED params="..pCount)
end

function EventLogger.SOCIAL_OPTIONS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SOCIAL_OPTIONS_UPDATED params="..pCount)
end

function EventLogger.SOCIAL_SEARCH_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SOCIAL_SEARCH_UPDATED params="..pCount)
end

function EventLogger.SOCIAL_YOU_HAVE_BEEN_FRIENDED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SOCIAL_YOU_HAVE_BEEN_FRIENDED params="..pCount)
end

function EventLogger.SPELL_CAST_CANCEL(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SPELL_CAST_CANCEL params="..pCount)
end

function EventLogger.STREAMING_STATUS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"STREAMING_STATUS_UPDATED params="..pCount)
end

function EventLogger.SUMMON_ACCEPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SUMMON_ACCEPT params="..pCount)
end

function EventLogger.SUMMON_DECLINE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SUMMON_DECLINE params="..pCount)
end

function EventLogger.SUMMON_SHOW_PROMPT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SUMMON_SHOW_PROMPT params="..pCount)
end

function EventLogger.SURVEY_POPUP(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SURVEY_POPUP params="..pCount)
end

function EventLogger.SURVEY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"SURVEY_UPDATED params="..pCount)
end

function EventLogger.TARGET_GROUP_MEMBER_1(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_GROUP_MEMBER_1 params="..pCount)
end

function EventLogger.TARGET_GROUP_MEMBER_1_PET(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_GROUP_MEMBER_1_PET params="..pCount)
end

function EventLogger.TARGET_GROUP_MEMBER_2(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_GROUP_MEMBER_2 params="..pCount)
end

function EventLogger.TARGET_GROUP_MEMBER_2_PET(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_GROUP_MEMBER_2_PET params="..pCount)
end

function EventLogger.TARGET_GROUP_MEMBER_3(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_GROUP_MEMBER_3 params="..pCount)
end

function EventLogger.TARGET_GROUP_MEMBER_3_PET(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_GROUP_MEMBER_3_PET params="..pCount)
end

function EventLogger.TARGET_GROUP_MEMBER_4(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_GROUP_MEMBER_4 params="..pCount)
end

function EventLogger.TARGET_GROUP_MEMBER_4_PET(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_GROUP_MEMBER_4_PET params="..pCount)
end

function EventLogger.TARGET_GROUP_MEMBER_5(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_GROUP_MEMBER_5 params="..pCount)
end

function EventLogger.TARGET_GROUP_MEMBER_5_PET(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_GROUP_MEMBER_5_PET params="..pCount)
end

function EventLogger.TARGET_GROUP_MEMBER_6(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_GROUP_MEMBER_6 params="..pCount)
end

function EventLogger.TARGET_GROUP_MEMBER_6_PET(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_GROUP_MEMBER_6_PET params="..pCount)
end

function EventLogger.TARGET_PET(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_PET params="..pCount)
end

function EventLogger.TARGET_SELF(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TARGET_SELF params="..pCount)
end

function EventLogger.TITLE_SCREEN_INIT_LOADING_UI(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TITLE_SCREEN_INIT_LOADING_UI params="..pCount)
end

function EventLogger.TITLE_SCREEN_INIT_LOADING_UI_COMPLETE(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TITLE_SCREEN_INIT_LOADING_UI_COMPLETE params="..pCount)
end

function EventLogger.TOGGLE_ABILITIES_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_ABILITIES_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_BACKPACK_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_BACKPACK_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_BATTLEGROUP_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_BATTLEGROUP_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_CHARACTER_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_CHARACTER_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_CURRENT_EVENTS_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_CURRENT_EVENTS_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_FULLSCREEN(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_FULLSCREEN params="..pCount)
end

function EventLogger.TOGGLE_GUILD_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_GUILD_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_HELP_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_HELP_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_HOTBAR(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_HOTBAR params="..pCount)
end

function EventLogger.TOGGLE_MENU(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_MENU params="..pCount)
end

function EventLogger.TOGGLE_MENU_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_MENU_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_PARTY_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_PARTY_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_SCENARIO_SUMMARY_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_SCENARIO_SUMMARY_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_SOCIAL_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_SOCIAL_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_TOME_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_TOME_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_USER_SETTINGS_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_USER_SETTINGS_WINDOW params="..pCount)
end

function EventLogger.TOGGLE_WORLD_MAP_WINDOW(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOGGLE_WORLD_MAP_WINDOW params="..pCount)
end

function EventLogger.TOME_ACHIEVEMENTS_SUBTYPE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_ACHIEVEMENTS_SUBTYPE_UPDATED params="..pCount)
end

function EventLogger.TOME_ACHIEVEMENTS_TOC_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_ACHIEVEMENTS_TOC_UPDATED params="..pCount)
end

function EventLogger.TOME_ACHIEVEMENTS_TYPE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_ACHIEVEMENTS_TYPE_UPDATED params="..pCount)
end

function EventLogger.TOME_ALERT_ADDED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_ALERT_ADDED params="..pCount)
end

function EventLogger.TOME_ALERTS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_ALERTS_UPDATED params="..pCount)
end

function EventLogger.TOME_BESTIARY_SPECIES_KILL_COUNT_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_BESTIARY_SPECIES_KILL_COUNT_UPDATED params="..pCount)
end

function EventLogger.TOME_BESTIARY_SPECIES_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_BESTIARY_SPECIES_UPDATED params="..pCount)
end

function EventLogger.TOME_BESTIARY_SUBTYPE_KILL_COUNT_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_BESTIARY_SUBTYPE_KILL_COUNT_UPDATED params="..pCount)
end

function EventLogger.TOME_BESTIARY_SUBTYPE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_BESTIARY_SUBTYPE_UPDATED params="..pCount)
end

function EventLogger.TOME_BESTIARY_TOC_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_BESTIARY_TOC_UPDATED params="..pCount)
end

function EventLogger.TOME_BESTIARY_TOTAL_KILL_COUNT_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_BESTIARY_TOTAL_KILL_COUNT_UPDATED params="..pCount)
end

function EventLogger.TOME_CARD_LIST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_CARD_LIST_UPDATED params="..pCount)
end

function EventLogger.TOME_GAME_FAQ_TOC_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_GAME_FAQ_TOC_UPDATED params="..pCount)
end

function EventLogger.TOME_HISTORY_AND_LORE_ENTRY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_HISTORY_AND_LORE_ENTRY_UPDATED params="..pCount)
end

function EventLogger.TOME_HISTORY_AND_LORE_PAIRING_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_HISTORY_AND_LORE_PAIRING_UPDATED params="..pCount)
end

function EventLogger.TOME_HISTORY_AND_LORE_TOC_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_HISTORY_AND_LORE_TOC_UPDATED params="..pCount)
end

function EventLogger.TOME_HISTORY_AND_LORE_ZONE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_HISTORY_AND_LORE_ZONE_UPDATED params="..pCount)
end

function EventLogger.TOME_ID_UNLOCKED_FOR_PLAYER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_ID_UNLOCKED_FOR_PLAYER params="..pCount)
end

function EventLogger.TOME_INITIALIZED_FOR_PLAYER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_INITIALIZED_FOR_PLAYER params="..pCount)
end

function EventLogger.TOME_ITEM_REWARDS_LIST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_ITEM_REWARDS_LIST_UPDATED params="..pCount)
end

function EventLogger.TOME_LIVE_EVENT_ENDED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_LIVE_EVENT_ENDED params="..pCount)
end

function EventLogger.TOME_LIVE_EVENT_LOADED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_LIVE_EVENT_LOADED params="..pCount)
end

function EventLogger.TOME_LIVE_EVENT_OVERALL_COUNTER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_LIVE_EVENT_OVERALL_COUNTER_UPDATED params="..pCount)
end

function EventLogger.TOME_LIVE_EVENT_REMOVED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_LIVE_EVENT_REMOVED params="..pCount)
end

function EventLogger.TOME_LIVE_EVENT_TASK_COUNTER_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_LIVE_EVENT_TASK_COUNTER_UPDATED params="..pCount)
end

function EventLogger.TOME_LIVE_EVENT_TASKS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_LIVE_EVENT_TASKS_UPDATED params="..pCount)
end

function EventLogger.TOME_NOTEWORTHY_PERSONS_ENTRY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_NOTEWORTHY_PERSONS_ENTRY_UPDATED params="..pCount)
end

function EventLogger.TOME_NOTEWORTHY_PERSONS_PAIRING_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_NOTEWORTHY_PERSONS_PAIRING_UPDATED params="..pCount)
end

function EventLogger.TOME_NOTEWORTHY_PERSONS_TOC_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_NOTEWORTHY_PERSONS_TOC_UPDATED params="..pCount)
end

function EventLogger.TOME_NOTEWORTHY_PERSONS_ZONE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_NOTEWORTHY_PERSONS_ZONE_UPDATED params="..pCount)
end

function EventLogger.TOME_OLD_WORLD_ARMORY_ARMOR_SET_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_OLD_WORLD_ARMORY_ARMOR_SET_UPDATED params="..pCount)
end

function EventLogger.TOME_OLD_WORLD_ARMORY_TOC_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_OLD_WORLD_ARMORY_TOC_UPDATED params="..pCount)
end

function EventLogger.TOME_PLAYER_TITLES_TOC_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_PLAYER_TITLES_TOC_UPDATED params="..pCount)
end

function EventLogger.TOME_PLAYER_TITLES_TYPE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_PLAYER_TITLES_TYPE_UPDATED params="..pCount)
end

function EventLogger.TOME_SIGIL_ENTRY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_SIGIL_ENTRY_UPDATED params="..pCount)
end

function EventLogger.TOME_SIGIL_TOC_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_SIGIL_TOC_UPDATED params="..pCount)
end

function EventLogger.TOME_STAT_PLAYED_TIME_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_STAT_PLAYED_TIME_UPDATED params="..pCount)
end

function EventLogger.TOME_STAT_TOTAL_CARDS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_STAT_TOTAL_CARDS_UPDATED params="..pCount)
end

function EventLogger.TOME_STAT_TOTAL_ITEM_REWARDS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_STAT_TOTAL_ITEM_REWARDS_UPDATED params="..pCount)
end

function EventLogger.TOME_STAT_TOTAL_TACTIC_REWARDS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_STAT_TOTAL_TACTIC_REWARDS_UPDATED params="..pCount)
end

function EventLogger.TOME_STAT_TOTAL_TITLE_REWARDS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_STAT_TOTAL_TITLE_REWARDS_UPDATED params="..pCount)
end

function EventLogger.TOME_STAT_TOTAL_UNLOCKS_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_STAT_TOTAL_UNLOCKS_UPDATED params="..pCount)
end

function EventLogger.TOME_STAT_TOTAL_XP_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_STAT_TOTAL_XP_UPDATED params="..pCount)
end

function EventLogger.TOME_TACTIC_REWARDS_LIST_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_TACTIC_REWARDS_LIST_UPDATED params="..pCount)
end

function EventLogger.TOME_WAR_JOURNAL_ENTRY_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_WAR_JOURNAL_ENTRY_UPDATED params="..pCount)
end

function EventLogger.TOME_WAR_JOURNAL_STORYLINE_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_WAR_JOURNAL_STORYLINE_UPDATED params="..pCount)
end

function EventLogger.TOME_WAR_JOURNAL_TOC_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TOME_WAR_JOURNAL_TOC_UPDATED params="..pCount)
end

function EventLogger.TRADE_SKILL_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TRADE_SKILL_UPDATED params="..pCount)
end

function EventLogger.TRANSFER_GUILD(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TRANSFER_GUILD params="..pCount)
end

function EventLogger.TRIAL_ALERT_POPUP(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"TRIAL_ALERT_POPUP params="..pCount)
end

function EventLogger.UPDATE_CREATION_CHARACTER(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"UPDATE_CREATION_CHARACTER params="..pCount)
end

function EventLogger.UPDATE_GUILDHERALDRY(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"UPDATE_GUILDHERALDRY params="..pCount)
end

function EventLogger.UPDATE_GUILDSTANDARD(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"UPDATE_GUILDSTANDARD params="..pCount)
end

function EventLogger.UPDATE_ITEM_ENHANCEMENT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"UPDATE_ITEM_ENHANCEMENT params="..pCount)
end

function EventLogger.UPDATE_PROCESSED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"UPDATE_PROCESSED params="..pCount)
end

function EventLogger.USER_SETTINGS_CHANGED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"USER_SETTINGS_CHANGED params="..pCount)
end

function EventLogger.VIDEO_PLAYER_START(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"VIDEO_PLAYER_START params="..pCount)
end

function EventLogger.VIDEO_PLAYER_STOP(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"VIDEO_PLAYER_STOP params="..pCount)
end

function EventLogger.VISIBLE_EQUIPMENT_UPDATED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"VISIBLE_EQUIPMENT_UPDATED params="..pCount)
end

function EventLogger.WORLD_EVENT_TEXT_ARRIVED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"WORLD_EVENT_TEXT_ARRIVED params="..pCount)
end

function EventLogger.WORLD_MAP_POINTS_LOADED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"WORLD_MAP_POINTS_LOADED params="..pCount)
end

function EventLogger.WORLD_OBJ_COMBAT_EVENT(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"WORLD_OBJ_COMBAT_EVENT params="..pCount)
end

function EventLogger.WORLD_OBJ_HEALTH_CHANGED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"WORLD_OBJ_HEALTH_CHANGED params="..pCount)
end

function EventLogger.WORLD_OBJ_INFLUENCE_GAINED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"WORLD_OBJ_INFLUENCE_GAINED params="..pCount)
end

function EventLogger.WORLD_OBJ_RENOWN_GAINED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"WORLD_OBJ_RENOWN_GAINED params="..pCount)
end

function EventLogger.WORLD_OBJ_XP_GAINED(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"WORLD_OBJ_XP_GAINED params="..pCount)
end

function EventLogger.ZONE_CONFIRMATION_NO(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ZONE_CONFIRMATION_NO params="..pCount)
end

function EventLogger.ZONE_CONFIRMATION_YES(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10)
	local pCount = 0
	if p1 then pCount = pCount + 1 end if p2 then pCount = pCount + 1 end if p3 then pCount = pCount + 1 end if p4 then pCount = pCount + 1 end if p5 then pCount = pCount + 1 end
	if p6 then pCount = pCount + 1 end if p7 then pCount = pCount + 1 end if p8 then pCount = pCount + 1 end if p9 then pCount = pCount + 1 end if p10 then pCount = pCount + 1 end

	EA_ChatWindow.Print(L"ZONE_CONFIRMATION_YES params="..pCount)
end

