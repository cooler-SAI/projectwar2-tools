// The following ifdef block is the standard way of creating macros which make exporting
// from a DLL simpler. All files within this DLL are compiled with the GEOM2FBX_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see
// GEOM2FBX_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef GEOM2FBX_EXPORTS
#define GEOM2FBX_API __declspec(dllexport)
#else
#define GEOM2FBX_API __declspec(dllimport)
#endif

// This class is exported from the dll
class GEOM2FBX_API CGeom2FBX {
public:
	CGeom2FBX(void);
	// TODO: add your methods here.
};

extern GEOM2FBX_API int nGeom2FBX;

GEOM2FBX_API int fnGeom2FBX(void);
