using WarServer.Game.Entities;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    public class F_PLAYER_RANK_UPDATE : Frame
    {
        private byte Level;
        private ushort ObjectID;

        public F_PLAYER_RANK_UPDATE() : base((int)GameOp.F_PLAYER_RANK_UPDATE)
        {
        }

        public static F_PLAYER_RANK_UPDATE Create(Player player)
        {
            return new F_PLAYER_RANK_UPDATE()
            {
                Level = player.Level,
                ObjectID = player.ObjectID
            };
        }
		protected override void SerializeInternal()
        {
            WriteByte((byte)Level);
            WriteByte((byte)0);
            WriteUInt16(ObjectID);
        }

    }
}
