using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_INFLUENCE_UPDATE, FrameType.Game)]
    public class F_INFLUENCE_UPDATE : Frame
    {
        public F_INFLUENCE_UPDATE() : base((int)GameOp.F_INFLUENCE_UPDATE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
