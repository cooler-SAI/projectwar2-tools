using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_REQUEST_CHAR_ERROR, FrameType.Game)]
    public class F_REQUEST_CHAR_ERROR : Frame
    {
        private byte Error;

        public F_REQUEST_CHAR_ERROR() : base((int)GameOp.F_REQUEST_CHAR_ERROR)
        {
        }

        public static F_REQUEST_CHAR_ERROR Create(byte error)
        {
            return new F_REQUEST_CHAR_ERROR()
            {
                Error = error
            };
        }
		
		protected override void SerializeInternal()
        {
            WriteByte(Error);
        }
    }
}
