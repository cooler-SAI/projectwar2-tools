using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_MAX_VELOCITY, FrameType.Game)]
    public class F_MAX_VELOCITY : Frame
    {
        private ushort Speed;
        public F_MAX_VELOCITY() : base((int)GameOp.F_MAX_VELOCITY)
        {
        }

        public static F_MAX_VELOCITY Create(ushort speed)
        {
            return new F_MAX_VELOCITY()
            {
                Speed = speed
            };
        }

        protected override void SerializeInternal()
        {
            WriteUInt16(Speed);
            WriteByte(1);
            WriteByte(100);
        }
    }
}
