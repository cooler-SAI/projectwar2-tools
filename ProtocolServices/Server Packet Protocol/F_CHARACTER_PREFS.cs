using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CHARACTER_PREFS, FrameType.Game)]
    public class F_CHARACTER_PREFS : Frame
    {
        public F_CHARACTER_PREFS() : base((int)GameOp.F_CHARACTER_PREFS)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
