using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_ITEM_SET_DATA, FrameType.Game)]
    public class F_ITEM_SET_DATA : Frame
    {
        private ItemSetData Set;

        private F_ITEM_SET_DATA() : base((int)GameOp.F_ITEM_SET_DATA)
        {
        }

        public static F_ITEM_SET_DATA Create(ItemSetData data)
        {
            return new F_ITEM_SET_DATA()
            {
                Set = data
            };
        }

		protected override void SerializeInternal()
        {
            WriteUInt32((uint)Set.ID);
            WritePascalString(Set.Name);
            WriteByte((byte)Set.Level);
            WriteByte(((byte)Set.Items.Count));

            foreach (var item in Set.Items.Values)
            {
                WriteUInt32((uint)item.ID);
                WritePascalString(item.Name);
            }

            WriteByte((byte)Set.Bonuses.Count);

            foreach (var bonus in Set.Bonuses.Values)
            {
                if (bonus.Ability == null)
                {
                    WriteByte((byte)(bonus.PieceCount | 0x20));
                    WriteByte((byte)bonus.BonusType);
                    WriteUInt16((byte)bonus.Value1);
                    WriteByte((byte)bonus.Value2);
                }
                else
                {
                    WriteByte((byte)(bonus.PieceCount | 0x50));
                    WriteUInt16((ushort)bonus.AbilityID);
                }
            }
        }
    }
}
