using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_SWITCH_ATTACK_MODE, FrameType.Game)]
    public class F_SWITCH_ATTACK_MODE : Frame
    {
        public F_SWITCH_ATTACK_MODE() : base((int)GameOp.F_SWITCH_ATTACK_MODE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
