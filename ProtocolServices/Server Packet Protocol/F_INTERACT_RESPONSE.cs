using System.Collections.Generic;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_INTERACT_RESPONSE, FrameType.Game)]
    public class F_INTERACT_RESPONSE : Frame
    {
        private byte type;
        private byte par1;
        private byte par2 = 0;
        private byte par3 = 0;
        private byte par4 = 0;
        private byte par5 = 0;
        private ushort objectID =0;
        private ushort responseOption = 0;
        private string Message = "";
        private ItemData item;
        private List<ScenarioData> Scenarios = new List<ScenarioData>();
        public F_INTERACT_RESPONSE() : base((int)GameOp.F_INTERACT_RESPONSE)
        {
        }

        public static F_INTERACT_RESPONSE Create(byte type, byte par1, byte par2)
        {
            return new F_INTERACT_RESPONSE()
            {
                type = type,
                par1 = par1,
                par2 = par2
            };
        }


        public static F_INTERACT_RESPONSE CreateBuildSiegeWeapon(ItemData item)
        {
            return new F_INTERACT_RESPONSE()
            {
                type = 0x17,
                item = item
            };
        }

        public static F_INTERACT_RESPONSE CreateScenarioQueue(List<ScenarioData> scenarios)
        {
            return new F_INTERACT_RESPONSE()
            {
                type = 9,
                Scenarios = scenarios
            };
        }


        public static F_INTERACT_RESPONSE Create(ushort objectID, ushort option, string message)
        {
            return new F_INTERACT_RESPONSE()
            {
                type = 0,
                responseOption = option,
                Message = message,
                objectID = objectID,
            };
        }


        protected override void SerializeInternal()
        {
            WriteByte(type);

            if (type == 0)
            {
                WriteUInt16(objectID);
                WriteByte(0);
                WriteByte(0);
                WriteByte(0);
                WriteUInt16(responseOption);
                WritePascalString(Message);
                WriteByte(0);
            }
            else if (type == 9)
            {
                WriteByte(0);//unk
                WriteByte((byte)Scenarios.Count);//unk
                foreach (var scenario in Scenarios)
                {
                    WriteUInt16(0);
                    WriteUInt16((ushort)scenario.ID);
                }
               WriteByte(0);
            }
            else if (type == 0x17)
            {
                WriteByte(3); //siege count
                for (int i = 0; i < 3; i++)
                {
                    WriteUInt16(0); //entry id
                    WritePascalString("test object"); //siege name
                    WriteByte(1); //unk
                    WriteByte(2); //level
                    WriteUInt32(666); //hit points
                    WriteUInt16(123); // abilityID

                    WriteByte(1); //required item count
                    F_GET_ITEM.BuildStoreItem(this, item);

                    WriteByte(1);             //unk
                    WriteByte(2);             //unk
                    WriteByte(3);             //unk
                    WriteByte(4);             //unk
                    WriteUInt16(123);         //unk
                    WriteUInt16(323);         //unk
                    WriteByte(6);             //unk
                }
            }
            else
            {

                WriteByte(par1);
                WriteByte(par2);
                WriteByte(par3);
                WriteByte(par4);
                WriteByte(par5);
            }
        }
    }
}
