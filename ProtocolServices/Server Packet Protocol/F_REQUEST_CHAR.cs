using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_REQUEST_CHAR, FrameType.Game)]
    public class F_REQUEST_CHAR : Frame
    {
        public ushort Operation;

        public F_REQUEST_CHAR() : base((int)GameOp.F_REQUEST_CHAR)
        {
        }
        protected override void DeserializeInternal()
        {
            Operation = ReadUInt16();
        }
    }
}
