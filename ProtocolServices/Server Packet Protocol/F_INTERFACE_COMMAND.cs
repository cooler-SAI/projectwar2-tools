using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_INTERFACE_COMMAND, FrameType.Game)]
    public class F_INTERFACE_COMMAND : Frame
    {
        public InterfaceCommand Command;
        public byte Param1;
        public ushort Param2;
        public ushort Param3;

        public F_INTERFACE_COMMAND() : base((int)GameOp.F_INTERFACE_COMMAND)
        {
        }

        protected override void DeserializeInternal()
        {
            Command = (InterfaceCommand)ReadByte();
            Param1 = ReadByte();
            Param2 = ReadUInt16();
            Param3 = ReadUInt16();

        }
    }
}
