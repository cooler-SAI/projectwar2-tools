using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_REALM_BONUS, FrameType.Game)]
    public class F_REALM_BONUS : Frame
    {
        public F_REALM_BONUS() : base((int)GameOp.F_REALM_BONUS)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
