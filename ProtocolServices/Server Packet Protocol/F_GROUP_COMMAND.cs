using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_GROUP_COMMAND, FrameType.Game)]
    public class F_GROUP_COMMAND : Frame
    {
        public uint Unk1;
        public GroupCommand Cmd;
        public uint CharacterID;

        public enum GroupCommand
        {
            ACCEPT_INVITATION = 2,
            DECLINE_INVITATION = 6,
            REMOVE_CHARACTER = 3,
            JOIN_SCENARIO_GROUP = 13,
            LEAVE_SCENARIO_GROUP = 14,
            SET_LEADER = 10,
            CLAIM_ASSIST = 17,
        }

        public F_GROUP_COMMAND() : base((int)GameOp.F_GROUP_COMMAND)
        {
        }

        protected override void DeserializeInternal()
        {
            Unk1 = ReadUInt32();
            Cmd = (GroupCommand)ReadByte();
            CharacterID = ReadUInt32();
        }
    }
}
