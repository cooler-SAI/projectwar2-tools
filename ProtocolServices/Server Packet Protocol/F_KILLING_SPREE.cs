using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_KILLING_SPREE, FrameType.Game)]
    public class F_KILLING_SPREE : Frame
    {
        public F_KILLING_SPREE() : base((int)GameOp.F_KILLING_SPREE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
