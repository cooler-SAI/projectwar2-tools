using WarServer.Game.Entities;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CLIENT_DATA, FrameType.Game)]
    public class F_CLIENT_DATA : Frame
    {
        public ushort ClientOffset;
        public ushort ClientSize;
        public byte[] ClientData;

        public F_CLIENT_DATA() : base((int)GameOp.F_CLIENT_DATA)
        {
        }

        public static F_CLIENT_DATA Create(Player player)
        {
            return new F_CLIENT_DATA()
            {
                ClientOffset = 0,
                ClientData = player.Data.UserData
            };
        }

		protected override void SerializeInternal()
        {
            WriteUInt16(0);
            if (ClientData != null)
            {
                WriteUInt16((ushort)ClientData.Length);
                WriteByte(0);
                Write(ClientData, 0, ClientData.Length);
            }
            else
            {
                WriteUInt16(0);
                WriteByte(0);
            }
        }

        protected override void DeserializeInternal()
        {
            ClientOffset = ReadUInt16();
            ClientSize = ReadUInt16();
            ClientData = ReadByteArray(ClientSize);
        }
    }
}
