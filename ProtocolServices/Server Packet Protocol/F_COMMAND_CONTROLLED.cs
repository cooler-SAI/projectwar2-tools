using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_COMMAND_CONTROLLED, FrameType.Game)]
    public class F_COMMAND_CONTROLLED : Frame
    {
        public F_COMMAND_CONTROLLED() : base((int)GameOp.F_COMMAND_CONTROLLED)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
