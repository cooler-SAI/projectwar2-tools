using WarServer.Game.Entities;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CAREER_PACKAGE_INFO, FrameType.Game)]
    public class F_CAREER_PACKAGE_INFO : Frame
    {
        private PackageData Package;
        private PlayerData Player;

        public F_CAREER_PACKAGE_INFO() : base((int)GameOp.F_CAREER_PACKAGE_INFO)
        {
        }

        public static F_CAREER_PACKAGE_INFO Create(Player player, PackageData data)
        {
            return new F_CAREER_PACKAGE_INFO()
            {
                Package = data,
                Player = player.Data
            };
        }
        private void SerializeStatPackage()
        {
            WriteByte((byte)Package.Category.CategoryIndex);
            WriteByte(1);
            WriteUInt16(Package.PackageIndex);
            WriteByte(0);
            WriteByte(Package.MinLevel); //minLevel
            WriteByte(Package.MinRenownLevel);

            WriteByte(0);
            WriteBool(Player.HasPackage(Package));

            byte costFlag = 0;
            if (Package.GoldCost > 0)
                costFlag |= 1;
            if (Package.RenownCost > 0)
                costFlag |= 2;

            WriteByte(costFlag); //cost flags (1=gold, 2=renown)

            Skip(14);

            if (Package.GoldCost > 0)
                WriteUInt32((uint)Package.GoldCost);
            if (Package.RenownCost > 0)
                WriteByte((byte)Package.RenownCost);

            WriteByte(1); //package count (client will loop this number times over next section)

            WriteUInt32((uint)Package.PackageNameID); //package nameID

            if ((int)Package.BonusType < 17)
            {
                WriteByte(3); //package type (2=AbilityID, 7=BonusType as AbilityID, 3=BonusTypeBit as abilityID)
                WriteUInt32((byte)(1 << (int)Package.BonusType));
                WriteByte(Package.Value);
            }
            else
            {
                WriteByte(7);
                WriteUInt32((byte)Package.BonusType);
                WriteByte(Package.Value);
            }

            WriteByte(Package.UnkV30);
            if (Package.ReqIndex > 0)
            {
                WriteByte(1); //requirment count, client will loop
                WriteUInt16(Package.ReqIndex);
                WriteByte(Package.Points);
            }
            else
            {
                WriteByte(0);
            }

            WriteUInt32(0);
        }

        private void SerializeAbilityPackage()
        {
            WriteByte((byte)Package.Category.CategoryIndex);
            WriteByte(1);
            WriteUInt16(Package.PackageIndex);
            WriteByte(0);
            WriteByte((byte)Package.MinLevel); //minLevel
            WriteByte((byte)Package.MinRenownLevel);

            bool hasPackage = Player.HasPackage(Package);
            WriteByte(0);
            if (hasPackage && Package.Category.CategoryIndex == WarShared.CareerCategoryType.MASTERY)
            {
                hasPackage = true;
            }
            WriteBool(hasPackage);
            byte costFlag = 0;

            if (Package.GoldCost > 0)
                costFlag |= 1;
            if (Package.RenownCost > 0)
                costFlag |= 2;

            WriteByte(costFlag); //cost flags (1=gold, 2=renown)
            Skip(2);
            WriteUInt16(0);
            WriteUInt16((ushort)Package.Unk410);
            WriteUInt16(0);

            Skip(6);

            if (Package.GoldCost > 0)
                WriteUInt32((uint)Package.GoldCost);
            if (Package.RenownCost > 0)
                WriteByte((byte)Package.RenownCost);

            WriteByte(1); //package count (client will loop this number times over next section)

            WriteUInt32((uint)Package.PackageNameID); //package nameID
            WriteByte(2); //packagetype 2 = ability
            WriteUInt32((uint)Package.AbilityID);
            WriteByte(0); //stat value, 0 for ability
            WriteByte(Package.UnkV30);

            WriteUInt16((ushort)Package.Ability.EffectID);
            WriteByte(1);
            WriteUInt16((ushort)(Package.Ability.RangeMax * 12));
            WriteByte((byte)Package.Ability.AP);
            WriteByte((byte)0);
            WriteByte((byte)Package.Ability.MoraleLevel);
            WriteUInt16((ushort)Package.Ability.MoraleCost);
            WriteByte(0);
            WriteByte((byte)Package.Ability.TacticType);
            WriteUInt16(0);
            WriteUInt16((ushort)Package.Ability.Castime);
            WritePascalString(Package.Ability.Name);


            if (Package.ReqIndex > 0)
            {
                WriteByte(1); //requirment count, client will loop
                WriteUInt16(Package.ReqIndex);
                WriteByte(Package.Points);
            }
            else
            {
                WriteByte(0);
            }
            WriteUInt32(0);
        }

        private void SerializeControlPackage(int level)
        {
            WriteByte((byte)Package.Category.CategoryIndex);
            WriteByte(1);
            WriteUInt16(Package.PackageIndex);
            WriteByte(0);
            WriteByte((byte)4); //minLevel
            WriteByte((byte)Package.MinRenownLevel);

            WriteByte(0);
            WriteByte((byte)level);

            byte costFlag = 0;

            if (Package.GoldCost > 0)
                costFlag |= 1;
            if (Package.RenownCost > 0)
                costFlag |= 2;

            WriteByte(costFlag); //cost flags (1=gold, 2=renown)

            Skip(14);

            if (Package.GoldCost > 0)
                WriteUInt32((uint)Package.GoldCost);
            if (Package.RenownCost > 0)
                WriteByte((byte)Package.RenownCost);


            WriteByte(1); //package count (client will loop this number times over next section)

            WriteUInt32((uint)Package.PackageNameID); //package nameID


            WriteByte(Package.PackageType); //package type (2=AbilityID, 7=BonusType as AbilityID, 3=BonusTypeBit as abilityID)
            WriteUInt32(0);
            WriteByte(Package.Value);


            WriteByte(Package.UnkV30);

            if (Package.ReqIndex > 0)
            {
                WriteByte(1); //requirment count, client will loop
                WriteUInt16(Package.ReqIndex);
                WriteByte(Package.Points);
            }
            else
            {
                WriteByte(0);
            }
            WriteUInt32(0);
        }

        private void SerializeItemPackage()
        {
            WriteByte((byte)Package.Category.CategoryIndex);
            WriteByte(1);
            WriteUInt16(Package.PackageIndex);
            WriteByte(0);
            WriteByte(0); //minLevel
            WriteByte(0);

            WriteByte(0);
            WriteBool(true);
            WriteByte(0); //cost flags (1=gold, 2=renown)
            Skip(14);
            //WriteByte(0);

            WriteByte(1); //package count (client will loop this number times over next section)

            WriteUInt32((uint)Package.PackageNameID); //package nameID
            WriteByte(1); //packagetype 2 = ability

            WriteUInt32((uint)Package.ItemID);
            WriteByte(0); //stat value, 0 for ability
            WriteByte(Package.UnkV30);

            F_GET_ITEM.BuildStoreItem(this, Package.Item);

            if (Package.ReqIndex > 0)
            {
                WriteByte(1); //requirment count, client will loop
                WriteUInt16(Package.ReqIndex);
                WriteByte(Package.Points);
            }
            else
            {
                WriteByte(0);
            }
            WriteUInt32(0);
        }

        protected override void SerializeInternal()
        {
            if (Package.PackageType == 1)
                SerializeItemPackage();
            else if (Package.PackageType == 2)
                SerializeAbilityPackage();
            else if (Package.PackageType == 3 || Package.PackageType == 7)
                SerializeStatPackage();
            else if (Package.PackageType == 6)
            {
                ushort level = 0;
                if (Package.PackageIndex == 1)
                    level = (ushort)Player.MasteryA;
                if (Package.PackageIndex == 2)
                    level = (ushort)Player.MasteryB;
                if (Package.PackageIndex == 3)
                    level = (ushort)Player.MasteryC;

                SerializeControlPackage(level);
            }
            else
                SerializeControlPackage(Package.MinLevel);
        }
    }
}
