using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_DO_ABILITY, FrameType.Game)]
    public class F_DO_ABILITY : Frame
    {
        public ushort CasterData;
        public ushort Direction;
        public ushort CasterX;
        public ushort CasterY;
        public byte Unk4;
        public byte CasterZoneID;
        public ushort CasterZ;
        public bool FriendlyTargetVisible;
        public bool EnemyTargetVisible;
        public bool Moving;
        public ushort AbilityID;
        public byte Sequence;
        public uint Time;

        public F_DO_ABILITY() : base((int)GameOp.F_DO_ABILITY)
        {
        }
		
        protected override void DeserializeInternal()
        {
            CasterData = ReadUInt16();
            Direction = ReadUInt16();
            CasterX = ReadUInt16();
            CasterY = ReadUInt16();
            Unk4 = ReadByte();
            CasterZoneID = ReadByte();
            CasterZ = ReadUInt16();
            FriendlyTargetVisible = (CasterData >> 11 & 1) == 1;
            EnemyTargetVisible = (CasterData >> 15 & 1) == 1;
            Moving = (CasterData & 1) == 1;
            AbilityID = ReadUInt16();
            Sequence = ReadByte();
            Time = ReadUInt32();
        }
    }
}
