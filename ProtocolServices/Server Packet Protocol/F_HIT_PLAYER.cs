using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_HIT_PLAYER, FrameType.Game)]
    public class F_HIT_PLAYER : Frame
    {
        private ushort CasterID;
        private ushort TargetID;
        private uint Health;
        private byte HealthPercent;

        public F_HIT_PLAYER() : base((int)GameOp.F_HIT_PLAYER)
        {
        }

        public static F_HIT_PLAYER Create(ushort casterID, ushort targetID, uint health, byte healthPercent)
        {
            return new F_HIT_PLAYER()
            {
                CasterID = casterID,
                TargetID = targetID,
                Health = health,
                HealthPercent = healthPercent
            };
        }

		protected override void SerializeInternal()
        {
            WriteUInt16(CasterID);
            WriteUInt16(TargetID);
            WriteUInt32((uint)Health);
            WriteByte(HealthPercent);
            WriteByte(0);
            WriteByte(0);
            WriteByte(0);
        }
    }
}
