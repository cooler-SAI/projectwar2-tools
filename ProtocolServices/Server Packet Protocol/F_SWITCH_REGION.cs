using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_SWITCH_REGION, FrameType.Game)]
    public class F_SWITCH_REGION : Frame
    {
        private ZoneData Zone;

        public F_SWITCH_REGION() : base((int)GameOp.F_SWITCH_REGION)
        {
        }

        public static F_SWITCH_REGION Create(ZoneData zone)
        {
            return new F_SWITCH_REGION()
            {
                Zone = zone
            };
        }
		protected override void SerializeInternal()
        {
            WriteUInt16((ushort)Zone.ID);
            Fill(0, 5);
            WriteByte(1);
            WriteByte(1);
            Fill(0, 11);
        }
    }
}
