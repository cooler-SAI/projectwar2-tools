using System;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_SET_TIME, FrameType.Game)]
    public class F_SET_TIME : Frame
    {
        public uint TimeFraction;

        public F_SET_TIME() : base((int)GameOp.F_SET_TIME)
        {
        }

        public static F_SET_TIME Create(uint timeFraction = 0)
        {
            return new F_SET_TIME()
            {
                TimeFraction = timeFraction
            };
        }

		protected override void SerializeInternal()
        {
            WriteUInt32((uint)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds);
            WriteUInt32(TimeFraction);
        }

    }
}
