using System;
using System.Threading.Tasks;
using WarServer.Game.Entities;
using WarShared;
using WarShared.Data;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CREATE_PLAYER, FrameType.Game)]
    public class F_CREATE_PLAYER : Frame
    {
        private PlayerData Player;
        private ushort ObjectID;
        private ushort PID;
        private ushort ZoneX;
        private ushort ZoneY;
        private float Heading;
        private byte FactionStatus;
        private byte WoundsPct;
        private F_CREATE_PLAYER() : base((int)GameOp.F_CREATE_PLAYER)
        {
        }

        public static async Task<F_CREATE_PLAYER> Create(Player player)
        {
            return new F_CREATE_PLAYER()
            {
                Player = player.Data,
                ObjectID = player.ObjectID,
                PID = (ushort)player.Client.PID,
                ZoneX = (ushort)player.ZoneX,
                ZoneY = (ushort)player.ZoneY,
                Heading = player.Heading,
                WoundsPct = (byte)(await player.GetWoundsPercent()),
                FactionStatus = player.GetFactionStatus(),
            };
        }

		protected override void SerializeInternal()
        {
            WriteUInt16((UInt16)PID);
            WriteUInt16((UInt16)ObjectID);


            WriteUInt16((UInt16)Player.ModelID);
            WriteUInt16((UInt16)Player.Career.ID);
            WriteUInt16((UInt16)Player.Z);
            WriteUInt16((UInt16)Player.Zone.ID);
            WriteUInt16((UInt16)(ZoneX));
            WriteUInt16((UInt16)(ZoneY));
            WriteUInt16((ushort)Util.ToHeading16(Player.Heading));


            WriteByte((byte)Player.Level); // Level
            WriteByte((byte)0); // Level

            WriteByte(0x2B);
            WriteByte((byte)FactionStatus); // Faction pve 6/8 pvp 72/68 (order/dest)
            WriteByte(0);
            WriteByte(0); // ?

            //  stream.Write(Player.Traits, 0, (int)character.Traits.Length);
            // WriteUInt32(0); //traits
            // WriteUInt32(0); //traits
            for (int t = 0; t < 15; t++)
            {
                if (t < Player.Traits.Count)
                {
                    WriteByte((byte)Player.Traits[t]);
                }
                else WriteByte(0);
            }
            Fill(0, 5);

            WriteByte((byte)Player.Career.RaceID);
            WriteByte(0); //sometimes 1
            WriteByte(0); // health/ap?
            WriteByte((byte)WoundsPct);//wounds percent
            Fill(0, 8);

            WritePascalString(Player.Name);
            WritePascalString(""); // suffix. title?
            WritePascalString(""); // guild name
            Fill(0, 4);
        }
    }
}
