using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PING_DATAGRAM, FrameType.Game)]
    public class F_PING_DATAGRAM : Frame
    {
        public F_PING_DATAGRAM() : base((int)GameOp.F_PING_DATAGRAM)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
