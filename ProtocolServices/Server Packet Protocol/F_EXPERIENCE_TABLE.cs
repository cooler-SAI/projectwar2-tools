using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_EXPERIENCE_TABLE, FrameType.Game)]
    public class F_EXPERIENCE_TABLE : Frame
    {
        public F_EXPERIENCE_TABLE() : base((int)GameOp.F_EXPERIENCE_TABLE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
