using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_REQUEST_INIT_OBJECT, FrameType.Game)]
    public class F_REQUEST_INIT_OBJECT : Frame
    {
        public ushort ObjectID { get; set; }

        public F_REQUEST_INIT_OBJECT() : base((int)GameOp.F_REQUEST_INIT_OBJECT)
        {
        }
		
        protected override void DeserializeInternal()
        {
            ObjectID = ReadUInt16();
        }
    }
}
