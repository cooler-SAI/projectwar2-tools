using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_FIRE_SIEGE_WEAPON, FrameType.Game)]
    public class F_FIRE_SIEGE_WEAPON : Frame
    {
        public F_FIRE_SIEGE_WEAPON() : base((int)GameOp.F_FIRE_SIEGE_WEAPON)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
