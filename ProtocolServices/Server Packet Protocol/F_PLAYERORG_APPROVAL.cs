using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PLAYERORG_APPROVAL, FrameType.Game)]
    public class F_PLAYERORG_APPROVAL : Frame
    {
        public F_PLAYERORG_APPROVAL() : base((int)GameOp.F_PLAYERORG_APPROVAL)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
