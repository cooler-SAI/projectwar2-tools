using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CHECK_NAME, FrameType.Game)]
    public class F_CHECK_NAME : Frame
    {
       private string Charname;
       private string Username;
       private byte Bad;
       private byte Unk1;
       private byte Unk2;
       private byte Unk3;

        private F_CHECK_NAME() : base((int)GameOp.F_CHECK_NAME)
        {
        }

        public static F_CHECK_NAME Create(string charname, string username, byte bad)
        {
            return new F_CHECK_NAME()
            {
                Charname = charname,
                Username = username,
                Bad = bad,
                Unk1 = 0,
                Unk2 = 0,
                Unk3 = 0
            };
        }

		protected override void SerializeInternal()
        {
            FillString(Charname, 30);
            FillString(Username.ToLower(), 20);
            WriteByte(Bad);
            WriteByte(Unk1);
            WriteByte(Unk2);
            WriteByte(Unk3);
        }
    }
}
