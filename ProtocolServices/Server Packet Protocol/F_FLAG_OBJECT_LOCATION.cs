using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_FLAG_OBJECT_LOCATION, FrameType.Game)]
    public class F_FLAG_OBJECT_LOCATION : Frame
    {
        public F_FLAG_OBJECT_LOCATION() : base((int)GameOp.F_FLAG_OBJECT_LOCATION)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
