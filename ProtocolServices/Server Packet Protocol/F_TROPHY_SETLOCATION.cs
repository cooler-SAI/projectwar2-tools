using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_TROPHY_SETLOCATION, FrameType.Game)]
    public class F_TROPHY_SETLOCATION : Frame
    {
        public F_TROPHY_SETLOCATION() : base((int)GameOp.F_TROPHY_SETLOCATION)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
