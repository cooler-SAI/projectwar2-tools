using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_PQLOOT_TRIGGER, FrameType.Game)]
    public class F_PQLOOT_TRIGGER : Frame
    {
        public F_PQLOOT_TRIGGER() : base((int)GameOp.F_PQLOOT_TRIGGER)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
