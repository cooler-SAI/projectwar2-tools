using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.S_PLAYER_LOADED, FrameType.Game)]
    public class S_PLAYER_LOADED : Frame
    {
        public S_PLAYER_LOADED() : base((int)GameOp.S_PLAYER_LOADED)
        {
        }

        public static S_PLAYER_LOADED Create()
        {
            return new S_PLAYER_LOADED();
        }
		protected override void SerializeInternal()
        {
            WriteByte(0);
            WriteByte(0);
        }
    }
}
