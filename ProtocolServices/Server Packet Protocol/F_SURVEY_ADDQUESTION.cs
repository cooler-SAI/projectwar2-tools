using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_SURVEY_ADDQUESTION, FrameType.Game)]
    public class F_SURVEY_ADDQUESTION : Frame
    {
        public F_SURVEY_ADDQUESTION() : base((int)GameOp.F_SURVEY_ADDQUESTION)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
