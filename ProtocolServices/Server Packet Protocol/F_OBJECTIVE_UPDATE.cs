using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    public enum ObjectiveUpdateType
    {
        EndPqStage = 1001
    }

    [FrameRoute((int)GameOp.F_OBJECTIVE_UPDATE, FrameType.Game)]
    public class F_OBJECTIVE_UPDATE : Frame
    {
        public ObjectiveUpdateType Type;
        public ushort ZoneID { get; set; }
        public ushort PQID { get; set; }
        public ushort ObjectiveID { get; set; }

        public static F_OBJECTIVE_UPDATE EndStage(ushort pqID, ushort stageID)
        {
            var state = new F_OBJECTIVE_UPDATE();
            state.Type = ObjectiveUpdateType.EndPqStage;
            state.PQID = pqID;
            state.ObjectiveID = stageID;
            return state;
        }


        public F_OBJECTIVE_UPDATE() : base((int)GameOp.F_OBJECTIVE_UPDATE)
        {
        }
		
		protected override void SerializeInternal()
        {
            if (Type == ObjectiveUpdateType.EndPqStage)
            {
                WriteUInt16(0);
                WriteUInt16(PQID);
                WriteUInt32R(3);
                WriteUInt16(ObjectiveID);
                WriteByte(1);
                WriteUInt32(0);
            }
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
