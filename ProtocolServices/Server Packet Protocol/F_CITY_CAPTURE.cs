using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CITY_CAPTURE, FrameType.Game)]
    public class F_CITY_CAPTURE : Frame
    {
        public F_CITY_CAPTURE() : base((int)GameOp.F_CITY_CAPTURE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
