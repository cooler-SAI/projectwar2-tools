using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_MONSTER_STATS, FrameType.Game)]
    public class F_MONSTER_STATS : Frame
    {
        public F_MONSTER_STATS() : base((int)GameOp.F_MONSTER_STATS)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
