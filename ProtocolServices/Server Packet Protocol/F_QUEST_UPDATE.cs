using System;
using System.Collections.Generic;
using System.Text;
using WarServer.Game;
using WarServer.Net;
using WarServer.Services;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_QUEST_UPDATE, FrameType.Game)]
    public class F_QUEST_UPDATE : Frame
    {
        public F_QUEST_UPDATE() : base((int)GameOp.F_QUEST_UPDATE)
        {
        }
		
		protected override void SerializeInternal()
        {
        }

        protected override void DeserializeInternal()
        {
        }
    }
}
