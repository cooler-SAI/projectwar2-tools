using System.Collections.Generic;
using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_TACTICS, FrameType.Game)]
    public class F_TACTICS : Frame
    {
        public List<ushort> Tactics = new List<ushort>();

        public F_TACTICS() : base((int)GameOp.F_TACTICS)
        {
        }

        public static F_TACTICS Create(List<ushort> tactics)
        {
            return new F_TACTICS()
            {
                Tactics = tactics
            };
        }
		protected override void SerializeInternal()
        {
            WriteByte(0x03);
            WriteByte((byte)Tactics.Count);

            foreach (var ability in Tactics)
                WriteUInt16((ushort)ability);
        }

        protected override void DeserializeInternal()
        {
            var unk = ReadByte();
            var count = ReadByte();

            for (int i = 0; i < count; i++)
            {
                Tactics.Add(ReadUInt16());
            }
        }
    }
}
