using WarShared.Net;
using WarShared.Protocol;

namespace WarGameServer.Protocol
{
    [FrameRoute((int)GameOp.F_CAREER_PACKAGE_UPDATE, FrameType.Game)]
    public class F_CAREER_PACKAGE_UPDATE : Frame
    {
        private byte CategoryID;
        private byte UpdateType;
        private byte Param0;
        private byte Param1;
        private ushort Param2;
        private ushort Param3;
        private uint Param4;
        private uint Param5;
        private uint Param6;

        public F_CAREER_PACKAGE_UPDATE() : base((int)GameOp.F_CAREER_PACKAGE_UPDATE)
        {
        }

        public static F_CAREER_PACKAGE_UPDATE CreatePackageUpdate(byte categoryID, byte updateType, byte param0, byte param1, ushort param2, ushort param3, uint param4, uint param5, uint param6)
        {
            return new F_CAREER_PACKAGE_UPDATE
            {
                CategoryID = categoryID,
                UpdateType = updateType,
                Param0 = param0,
                Param1 = param1,
                Param2 = param2,
                Param3 = param3,
                Param4 = param4,
                Param5 = param5,
                Param6 = param6
            };
        }

        protected override void SerializeInternal()
        {
            WriteByte(CategoryID);
            WriteByte(UpdateType);
            WriteByte(Param0);
            WriteByte(Param1);
            WriteUInt16R(Param2);
            WriteUInt16(Param3);
            WriteUInt32R(Param4);
            WriteUInt32(Param5);
            WriteUInt32(Param6);
        }
    }
}
