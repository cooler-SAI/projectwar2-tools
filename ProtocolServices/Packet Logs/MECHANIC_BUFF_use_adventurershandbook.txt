
[Client] packet : (0x0B) F_PING  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 0C B6 00 36 00 00 00 0B 00 10 3B 57 F7 B7 |.....6......;W..|
|6F 7C BB 7F EE 14 00 00 00 00 00 00 00 00 02 E8 |o|..............|                                                
-------------------------------------------------------------------
[Server] packet : (0x81) S_PONG  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 81 00 10 3B 57 52 84 AF B2 00 00 00 00 00 |.....;WR........|
|00 0C B7 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0x1B) F_REQ_CAMPAIGN_STATUS  Size = 15 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 1B 00 14 58 00 00 12 C1 5D 00 0D 7B 00    |............... |
-------------------------------------------------------------------
[Client] packet : (0xD9) F_USE_ITEM  Size = 20 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 08 0C BB 00 36 FF FF 00 D9 00 2D 00 00 A8 01 |.....6.....-....|
|00 00 00 00                                     |....            |
-------------------------------------------------------------------
[Server] packet : (0x7E) F_SET_ABILITY_TIMER  Size = 15 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 7E 00 01 01 41 00 00 07 D0 3B 42 00 00    |............... |
-------------------------------------------------------------------
[Server] packet : (0xDA) F_USE_ABILITY  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 DA 00 00 3B 42 20 C9 01 FB 20 C9 01 01 00 |.....;B ... ....|
|00 07 D0 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0x1B) F_REQ_CAMPAIGN_STATUS  Size = 15 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 1B 00 05 25 00 00 12 D5 97 00 0D 7D 00    |............... |
-------------------------------------------------------------------
[Server] packet : (0xD7) F_INIT_EFFECTS  Size = 21 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 12 D7 01 01 9E FF 20 C9 0D 00 42 3B C0 70 C9 |....... ...B;.p.|
|20 01 00 14 00                                  |.....           |
-------------------------------------------------------------------
[Server] packet : (0xDA) F_USE_ABILITY  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 DA 00 00 3B 42 20 C9 01 FB 20 C9 02 01 00 |.....;B ... ....|
|00 00 28 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0xB9) UNKNOWN  Size = 16 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0D B9 01 00 03 24 CD 00 00 00 00 00 01 51 00 |......$.......Q.|                                                
-------------------------------------------------------------------
[Server] packet : (0xAA) F_GET_ITEM  Size = 163 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 A0 AA 01 00 00 00 00 2D 00 00 03 24 CD 1B 7C |........-...$..||
|00 00 00 00 00 00 00 00 00 00 01 01 00 00 00 04 |................|
|00 00 00 00 00 00 00 00 00 00 00 00 00 11 00 01 |................|
|00 01 00 00 00 00 00 00 00 00 00 00 00 00 15 41 |...............A|
|64 76 65 6E 74 75 72 65 72 27 73 20 48 61 6E 64 |dventurer's Hand|
|62 6F 6F 6B 00 00 01 00 03 3B 42 51 80 51 80 00 |book.....;BQ.Q..|
|00 00 24 52 69 67 68 74 2D 63 6C 69 63 6B 20 6F |..$Right-click o|
|6E 20 69 74 65 6D 20 74 6F 20 61 63 74 69 76 61 |n item to activa|
|74 65 20 62 75 66 66 01 00 00 01 03 06 20 08 01 |te buff...... ..|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00                                        |...             |
-------------------------------------------------------------------
[Server] packet : (0xAA) F_GET_ITEM  Size = 162 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 9F AA 01 00 00 00 00 2D 00 00 03 24 CD 1B 7C |........-...$..||
|00 00 00 00 00 00 00 00 00 00 01 01 00 00 00 04 |................|
|00 00 00 00 00 00 00 00 00 00 00 00 00 11 00 01 |................|
|00 01 00 00 00 00 00 00 00 00 00 00 00 00 15 41 |...............A|
|64 76 65 6E 74 75 72 65 72 27 73 20 48 61 6E 64 |dventurer's Hand|
|62 6F 6F 6B 00 00 01 00 03 3B 42 51 80 51 80 00 |book.....;BQ.Q..|
|00 00 24 52 69 67 68 74 2D 63 6C 69 63 6B 20 6F |..$Right-click o|
|6E 20 69 74 65 6D 20 74 6F 20 61 63 74 69 76 61 |n item to activa|
|74 65 20 62 75 66 66 01 00 00 01 03 06 20 08 01 |te buff...... ..|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00                                           |..              |
-------------------------------------------------------------------
[Server] packet : (0x7E) F_SET_ABILITY_TIMER  Size = 15 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 7E 00 01 01 00 00 00 00 00 00 00 00 00    |............... |
-------------------------------------------------------------------
[Client] packet : (0x01) UNKNOWN  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 0C C3 00 36 00 00 00 01 00 10 47 E8 00 00 |.....6......G...|
|00 00 00 00 00 00 00 00 46 49 00 00 22 24 AE 59 |........FI.."$.Y|                                                
-------------------------------------------------------------------
[Client] packet : (0x0B) F_PING  Size = 32 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 0C C5 00 36 00 00 00 0B 00 10 4E 64 4E 48 |.....6......NdNH|
|C4 4C 53 05 2D 91 00 00 00 00 00 00 00 00 02 E8 |.LS.-...........|                                                
-------------------------------------------------------------------
[Server] packet : (0x00) UNKNOWN  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 00 00 10 47 E8 B7 CE 48 B4 00 00 00 00 00 |.....G...H......|
|00 46 49 00 00 22 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0x81) S_PONG  Size = 23 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 14 81 00 10 4E 64 52 84 AF B7 00 00 00 00 00 |.....NdR........|
|00 0C C6 00 00 00 00                            |.......         |
-------------------------------------------------------------------
[Server] packet : (0x1A) F_CAMPAIGN_STATUS  Size = 159 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 9C 1A 00 05 00 69 00 CD 00 00 32 32 00 32 32 |......i....22.22|
|00 32 32 00 32 32 00 22 00 00 32 32 00 32 32 00 |.22.22."..22.22.|
|32 32 00 00 00 00 32 32 00 32 32 00 32 32 00 00 |22....22.22.22..|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 00 00 00 00 64 63 00 00 32 00 00 32 00 |.......dc..2..2.|
|32 00 00 32 64 00 00 00 00 00 00 00 00 00 00 00 |2..2d...........|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 |................|
|00 03 01 00 02 03 03 01 00 02 03 03 01 00 02 03 |................|
|FE 02 3C 28 0A 3C 00 00 00 03 84 00 00 00 00    |............... |
-------------------------------------------------------------------
[Server] packet : (0x1B) F_REQ_CAMPAIGN_STATUS  Size = 15 
|------------------------------------------------|----------------|
|00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F |0123456789ABCDEF|
|------------------------------------------------|----------------|
|00 0C 1B 00 14 58 00 00 12 BA A6 00 0D 78 00    |............... |
-------------------------------------------------------------------