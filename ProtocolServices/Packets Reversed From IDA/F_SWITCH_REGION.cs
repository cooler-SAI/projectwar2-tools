﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_SWITCH_REGION:MythicPacket
    {
        public ushort A00_RegionID { get; set; }
        public ushort Unused1 { get; set; } 
        public uint Unk1 { get; set; } //+4
        public uint Unused2 { get; set; } //+8
        public uint Unk2 { get; set; } // + 12
        public uint Unk3 { get; set; } // + 16

        public F_SWITCH_REGION(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_SWITCH_REGION, ms, true)
        {
        }
    }
}
