﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_INTERACT_RESPONSE : MythicPacket
    {
        public byte Type { get; set; }
        public ushort ObjectID { get; set; }
        public byte Unk1 { get; set; }
        public byte Unk2 { get; set; }
        public byte Unk3 { get; set; }
        public byte Unk4 { get; set; }
        public byte Unk5 { get; set; }
        public string Message { get; set; }
        public List<F_GET_ITEM.ItemData> Items { get; set; }
        public F_INTERACT_RESPONSE(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_INTERACT_RESPONSE, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            byte[] data = ms.ToArray();

            Type = FrameUtil.GetUint8(ms);
            if (Type == 0)
            {
                ObjectID = FrameUtil.GetUint16(ms);
                Unk1 = FrameUtil.GetUint8(ms);
                Unk2 = FrameUtil.GetUint8(ms);
                Unk3 = FrameUtil.GetUint8(ms);
                Unk4 = FrameUtil.GetUint8(ms);
                Unk5 = FrameUtil.GetUint8(ms);

                Message = FrameUtil.GetPascalString(ms);
            }
            else if (Type == 4)
            {
                Items = new List<F_GET_ITEM.ItemData>();
                byte count = FrameUtil.GetUint8(ms);
                for (int i = 0; i < count; i++)
                {
                    byte index = FrameUtil.GetUint8(ms);
                    F_GET_ITEM.ItemData t = new F_GET_ITEM.ItemData();
                    t.DeSerialize(ms, true);
                    Items.Add(t);
                }
            }
            else if (Type == 7 || Type == 8)
            {
                Items = new List<F_GET_ITEM.ItemData>();
                var a = FrameUtil.GetUint8(ms);
                var b = FrameUtil.GetUint8(ms);
                var c = FrameUtil.GetUint8(ms);
                var d = FrameUtil.GetUint8(ms);
                F_GET_ITEM.ItemData t = new F_GET_ITEM.ItemData();
                t.DeSerialize(ms, true);
                Items.Add(t);
            }

        }
    }
 
}
