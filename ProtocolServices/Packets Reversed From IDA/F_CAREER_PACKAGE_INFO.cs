﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{
    public class F_CAREER_PACKAGE_INFO: MythicPacket
    {
        public string AType { get; set; }
        public byte A00_CategoryID { get; set; }
        public byte A01_v3 { get; set; }
        public ushort A02_PackageIndex { get; set; }
        public byte A02_dword14 { get; set; }
        public byte A03_CostFlags { get; set; }
        public byte A04_Trained { get; set; }
        public byte A05_byte40A { get; set; }
        public byte A06_MinRenownLevel { get; set; }
        public byte A07_MinLevel { get; set; }
        public ushort A08_byte40c { get; set; }
        public ushort A09_byte410 { get; set; }
        public ushort A10_byte414 { get; set; }

        public ushort A21_AbilityEffectID { get; set; }
        public byte A22_Abilitybytec5 { get; set; }
        public ushort A23_AbilityRange { get; set; }
        public byte A24_Ability_AP { get; set; }
        public TargetType A25_Ability_TargetType { get; set; }
        public byte A26_Ability_MoraleLevel { get; set; }
        public ushort A27_Ability_MoraleCost { get; set; }
        public byte A28_Ability_wordc2 { get; set; }
        public TacticType A29_Ability_TacticType { get; set; }
        public ushort A30_Ability_word36 { get; set; }
        public ushort A31_Ability_CastTime { get; set; }
        public string A32_Ability_Name { get; set; }
        public uint A10_GoldCost { get; set; }
        public byte A10_RenownCost { get; set; }

        public uint A13_packageID { get; set; }
        public string A13_PackageInfo { get; set; }
        public byte A14_PackageType { get; set; }
        public uint A15_AbilityID { get; set; }
        public byte A16_StatValue { get; set; }
        public byte A17_v30 { get; set; }

        public byte A34_d1 { get; set; }
        public byte A35_MasteryIndex_ReqPackageIndex { get; set; }
        public byte A36_PointsSpent { get; set; }

        public F_GET_ITEM.ItemData A19_Item { get; set; }

        public F_CAREER_PACKAGE_INFO(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_CAREER_PACKAGE_INFO, ms, false)
        {
        }

        public override void Load(MemoryStream stream)
        {
            stream.Position = 3;
            var data = stream.ToArray().ToList().Skip(3).ToArray();
            A00_CategoryID = FrameUtil.GetUint8(stream); //data[0];
            A01_v3 = FrameUtil.GetUint8(stream); //data[1];

            // var unk1 = FrameUtil.GetUint8(stream);

            if (A00_CategoryID < 20)
            {
                A02_PackageIndex = FrameUtil.GetUint16(stream); //data[2,3];
                A02_dword14 = FrameUtil.GetUint8(stream); //data[4]; //+4
                A07_MinLevel = FrameUtil.GetUint8(stream); //data[5];
                A06_MinRenownLevel = FrameUtil.GetUint8(stream); //data[6];
                A05_byte40A = FrameUtil.GetUint8(stream); //data[7];
                A04_Trained = FrameUtil.GetUint8(stream); //data[8];
                A03_CostFlags = FrameUtil.GetUint8(stream); //data[9];


                stream.Position = 15;
                A08_byte40c = FrameUtil.GetUint16(stream);
                A09_byte410 = FrameUtil.GetUint16(stream);
                A10_byte414 = FrameUtil.GetUint16(stream);

                stream.Position = 27;

                if ((A03_CostFlags & 1) > 0)
                {

                    A10_GoldCost = FrameUtil.GetUint32(stream);
                }
                if (((A03_CostFlags >> 1) & 1) > 0)
                {
                    A10_RenownCost = FrameUtil.GetUint8(stream);
                }


                var v42 = FrameUtil.GetUint8(stream);
                for (int i = 0; i < v42; i++)
                {

                    A13_packageID = FrameUtil.GetUint32(stream);
                   // A13_PackageInfo = frmServerTools.MYP.GetString(MypLib.Language.english, "packageinfo.txt", (int)A13_packageID);
                    A14_PackageType = FrameUtil.GetUint8(stream);
                    A15_AbilityID = FrameUtil.GetUint32(stream);
                    A16_StatValue = FrameUtil.GetUint8(stream);
                    A17_v30 = FrameUtil.GetUint8(stream);
                    if (A17_v30 == 0)
                        A17_v30 = 1;

                    if (A14_PackageType == 1)
                    {
                        A19_Item = new F_GET_ITEM.ItemData();
                        A19_Item.DeSerialize(stream, true);
                        AType = "Item";

                        //read inventory
                    }
                    else if (A14_PackageType == 2)
                    {
                        AType = "AbilityNames";
                        //ability names
                        A21_AbilityEffectID = FrameUtil.GetUint16(stream);
                        A22_Abilitybytec5 = FrameUtil.GetUint8(stream);
                        A23_AbilityRange = FrameUtil.GetUint16(stream);
                        A24_Ability_AP = FrameUtil.GetUint8(stream);
                        A25_Ability_TargetType = (TargetType)FrameUtil.GetUint8(stream);
                        A26_Ability_MoraleLevel = FrameUtil.GetUint8(stream);
                        A27_Ability_MoraleCost = FrameUtil.GetUint16(stream);
                        A28_Ability_wordc2 = FrameUtil.GetUint8(stream);
                        A29_Ability_TacticType = (TacticType)FrameUtil.GetUint8(stream);
                        A30_Ability_word36 = FrameUtil.GetUint16(stream);
                        A31_Ability_CastTime = FrameUtil.GetUint16(stream);
                        A32_Ability_Name = FrameUtil.GetPascalString(stream);
                    }
                    else
                    {
                        AType = "Package Names";
                        //package names
                    }

                }
                var v18 = FrameUtil.GetUint8(stream);
                for (int i = 0; i < v18; i++)
                {
                    A34_d1 = FrameUtil.GetUint8(stream);
                    A35_MasteryIndex_ReqPackageIndex = FrameUtil.GetUint8(stream);
                    A36_PointsSpent = FrameUtil.GetUint8(stream);
                }


            }
        }
    }
}
