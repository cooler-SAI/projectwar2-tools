﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarCommon;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{
    public class F_PLAYER_INVENTORY:MythicPacket 
    {
        public ushort ObjectID { get; set; }
        public byte Count { get; set; }
        public byte Stance { get; set; }
        public List<ItemData> Items { get; set; }

        public class ItemData
        {
            public int Flags { get; set; }
            public int Type { get; set; }
            public int HasAltApperance { get; set; }

            public InventorySlotType SlotIndex { get; set; }

            public int ModelID { get; set; }
            public int ModelIDAlternate { get; set; }

            public int Dye1 { get; set; }
            public int Dye2 { get; set; }
     
        
            public byte HasHeraldry { get; set; }
            public ushort HeraldryEmblemID { get; set; }
            public ushort HeraldryPatternID { get; set; }
            public byte HeraldryColorID1 { get; set; }
            public byte HeraldryColorID2 { get; set; }
            public byte HeraldryShapeIndex{ get; set; }
            public byte HeraldryUnk1 { get; set; }
            public byte HeraldryUnk2 { get; set; }

            public int TrophyIndex { get; set; }
            public int TrophyData { get; set; }

            public void Load(MemoryStream ms)
            {
                Flags = (int)FrameUtil.GetUint8(ms);
                Type = Flags & 0x1F;
                SlotIndex = (InventorySlotType )FrameUtil.GetUint8(ms);
                HasAltApperance = (Flags >> 5) & 1;

                if (Type == 0) //MODEL ONLY
                {
                    ModelID = FrameUtil.GetUint16(ms);
                    if (HasAltApperance == 1)
                        ModelIDAlternate = FrameUtil.GetUint16(ms);
                }
                else if (Type == 1) //MODEL AND DYE
                {
                    ModelID = FrameUtil.GetUint16(ms);
                    if (HasAltApperance == 1)
                        ModelIDAlternate = FrameUtil.GetUint16(ms);

                    if ((Flags & 0x80) == 0)
                        Dye1 = FrameUtil.GetUint8(ms);
                    else
                        Dye1 = FrameUtil.GetUint16(ms);


                    if ((Flags & 0x40) == 0)
                        Dye2 = FrameUtil.GetUint8(ms);
                    else
                        Dye2 = FrameUtil.GetUint16(ms);
                }
                else if (Type == 3) //TROPHY
                {
                    ModelID = FrameUtil.GetUint16(ms);
                    if (HasAltApperance != 0)
                        ModelIDAlternate = FrameUtil.GetUint16(ms);

                    TrophyIndex = FrameUtil.GetUint8(ms);
                    TrophyData = FrameUtil.GetUint8(ms);
                }
                else if (Type == 4) //HERALDRY
                {
                    ModelID = FrameUtil.GetUint16(ms);
                    if (HasAltApperance == 1)
                        ModelIDAlternate = FrameUtil.GetUint16(ms);

                    byte hasHeraldry = FrameUtil.GetUint8(ms);

                    if (hasHeraldry != 0)
                    {
                        HeraldryEmblemID = FrameUtil.GetUint16(ms);
                        HeraldryPatternID = FrameUtil.GetUint16(ms);
                        HeraldryColorID1 = FrameUtil.GetUint8(ms);
                        HeraldryColorID2 = FrameUtil.GetUint8(ms);
                        HeraldryShapeIndex = FrameUtil.GetUint8(ms);
                        HeraldryUnk1 = FrameUtil.GetUint8(ms);
                        HeraldryUnk2 = FrameUtil.GetUint8(ms);
                    }
                }
            }

        }

        public F_PLAYER_INVENTORY(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_PLAYER_INVENTORY, ms, true)
        {
        }

        public F_PLAYER_INVENTORY()
            : base()
        {
        }


        public override void AutoLoad(MemoryStream ms)
        {
            byte[] data = ms.ToArray();
            ObjectID = FrameUtil.GetUint16(ms);
            Stance = FrameUtil.GetUint8(ms);
            Count = FrameUtil.GetUint8(ms);
            Items = new List<ItemData>();

            for (int i = 0; i < Count && ms.Position < ms.Length; i++)
            {
                var item = new ItemData();
                item.Load(ms);
                Items.Add(item);
            }

            FrameUtil.GetUint8(ms);
        }
    }
}
