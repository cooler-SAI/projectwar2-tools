﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{

    public class ScenarioPlayer
    {

        public uint A07_CharacterID { get; set; }
        public byte A11_Realm { get; set; }
        public byte A12_Level { get; set; }
        public byte A13_Unk { get; set; }
        public byte A14_Unk { get; set; }
        public byte A14_Career { get; set; }
        [PascalString]
        public string A15_Name { get; set; }

        public void Load(MemoryStream ms)
        {
            A07_CharacterID = FrameUtil.GetUint32(ms);
            A11_Realm = FrameUtil.GetUint8(ms);
            A12_Level = FrameUtil.GetUint8(ms);
            A13_Unk = FrameUtil.GetUint8(ms);
            A14_Unk = FrameUtil.GetUint8(ms);
            A14_Career = FrameUtil.GetUint8(ms);
            A15_Name = FrameUtil.GetPascalString(ms);
        }
    }
    public class ScenarioPlayers
    {
        public byte A04_Unk { get; set; }
        public ushort A05_PlayerCount { get; set; }
        public List<ScenarioPlayer> Players { get; set; }

        public void Load(MemoryStream ms)
        {

            A04_Unk = FrameUtil.GetUint8(ms);
            A05_PlayerCount = FrameUtil.GetUint16(ms);
            Players = new List<ScenarioPlayer>();
            for (int i = 0; i < A05_PlayerCount; i++)
            {
                ScenarioPlayer player = new ScenarioPlayer();
                player.Load(ms);
                Players.Add(player);
            }

        }
    }

    public class ReserveSlots
    {
        public byte Unk1 { get; set; }
        public byte Unk2 { get; set; }
        public byte TotalReservedSlots { get; set; }
        public Dictionary<int, List<int>> Reserved { get; set; } //partyIndex->SlotIndex
        public string StringData
        {
            get
            {
                string data = "";
                foreach (var groupIndex in Reserved.Keys)
                {
                    data += "Group [" + groupIndex + " ]:";
                    foreach (var slotIndex in Reserved[groupIndex])
                        data += " " + slotIndex;
                    data += " ";
                }
                return data;
            }
        }
        public void Load(MemoryStream ms)
        {
             Unk1 = FrameUtil.GetUint8(ms);
             Unk2 = FrameUtil.GetUint8(ms);
             TotalReservedSlots = FrameUtil.GetUint8(ms);
             Reserved = new Dictionary<int, List<int>>();
             for (int i = 0; i < TotalReservedSlots; i++)
             {
                 byte groupIndex = FrameUtil.GetUint8(ms);
                 if (!Reserved.ContainsKey(groupIndex))
                     Reserved[groupIndex] = new List<int>();

                 Reserved[groupIndex].Add(FrameUtil.GetUint8(ms));
             }
        }
    }
    public class ScenarioPlayerPartyInfo
    {

        public uint CharacterID { get; set; }
        public byte PartyIndex { get; set; }
        public byte SlotIndex { get; set; }
        public byte Health { get; set; }
        public byte AP { get; set; }
        public byte Morale { get; set; }
        public void Load(MemoryStream ms)
        {
            CharacterID = FrameUtil.GetUint32(ms);
            PartyIndex = FrameUtil.GetUint8(ms);
            SlotIndex = FrameUtil.GetUint8(ms);
            Health = FrameUtil.GetUint8(ms);
            AP = FrameUtil.GetUint8(ms);
            Morale = FrameUtil.GetUint8(ms);
        }
    }
    public class UpdateScenarioParties
    {
        public byte Unk1 { get; set; }
        public byte Unk2 { get; set; }
        public byte PlayerCount { get; set; }
        public List<ScenarioPlayerPartyInfo> Players { get; set; }

        public void Load(MemoryStream ms)
        {
            Unk1 = FrameUtil.GetUint8(ms);
            Unk2 = FrameUtil.GetUint8(ms);
            PlayerCount = FrameUtil.GetUint8(ms);

            Players = new List<ScenarioPlayerPartyInfo>();
            for (int i = 0; i < PlayerCount; i++)
            {
                ScenarioPlayerPartyInfo info = new ScenarioPlayerPartyInfo();
                info.Load(ms);
                Players.Add(info);
            }
        }
    }

    public class ScenarioScoreboard
    {
        public byte A04_Unk { get; set; }
        public byte A05_Unk { get; set; }
        public byte A06_Count { get; set; }
        public List<ScenarioPlayerScore> Scores { get; set; }
        public void Load(MemoryStream ms)
        {
            A04_Unk = FrameUtil.GetUint8(ms);
            A05_Unk = FrameUtil.GetUint8(ms);
            A06_Count = FrameUtil.GetUint8(ms);
            Scores = new List<ScenarioPlayerScore>();
            for (int i = 0; i < A06_Count; i++)
            {
                ScenarioPlayerScore player = new ScenarioPlayerScore();
                player.Load(ms);
                Scores.Add(player);
            }
        }
    }
    public class ScenarioPlayerScore
    {
        public uint CharacterID { get; set; }
        public uint SoloKills { get; set; }
        public uint Kills { get; set; }
        public uint DeathBlows { get; set; }
        public uint Deaths { get; set; }
        public uint Damage { get; set; }
        public uint Healing { get; set; }
        public uint Renown { get; set; }
        public uint Unk1 { get; set; }
        public uint XP { get; set; }
        public uint Unk2 { get; set; }


        public void Load(MemoryStream ms)
        {

            CharacterID = FrameUtil.GetUint32(ms);
            SoloKills = FrameUtil.GetUint32(ms);
            Kills = FrameUtil.GetUint32(ms);
            DeathBlows = FrameUtil.GetUint32(ms);
            Deaths = FrameUtil.GetUint32(ms);
            Damage = FrameUtil.GetUint32(ms);
            Healing = FrameUtil.GetUint32(ms);
            Renown = FrameUtil.GetUint32(ms);
            Unk1 = FrameUtil.GetUint32(ms);
            XP = FrameUtil.GetUint32(ms);
            Unk2 = FrameUtil.GetUint32(ms);
        }
    }

    public enum InfoType
    {
        UNK = 255,
        PLAYERS = 0,
        SCOREBOARD = 1,
        UPDATE_PARTIES = 3,
        RESERVE = 4
    }
    public class F_SCENARIO_PLAYER_INFO : MythicPacket
    {

      
        public byte A03_Type { get; set; }
        public InfoType InfoType { get; set; }
        public List<object> Data { get; set; }

        

        public ushort UnRead { get; set; }

       

        public F_SCENARIO_PLAYER_INFO()
        {
        }
        public F_SCENARIO_PLAYER_INFO(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_SCENARIO_PLAYER_INFO, ms, false)
        {
        }

       
        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            A03_Type = FrameUtil.GetUint8(ms);
            InfoType = Packets.InfoType.UNK;

            Data = new List<object>();
            if (A03_Type == 0)
            {
                InfoType = Packets.InfoType.PLAYERS;
                ScenarioPlayers T0 = new ScenarioPlayers();
                T0.Load(ms);
                Data.Add(T0);
            }
            else if (A03_Type == 1)
            {
                InfoType = Packets.InfoType.SCOREBOARD;
                ScenarioScoreboard board = new ScenarioScoreboard();
                board.Load(ms);
                Data.Add(board);
            }
            else if (A03_Type == 3)
            {
                InfoType = Packets.InfoType.UPDATE_PARTIES;
                UpdateScenarioParties parties = new UpdateScenarioParties();
                parties.Load(ms);
                Data.Add(parties);
            }
            else if (A03_Type == 4)
            {
                InfoType = Packets.InfoType.RESERVE;
                ReserveSlots slots = new ReserveSlots();
                slots.Load(ms);
                Data.Add(slots);
            }

            UnRead = (ushort)(ms.Length - ms.Position);
        }
    }
}
