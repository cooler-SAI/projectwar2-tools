﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;


namespace WarAdmin.Packets
{
    public class F_INIT_STORE : MythicPacket
    {
        public byte A03_Unk { get; set; }
        public byte A04_Unk { get; set; }
        public byte A05_Page { get; set; }
        public byte A06_ItemCount { get; set; }
        public byte A07_MorePages { get; set; }
        public byte A08_Unk { get; set; }
        public byte A09_Unk { get; set; }
        public byte A10_FirstPage{ get; set; }
        public byte A11_PageCount{ get; set; }
        public List<ItemInfo> Items { get; set; }

        public class ItemRequirmentAlt
        {
            public uint ItemID { get; set; }
            public ushort ModelID { get; set; }
            public string Name { get; set; }
            public ushort Quantity { get; set; }
            public string Name2 { get; set; }
            public void DeSerialize(MemoryStream ms)
            {

                ItemID = FrameUtil.GetUint32(ms);
                ModelID = FrameUtil.GetUint16(ms);
                Name = FrameUtil.GetPascalString(ms);
                Quantity = FrameUtil.GetUint16(ms);
            }
        }
        public class ItemRequirment
        {
            public List<ItemRequirmentAlt> Alternatives { get; set; }
            public void DeSerialize(MemoryStream ms)
            {
                Alternatives = new List<ItemRequirmentAlt>();
                for (int i = 0; i < 3; i++)
                {
                    var alt = new ItemRequirmentAlt();
                    alt.DeSerialize(ms);
                    if(alt.ItemID != 0)
                        Alternatives.Add(alt);
                }
            }
        }
        public class ItemInfo
        {
            public byte A12_Unk { get; set; }
            public byte A13_Unk { get; set; }
            public uint A14_Price { get; set; }
            public WarAdmin.Packets.F_GET_ITEM.ItemData Item { get; set; }
            public byte RequiredItemCount { get; set; }
            public object Tag { get; set; }
            public List<ItemRequirment> RequiredItems { get; set; }
            public override string ToString()
            {
                return Item.Name;
            }
            public void DeSerialize(MemoryStream ms)
            {
                RequiredItems = new List<ItemRequirment>();

                A12_Unk = FrameUtil.GetUint8(ms);
                A13_Unk = FrameUtil.GetUint8(ms);
                A14_Price = FrameUtil.GetUint32(ms);
                Item = new F_GET_ITEM.ItemData();
                Item.DeSerialize(ms,true);
                RequiredItemCount = FrameUtil.GetUint8(ms);
                for (int i = 0; i < RequiredItemCount; i++)
                {
                    var req = new ItemRequirment();
                    req.DeSerialize(ms);
                    RequiredItems.Add(req);
                }
            }
        }
      

        public F_INIT_STORE(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_INIT_STORE, ms, true)
        {
        }

        public override void Load(MemoryStream ms)
        {
            Items = new List<ItemInfo>();
            ms.Position = 3;

            A03_Unk = FrameUtil.GetUint8(ms);
            A04_Unk = FrameUtil.GetUint8(ms);
            A05_Page = FrameUtil.GetUint8(ms);
            A06_ItemCount = FrameUtil.GetUint8(ms);
            A07_MorePages = FrameUtil.GetUint8(ms);
            A08_Unk = FrameUtil.GetUint8(ms);
            A09_Unk = FrameUtil.GetUint8(ms);
            if (A07_MorePages == 1)
                A10_FirstPage = FrameUtil.GetUint8(ms);

            for (int i = 0; i < A06_ItemCount; i++)
            {
                ItemInfo info = new ItemInfo();
                info.DeSerialize(ms);
                Items.Add(info);
            }
        }
    }
}
