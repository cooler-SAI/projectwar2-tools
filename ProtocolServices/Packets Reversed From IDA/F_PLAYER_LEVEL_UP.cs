﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{
    public class F_PLAYER_LEVEL_UP : MythicPacket
    {
        public uint Unk0 { get; set; }
        public uint Count { get; set; }
        public List<BonusTypeIncrease> BonusChanges { get; set; }
        public class BonusTypeIncrease
        {
            public BonusType BonusType { get; set; }
            public short Value { get; set; }
            public void Load(MemoryStream ms)
            {

                BonusType = (BonusType)FrameUtil.GetUint8(ms);
                Value = (short)FrameUtil.GetUint16(ms);
            }
        }


        public F_PLAYER_LEVEL_UP(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_PLAYER_LEVEL_UP, ms, false)
        {
            ms.Position = 3;
            Unk0 = FrameUtil.GetUint32(ms);
            Count = FrameUtil.GetUint8(ms);

            BonusChanges = new List<BonusTypeIncrease>();
            for (int i = 0; i < Count; i++)
            {
                var b = new BonusTypeIncrease();
                b.Load(ms);
                BonusChanges.Add(b);
            }



        }
    }
}
