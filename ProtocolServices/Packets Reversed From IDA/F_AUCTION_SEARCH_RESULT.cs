﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;


namespace WarAdmin.Packets
{
    public class F_AUCTION_SEARCH_RESULT : MythicPacket
    {

        public uint Unk1 { get; set; }
        public ushort Count { get; set; }
        public List<F_GET_ITEM.ItemData> Items { get; set; }
        public F_AUCTION_SEARCH_RESULT(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_AUCTION_SEARCH_RESULT, ms, true)
        {
        }
        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;
            FrameUtil.GetByteArray(ms, 5);
            Count = FrameUtil.GetUint8(ms);
            FrameUtil.GetUint8(ms);

            Items = new List<F_GET_ITEM.ItemData>();

            for (int i = 0; i < Count; i++)
            {
                FrameUtil.GetUint64(ms);
                FrameUtil.GetUint16(ms);
                FrameUtil.GetUint16(ms);
                FrameUtil.GetUint32(ms);
                FrameUtil.GetUint32(ms);
                FrameUtil.GetUint8(ms);
                var name = FrameUtil.GetString(ms, 48);
                FrameUtil.GetByteArray(ms, 3);
                var Item = new F_GET_ITEM.ItemData();
                Item.DeSerialize(ms, true);
                Items.Add(Item);
                if (Item.ItemID == 199956)
                    Item = Item;
            }
        }

    }
}
