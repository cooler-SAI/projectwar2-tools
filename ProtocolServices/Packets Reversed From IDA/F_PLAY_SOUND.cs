﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;



namespace WarAdmin.Packets
{
    public class F_PLAY_SOUND : MythicPacket
    {
        public byte A03_Unk { get; set; }
        public ushort A04_SoundID { get; set; }
        public string A04_SoundName { get; set; }
        public string A05_SoundSpeaker { get; set; }
        public string A06_SoundText { get; set; }

        public F_PLAY_SOUND(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_PLAY_SOUND, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            byte[] data = ms.ToArray();
            ms.Position = 3;
            A03_Unk = FrameUtil.GetUint8(ms);
            A04_SoundID = FrameUtil.GetUint16(ms);
            //if (frmServerTools.AudioServer.ContainsKey(A04_SoundID))
            //{
            //    A04_SoundName = frmServerTools.AudioServer[A04_SoundID].Sound;
            //    A05_SoundSpeaker = frmServerTools.AudioServer[A04_SoundID].Speaker;
            //    A06_SoundText = frmServerTools.AudioServer[A04_SoundID].Text;
            //}

        }
    }
}
