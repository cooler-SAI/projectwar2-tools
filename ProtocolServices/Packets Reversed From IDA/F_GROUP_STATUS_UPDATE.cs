﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarCommon;
using WarAdmin.Net;
using WarShared;

namespace WarAdmin.Packets
{
   
    public class F_GROUP_STATUS_UPDATE: MythicPacket
    {
        public int Flag1_Position { get; set; }
        public int Flag2_Zone { get; set; }
        public int Flag3_Vitals { get; set; }
        public int Flag4 { get; set; }
        public int Flag5 { get; set; }
        public int data8 { get; set; }
        public int CharacterID { get; set; }
        public int Position_X { get; set; }
        public int Position_Y { get; set; }
        public int Position_Z { get; set; }
        public double Position_Heading { get; set; }
        public int Bdata32 { get; set; }
        public int Bdata36 { get; set; }
        public int Zone_ID { get; set; }
        public int Bdata44 { get; set; }

        public int Vitals_WoundsPct { get; set; }
        public int Vitals_APPct { get; set; }
        public int Vitals_MoralePct { get; set; }

        public int Edata60 { get; set; }
        public int Edata62 { get; set; }
        public int Edata64 { get; set; }
        public int Ddata68 { get; set; }
        public int Ddata72 { get; set; }
        public int Ddata76 { get; set; }
        public int Edata80 { get; set; }
        public int Edata84 { get; set; }
        public int Edata88 { get; set; }
        public int Edata92 { get; set; }
        public int Ddata96 { get; set; }
        public int Edata100 { get; set; }
        public int Edata104 { get; set; }
        public int Edata108 { get; set; }
        public int Edata112 { get; set; }
        public string EdataName { get; set; }
        public string EdataName2 { get; set; }
        public F_GROUP_STATUS_UPDATE()
        {
        }
        public F_GROUP_STATUS_UPDATE(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_GROUP_STATUS_UPDATE, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            byte[] data = ms.ToArray().Skip(3).ToArray();
            byte[] bdPad = new byte[ms.Length + 100];
            Buffer.BlockCopy(data, 0, bdPad, 0, data.Length);
            BitReader reader = new BitReader();
            reader.eax_data = bdPad;

            Flag1_Position = reader.ReadBit();
            Flag2_Zone = reader.ReadBit();
            Flag3_Vitals = reader.ReadBit();
            Flag4 = reader.ReadBit();
            Flag5 = reader.ReadBit();

            data8 = reader.ReadInt(3);

            CharacterID = reader.ReadInt(0x18);

            if (Flag1_Position > 0)
            {
                Position_X = reader.ReadInt(16);
                Position_Y = reader.ReadInt(16);
                Position_Z = reader.ReadInt(16);
                Position_Heading = reader.ReadAngleRad(16);
            }

            if (Flag2_Zone > 0)
            {
                Bdata32 = reader.ReadInt(0x20);
                Bdata36 = reader.ReadInt(0x20);
                Zone_ID = reader.ReadInt(0x20);
                Bdata44 = reader.ReadInt(8);
            }

            if (Flag3_Vitals > 0)
            {
                Vitals_WoundsPct = reader.ReadInt(8);
                Vitals_APPct = reader.ReadInt(8);
                Vitals_MoralePct = reader.ReadInt(8);
            }

            if (Flag5 > 0)
            {
                Edata60 = reader.ReadInt(16);
                Edata64 = reader.ReadInt(16);
            }

            if (Flag4 > 0)
            {
                Ddata68 = reader.ReadInt(16);
                Ddata72 = reader.ReadInt(8);
                Ddata76 = reader.ReadInt(8);
            }

            if (Flag5 > 0)
            {
                Edata80 = reader.ReadInt(8);
                Edata84 = reader.ReadInt(8);
                Edata88 = reader.ReadInt(8);
                Edata92 = reader.ReadInt(8);
            }

            if (Flag4 > 0)
            {
                Ddata96 = reader.ReadInt(8);
            }

            if(Flag5 > 0)
            {
                Edata100 = reader.ReadInt(16);
                Edata104 = reader.ReadInt(8);
                Edata108 = reader.ReadInt(16);
                Edata112 = reader.ReadInt(8);

                for (int i = 0; i < Edata100+1; i++)
                {
                    EdataName += (char)reader.ReadInt(8);
                }


                for (int i = 0; i < Edata112; i++)
                {
                    EdataName2 += (char)reader.ReadInt(8);
                }
            }

        }
    }
    
            //        MemoryStream stream = client.GetWriteBuffer();
            //FrameUtil.WriteByte(stream, (byte)WarShared.Protocol.GameOp.F_GROUP_STATUS_UPDATE);
            //FrameUtil.WriteByte(stream, 4);
            //FrameUtil.WriteUInt32R(stream, character.ID);
            //stream.Position--;
            //FrameUtil.WriteByte(stream, (byte)character.Ragdoll.GetWoundPercent());
            //FrameUtil.WriteByte(stream, (byte)character.Ragdoll.GetAPPercent()); // action points
            //FrameUtil.WriteByte(stream, (byte)character.Ragdoll.GetMoralePercent());

    
}
