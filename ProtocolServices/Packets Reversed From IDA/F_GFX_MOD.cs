﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;




namespace WarAdmin.Packets
{
    public class F_GFX_MOD:MythicPacket 
    {
        public ushort Count { get; set; }
        public ushort ObjectID { get; set; }
        public List<GFX> Gfx { get; set; }
        public class GFX
        {
            public ushort Unk1 { get; set; }
            public ushort Unk2 { get; set; }
        }

        public F_GFX_MOD(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_GFX_MOD, ms, false)
        {
          

        }

        public override void Load(MemoryStream ms)
        {
            ms.Position = 3;

            Count = FrameUtil.GetUint16R(ms);
            ObjectID = FrameUtil.GetUint16(ms);
            Gfx = new List<GFX>();
            for (int i = 0; i < Count; i++)
            {
                Gfx.Add(new GFX()
                {
                    Unk1 = FrameUtil.GetUint16(ms),
                    Unk2 = FrameUtil.GetUint16(ms),
                });
            }
        }


    }
}
