﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace WarAdmin.Packets
{
    public class F_PLAY_EFFECT:MythicPacket
    {
        public ushort A03_EffectID { get; set; }
        [IgnoreField]
        public string A03_EffectName { get; set; }
        public ushort A05_Unk{ get; set; }
                      
        public uint A07_X { get; set; }
        public uint A10_Y { get; set; }
        public uint A14_Z { get; set; }
        public ushort A18_Unk { get; set; }
        public ushort A20_Unk { get; set; }
        public ushort A22_Unk { get; set; }
        public ushort A24_Unk { get; set; }
        public F_PLAY_EFFECT(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_PLAY_EFFECT, ms, true)
        {
        }

        public override void AutoLoad(MemoryStream ms)
        {
            base.AutoLoad(ms);
            //if(frmServerTools.EffectList.ContainsKey(A03_EffectID))
            
            //A03_EffectName = frmServerTools.EffectList[A03_EffectID].Name;
        }
    }
}
