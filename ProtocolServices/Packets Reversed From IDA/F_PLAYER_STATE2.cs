﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarCommon;
using WarShared;

namespace WarAdmin.Packets
{
    public class F_PLAYER_STATE2 : MythicPacket
    {
        public int PID { get; set; }
        public int data4 { get; set; }
        public int MoveVelocity { get; set; }
        public int FallTime { get; set; }
        public int data16 { get; set; }
        public int FreeFall { get; set; }
        public ActionType Float { get; set; }
        public StrafeType Strafe { get; set; }
        public int data32 { get; set; }
        public int Counter { get; set; }
        public int data40 { get; set; }
        public double Heading { get; set; }
        public int data48 { get; set; }
        public int Grounded { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int data60 { get; set; }
        public int ZoneID { get; set; }
        public int Z { get; set; }
        public int data70 { get; set; }
        public int data72 { get; set; }
        public int data76 { get; set; }
        public int data80 { get; set; }
        public int data84 { get; set; }
        public int HasTarget { get; set; }
        public int data89 { get; set; }
        public int data90 { get; set; }
        public int data91 { get; set; }
        public int data92 { get; set; }
        public GroundType GroundType { get; set; }
        public int data100 { get; set; }
        public int Floating2 { get; set; }
        public int data102 { get; set; }
        public int data103 { get; set; }
        public int data104 { get; set; }
        public int data105 { get; set; }
        public int data106 { get; set; }

        public F_PLAYER_STATE2(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.F_PLAYER_STATE2, ms, false)
        {
        }

        public override void Load(MemoryStream ms)
        {
            byte[] data = ms.ToArray().Skip(3).ToArray();
            PlayerState state = new PlayerState();
            byte[] bdPad = new byte[100];
            Buffer.BlockCopy(data, 0, bdPad, 0, data.Length);
            state.Read(bdPad);

            PID = state.PID;
            data4 = state.data4;
            MoveVelocity = state.MoveVelocity;
            FallTime = state.FallTime;
            data16 = state.HasEnemyTarget;
            FreeFall = state.FreeFall;
            Float = state.Action;
            Strafe = state.Strafe;
            data32 = state.data32;
            Counter = state.Heartbeat;
            data40 = state.HasPosition;
            Heading = state.Heading;
            data48 = state.HasPositionChange;
            Grounded = state.Grounded;
            X = state.X;
            Y = state.Y;
            data60 = state.data60;
            ZoneID = state.ZoneID;
            Z = state.Z;
            data70 = state.data70;
            data72 = state.data72;
            data76 = state.data76;
            data80 = state.data80;
            data84 = state.data84;
            HasTarget = state.TargetLos;
            data89 = state.data89;
            data90 = state.data90;
            data91 = state.data91;
            data92 = state.data92;
            GroundType = state.GroundType;
            data100 = state.data100;
            Floating2 = state.Floating2;
            data102 = state.data102;
            data103 = state.data103;
            data104 = state.data104;
            data105 = state.data105;
            data106 = state.data106;
        }

    }
}
