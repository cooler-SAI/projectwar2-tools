﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WarAdmin.Net;


namespace WarAdmin.Packets
{
    public class S_PLAYER_INITTED : MythicPacket
    {
       // public ushort UnRead { get; set; }
        public ushort A03_ObjectID { get; set; }
        public ushort A05_Unk { get; set; }
        public uint A07_ID { get; set; }
        public ushort A11_Z { get; set; }
        public ushort A13_Unk { get; set; }
        public uint A15_X { get; set; }
        public uint A15_XA { get; set; }
        public uint A19_Y { get; set; }
        public uint A19_YA { get; set; }
        public ushort A23_Direction { get; set; }
        public byte A21_Unk { get; set; }
        public byte A22_Realm { get; set; }

        public ushort A24_ShiftX { get; set; }
        public ushort A26_ShiftY { get; set; }

        public uint A24_ShiftXV { get; set; }
        public uint A26_ShiftYV { get; set; }

        public ushort A28_RegionID { get; set; }
        public ushort A30_RegionID { get; set; }
        public ushort A32_CareerID { get; set; }

        public ushort A34_Unk { get; set; }
        public ushort A36_Unk { get; set; }
        public ushort A38_Unk { get; set; }


        [PascalString]
        public string A40_ServerName { get; set; }

        [PascalString]
        public string A41_Unk { get; set; }

        [PascalString]
        public string A42_Unk { get; set; }



        //MemoryStream stream = client.GetWriteBuffer();
        //   FrameUtil.WriteByte(stream, (byte)WarShared.Protocol.GameOp.S_PLAYER_INITTED);
        //   FrameUtil.WriteUInt16(stream, client.Character.ObjectID);
        //   FrameUtil.WriteUInt16(stream, 0);
        //   FrameUtil.WriteUInt32(stream, client.Character.ID);
        //   FrameUtil.WriteUInt16(stream, (ushort)client.Character.Z);
        //   FrameUtil.WriteUInt16(stream, 0);
        //   FrameUtil.WriteUInt32(stream, (uint)(client.Character.X + client.Character.XZone));
        //   FrameUtil.WriteUInt32(stream, (uint)(client.Character.Y + client.Character.YZone));
        //   FrameUtil.WriteUInt16(stream, (ushort)client.Character.Direction);
        //   FrameUtil.WriteByte(stream, 0);
        //   FrameUtil.WriteByte(stream, (byte)client.Character.Career.Realm);
        //   FrameUtil.Fill(stream, 0, 5); // ??
        //   FrameUtil.WriteByte(stream, (byte)client.Character.Zone.RegionID);
        //   FrameUtil.WriteUInt16(stream, 1);
        //   FrameUtil.WriteByte(stream, 0);
        //   FrameUtil.WriteByte(stream, (byte)client.Character.CareerID);
        //   FrameUtil.Fill(stream, 0, 6);
        //   FrameUtil.WritePascalString(stream, client.Account.ServerInfo.Name);
        //   FrameUtil.Fill(stream, 0, 3);


        //   client.SendPacket("S_PLAYER_INITTED", stream);
        public S_PLAYER_INITTED()
        {
        }
        public S_PLAYER_INITTED(MemoryStream ms)
            : base(WarShared.Protocol.GameOp.S_PLAYER_INITTED, ms, false)
        {

            ms.Position = 3;

            A03_ObjectID = FrameUtil.GetUint16(ms);
            A05_Unk = FrameUtil.GetUint16(ms);
            A07_ID = FrameUtil.GetUint32(ms);
            A11_Z = FrameUtil.GetUint16(ms);
            A13_Unk = FrameUtil.GetUint16(ms);
            A15_X = FrameUtil.GetUint32(ms);
            A19_Y = FrameUtil.GetUint32(ms);

            A23_Direction = FrameUtil.GetUint16(ms);
            A21_Unk = FrameUtil.GetUint8(ms);
            A22_Realm = FrameUtil.GetUint8(ms);

            A24_ShiftX = FrameUtil.GetUint16(ms);
            A26_ShiftY = FrameUtil.GetUint16(ms);

            A24_ShiftXV = (uint)(A24_ShiftX * 0x2000);
            A26_ShiftYV = (uint)(A26_ShiftY * 0x2000);



            A15_XA = (uint)(A15_X - (A24_ShiftX * 0x2000));
            A19_YA = (uint)(A19_Y - (A26_ShiftY * 0x2000));

            // A27_Unk = FrameUtil.GetUint8(ms);
            A28_RegionID = FrameUtil.GetUint16(ms);
            A30_RegionID = FrameUtil.GetUint16(ms);
            A32_CareerID = FrameUtil.GetUint16(ms);

            A34_Unk = FrameUtil.GetUint16(ms);
            A36_Unk = FrameUtil.GetUint16(ms);
            A38_Unk = FrameUtil.GetUint16(ms);


            A40_ServerName = FrameUtil.GetPascalString(ms);

        }

    }
}
