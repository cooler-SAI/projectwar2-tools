﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using WarClient.Figleaf.Tables;

namespace WarClient.Figleaf
{
    public interface IFigTable
    {
        List<FigRecord> FigRecords { get; }
        string Name { get; }
    }

    public abstract class FigTable<T>:IFigTable where T : FigRecord
    {
        public virtual uint EntryCount { get; protected set; }
        public uint Offset { get; protected set; }
        public uint DataSize { get; protected set; }
        protected FigleafDB _db;
        public List<T> Records { get; private set; } = new List<T>();
        public List<FigRecord> FigRecords => Records.Select(e => (FigRecord)e).ToList();
        public abstract void Load(BinaryReader reader);
        public abstract void Save(BinaryWriter writer);
        public string Name => GetType().Name + " [" + Records.Count + "]";

        public FigTable(FigleafDB db)
        {
            _db = db;
        }
    }
}
