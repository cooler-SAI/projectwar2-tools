﻿using System.Collections.Generic;
using System.IO;

namespace WarClient.Figleaf.Tables
{
    public class RegionTable : FigTable<Region>
    {
        private const int HeaderPosition = 0x9C;
        private const int RecordSize = 0x20;

        public RegionTable(FigleafDB db) : base(db)
        {
        }

        public override void Load(BinaryReader reader)
        {
            reader.BaseStream.Position = HeaderPosition;
            EntryCount = reader.ReadUInt32();
            Offset = reader.ReadUInt32();
            DataSize = reader.ReadUInt32();

            reader.BaseStream.Position = Offset;

            for (int i = 0; i < EntryCount; i++)
            {
                var region = new Region(_db, i);
                region.A00 = reader.ReadUInt32();
                region.SliceCount = reader.ReadUInt32();
                region.SliceStart = reader.ReadInt32();
                region.RegionID = reader.ReadUInt32();
                region.A16 = reader.ReadInt32();
                region.A20 = reader.ReadInt32();
                region.ZoneCount = reader.ReadUInt32();
                region.ZoneStart = reader.ReadInt32();
                Records.Add(region);
            }

            for (int i = 0; i < EntryCount; i++)
            {
                var record = Records[i];

                if (record.SliceCount > 0)
                {
                    reader.BaseStream.Position = Offset + record.SliceStart + (i * RecordSize);
                    for (int c = 0; c < record.SliceCount; c++)
                    {
                        var a2 = new ZoneAsset();
                        a2.Unk1 = new FigStringRef(_db, reader.ReadUInt32());

                        a2.Unk02 = reader.ReadUInt32();
                        a2.Unk03= reader.ReadUInt32();

                        a2.Unk02S = a2.Unk02.ToString("X2").PadLeft(8, '0');
                        a2.Unk03S = a2.Unk03.ToString("X2").PadLeft(8, '0');

                        a2.Unk10 = reader.ReadByte();
                        a2.Unk11 = reader.ReadByte();
                        a2.Unk12 = reader.ReadByte();
                        a2.Unk13 = reader.ReadByte();


                        record.Assets.Add(a2);
                    }
                }

                if (record.ZoneCount > 0)
                {
                    reader.BaseStream.Position = Offset + record.ZoneStart + (i * RecordSize);
                    for (int c = 0; c < record.ZoneCount; c++)
                    {
                        var a16 = new Zone();
                        a16.ZoneID = reader.ReadUInt16();
                        a16.MiniMapID = reader.ReadUInt16();
                        a16.ZoneXOff = reader.ReadUInt16();
                        a16.ZoneYOff = reader.ReadUInt16();
                        a16.Unk1 = reader.ReadUInt16();
                        a16.Unk2 = reader.ReadUInt16();
                        a16.Unk3 = reader.ReadUInt16();
                        a16.Unk4 = reader.ReadUInt16();
                        a16.Unk5 = reader.ReadUInt16();
                        a16.Unk6 = reader.ReadUInt16();
                        a16.Unk7 = reader.ReadUInt16();
                        a16.Unk8 = reader.ReadUInt16();
                        a16.Unk9 = reader.ReadUInt16();
                        a16.Unk10 = reader.ReadUInt16();
                        a16.Unk11 = reader.ReadUInt16();
                        record.Zones.Add(a16);
                    }
                }
            }
            ushort pad = reader.ReadUInt16();
        }

        public override void Save(BinaryWriter writer)
        {
            var pos = writer.BaseStream.Position;

            writer.BaseStream.Position += RecordSize * Records.Count;
            var DataStart = writer.BaseStream.Position;
            var ExtStart = RecordSize * Records.Count;

            for (int i = 0; i < Records.Count; i++)
            {
                var record = Records[i];

                // record.Part1Start = 0;
                if (record.Assets.Count > 0)
                {
                    record.SliceStart = (int)(ExtStart + (writer.BaseStream.Position - DataStart) - (i * RecordSize));

                    foreach (var part in record.Assets)
                    {
                        writer.Write(part.Unk1);

                        writer.Write(part.Unk02);
                        writer.Write(part.Unk03);
                        writer.Write(part.Unk10);
                        writer.Write(part.Unk11);
                        writer.Write(part.Unk12);
                        writer.Write(part.Unk13);
                    }
                }

                // record.Part2Start = 0;
                if (record.Zones.Count > 0)
                {
                    record.ZoneStart = (int)(ExtStart + (writer.BaseStream.Position - DataStart) - (i * RecordSize));

                    foreach (var part in record.Zones)
                    {
                        writer.Write(part.ZoneID);
                        writer.Write(part.MiniMapID);
                        writer.Write(part.ZoneXOff);
                        writer.Write(part.ZoneYOff);
                        writer.Write(part.Unk1);
                        writer.Write(part.Unk2);
                        writer.Write(part.Unk3);
                        writer.Write(part.Unk4);
                        writer.Write(part.Unk5);
                        writer.Write(part.Unk6);
                        writer.Write(part.Unk7);
                        writer.Write(part.Unk8);
                        writer.Write(part.Unk9);
                        writer.Write(part.Unk10);
                        writer.Write(part.Unk11);
                    }
                }
            }

            writer.Write((ushort)0);

            var endPos = writer.BaseStream.Position;
            writer.BaseStream.Position = pos;

            for (int i = 0; i < Records.Count; i++)
            {
                var record = Records[i];

                writer.Write(record.A00);
                writer.Write((int)record.Assets.Count);
                writer.Write(record.SliceStart);
                writer.Write(record.RegionID);
                writer.Write(record.A16);
                writer.Write(record.A20);
                writer.Write((int)record.Zones.Count);
                writer.Write(record.ZoneStart);
            }

            writer.BaseStream.Position = HeaderPosition;
            writer.Write((uint)Records.Count);
            writer.Write((uint)(Records.Count > 0 ? pos : 0));
            var size = (uint)((endPos - DataStart) + (Records.Count * RecordSize));
            writer.Write(size);

            writer.BaseStream.Position = endPos;
        }
    }

    public class Region : FigRecord
    {
        public int Index { get; }
        public uint A00 { get; set; }
        public uint SliceCount{ get; set; }
        public int SliceStart{ get; set; }
        public uint RegionID{ get; set; }
        public int A16{ get; set; }
        public int A20{ get; set; }
        public uint ZoneCount{ get; set; }
        public int ZoneStart{ get; set; }

        public List<Zone> Zones = new List<Zone>();
        public List<ZoneAsset> Assets = new List<ZoneAsset>();

        public Region(FigleafDB db, int index): base(db) { Index = index; }
    }

    public class ZoneAsset
    {
        public FigStringRef Unk1 { get; set; }
                public string Unk02S { get; set; }
        public string Unk03S { get; set; }

        public uint Unk02 { get; set; }
        public uint Unk03 { get; set; }

        public byte Unk10 { get; set; }
        public byte Unk11 { get; set; }
        public byte Unk12 { get; set; }
        public byte Unk13 { get; set; }

    }

    public class Zone
    {
        public ushort ZoneID { get; set; }
        public ushort MiniMapID { get; set; }
        public ushort ZoneXOff { get; set; }
        public ushort ZoneYOff { get; set; }
        public ushort Unk1 { get; set; }
        public ushort Unk2 { get; set; }
        public ushort Unk3 { get; set; }
        public ushort Unk4 { get; set; }
        public ushort Unk5 { get; set; }
        public ushort Unk6 { get; set; }
        public ushort Unk7 { get; set; }
        public ushort Unk8 { get; set; }
        public ushort Unk9 { get; set; }
        public ushort Unk10 { get; set; }
        public ushort Unk11 { get; set; }
    }
}
