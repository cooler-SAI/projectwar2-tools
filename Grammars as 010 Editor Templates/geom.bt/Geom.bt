//------------------------------------------------
//--- 010 Editor v9.0.1 Binary Template
//
//      File: Geom.bt
//   Authors: Andi 'Debug' Ireland
//   Version:
//   Purpose: Parse WAR: .geom
//  Category: Game Data Mining
// File Mask:
//  ID Bytes:
//   History: 21/01/2019 Created
//------------------------------------------------

typedef struct {
        float   /*0x00*/ x;
        float   /*0x04*/ y;
} Vector2 <read=Vector2Read, write=Vector2Write, optimize=false, open=false>;

typedef struct {
        float   /*0x00*/ x;
        float   /*0x04*/ y;
        float   /*0x08*/ z;
} Vector3 <read=Vector3Read, write=Vector3Write, optimize=false, open=false>;

typedef struct {
        float   /*0x00*/ x;
        float   /*0x04*/ y;
        float   /*0x08*/ z;
        float   /*0x12*/ w;
} Vector4 <read=Vector4Read, write=Vector4Write, optimize=false, open=false>;

typedef struct {
        float   /*0x00*/ x;
        float   /*0x04*/ y;
        float   /*0x08*/ z;
        float   /*0x12*/ w;
} Quaternion <read=QuaternionRead, write=QuaternionWrite, optimize=false, open=false>;

typedef struct {
        Vector3 /*0x00*/ r1;
        Vector3 /*0x0C*/ r2;
        Vector3 /*0x18*/ r3;
} Matrix <read=MatrixRead, optimize=false, open=false>;

typedef struct {
        ushort  /*0x00*/ index_a;
        ushort  /*0x02*/ index_b;
        ushort  /*0x04*/ index_c;
} Triangle <read=TriangleRead, write=TriangleWrite, optimize=false, open=false>;

typedef struct {
        char    /*0x00*/ magic[4]     		<bgcolor=0x005000, open=false>;
        uint    /*0x04*/ version      		<format=decimal, bgcolor=0x006000>;
        uint    /*0x08*/ file_size    		<format=decimal, bgcolor=0x007000>;
        uint    /*0x0C*/ id           		<format=hex, bgcolor=0x008000>;
        uint    /*0x10*/ bone_count   		<format=decimal, bgcolor=0x009000>;
        uint    /*0x14*/ bones_offset 		<format=hex, bgcolor=0x008000>;
        uint    /*0x18*/ mesh_count   		<format=decimal, bgcolor=0x007000>;
        uint    /*0x1C*/ meshes_offset		<format=hex, bgcolor=0x006000>;
} Header <read=HeaderRead, bgcolor=0x004000, optimize=false, open=true>;

typedef struct {
        local string name <hidden=true>;
        uint    /*0x00*/ name_offset        <format=hex, bgcolor=0x506050, optimize=false, open=true>;
        Matrix  /*0x04*/ bind_pose_rotation <bgcolor=0x809080, optimize=false, open=true>;
        Vector4 /*0x28*/ bind_pose_position <bgcolor=0x506050, optimize=false, open=true>;
        Matrix  /*0x32*/ rotation    		<bgcolor=0x809080, optimize=false, open=true>;
        Vector4 /*0x60*/ position    		<bgcolor=0x809080, optimize=false, open=true>;
} BoneHeader <read=BoneHeaderRead, bgcolor=0x303000, optimize=false, open=false>;

typedef struct {
        short    /*0x00*/ placement        			<format=binary, bgcolor=0x202020>;
        ushort   /*0x02*/ vertex_count    		    <format=decimal, bgcolor=0x000020>;
        uint     /*0x04*/ vertices_offset		    <format=hex, bgcolor=0x000040>;
        uint  	 /*0x08*/ triangle_count     		<format=decimal, bgcolor=0x000060>;
        uint     /*0x0C*/ triangles_offset   		<format=hex, bgcolor=0x000080>;
        uint     /*0x10*/ skin_weight_group_count	<format=decimal, bgcolor=0x0000a0>;
        uint     /*0x14*/ skin_weight_group_offset	<format=hex, bgcolor=0x000080>;
        uint     /*0x18*/ bone_count   		   	    <format=decimal, bgcolor=0x000060>;
        uint     /*0x1C*/ bones_offset				<format=hex, bgcolor=0x000040>;
} MeshHeader <read=MeshHeaderRead, bgcolor=0x000040, optimize=false, open=false>;

typedef struct{
        ushort   /*0x00*/ index <format=decimal, bgcolor=0xabcdef, optimize=false, open=false>;
        ushort   /*0x02*/ count <format=hex, bgcolor=0xabcdef, optimize=false, open=false>;
		uint     /*0x04*/ offset <format=hex, bgcolor=0xabcdef, optimize=false, open=false>;
} SkinWeightInfo <read=SkinWeightInfoRead, optimize=false, open=false>;

typedef struct{
    uint         /*0x00*/ index <format=decimal, bgcolor=0xabcdef, optimize=false, open=false>;
    float        /*0x02*/ weight <format=hex, bgcolor=0xabcdef, optimize=false, open=false>;
} SkinWeight <read=SkinWeightRead, optimize=false, open=false>;

typedef struct {
        local string name <hidden=true>;
		ushort   /*0x00*/ index <format=decimal, bgcolor=0xabcdef, optimize=false, open=false>;
		ushort   /*0x02*/ unk2 <format=hex, bgcolor=0xabcdef, optimize=false, open=false>;
        Matrix 	 /*0x04*/ rotation <bgcolor=0x809080, optimize=false, open=true>;
        Vector4  /*0x28*/ translation <bgcolor=0x506050, optimize=false, open=true>;
} MeshBoneHeader <read=MeshBoneHeaderRead, optimize=false, open=false>;

typedef struct {
    Vector3 position <bgcolor=0xA09070>;
    Vector3 normal <bgcolor=0x90a070>;
    Vector2 uv <bgcolor=0x80b070>;
} Vertex <read=VertexRead, optimize=false, open=false>;

string Vector2Read(Vector2 &v) {
    string s; SPrintf(s, "[%.2f, %.2f]", v.x, v.y); return s;
}
void Vector2Write(Vector2 &v, string s) {
    SScanf(s, "[%.2f, %.2f]", v.x, v.y);
}

string Vector3Read(Vector3 &v) {
    string s; SPrintf(s, "[%.2f, %.2f, %.2f]", v.x, v.y, v.z); return s;
}
void Vector3Write(Vector3 &v, string s) {
    SScanf(s, "[%.2f, %.2f, %.2f]", v.x, v.y, v.z);
}

string Vector4Read(Vector4 &v) {
    string s; SPrintf(s, "[%.2f, %.2f, %.2f, %.2f]", v.x, v.y, v.z, v.w); return s;
}
void Vector4Write(Vector4 &v, string s) {
    SScanf(s, "[%.2f, %.2f, %.2f, %.2f]", v.x, v.y, v.z, v.w);
}

string QuaternionRead(Quaternion &q) {
    string s; SPrintf(s, "[%.2f, %.2f, %.2f, %.2f]", q.x, q.y, q.z, q.wf); return s;
}
void QuaternionWrite(Quaternion &q, string s) {
    SScanf(s, "[%.2f, %.2f, %.2f, %.2f]", q.x, q.y, q.z, q.w);
}

string TriangleRead(Triangle &t) {
    string s; SPrintf(s, "%d => %d => %d", t.index_a, t.index_b, t.index_c); return s;
}
void TriangleWrite(Triangle &t, string s) {
    SScanf(s, "%d => %d => %d", t.index_a, t.index_b, t.index_c);
}

string MatrixRead(Matrix &m) {
    string s; SPrintf(s, "[%.2f, %.2f, %.2f][%.2f, %.2f, %.2f][%.2f, %.2f, %.2f]", m.r1.x, m.r1.y, m.r1.z, m.r2.x, m.r2.y, m.r2.z, m.r3.x, m.r3.y, m.r3.z); return s;
}

string HeaderRead(Header &h) {
    return h.magic;
}

string BoneHeaderRead(BoneHeader &bh) {
    return bh.name;
}

string MeshHeaderRead(MeshHeader &h) {
    string s;
    SPrintf(s, "[%d][%d, %d][%d, %d][%d, %d][%d, %d]", h.placement, h.vertex_count, h.vertices_offset, h.triangle_count, h.triangles_offset, h.bone_count, h.bones_offset, h.skin_weight_group_count, h.skin_weight_group_offset);
    return s;
}

string SkinWeightInfoRead(SkinWeightInfo &vwgi) {
    string s;
    SPrintf(s, "[%d, %d, %d]", vwgi.index, vwgi.count, vwgi.offset);
    return s;
}

string SkinWeightRead(SkinWeight &u) {
    string s;
    SPrintf(s, "[%d, %f]", u.index, u.weight);
    return s;
}

string MeshBoneHeaderRead(MeshBoneHeader &b) {
    string s;
    SPrintf(s, "[%d, %d]", b.index, b.unk2);
    return s;
}

string VertexRead(Vertex &v) {
    string s;
    SPrintf(s, "[%.2f, %.2f, %.2f]", v.position.x, v.position.y, v.position.z);
    return s;
}

struct Geom
{
	local int i <hidden=true>;
	local int j <hidden=true>;
	local int k <hidden=true>;
	local int offset <hidden=true>;

    Header header <optimize=false, open=true>;

	struct Metadata {

	    if(header.bone_count > 0) {
		    offset = (header.bones_offset);
		    if(FTell() != offset) {
				//Printf(" *** [BoneHeader] Reader position '%d' unexpected, seeking '%d' ***\r\n", FTell(), offset);
		    }
		    FSeek(offset);
			struct BoneGroup {
			    BoneHeader bone[header.bone_count] <optimize=false, open=false, fgcolor=0xaaccbb>;
			} bone_group <optimize=false, open=false, fgcolor=0xaaccbb>;
		}

		if(header.mesh_count > 0) {
			offset = (header.meshes_offset);
			if(FTell() != offset) {
				//Printf(" *** [MeshHeader] Reader position '%d' unexpected, seeking '%d' ***\r\n", FTell(), offset);
			}
			FSeek(offset);
			struct MeshGroup {
					MeshHeader mesh[header.mesh_count] <optimize=false, open=false, fgcolor=0xaaccbb>;
			} mesh_group <optimize=false, open=false, fgcolor=0xaaccbb>;
		}

    } metadata <optimize=false, open=true, fgcolor=0xaaccbb>;

	struct Data {
		for(i = 0; i < header.mesh_count; ++i) {
			
			struct Mesh {
				if(metadata.mesh_group.mesh[i].skin_weight_group_count > 0) {
					offset = (header.meshes_offset + metadata.mesh_group.mesh[i].skin_weight_group_offset) + (i * sizeof(MeshHeader));
					if(FTell() != offset) {
						//Printf(" *** [SkinWeightsInfos] Reader position '%d' unexpected, seeking '%d' mesh '%d' ***\r\n", FTell(), offset, i);
					}
					FSeek(offset);
					
					struct SkinWeightsInfos {
						SkinWeightInfo info[metadata.mesh_group.mesh[i].skin_weight_group_count] <optimize=false, open=false>;
					} skin_weights_infos <optimize=false, open=false, bgcolor=0xaaccbb>;
				}

				if(metadata.mesh_group.mesh[i].bone_count > 0) {
					offset = (header.meshes_offset + metadata.mesh_group.mesh[i].bones_offset) + (i * sizeof(MeshHeader));
					if(FTell() != offset) {
						//Printf(" *** [MeshBones] Reader position '%d' unexpected, seeking '%d' mesh '%d' ***\r\n", FTell(), offset, i);
                    }
					FSeek(offset);
					
					struct MeshBones {
					    MeshBoneHeader bone[metadata.mesh_group.mesh[i].bone_count] <optimize=false, open=false>;
                    } mesh_bones <optimize=false, open=false, bgcolor=0xaaccbb>;
				}

				if(metadata.mesh_group.mesh[i].vertex_count > 0) {
					offset = (header.meshes_offset + metadata.mesh_group.mesh[i].vertices_offset) + (i * sizeof(MeshHeader));
					if(FTell() != offset) {
						//Printf(" *** [Vertices] Reader position '%d' unexpected, seeking '%d' mesh '%d' ***\r\n", FTell(), offset, i);
					}
					FSeek(offset);
					
					struct Vertices {
						Vertex vertex[metadata.mesh_group.mesh[i].vertex_count] <optimize=false, open=false>;
					} vertices <optimize=false, open=false, bgcolor=0xaaccbb>;
				}

				if(metadata.mesh_group.mesh[i].triangle_count > 0) {
					offset = (header.meshes_offset + metadata.mesh_group.mesh[i].triangles_offset) + (i * sizeof(MeshHeader));
					if(FTell() != offset) {
						//Printf(" *** [Triangles] Reader position '%d' unexpected, seeking '%d' mesh '%d' ***\r\n", FTell(), offset, i);
					}
					FSeek(offset);
					
					struct Triangles {
						Triangle triangle[metadata.mesh_group.mesh[i].triangle_count] <optimize=false, open=false>;
					} triangles <optimize=false, open=false, bgcolor=0xaaccbb>;
				}

				if(metadata.mesh_group.mesh[i].skin_weight_group_count > 0) {
					
					struct SkinWeightsGroups {
            		    for(j = 0; j < metadata.mesh_group.mesh[i].skin_weight_group_count; ++j) {
							offset = (header.meshes_offset + metadata.mesh_group.mesh[i].skin_weight_group_offset) + skin_weights_infos.info[j].offset + (i * sizeof(MeshHeader)) + (j * sizeof(SkinWeightInfo));
							if(FTell() != offset) {
								//Printf(" *** [SkinWeightsGroups] Reader position '%d' unexpected, seeking '%d' mesh '%d' skin weights group '%d' ***\r\n", FTell(), offset, i, j);
							}
							FSeek(offset);

							struct SkinWeights {
								SkinWeight skin_weight[skin_weights_infos.info[j].count] <optimize=false, open=false>;
							} skin_weights <optimize=false, open=false, bgcolor=0xaaccbb>;
				        }
					} skin_weights_groups <optimize=false, open=false, bgcolor=0xaaccbb>;
				}
			} mesh <optimize=false, open=false, fgcolor=0xaaccbb>;
		}

		struct BoneNamesGroup {
			if(header.bone_count > 0) {
				offset = sizeof(Header) + metadata.bone_group.bone[0].name_offset;
				if(FTell() != offset) {
					//Printf(" *** [BoneNamesGroup] Reader position '%d' unexpected, seeking '%d' ***\r\n", FTell(), offset);
				}
				FSeek(offset);

                struct BoneNames {
                    char bone_name[] <optimize=false, open=false, fgcolor=0xaaccbb>;
                } bone_names[header.bone_count] <optimize=false, open=false, fgcolor=0xaaccbb>;

				for(i = 0; i < header.bone_count; ++i) {
					metadata.bone_group.bone[i].name = bone_names[i].bone_name;
				}
			}
		} bone_names_group <optimize=false, open=false, fgcolor=0xaaccbb>;
    } data <optimize=false, open=true, fgcolor=0xaaccbb>;
} geom <optimize=false, open=true, fgcolor=0xaaccbb, size=275622>;