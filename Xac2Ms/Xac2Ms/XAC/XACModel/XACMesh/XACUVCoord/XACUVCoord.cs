﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnexpectedBytes
{

    public class XACUVCoord
    {
        public float mU;
        public float mV;

        public XACUVCoord(float iU, float iV)
        {
            mU = iU;
            mV = iV;
        }

        public override string ToString()
        {
            string vTheString = "XACUVCoord U:" + mU.ToString() + " V:" + mV.ToString();
            return vTheString;
        }
    }


}
