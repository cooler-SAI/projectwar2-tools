﻿using System.IO;
using System.Runtime.InteropServices;

namespace UnexpectedBytes
{
    public class Vector3F
    {
        public float mX;
        public float mY;
        public float mZ;

        public Vector3F()
        {
            mX = 0.0f;
            mY = 0.0f;
            mZ = 0.0f;
        }

        public Vector3F(float iX, float iY, float iZ)
        {
            mX = iX;
            mY = iY;
            mZ = iZ;
        }

        public void ReadIn(BinaryReader iStream)
        {
            mX = iStream.ReadSingle();
            mY = iStream.ReadSingle();
            mZ = iStream.ReadSingle();
        }

        public void WriteOut(BinaryWriter iStream)
        {
            iStream.Write(mX);
            iStream.Write(mY);
            iStream.Write(mZ);
        }

        public long GetSize()
        {
            long vSize = 0;
            vSize += Marshal.SizeOf(mX);
            vSize += Marshal.SizeOf(mY);
            vSize += Marshal.SizeOf(mZ);
            return vSize;
        }

        public override string ToString()
        {
            return $"{Common.FormatFloat(mX)}, {Common.FormatFloat(mY)}, {Common.FormatFloat(mZ)}";
        }
    }
}
