﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Immutable;
using System.Text;

namespace AssetHashHunter
{
    public class Program
    {
        static bool only_use_verified_folders;
        static bool only_use_verified_extensions;
        static bool use_filename_list;
        static bool use_builder_lists;

        static ImmutableDictionary<long, string> hash2path;
        static ImmutableDictionary<string, long> path2hash;

        static ImmutableHashSet<long> knownHashes;
        static ImmutableHashSet<long> unknownHashes;

        static HashSet<long> newHashes;

        static ImmutableHashSet<string> knownPaths;
        static ImmutableHashSet<string> extensions;
        static ImmutableHashSet<string> filenames;
        static ImmutableHashSet<string> folders;
        static ImmutableHashSet<string> suffixes;
        static ImmutableHashSet<string> components;
        static ImmutableHashSet<string> prefixes;

        static long total_permutations = 0;
        static uint delta_permutations = 0;
        static long previous_permutations = 0;
        static float speed_permutations = 0.0f;

        static long total_progress = 0;
        static float delta_progress = 0;
        static long progress_upper_bound = 0;
        static float percent_complete = 0.0f;

        static StreamWriter hash_results_tsv_stream;
        static StreamWriter hash_reference_stream;
        static StreamWriter hash_dehash_stream;

        static JenkinsLookup3 lookup3;

        static void Main(string[] args)
        {
            Console.WriteLine($"[^]");
            Console.CursorVisible = false;
            Console.CursorSize = 1;
            Console.Title = "";
            Console.TreatControlCAsInput = true;
            Console.BufferHeight = 32678;
            Console.BufferWidth = 512;

            only_use_verified_folders = true;
            only_use_verified_extensions = true;
            use_filename_list = true;
            use_builder_lists = !use_filename_list;

            string data_directory = "Data";
            string output_directory = "Output";
            string date = $"{DateTime.UtcNow.ToShortDateString().Replace("/", "-")}";
            string time = $"{DateTime.UtcNow.ToShortTimeString().Replace(":", "-")}";
            string hash_results_tsv_file = $"asset_hashes_{time}.txt";
            string hash_reference_file = $"asset_hashes.tab";
            string hash_dehash_file = $"asset_hash_path.tab";

            newHashes = new HashSet<long>();

            lookup3 = new JenkinsLookup3();

            if (!Directory.Exists($"{data_directory}"))
                Directory.CreateDirectory($"{data_directory}");

            if (!Directory.Exists($"{output_directory}"))
                Directory.CreateDirectory($"{output_directory}");

            if (!Directory.Exists($"{output_directory}/{date}"))
                Directory.CreateDirectory($"{output_directory}/{date}");

            hash_results_tsv_stream = new StreamWriter($"{output_directory}/{date}/{hash_results_tsv_file}");
            hash_reference_stream = new StreamWriter($"{output_directory}/{hash_reference_file}");
            hash_dehash_stream = new StreamWriter($"{output_directory}/{hash_dehash_file}");

            Console.WriteLine("[|]");

            {
                string[] lines = File.ReadAllLines($"{data_directory}/asset_paths.txt");
                Console.WriteLine($"[>] Loaded {lines.LongLength} known asset paths and hashes.");

                Dictionary<string, long> path2hash_mutable = new Dictionary<string, long>();
                Dictionary<long, string> hash2path_mutable = new Dictionary<long, string>();

                HashSet<long> knownHashes_mutable = new HashSet<long>();
                HashSet<string> knownPaths_mutable = new HashSet<string>();

                long hash = 0;
                string path = "";

                foreach (var line in lines)
                {
                    path = line.ToLower();

                    hash = HashWAR(path);

                    knownHashes_mutable.Add(hash);
                    knownPaths_mutable.Add(path);

                    path2hash_mutable.Add(path, hash);
                    hash2path_mutable.Add(hash, path);

                    hash_reference_stream.WriteLine($"{hash}");
                    hash_dehash_stream.WriteLine($"{hash}\t{path}");
                }
                hash_reference_stream.Flush();
                hash_reference_stream.Close();
                hash_dehash_stream.Flush();
                hash_dehash_stream.Close();

                knownHashes = knownHashes_mutable.ToImmutableHashSet();
                knownPaths = knownPaths_mutable.ToImmutableHashSet();

                path2hash = path2hash_mutable.ToImmutableDictionary();
                Console.WriteLine($"[>] Constructed {path2hash.Count} path => hash lookups.");

                hash2path = hash2path_mutable.ToImmutableDictionary();
                Console.WriteLine($"[>] Constructed {hash2path.Count} hash => path lookups.");
            }

            {
                HashSet<long> unknown_mutable = new HashSet<long>();

                if (File.Exists($"{data_directory}/unknown.txt"))
                {
                    string[] lines = File.ReadAllLines($"{data_directory}/unknown.txt");
                    bool update_unknown = false;

                    foreach (var line in lines)
                    {
                        long hash = Convert.ToInt64(line.ToLower(), 16);
                        if (!hash2path.ContainsKey(hash))
                        {
                            unknown_mutable.Add(hash);
                        }
                        else
                        {
                            update_unknown = true;
                        }
                    }

                    if (update_unknown)
                    {
                        StreamWriter unknown_hashes_stream = new StreamWriter($"{output_directory}/unknown.txt");
                        StreamWriter unknown_hashes_helper_stream = new StreamWriter($"{output_directory}/unknown_helper.txt");
                        foreach (var hash in unknown_mutable)
                        {
                            byte[] bytes = BitConverter.GetBytes(hash);
                            var stringBuilder = new StringBuilder(bytes.Length);
                            foreach (var byteValue in bytes)
                                stringBuilder.Append(byteValue.ToString("x2"));

                            ulong uhash = (ulong)hash;
                            uhash = (uhash >> 32) | (uhash << 32); // swap adjacent 32-bit blocks
                            uhash = ((uhash & 0xFFFF0000FFFF0000) >> 16) | ((uhash & 0x0000FFFF0000FFFF) << 16); // swap adjacent 16-bit blocks
                            uhash = ((uhash & 0xFF00FF00FF00FF00) >> 8) | ((uhash & 0x00FF00FF00FF00FF) << 8); // swap adjacent 8-bit blocks
                            byte[] ubytes = BitConverter.GetBytes(uhash);
                            var ustringBuilder = new StringBuilder(ubytes.Length);
                            foreach (var byteValue in ubytes)
                                ustringBuilder.Append(byteValue.ToString("x2"));

                            unknown_hashes_stream.WriteLine($"{stringBuilder.ToString()}");
                            unknown_hashes_helper_stream.WriteLine($"{stringBuilder.ToString()}\t{hash}\t\t{ustringBuilder.ToString()}\t{uhash}");
                        }
                        unknown_hashes_stream.Flush();
                        unknown_hashes_stream.Close();
                        unknown_hashes_helper_stream.Flush();
                        unknown_hashes_helper_stream.Close();
                        Console.WriteLine($"[>] Updated unknown hash file has been created in the output folder.");
                    }
                }
                unknownHashes = unknown_mutable.ToImmutableHashSet();
                Console.WriteLine($"[>] Loaded {unknownHashes.Count} unknown hashes.");
            }

            {
                HashSet<string> extensions_mutable = new HashSet<string>();

                if (only_use_verified_extensions ? File.Exists($"{data_directory}/extensions_verified.txt") : File.Exists($"{data_directory}/extensions.txt"))
                {
                    string[] lines = File.ReadAllLines(only_use_verified_extensions ? $"{data_directory}/extensions_verified.txt" : $"{data_directory}/extensions.txt");

                    foreach (var line in lines)
                    {
                        extensions_mutable.Add($".{line.ToLower()}");
                    }
                    extensions_mutable.Add("");
                }
                extensions = extensions_mutable.ToImmutableHashSet();
                Console.WriteLine($"[>] Loaded {extensions.Count} filename extensions (Use small verified known list [{only_use_verified_extensions}]).");
            }

            {
                HashSet<string> filenames_mutable = new HashSet<string>();

                if (File.Exists($"{data_directory}/filenames.txt"))
                {
                    string[] lines = File.ReadAllLines($"{data_directory}/filenames.txt");

                    foreach (var line in lines)
                    {
                        filenames_mutable.Add(line.ToLower());
                    }
                }
                filenames = filenames_mutable.ToImmutableHashSet();
                progress_upper_bound = filenames.Count;
                Console.WriteLine($"[>] Loaded {filenames.Count} filenames.");
            }

            {
                HashSet<string> folders_mutable = new HashSet<string>();

                if (only_use_verified_folders ? File.Exists($"{data_directory}/folders_verified.txt") : File.Exists($"{data_directory}/folders.txt"))
                {
                    string[] lines = File.ReadAllLines(only_use_verified_folders ? $"{data_directory}/folders_verified.txt" : ($"{data_directory}/folders.txt"));

                    foreach (var line in lines)
                    {
                        folders_mutable.Add(line.ToLower());
                    }
                    folders_mutable.Add("");
                }
                folders = folders_mutable.ToImmutableHashSet();
                Console.WriteLine($"[>] Loaded {folders.Count} folders (Use smaller verified known list [{only_use_verified_folders}]).");
            }

            {
                HashSet<string> prefixes_mutable = new HashSet<string>();

                if (File.Exists($"{data_directory}/prefixes.txt"))
                {
                    string[] lines = File.ReadAllLines($"{data_directory}/prefixes.txt");


                    foreach (var line in lines)
                    {
                        prefixes_mutable.Add(line.ToLower());
                    }
                    prefixes_mutable.Add("");
                }
                prefixes = prefixes_mutable.ToImmutableHashSet();
                Console.WriteLine($"[>] Loaded {prefixes.Count} builder prefixes.");
            }

            {
                HashSet<string> components_mutable = new HashSet<string>();

                if (File.Exists($"{data_directory}/components.txt"))
                {
                    string[] lines = File.ReadAllLines($"{data_directory}/components.txt");

                    foreach (var line in lines)
                    {
                        components_mutable.Add(line.ToLower());
                    }
                    components_mutable.Add("");
                }
                components = components_mutable.ToImmutableHashSet();
                Console.WriteLine($"[>] Loaded {components.Count} builder components.");
            }

            {
                string[] lines = File.ReadAllLines($"{data_directory}/suffixes.txt");

                HashSet<string> suffixes_mutable = new HashSet<string>();

                foreach (var line in lines)
                {
                    suffixes_mutable.Add(line.ToLower());
                }
                suffixes_mutable.Add("");
                suffixes = suffixes_mutable.ToImmutableHashSet();
                Console.WriteLine($"[>] Loaded {suffixes.Count} builder suffixes.");
            }

            //string settings_filename = "settings.txt";

            //if (!File.Exists($"{data_directory}/{settings_filename}"))
            //{
            //    StreamWriter settings_temp = new StreamWriter($"{data_directory}/{settings_filename}");
            //    settings_temp.WriteLine(0);
            //    settings_temp.Flush();
            //    settings_temp.Close();
            //}

            //StreamReader settings_in = new StreamReader($"{data_directory}/{settings_filename}");
            //long permutations_continue = Convert.ToInt64(settings_in.ReadLine());
            //settings_in.Close();

            //settings_out.AutoFlush = true;

            // float last_saved = 0.2f;
            float percent_complete_percent = 0.0f;
            // object settings_lock = 1;

            Task.Run(() =>
            {
                while (true)
                {
                    unchecked
                    {
                        // total_permutations = Interlocked.Read(ref permutations);
                        delta_permutations = (uint)(total_permutations - previous_permutations);
                        previous_permutations = total_permutations;
                        speed_permutations = (float)(delta_permutations / 1000000);

                        //total_progress = Interlocked.Read(ref progress);
                        delta_progress = 100.0f / progress_upper_bound;
                        percent_complete = delta_progress * total_progress;

                        percent_complete_percent = (float)Math.Round(percent_complete, 2);

                        //if (permutations_continue < total_permutations && percent_complete_percent > last_saved)
                        //{
                        //    lock (settings_lock)
                        //    {
                        //        last_saved += 0.2f;
                        //        StreamWriter settings_out = new StreamWriter($"{data_directory}/{settings_filename}");
                        //        settings_out.WriteLine(total_permutations);
                        //        settings_out.Flush();
                        //        settings_out.Close();
                        //    }
                        //}

                        Console.Title = $"WAaaAGHhhH TOolz WOT iZ 4 MeKAniks =>< {total_permutations,20} Permutations {Math.Round(speed_permutations, 2),14}Mhz {percent_complete_percent,12}% Complete";
                    }
                    Thread.Yield();
                    Thread.Sleep(1000);
                }
            });

            //ParallelOptions opt = new ParallelOptions
            //{
            //    MaxDegreeOfParallelism = 512
            //};

            Console.WriteLine("[|]");

            Parallel.ForEach(filenames, /* new ParallelOptions { MaxDegreeOfParallelism = 512 }, */ (filename) =>
            {
                Parallel.ForEach(folders,  /* new ParallelOptions { MaxDegreeOfParallelism = 512 }, */ (folder) =>
                {
                    Parallel.ForEach(extensions,  /* new ParallelOptions { MaxDegreeOfParallelism = 512 }, */ (extension) =>
                    {

                        // if (total_progress > permutations_continue)
                        //{
                        Compare($"{folder}/{filename}{extension}");
                        //Compare($"{folder}/it.0.0.{filename}{extension}");
                        //Compare($"{folder}/fi.0.0.{filename}{extension}");
                        //Compare($"{folder}/sk.0.0.{filename}{extension}");
                        //Compare($"{folder}/{filename}_entries{extension}");
                        //Compare($"{folder}/{filename}_subtypes{extension}");
                        //Compare($"{folder}/{filename}_use{extension}");
                        //Compare($"{folder}/{filename}_click{extension}");
                        Compare($"{folder}/{filename}_names{extension}");
                        Compare($"{folder}/{filename}_descs{extension}");
                        Compare($"{folder}/{filename}_disabled{extension}");
                        //Compare($"{folder}/{filename}_collision{extension}");
                        //Compare($"{folder}/{filename}_burning{extension}");
                        //Compare($"{folder}/{filename}_contested{extension}");
                        //Compare($"{folder}/{filename}_hanging{extension}");
                        Compare($"{folder}/{filename}_text{extension}");
                        //Compare($"{folder}/{filename}_data{extension}");
                        //Compare($"{folder}/{filename}_textures{extension}");
                        //Compare($"{folder}/{filename}_fixtures{extension}");
                        //Compare($"{folder}/{filename}_texture{extension}");
                        //Compare($"{folder}/{filename}_fixture{extension}");
                        //Compare($"{folder}/{filename}_anims{extension}");
                        //Compare($"{folder}/{filename}_walk{extension}");
                        //Compare($"{folder}/{filename}_run{extension}");
                        //Compare($"{folder}/{filename}_active{extension}");
                        //Compare($"{folder}/{filename}_hover{extension}");
                        //Compare($"{folder}/{filename}_ready{extension}");
                        //Compare($"{folder}/{filename}_jump{extension}");
                        //Compare($"{folder}/{filename}_land{extension}");
                        //Compare($"{folder}/{filename}_back{extension}");
                        //Compare($"{folder}/{filename}_flinch{extension}");
                        //Compare($"{folder}/{filename}_fidget{extension}");
                        //Compare($"{folder}/{filename}_fidget2{extension}");
                        //Compare($"{folder}/{filename}_holster{extension}");
                        //Compare($"{folder}/{filename}_holster-in{extension}");
                        //Compare($"{folder}/{filename}_holster-out{extension}");
                        //Compare($"{folder}/{filename}_fall{extension}");
                        //Compare($"{folder}/{filename}_draw{extension}");
                        //Compare($"{folder}/{filename}_draw-in{extension}");
                        //Compare($"{folder}/{filename}_draw-out{extension}");
                        //Compare($"{folder}/{filename}_kick{extension}");
                        //Compare($"{folder}/{filename}_dd-in{extension}");
                        //Compare($"{folder}/{filename}_dd-out{extension}");
                        //Compare($"{folder}/{filename}_auto1{extension}");
                        //Compare($"{folder}/{filename}_auto2{extension}");
                        //Compare($"{folder}/{filename}_aggro{extension}");
                        //Compare($"{folder}/{filename}_aggro-in{extension}");
                        //Compare($"{folder}/{filename}_aggro-out{extension}");
                        //Compare($"{folder}/{filename}spec{extension}");
                        //Compare($"{folder}/{filename}tint{extension}");
                        //Compare($"{folder}/{filename}glow{extension}");
                        //Compare($"{folder}/{filename}base{extension}");
                        //Compare($"{folder}/{filename}diffuse{extension}");
                        //Compare($"{folder}/{filename}spec01{extension}");
                        //Compare($"{folder}/{filename}tint01{extension}");
                        //Compare($"{folder}/{filename}glow01{extension}");
                        //Compare($"{folder}/{filename}base01{extension}");
                        //Compare($"{folder}/{filename}diffuse01{extension}");
                        //Compare($"{folder}/{filename}spec_01{extension}");
                        //Compare($"{folder}/{filename}tint_01{extension}");
                        //Compare($"{folder}/{filename}glow_01{extension}");
                        //Compare($"{folder}/{filename}base_01{extension}");
                        //Compare($"{folder}/{filename}diffuse_01{extension}");
                        //Compare($"{folder}/{filename}spec02{extension}");
                        //Compare($"{folder}/{filename}tint02{extension}");
                        //Compare($"{folder}/{filename}glow02{extension}");
                        //Compare($"{folder}/{filename}base02{extension}");
                        //Compare($"{folder}/{filename}diffuse02{extension}");
                        //Compare($"{folder}/{filename}spec_02{extension}");
                        //Compare($"{folder}/{filename}tint_02{extension}");
                        //Compare($"{folder}/{filename}glow_02{extension}");
                        //Compare($"{folder}/{filename}base_02{extension}");
                        //Compare($"{folder}/{filename}diffuse_02{extension}");
                        //Compare($"{folder}/{filename}_spec{extension}");
                        //Compare($"{folder}/{filename}_tint{extension}");
                        //Compare($"{folder}/{filename}_glow{extension}");
                        //Compare($"{folder}/{filename}_base{extension}");
                        //Compare($"{folder}/{filename}_diffuse{extension}");
                        //Compare($"{folder}/{filename}_spec01{extension}");
                        //Compare($"{folder}/{filename}_tint01{extension}");
                        //Compare($"{folder}/{filename}_glow01{extension}");
                        //Compare($"{folder}/{filename}_base01{extension}");
                        //Compare($"{folder}/{filename}_diffuse01{extension}");
                        //Compare($"{folder}/{filename}_spec_01{extension}");
                        //Compare($"{folder}/{filename}_tint_01{extension}");
                        //Compare($"{folder}/{filename}_glow_01{extension}");
                        //Compare($"{folder}/{filename}_base_01{extension}");
                        //Compare($"{folder}/{filename}_diffuse_01{extension}");
                        //Compare($"{folder}/{filename}_spec02{extension}");
                        //Compare($"{folder}/{filename}_tint02{extension}");
                        //Compare($"{folder}/{filename}_glow02{extension}");
                        //Compare($"{folder}/{filename}_base02{extension}");
                        //Compare($"{folder}/{filename}_diffuse02{extension}");
                        //Compare($"{folder}/{filename}_spec_02{extension}");
                        //Compare($"{folder}/{filename}_tint_02{extension}");
                        //Compare($"{folder}/{filename}_glow_02{extension}");
                        //Compare($"{folder}/{filename}_base_02{extension}");
                        //Compare($"{folder}/{filename}_diffuse_02{extension}");
                        //Compare($"{folder}/{filename}_point_names{extension}");
                        //Compare($"{folder}/{filename}_area_names{extension}");
                        //Compare($"{folder}/{filename}_point_descs{extension}");
                        //Compare($"{folder}/{filename}_area_descs{extension}");
                        //Compare($"{folder}/{filename}_mono{extension}");
                        //Compare($"{folder}/{filename}_body{extension}");
                        //Compare($"{folder}/{filename}_xtra{extension}");
                        //Compare($"{folder}/{filename}_display{extension}");
                        //Compare($"{folder}/{filename}_detail{extension}");
                        //Compare($"{folder}/{filename}_head{extension}");
                        //Compare($"{folder}/{filename}_skin{extension}");
                        Compare($"{folder}/{filename}_skeleton{extension}");
                        Compare($"{folder}/{filename}_anim{extension}");


                        
                        if (!filename.StartsWith("0.fg.0.0."))
                        {
                            string prefix1 = $"{folder}/0.fg.0.0.{filename}";
                            string prefix = $"{prefix1}.@.fg.0.0.";

                            Compare($"{prefix1}{extension}");
                            Compare($"{prefix1}.@.@{extension}");
                            Compare($"{prefix1}.fg.0.0.{filename}.@{extension}");
                            Compare($"{prefix1}.@.fg.0.0{extension}");
                            Compare($"{prefix}{extension}");
                            Compare($"{prefix}.@{extension}");

                            Compare($"{prefix}_spec{extension}");
                            Compare($"{prefix}_tint{extension}");
                            Compare($"{prefix}_glow{extension}");
                            Compare($"{prefix}_base{extension}");
                            Compare($"{prefix}_diffuse{extension}");
                            Compare($"{prefix}_spec01{extension}");
                            Compare($"{prefix}_tint01{extension}");
                            Compare($"{prefix}_glow01{extension}");
                            Compare($"{prefix}_base01{extension}");
                            Compare($"{prefix}_diffuse01{extension}");
                            Compare($"{prefix}_spec_01{extension}");
                            Compare($"{prefix}_tint_01{extension}");
                            Compare($"{prefix}_glow_01{extension}");
                            Compare($"{prefix}_base_01{extension}");
                            Compare($"{prefix}_diffuse_01{extension}");
                            Compare($"{prefix}_spec02{extension}");
                            Compare($"{prefix}_tint02{extension}");
                            Compare($"{prefix}_glow02{extension}");
                            Compare($"{prefix}_base02{extension}");
                            Compare($"{prefix}_diffuse02{extension}");
                            Compare($"{prefix}_spec_02{extension}");
                            Compare($"{prefix}_tint_02{extension}");
                            Compare($"{prefix}_glow_02{extension}");
                            Compare($"{prefix}_base_02{extension}");
                            Compare($"{prefix}_diffuse_02{extension}");

                            Compare($"{prefix}_spec.fg.0.0.{filename}_spec{extension}");
                            Compare($"{prefix}_tint.fg.0.0.{filename}_tint{extension}");
                            Compare($"{prefix}_glow.fg.0.0.{filename}_glow{extension}");
                            Compare($"{prefix}_base.fg.0.0.{filename}_base{extension}");
                            Compare($"{prefix}_diffuse.fg.0.0.{filename}_diffuse{extension}");
                            Compare($"{prefix}_spec01.fg.0.0.{filename}_spec01{extension}");
                            Compare($"{prefix}_tint01.fg.0.0.{filename}_tint01{extension}");
                            Compare($"{prefix}_glow01.fg.0.0.{filename}_glow01{extension}");
                            Compare($"{prefix}_base01.fg.0.0.{filename}_base01{extension}");
                            Compare($"{prefix}_diffuse01.fg.0.0.{filename}_diffuse01{extension}");
                            Compare($"{prefix}_spec_01.fg.0.0.{filename}_spec_01{extension}");
                            Compare($"{prefix}_tint_01.fg.0.0.{filename}_tint_01{extension}");
                            Compare($"{prefix}_glow_01.fg.0.0.{filename}_glow_01{extension}");
                            Compare($"{prefix}_base_01.fg.0.0.{filename}_base_01{extension}");
                            Compare($"{prefix}_diffuse_01.fg.0.0.{filename}_diffuse_01{extension}");
                            Compare($"{prefix}_spec02.fg.0.0.{filename}_spec02{extension}");
                            Compare($"{prefix}_tint02.fg.0.0.{filename}_tint02{extension}");
                            Compare($"{prefix}_glow02.fg.0.0.{filename}_glow02{extension}");
                            Compare($"{prefix}_base02.fg.0.0.{filename}_base02{extension}");
                            Compare($"{prefix}_diffuse02.fg.0.0.{filename}_diffuse02{extension}");
                            Compare($"{prefix}_spec_02.fg.0.0.{filename}_spec_02{extension}");
                            Compare($"{prefix}_tint_02.fg.0.0.{filename}_tint_02{extension}");
                            Compare($"{prefix}_glow_02.fg.0.0.{filename}_glow_02{extension}");
                            Compare($"{prefix}_base_02.fg.0.0.{filename}_base_02{extension}");
                            Compare($"{prefix}_diffuse_02.fg.0.0.{filename}_diffuse_02{extension}");

                            Compare($"{prefix}_spec.fg.0.0.{filename}_spec.fg.0.0{extension}");
                            Compare($"{prefix}_tint.fg.0.0.{filename}_tint.fg.0.0{extension}");
                            Compare($"{prefix}_glow.fg.0.0.{filename}_glow.fg.0.0{extension}");
                            Compare($"{prefix}_base.fg.0.0.{filename}_base.fg.0.0{extension}");
                            Compare($"{prefix}_diffuse.fg.0.0.{filename}_diffuse.fg.0.0{extension}");
                            Compare($"{prefix}_spec01.fg.0.0.{filename}_spec01.fg.0.0{extension}");
                            Compare($"{prefix}_tint01.fg.0.0.{filename}_tint01.fg.0.0{extension}");
                            Compare($"{prefix}_glow01.fg.0.0.{filename}_glow01.fg.0.0{extension}");
                            Compare($"{prefix}_base01.fg.0.0.{filename}_base01.fg.0.0{extension}");
                            Compare($"{prefix}_diffuse01.fg.0.0.{filename}_diffuse01.fg.0.0{extension}");
                            Compare($"{prefix}_spec_01.fg.0.0.{filename}_spec_01.fg.0.0{extension}");
                            Compare($"{prefix}_tint_01.fg.0.0.{filename}_tint_01.fg.0.0{extension}");
                            Compare($"{prefix}_glow_01.fg.0.0.{filename}_glow_01.fg.0.0{extension}");
                            Compare($"{prefix}_base_01.fg.0.0.{filename}_base_01.fg.0.0{extension}");
                            Compare($"{prefix}_diffuse_01.fg.0.0.{filename}_diffuse_01.fg.0.0{extension}");
                            Compare($"{prefix}_spec02.fg.0.0.{filename}_spec02.fg.0.0{extension}");
                            Compare($"{prefix}_tint02.fg.0.0.{filename}_tint02.fg.0.0{extension}");
                            Compare($"{prefix}_glow02.fg.0.0.{filename}_glow02.fg.0.0{extension}");
                            Compare($"{prefix}_base02.fg.0.0.{filename}_base02.fg.0.0{extension}");
                            Compare($"{prefix}_diffuse02.fg.0.0.{filename}_diffuse02.fg.0.0{extension}");
                            Compare($"{prefix}_spec_02.fg.0.0.{filename}_spec_02.fg.0.0{extension}");
                            Compare($"{prefix}_tint_02.fg.0.0.{filename}_tint_02.fg.0.0{extension}");
                            Compare($"{prefix}_glow_02.fg.0.0.{filename}_glow_02.fg.0.0{extension}");
                            Compare($"{prefix}_base_02.fg.0.0.{filename}_base_02.fg.0.0{extension}");
                            Compare($"{prefix}_diffuse_02.fg.0.0.{filename}_diffuse_02.fg.0.0{extension}");

                            Compare($"{prefix}_spec.fg.0.0.{filename}_spec.@.fg.0.0{extension}");
                            Compare($"{prefix}_tint.fg.0.0.{filename}_tint.@.fg.0.0{extension}");
                            Compare($"{prefix}_glow.fg.0.0.{filename}_glow.@.fg.0.0{extension}");
                            Compare($"{prefix}_base.fg.0.0.{filename}_base.@.fg.0.0{extension}");
                            Compare($"{prefix}_diffuse.fg.0.0.{filename}_diffuse.@.fg.0.0{extension}");
                            Compare($"{prefix}_spec01.fg.0.0.{filename}_spec01.@.fg.0.0{extension}");
                            Compare($"{prefix}_tint01.fg.0.0.{filename}_tint01.@.fg.0.0{extension}");
                            Compare($"{prefix}_glow01.fg.0.0.{filename}_glow01.@.fg.0.0{extension}");
                            Compare($"{prefix}_base01.fg.0.0.{filename}_base01.@.fg.0.0{extension}");
                            Compare($"{prefix}_diffuse01.fg.0.0.{filename}_diffuse01.@.fg.0.0{extension}");
                            Compare($"{prefix}_spec_01.fg.0.0.{filename}_spec_01.@.fg.0.0{extension}");
                            Compare($"{prefix}_tint_01.fg.0.0.{filename}_tint_01.@.fg.0.0{extension}");
                            Compare($"{prefix}_glow_01.fg.0.0.{filename}_glow_01.@.fg.0.0{extension}");
                            Compare($"{prefix}_base_01.fg.0.0.{filename}_base_01.@.fg.0.0{extension}");
                            Compare($"{prefix}_diffuse_01.fg.0.0.{filename}_diffuse_01.@.fg.0.0{extension}");
                            Compare($"{prefix}_spec02.fg.0.0.{filename}_spec02.@.fg.0.0{extension}");
                            Compare($"{prefix}_tint02.fg.0.0.{filename}_tint02.@.fg.0.0{extension}");
                            Compare($"{prefix}_glow02.fg.0.0.{filename}_glow02.@.fg.0.0{extension}");
                            Compare($"{prefix}_base02.fg.0.0.{filename}_base02.@.fg.0.0{extension}");
                            Compare($"{prefix}_diffuse02.fg.0.0.{filename}_diffuse02.@.fg.0.0{extension}");
                            Compare($"{prefix}_spec_02.fg.0.0.{filename}_spec_02.@.fg.0.0{extension}");
                            Compare($"{prefix}_tint_02.fg.0.0.{filename}_tint_02.@.fg.0.0{extension}");
                            Compare($"{prefix}_glow_02.fg.0.0.{filename}_glow_02.@.fg.0.0{extension}");
                            Compare($"{prefix}_base_02.fg.0.0.{filename}_base_02.@.fg.0.0{extension}");
                            Compare($"{prefix}_diffuse_02.fg.0.0.{filename}_diffuse_02.@.fg.0.0{extension}");

                            Compare($"{prefix}_spec.fg.0.0.{filename}_spec.@{extension}");
                            Compare($"{prefix}_tint.fg.0.0.{filename}_tint.@{extension}");
                            Compare($"{prefix}_glow.fg.0.0.{filename}_glow.@{extension}");
                            Compare($"{prefix}_base.fg.0.0.{filename}_base.@{extension}");
                            Compare($"{prefix}_diffuse.fg.0.0.{filename}_diffuse.@{extension}");
                            Compare($"{prefix}_spec01.fg.0.0.{filename}_spec01.@{extension}");
                            Compare($"{prefix}_tint01.fg.0.0.{filename}_tint01.@{extension}");
                            Compare($"{prefix}_glow01.fg.0.0.{filename}_glow01.@{extension}");
                            Compare($"{prefix}_base01.fg.0.0.{filename}_base01.@{extension}");
                            Compare($"{prefix}_diffuse01.fg.0.0.{filename}_diffuse01.@{extension}");
                            Compare($"{prefix}_spec_01.fg.0.0.{filename}_spec_01.@{extension}");
                            Compare($"{prefix}_tint_01.fg.0.0.{filename}_tint_01.@{extension}");
                            Compare($"{prefix}_glow_01.fg.0.0.{filename}_glow_01.@{extension}");
                            Compare($"{prefix}_base_01.fg.0.0.{filename}_base_01.@{extension}");
                            Compare($"{prefix}_diffuse_01.fg.0.0.{filename}_diffuse_01.@{extension}");
                            Compare($"{prefix}_spec02.fg.0.0.{filename}_spec02.@{extension}");
                            Compare($"{prefix}_tint02.fg.0.0.{filename}_tint02.@{extension}");
                            Compare($"{prefix}_glow02.fg.0.0.{filename}_glow02.@{extension}");
                            Compare($"{prefix}_base02.fg.0.0.{filename}_base02.@{extension}");
                            Compare($"{prefix}_diffuse02.fg.0.0.{filename}_diffuse02.@{extension}");
                            Compare($"{prefix}_spec_02.fg.0.0.{filename}_spec_02.@{extension}");
                            Compare($"{prefix}_tint_02.fg.0.0.{filename}_tint_02.@{extension}");
                            Compare($"{prefix}_glow_02.fg.0.0.{filename}_glow_02.@{extension}");
                            Compare($"{prefix}_base_02.fg.0.0.{filename}_base_02.@{extension}");
                            Compare($"{prefix}_diffuse_02.fg.0.0.{filename}_diffuse_02.@{extension}");
                        }
                        else
                        {
                            var rx = new Regex(@"((?![\.\@|\.\@\.|\@\.\@|\.fg\.0\.0\.|0\.fg\.0\.0\.|\.\@|\.])[\w\d]+]*)");
                            var ms = rx.Matches(filename);
                            if (ms.Count > 0)
                            {
                                string extracted = "";
                                if (ms[0].Captures.Count > 0)
                                    extracted = ms[0].Captures[0].Value;
                                if (!string.IsNullOrWhiteSpace(extracted))
                                {
                                    string prefix1 = $"{folder}/0.fg.0.0.{extracted}";
                                    string prefix = $"{prefix1}.@.fg.0.0.";

                                    Compare($"{prefix1}{extension}");
                                    Compare($"{prefix1}.@.@{extension}");
                                    Compare($"{prefix1}.fg.0.0.{filename}.@{extension}");
                                    Compare($"{prefix1}.@.fg.0.0{extension}");
                                    Compare($"{prefix}{extension}");
                                    Compare($"{prefix}.@{extension}");

                                    Compare($"{prefix}_spec{extension}");
                                    Compare($"{prefix}_tint{extension}");
                                    Compare($"{prefix}_glow{extension}");
                                    Compare($"{prefix}_base{extension}");
                                    Compare($"{prefix}_spec01{extension}");
                                    Compare($"{prefix}_tint01{extension}");
                                    Compare($"{prefix}_glow01{extension}");
                                    Compare($"{prefix}_base01{extension}");
                                    Compare($"{prefix}_spec_01{extension}");
                                    Compare($"{prefix}_tint_01{extension}");
                                    Compare($"{prefix}_glow_01{extension}");
                                    Compare($"{prefix}_base_01{extension}");
                                    Compare($"{prefix}_spec02{extension}");
                                    Compare($"{prefix}_tint02{extension}");
                                    Compare($"{prefix}_glow02{extension}");
                                    Compare($"{prefix}_base02{extension}");
                                    Compare($"{prefix}_spec_02{extension}");
                                    Compare($"{prefix}_tint_02{extension}");
                                    Compare($"{prefix}_glow_02{extension}");
                                    Compare($"{prefix}_base_02{extension}");

                                    Compare($"{prefix}_spec.fg.0.0.{filename}_spec{extension}");
                                    Compare($"{prefix}_tint.fg.0.0.{filename}_tint{extension}");
                                    Compare($"{prefix}_glow.fg.0.0.{filename}_glow{extension}");
                                    Compare($"{prefix}_base.fg.0.0.{filename}_base{extension}");
                                    Compare($"{prefix}_diffuse.fg.0.0.{filename}_diffuse{extension}");
                                    Compare($"{prefix}_spec01.fg.0.0.{filename}_spec01{extension}");
                                    Compare($"{prefix}_tint01.fg.0.0.{filename}_tint01{extension}");
                                    Compare($"{prefix}_glow01.fg.0.0.{filename}_glow01{extension}");
                                    Compare($"{prefix}_base01.fg.0.0.{filename}_base01{extension}");
                                    Compare($"{prefix}_diffuse01.fg.0.0.{filename}_diffuse01{extension}");
                                    Compare($"{prefix}_spec_01.fg.0.0.{filename}_spec_01{extension}");
                                    Compare($"{prefix}_tint_01.fg.0.0.{filename}_tint_01{extension}");
                                    Compare($"{prefix}_glow_01.fg.0.0.{filename}_glow_01{extension}");
                                    Compare($"{prefix}_base_01.fg.0.0.{filename}_base_01{extension}");
                                    Compare($"{prefix}_diffuse_01.fg.0.0.{filename}_diffuse_01{extension}");
                                    Compare($"{prefix}_spec02.fg.0.0.{filename}_spec02{extension}");
                                    Compare($"{prefix}_tint02.fg.0.0.{filename}_tint02{extension}");
                                    Compare($"{prefix}_glow02.fg.0.0.{filename}_glow02{extension}");
                                    Compare($"{prefix}_base02.fg.0.0.{filename}_base02{extension}");
                                    Compare($"{prefix}_diffuse02.fg.0.0.{filename}_diffuse02{extension}");
                                    Compare($"{prefix}_spec_02.fg.0.0.{filename}_spec_02{extension}");
                                    Compare($"{prefix}_tint_02.fg.0.0.{filename}_tint_02{extension}");
                                    Compare($"{prefix}_glow_02.fg.0.0.{filename}_glow_02{extension}");
                                    Compare($"{prefix}_base_02.fg.0.0.{filename}_base_02{extension}");
                                    Compare($"{prefix}_diffuse_02.fg.0.0.{filename}_diffuse_02{extension}");

                                    Compare($"{prefix}_spec.fg.0.0.{filename}_spec.fg.0.0{extension}");
                                    Compare($"{prefix}_tint.fg.0.0.{filename}_tint.fg.0.0{extension}");
                                    Compare($"{prefix}_glow.fg.0.0.{filename}_glow.fg.0.0{extension}");
                                    Compare($"{prefix}_base.fg.0.0.{filename}_base.fg.0.0{extension}");
                                    Compare($"{prefix}_diffuse.fg.0.0.{filename}_diffuse.fg.0.0{extension}");
                                    Compare($"{prefix}_spec01.fg.0.0.{filename}_spec01.fg.0.0{extension}");
                                    Compare($"{prefix}_tint01.fg.0.0.{filename}_tint01.fg.0.0{extension}");
                                    Compare($"{prefix}_glow01.fg.0.0.{filename}_glow01.fg.0.0{extension}");
                                    Compare($"{prefix}_base01.fg.0.0.{filename}_base01.fg.0.0{extension}");
                                    Compare($"{prefix}_diffuse01.fg.0.0.{filename}_diffuse01.fg.0.0{extension}");
                                    Compare($"{prefix}_spec_01.fg.0.0.{filename}_spec_01.fg.0.0{extension}");
                                    Compare($"{prefix}_tint_01.fg.0.0.{filename}_tint_01.fg.0.0{extension}");
                                    Compare($"{prefix}_glow_01.fg.0.0.{filename}_glow_01.fg.0.0{extension}");
                                    Compare($"{prefix}_base_01.fg.0.0.{filename}_base_01.fg.0.0{extension}");
                                    Compare($"{prefix}_diffuse_01.fg.0.0.{filename}_diffuse_01.fg.0.0{extension}");
                                    Compare($"{prefix}_spec02.fg.0.0.{filename}_spec02.fg.0.0{extension}");
                                    Compare($"{prefix}_tint02.fg.0.0.{filename}_tint02.fg.0.0{extension}");
                                    Compare($"{prefix}_glow02.fg.0.0.{filename}_glow02.fg.0.0{extension}");
                                    Compare($"{prefix}_base02.fg.0.0.{filename}_base02.fg.0.0{extension}");
                                    Compare($"{prefix}_diffuse02.fg.0.0.{filename}_diffuse02.fg.0.0{extension}");
                                    Compare($"{prefix}_spec_02.fg.0.0.{filename}_spec_02.fg.0.0{extension}");
                                    Compare($"{prefix}_tint_02.fg.0.0.{filename}_tint_02.fg.0.0{extension}");
                                    Compare($"{prefix}_glow_02.fg.0.0.{filename}_glow_02.fg.0.0{extension}");
                                    Compare($"{prefix}_base_02.fg.0.0.{filename}_base_02.fg.0.0{extension}");
                                    Compare($"{prefix}_diffuse_02.fg.0.0.{filename}_diffuse_02.fg.0.0{extension}");

                                    Compare($"{prefix}_spec.fg.0.0.{filename}_spec.@.fg.0.0{extension}");
                                    Compare($"{prefix}_tint.fg.0.0.{filename}_tint.@.fg.0.0{extension}");
                                    Compare($"{prefix}_glow.fg.0.0.{filename}_glow.@.fg.0.0{extension}");
                                    Compare($"{prefix}_base.fg.0.0.{filename}_base.@.fg.0.0{extension}");
                                    Compare($"{prefix}_diffuse.fg.0.0.{filename}_diffuse.@.fg.0.0{extension}");
                                    Compare($"{prefix}_spec01.fg.0.0.{filename}_spec01.@.fg.0.0{extension}");
                                    Compare($"{prefix}_tint01.fg.0.0.{filename}_tint01.@.fg.0.0{extension}");
                                    Compare($"{prefix}_glow01.fg.0.0.{filename}_glow01.@.fg.0.0{extension}");
                                    Compare($"{prefix}_base01.fg.0.0.{filename}_base01.@.fg.0.0{extension}");
                                    Compare($"{prefix}_diffuse01.fg.0.0.{filename}_diffuse01.@.fg.0.0{extension}");
                                    Compare($"{prefix}_spec_01.fg.0.0.{filename}_spec_01.@.fg.0.0{extension}");
                                    Compare($"{prefix}_tint_01.fg.0.0.{filename}_tint_01.@.fg.0.0{extension}");
                                    Compare($"{prefix}_glow_01.fg.0.0.{filename}_glow_01.@.fg.0.0{extension}");
                                    Compare($"{prefix}_base_01.fg.0.0.{filename}_base_01.@.fg.0.0{extension}");
                                    Compare($"{prefix}_diffuse_01.fg.0.0.{filename}_diffuse_01.@.fg.0.0{extension}");
                                    Compare($"{prefix}_spec02.fg.0.0.{filename}_spec02.@.fg.0.0{extension}");
                                    Compare($"{prefix}_tint02.fg.0.0.{filename}_tint02.@.fg.0.0{extension}");
                                    Compare($"{prefix}_glow02.fg.0.0.{filename}_glow02.@.fg.0.0{extension}");
                                    Compare($"{prefix}_base02.fg.0.0.{filename}_base02.@.fg.0.0{extension}");
                                    Compare($"{prefix}_diffuse02.fg.0.0.{filename}_diffuse02.@.fg.0.0{extension}");
                                    Compare($"{prefix}_spec_02.fg.0.0.{filename}_spec_02.@.fg.0.0{extension}");
                                    Compare($"{prefix}_tint_02.fg.0.0.{filename}_tint_02.@.fg.0.0{extension}");
                                    Compare($"{prefix}_glow_02.fg.0.0.{filename}_glow_02.@.fg.0.0{extension}");
                                    Compare($"{prefix}_base_02.fg.0.0.{filename}_base_02.@.fg.0.0{extension}");
                                    Compare($"{prefix}_diffuse_02.fg.0.0.{filename}_diffuse_02.@.fg.0.0{extension}");

                                    Compare($"{prefix}_spec.fg.0.0.{filename}_spec.@{extension}");
                                    Compare($"{prefix}_tint.fg.0.0.{filename}_tint.@{extension}");
                                    Compare($"{prefix}_glow.fg.0.0.{filename}_glow.@{extension}");
                                    Compare($"{prefix}_base.fg.0.0.{filename}_base.@{extension}");
                                    Compare($"{prefix}_diffuse.fg.0.0.{filename}_diffuse.@{extension}");
                                    Compare($"{prefix}_spec01.fg.0.0.{filename}_spec01.@{extension}");
                                    Compare($"{prefix}_tint01.fg.0.0.{filename}_tint01.@{extension}");
                                    Compare($"{prefix}_glow01.fg.0.0.{filename}_glow01.@{extension}");
                                    Compare($"{prefix}_base01.fg.0.0.{filename}_base01.@{extension}");
                                    Compare($"{prefix}_diffuse01.fg.0.0.{filename}_diffuse01.@{extension}");
                                    Compare($"{prefix}_spec_01.fg.0.0.{filename}_spec_01.@{extension}");
                                    Compare($"{prefix}_tint_01.fg.0.0.{filename}_tint_01.@{extension}");
                                    Compare($"{prefix}_glow_01.fg.0.0.{filename}_glow_01.@{extension}");
                                    Compare($"{prefix}_base_01.fg.0.0.{filename}_base_01.@{extension}");
                                    Compare($"{prefix}_diffuse_01.fg.0.0.{filename}_diffuse_01.@{extension}");
                                    Compare($"{prefix}_spec02.fg.0.0.{filename}_spec02.@{extension}");
                                    Compare($"{prefix}_tint02.fg.0.0.{filename}_tint02.@{extension}");
                                    Compare($"{prefix}_glow02.fg.0.0.{filename}_glow02.@{extension}");
                                    Compare($"{prefix}_base02.fg.0.0.{filename}_base02.@{extension}");
                                    Compare($"{prefix}_diffuse02.fg.0.0.{filename}_diffuse02.@{extension}");
                                    Compare($"{prefix}_spec_02.fg.0.0.{filename}_spec_02.@{extension}");
                                    Compare($"{prefix}_tint_02.fg.0.0.{filename}_tint_02.@{extension}");
                                    Compare($"{prefix}_glow_02.fg.0.0.{filename}_glow_02.@{extension}");
                                    Compare($"{prefix}_base_02.fg.0.0.{filename}_base_02.@{extension}");
                                    Compare($"{prefix}_diffuse_02.fg.0.0.{filename}_diffuse_02.@{extension}");

                                }
                            }
                        }
                        
                        //unchecked { total_permutations += 158; }
                        Interlocked.Add(ref total_permutations, 239);
                    });
                });
                //unchecked { ++total_progress; }
                Interlocked.Increment(ref total_progress);
            });

            hash_results_tsv_stream.Flush();
            hash_results_tsv_stream.Close();
        }

        public static void Compare(string path)
        {
            //long hash2 = lookup3.ComputeInt64Hash(path);
            long hash = HashWAR(path);
            if (unknownHashes.Contains(hash))
            {
                lock (hash_results_tsv_stream)
                {
                    Console.WriteLine($"[$] discovered hash {hash,-20} with path permutation {path}");
                    //newHashes.Add(hash);
                    hash_results_tsv_stream.WriteLine($"{hash}\t{path}");
                    hash_results_tsv_stream.Flush();
                }
            }
        }

        public static long HashWAR(string s)
        {
            uint edx, eax, esi, ebx, edi, ecx;

            eax = ecx = edx = ebx = 0;
            ebx = edi = esi = (uint)s.Length + 0xDEADBEEF;

            int i = 0;

            for (i = 0; i + 12 < s.Length; i += 12)
            {
                edi = (uint)((s[i + 7] << 24) | (s[i + 6] << 16) | (s[i + 5] << 8) | s[i + 4]) + edi;
                esi = (uint)((s[i + 11] << 24) | (s[i + 10] << 16) | (s[i + 9] << 8) | s[i + 8]) + esi;
                edx = (uint)((s[i + 3] << 24) | (s[i + 2] << 16) | (s[i + 1] << 8) | s[i]) - esi;

                edx = (edx + ebx) ^ (esi >> 28) ^ (esi << 4);
                esi += edi;
                edi = (edi - edx) ^ (edx >> 26) ^ (edx << 6);
                edx += esi;
                esi = (esi - edi) ^ (edi >> 24) ^ (edi << 8);
                edi += edx;
                ebx = (edx - esi) ^ (esi >> 16) ^ (esi << 16);
                esi += edi;
                edi = (edi - ebx) ^ (ebx >> 13) ^ (ebx << 19);
                ebx += esi;
                esi = (esi - edi) ^ (edi >> 28) ^ (edi << 4);
                edi += ebx;
            }

            if (s.Length - i > 0)
            {
                switch (s.Length - i)
                {
                    case 12:
                        esi += (uint)s[i + 11] << 24;
                        goto case 11;
                    case 11:
                        esi += (uint)s[i + 10] << 16;
                        goto case 10;
                    case 10:
                        esi += (uint)s[i + 9] << 8;
                        goto case 9;
                    case 9:
                        esi += s[i + 8];
                        goto case 8;
                    case 8:
                        edi += (uint)s[i + 7] << 24;
                        goto case 7;
                    case 7:
                        edi += (uint)s[i + 6] << 16;
                        goto case 6;
                    case 6:
                        edi += (uint)s[i + 5] << 8;
                        goto case 5;
                    case 5:
                        edi += s[i + 4];
                        goto case 4;
                    case 4:
                        ebx += (uint)s[i + 3] << 24;
                        goto case 3;
                    case 3:
                        ebx += (uint)s[i + 2] << 16;
                        goto case 2;
                    case 2:
                        ebx += (uint)s[i + 1] << 8;
                        goto case 1;
                    case 1:
                        ebx += s[i];
                        break;
                }

                esi = (esi ^ edi) - ((edi >> 18) ^ (edi << 14));
                ecx = (esi ^ ebx) - ((esi >> 21) ^ (esi << 11));
                edi = (edi ^ ecx) - ((ecx >> 7) ^ (ecx << 25));
                esi = (esi ^ edi) - ((edi >> 16) ^ (edi << 16));
                edx = (esi ^ ecx) - ((esi >> 28) ^ (esi << 4));
                edi = (edi ^ edx) - ((edx >> 18) ^ (edx << 14));
                eax = (esi ^ edi) - ((edi >> 8) ^ (edi << 24));

                return ((long)edi << 32) + eax;
            }
            return ((long)esi << 32) + eax;
        }




        //oldHashes_stream = new StreamWriter("{output_directory}/dehash_test.txt");
        //oldHashes_stream.WriteLine($"{hash}\t{line.ToLower()}");
        //DumpFolders();
        //pathtohash.ToImmutableConcurrentDictionary<string, long>();
        //hashtopath.ToImmutableConcurrentDictionary<long, string>();
        //iunknown = unknown.ToImmutableConcurrentBag<long>();
        //iextensions = extensions.ToImmutableConcurrentBag<string>();
        //ifilenames = filenames.ToImmutableConcurrentBag<string>();
        //ifolders = suffixes.ToImmutableConcurrentBag<string>();
        //ifolders = folders.ToImmutableConcurrentBag<string>();
        //using System.IO.Enumeration;
        //using System.Collections.Immutable;
        //using System.Transactions;

        //static ImmutableConcurrentDictionary<string, long> ipathtohash;
        //static ImmutableConcurrentDictionary<long, string> ihashtopath;
        //static ImmutableConcurrentBag<long> iunknown;
        //static ImmutableConcurrentBag<string> iextensions;
        //static ImmutableConcurrentBag<string> ifilenames;
        //static ImmutableConcurrentBag<string> ifolders;
        //static ImmutableConcurrentBag<string> isuffixes;
        //static ImmutableConcurrentBag<string> iprefixes;
        //static ImmutableConcurrentBag<string> ifolderleaf;
        //static ImmutableConcurrentBag<string> ifolderx;

        //static ConcurrentBag<string> prefixes = new ConcurrentBag<string>();
        //static ConcurrentBag<string> folderleaf = new ConcurrentBag<string>();
        //static ConcurrentBag<string> folderx = new ConcurrentBag<string>();
        //static StreamWriter oldHashes_stream;


        //static readonly string myp_path = @"D:\Games\Warhammer Online - Age of Reckoning\myps";


        //private static void OldExtractor()
        //{
        //++locked_cursor;
        //newHashes_stream.WriteLine($"{((long)hash)}\t{path}");

        //newHashes_stream.Flush();

        //foreach (var filename in filenames)
        //{

        //    //var filename = "thumbs";
        //    foreach (var folder in folders)
        //    {
        //        foreach (var extension in extensions)
        //        {
        //            string path = folder + "/" + filename + "." + extension;
        //            long hash = HashWAR(path);
        //            if (unknown.Contains(hash))
        //            {
        //                ++locked_cursor;
        //                Console.WriteLine($"[$] new hash {hash,-20} found {path}");
        //                found.Add(hash, path);
        //                newHashes_stream.WriteLine($"{(long)hash}\t{path}");
        //            }
        //            //Console.WriteLine($"[$] {path}");
        //            ++permutations;
        //        }
        //    }
        //    if (++filenames_checked % patience == 1)
        //    {
        //        Console.WriteLine($"[?] Tested {permutations} permutations of {filenames_checked} filenames.");
        //        Console.SetCursorPosition(0, locked_cursor);
        //        newHashes_stream.Flush();
        //    }
        //}
        //}

        //private static void DumpFolders()
        //{
        //{
        //    string[] lines = File.ReadAllLines("folder_leaf.txt");

        //    foreach (var line in lines)
        //    {
        //        folderleaf.Add(line.ToLower());
        //    }

        //    Console.WriteLine($"[>] Loaded {folderleaf.Count} folder leaf nodes.");
        //    ifilenames = filenames.ToImmutableConcurrentBag<string>();
        //    filenames.Clear();
        //}

        //{
        //    StreamWriter fpstream = new StreamWriter("folders_proven.txt");
        //    string[] dirs = Directory.GetDirectories(myp_path, "*", SearchOption.AllDirectories);
        //    foreach (string dir in dirs)
        //    {

        //        string dir2 = dir.Replace("\\", "/");
        //        string dir3 = dir2.ToLower();
        //        int index = dir3.LastIndexOf(".myp") + 4;
        //        if (index < dir3.Length)
        //        {
        //            string dir4 = dir3.Substring(index);
        //            if (!string.IsNullOrWhiteSpace(dir4))
        //            {
        //                string dir5 = dir4.StartsWith('/') ? dir4.Substring(1) : dir4;
        //                if (!string.IsNullOrWhiteSpace(dir5))
        //                {
        //                    if (!folders.Contains(dir5))
        //                    {
        //                        if (!dir5.Contains("dehash"))
        //                        {
        //                            fpstream.WriteLine(dir5);
        //                            folders.Add(dir5);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    fpstream.Flush();
        //    fpstream.Close();

        //    foreach (var leaf in folderleaf)
        //    {
        //        if (!folders.Contains(leaf))
        //        {
        //            if (!leaf.Contains("dehash"))
        //            {
        //                folders.Add(leaf.ToLower());
        //            }
        //        }
        //    }

        //    folderx = new ConcurrentBag<string>(folders);
        //    foreach (string folder in folderx)
        //    {
        //        foreach (var leaf in folderleaf)
        //        {
        //            if (!string.IsNullOrWhiteSpace(folder) && !folder.StartsWith("zone"))
        //            {
        //                if (!folder.EndsWith(leaf))
        //                {
        //                    string f = folder + "/" + leaf;
        //                    if (!folders.Contains(f))
        //                    {
        //                        var rx = new Regex(@"([zone]{1}[0-9]{3})");
        //                        var ms = rx.Matches(f);

        //                        if (ms.Count < 2)
        //                        {
        //                            folders.Add(f);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //    StreamWriter fstream = new StreamWriter("folders.txt");
        //    foreach (string folder in folders)
        //    {
        //        fstream.WriteLine(folder);
        //    }
        //    fstream.Flush();
        //    fstream.Close();
        //}
        // }
    }
}